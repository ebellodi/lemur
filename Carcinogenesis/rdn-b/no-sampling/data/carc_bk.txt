useStdLogicVariables: true.

mode: active(+Drug).

mode: ames(+Drug).
mode: mutagenic(+Drug).
mode: has_property(+Drug,#Prop,#Propval).
mode: ashby_alert(#Alert,+Drug,-Ring).
mode: ind(+Drug,#Alert,-Nalerts).
mode: atm(+Drug,-Atomid,#Element,#Atomtype,-Charge).
mode: nitro(+Drug,-Ring).
mode: sulfo(+Drug,-Ring).
mode: methyl(+Drug,-Ring).
mode: methoxy(+Drug,-Ring).
mode: amine(+Drug,-Ring).
mode: ketone(+Drug,-Ring).
mode: ether(+Drug,-Ring).
mode: sulfide(+Drug,-Ring).
mode: alcohol(+Drug,-Ring).
mode: phenol(+Drug,-Ring).
mode: ester(+Drug,-Ring).
mode: imine(+Drug,-Ring).
mode: alkyl_halide(+Drug,-Ring).
mode: ar_halide(+Drug,-Ring).
mode: non_ar_6c_ring(+Drug,-Ring).
mode: non_ar_hetero_6_ring(+Drug,-Ring).
mode: six_ring(+Drug,-Ring).
mode: non_ar_5c_ring(+Drug,-Ring).
mode: non_ar_hetero_5_ring(+Drug,-Ring).
mode: five_ring(+Drug,-Ring).
mode: gteq(+Charge,+Charge).
mode: lteq(+Charge,+Charge).
mode: eq(+Charge,+Charge).
mode: gteq1(+Nalerts,+Nalerts).
mode: lteq1(+Nalerts,+Nalerts).
mode: eq1(+Nalerts,+Nalerts).
mode: connected(+Ring,+Ring).
mode: symbond(+Drug,+Atomid,-Atomid,+Bondtype).


// background knowledge 

gteq(x,y):-  x >= y.
lteq(x,y):-  x =< y.
eq(x,y):-  x =:= y.
gteq1(x,y):-  x >= y.
lteq1(x,y):-  x =< y.
eq1(x,y):-  x =:= y.

connected(ring1,ring2):- ring1 \= ring2, element(a,ring1), element(a,ring2).

element(h,[h|_]):- !.
element(h,[_|t]):- element(h,t).

symbond(d,a,b,t):- var(t), !, sym_bond(d,a,b,t).
symbond(d,a,b,t):- sym_bond(d,a,b,t), !.
sym_bond(d,a,b,t) :- bond(d,a,b,t).
sym_bond(d,a,b,t) :- bond(d,b,a,t).


setParam: treeDepth=4.
setParam: nodeSize=2.
setParam: numOfClauses=8.
