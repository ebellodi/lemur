#script for training on train_neg.txt and train_pos.txt with RDN-B

java -cp ../Boostr/WILL.jar edu.wisc.cs.will.Boosting.Common.RunBoostedModels \
-target active \
-trees 20 \
-save -l -train ../data_sampl/train/ \
1> ris.txt 2>stderr.txt
