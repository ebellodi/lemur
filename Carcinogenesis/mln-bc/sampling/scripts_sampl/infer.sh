java -cp ../Boostr/WILL.jar edu.wisc.cs.will.Boosting.Common.RunBoostedModels \
-aucJarPath "../Boostr" \
-target active \
-trees 20 \
-model ../data_sampl/train/models \
-save -i -test ../data_sampl/test/ \
1>infer1.txt
