:-set(single_var,false).
:-set(megaex_bottom,1).
:-set(max_iter,60).
:-set(beamsize,100).
:-set(d,2).
:-set(max_var,4).
:-set(max_iter_structure,50).
:-set(seed,rand(12452,14223,9322)).
:-set(initial_clauses_per_megaex,4).
:-set(depth_bound,false).
:-set(neg_ex,given).
output(active/1).

input(ames/1).
input(has_property/3).
input(ashby_alert/3).
input(mutagenic/1).
input(ind/3).
input(atm/5).
input(symbond/4).
input(nitro/2).
input(nitro/2).
input(sulfo/2).
input(methyl/2).
input(methoxy/2).
input(amine/2).
input(aldehyde/2).
input(ketone/2).
input(ether/2).
input(sulfide/2).
input(alcohol/2).
input(phenol/2).
input(carboxylic_acid/2).
input(ester/2).
input(imine/2).
input(deoxy_amide/2).
input(alkyl_halide/2).
input(ar_halide/2).
input(benzene/2).
input(hetero_ar_6_ring/2).
input(non_ar_6c_ring/2).
input(non_ar_hetero_6_ring/2).
input(six_ring/2).
input(carbon_5_ar_ring/2).
input(hetero_ar_5_ring/2).
input(non_ar_5c_ring/2).
input(non_ar_hetero_5_ring/2).
input(five_ring/2).
input(connected/2).


modeh(1,active(+drug)).

modeb(*,ames(+drug)).
modeb(*,mutagenic(+drug)).
modeb(*,has_property(+drug,-#property,-#propval)).
modeb(*,ashby_alert(-#alert,+drug,-ring)).
modeb(*,ind(+drug,-#alert,-nalerts)).

modeb(*,atm(+drug,-atomid,-#element,-#integer,-charge)).
modeb(*,symbond(+drug,+atomid,-atomid,-#integer)).

/*modeb(1,(+charge) >= (#charge)).
modeb(1,(+charge) =< (#charge)).
modeb(1,(+charge) = (#charge)).
modeb(1,(+nalerts) >= (#nalerts)).
modeb(1,(+nalerts) =< (#nalerts)).
modeb(1,(+nalerts) = (#nalerts)).
*/

modeb(*,nitro(+drug,-ring)).
modeb(*,sulfo(+drug,-ring)).
modeb(*,sulfo(+drug,-ring)).
modeb(*,methyl(+drug,-ring)).
modeb(*,methoxy(+drug,-ring)).
modeb(*,amine(+drug,-ring)).
modeb(*,aldehyde(+drug,-ring)).
modeb(*,ketone(+drug,-ring)).
modeb(*,ether(+drug,-ring)).
modeb(*,sulfide(+drug,-ring)).
modeb(*,alcohol(+drug,-ring)).
modeb(*,phenol(+drug,-ring)).
modeb(*,carboxylic_acid(+drug,-ring)).
modeb(*,ester(+drug,-ring)).
modeb(*,imine(+drug,-ring)).
modeb(*,deoxy_amide(+drug,-ring)).
modeb(*,imine(+drug,-ring)).
modeb(*,alkyl_halide(+drug,-ring)).
modeb(*,ar_halide(+drug,-ring)).
modeb(*,benzene(+drug,-ring)).
modeb(*,hetero_ar_6_ring(+drug,-ring)).
modeb(*,non_ar_6c_ring(+drug,-ring)).
modeb(*,non_ar_hetero_6_ring(+drug,-ring)).
modeb(*,six_ring(+drug,-ring)).
modeb(*,carbon_5_ar_ring(+drug,-ring)).
modeb(*,hetero_ar_5_ring(+drug,-ring)).
modeb(*,non_ar_5c_ring(+drug,-ring)).
modeb(*,non_ar_hetero_5_ring(+drug,-ring)).
modeb(*,five_ring(+drug,-ring)).
modeb(1,connected(+ring,+ring)).

determination(active/1,ames/1).
determination(active/1,has_property/3).
determination(active/1,ashby_alert/3).
determination(active/1,mutagenic/1).
determination(active/1,ind/3).
determination(active/1,atm/5).
determination(active/1,symbond/4).
determination(active/1,nitro/2).
determination(active/1,sulfo/2).
determination(active/1,methyl/2).
determination(active/1,methoxy/2).
determination(active/1,amine/2).
determination(active/1,aldehyde/2).
determination(active/1,ketone/2).
determination(active/1,ether/2).
determination(active/1,sulfide/2).
determination(active/1,alcohol/2).
determination(active/1,phenol/2).
determination(active/1,carboxylic_acid/2).
determination(active/1,ester/2).
determination(active/1,imine/2).
determination(active/1,deoxy_amide/2).
determination(active/1,alkyl_halide/2).
determination(active/1,ar_halide/2).
determination(active/1,benzene/2).
determination(active/1,hetero_ar_6_ring/2).
determination(active/1,non_ar_6c_ring/2).
determination(active/1,non_ar_hetero_6_ring/2).
determination(active/1,six_ring/2).
determination(active/1,carbon_5_ar_ring/2).
determination(active/1,hetero_ar_5_ring/2).
determination(active/1,non_ar_5c_ring/2).
determination(active/1,non_ar_hetero_5_ring/2).
determination(active/1,five_ring/2).
determination(active/1,connected/2).
determination(active/1,'=<'/2).
determination(active/1,'>='/2).
determination(active/1,'='/2).




/*
connected(_M,Ring1,Ring2):-
        Ring1 \= Ring2,
        member(A,Ring1),
        member(A,Ring2), !.

symbond(Mod,M,A,B,T):- bond(Mod,M,A,B,T).
symbond(Mod,M,A,B,T):- bond(Mod,M,B,A,T).
*/
