/* test according to the method of Davis and Goadrich*/

:-source.
:-consult(slipcover).

t:-
	open('cll.pl',write,S), %AI fold 
	open('res_atoms.csv',write,S1),  %replace with the other correspodent folds
    ta(S,S1),
	close(S),
	close(S1).

ta(S,S1):-
	generate_file_names(train,FileKB,FileOut,FileL,_FileLPAD,FileBG),  %file di test pte1.f/n (vedi find_ga)
	reconsult(FileL),
%	load_models(FileKB,_),
        load_models(FileKB,_DB),%assert all facts in kb
       
	(file_exists(FileBG)->
		set(compiling,on),
		load(FileBG,_ThBG,RBG),
		set(compiling,off),
		generate_clauses(RBG,_RBG1,0,[],ThBG), 
		assert_all(ThBG)
	;
		true
	),

	find_ga(m1,LG,Pos,Neg),

	set(compiling,on),
	load(FileOut,Th1,R1),	%FILE RULES
	format("loaded rules~n"),
	set(compiling,off),
%	generate_clauses(R1,R2,0,[],Th),  FALLISCE
%        %assert_all(R2),
%	assert_all(Th),
        assert_all(Th1),
        assert_all(R1),
	compute_CLL_atoms(LG,0,0,CLL1,LG1),
        retract_all(Th1),
        retract_all(R1),
	keysort(LG1,LGOrd1),
	format(S,"cll(~d,~d,[\n",[Pos,Neg]),
	writes(LGOrd1,S),
	format(S1,"~f~n",[CLL1]).
    
/*
ta(Model,File,FileKB,S,N,S1):-
	generate_file_names(File,_FileKB,FileOut,FileL,FileLPAD),
	format("~a~n",[File]),
	reconsult(FileL),
	load_models(FileKB,_),
	find_ga(Model,LG,Pos,Neg),
        set(compiling,on),
	load(FileLPAD,Th,R),		%file CPL
	set(compiling,off),
        assert_all(Th),  
	assert_all(R),
	compute_CLL_atoms(LG,0,0,CLL0,LG0),
        retract_all(Th),
	retract_all(R),
	keysort(LG0,LGOrd0),
	format(S,"cll(~a,pre,~d,~d,[",[File,Pos,Neg]),
	writes(LGOrd0,S),
	abolish_rules,
	set(compiling,on),
	load(FileOut,Th1,R1),	%FILE RULES
	set(compiling,off),
	assert_all(Th1),  
        assert_all(R1),
	compute_CLL_atoms(LG,0,0,CLL1,LG1),
        retract_all(Th1),
	retract_all(R1),
	keysort(LG1,LGOrd1),
	format(S,"cll(~a,post,~d,~d,[",[File,Pos,Neg]),
	writes(LGOrd1,S),
	format(S1,"~a;~f~f~n",[File,CLL0,CLL1]).
    */


writes([H-H1],S):-
	format(S,"~f - (~p)]).~n~n",[H,H1]).

writes([H-H1|T],S):-
	format(S,"~f - (~p),~n",[H,H1]),
	writes(T,S).


load_exaset(File, L) :-
    open(File, read, S),
    read_all_exas(S, [], L),
    close(S).

read_all_exas(S, L0, L) :-
    read(S, At),
    (At \= end_of_file
    -> append(L0, [At], L1), read_all_exas(S, L1, L) 
    ; L = L0
    ).

find_ga(M,LG,Pos,Neg):-                   
    load_exaset('pte1.f', LGP0),
    load_exaset('pte1.n', LGN0),
    findall(\+ At1,( member(At, LGN0), At =.. [P|Args], At1 =..[P,M|Args]), LGN),
    findall(At1,( member(At, LGP0),At =.. [P|Args],At1=..[P,M|Args]), LGP),
    writeln(LGP),
    writeln(LGN),
    length(LGP,Pos),
	length(LGN,Neg),
%	sample(50,LGN0,LGN),
%	length(LGP,Pos),
%	length(LGN,Neg),
	append(LGP,LGN,LG).

/*
find_ga(M,LG,Pos,Neg):-                   
	setof(A,(B,C)^(
	advisedby(M,A,B);advisedby(M,B,A);
	hasposition(M,A,B);professor(M,A);
	tempadvisedby(M,A,B);tempadvisedby(M,B,A);
	publication(M,B,A);inphase(M,A,B);yearsinprogram(M,A,B);
	sameperson(M,A,B);sameperson(M,B,A);ta(M,B,A,C);
	taught_by(M,B,A,C);student(M,A);projectmember(M,B,A)),L1),
	findall(advisedby(M,A,B),(member(A,L1),member(B,L1),advisedby(M,A,B)),LGP),
	findall(\+ advisedby(M,A,B),(member(A,L1),member(B,L1),\+ advisedby(M,A,B)),LGN),%generates all possible  negative atoms
	length(LGP,Pos),
	length(LGN,Neg),
%	sample(50,LGN0,LGN),
%	length(LGP,Pos),
%	length(LGN,Neg),
	append(LGP,LGN,LG).
*/

write_list([],_).

write_list([H|T],S):-
	format(S,"~p~n",[H]),
	write_list(T,S).

generate_file_names(File,FileKB,FileOut,FileL, FileLPAD,FileBG):-
    name(File,FileString),
    generate_file_name(File,".kb",FileKB),
    generate_file_name(File,".cpl",FileLPAD),
    generate_file_name(File,".rules",FileOut),
    generate_file_name(File,".bg",FileBG),
    generate_file_name(File,".l",FileL).


compute_CLL_atoms([],_N,CLL,CLL,[]):-!.


compute_CLL_atoms([\+ H|T],N,CLL0,CLL1,[PG- (\+ H)|T1]):-!,
	rule_n(NR),
        init_test(NR),
	write((\+ H)),nl,
	get_node(H,BDD),!,
	ret_prob(BDD,PG),
	end_test,!,
		%
		PG1 is 1-PG,
			
		(PG1=:=0.0->
			CLL2 is CLL0-10
%			format("-inf~n",[])
		;
			CLL2 is CLL0+ log(PG1)
%			format("~f~n",[PG])
		),		
		N1 is N+1,
	compute_CLL_atoms(T,N1,CLL2,CLL1,T1).	

compute_CLL_atoms([H|T],N,CLL0,CLL1,[PG-H|T1]):-
       	rule_n(NR),
 	init_test(NR),
	write(H),nl,
	get_node(H,BDD),!,
	ret_prob(BDD,PG),
	end_test,!,
		(PG=:=0.0->
			CLL2 is CLL0-10
%			format("-inf~n",[])
		;	
			CLL2 is CLL0+ log(PG)
%			format("~f~n",[PG])
		),
		N1 is N+1,
	compute_CLL_atoms(T,N1,CLL2,CLL1,T1).		

