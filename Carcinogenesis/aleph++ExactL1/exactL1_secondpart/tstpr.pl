/* test according to the method of Davis and Goadrich
*/

:-source.
:-use_module(library(lists)).


t:-
	open('area_prs.csv',write,S0),
	close(S0),
	open('area_prl.csv',write,S1),
	close(S1),
	open('area_roc.csv',write,S2),
	close(S2),
	open('curves_pr_sp.m',write,S),
	open('roc_pr_sp.m',write,SP),
	reconsult(cll),
	ta('test-pte.result',S,SP),
	close(S),close(SP).

ta(File,S,SR):-
	cll(File,post,Pos,Neg,LGOrd0),
	compute_cll(LGOrd0,0,CLL0),
	Prec is Pos/(Pos+Neg),
	compute_points(LGOrd0,Pos,Neg,0,0,[],_Points0,[1.0-Prec],_Points0pr),
	% find points scanning the list from least probable to most probable
	% see the paper on precision and recal by Davis and Goadrich
	reverse(LGOrd0,LGROrd0),
	compute_pointsroc(LGROrd0,+inf,0,0,Pos,Neg,[],Points0,[],Points0pr),
	% find points scanning the list from most probable to least probable
	% see the paper roc 101 by Provost
	Points0=Points0ord,
%	reverse(Points0pr,Points0prord),
	[_H0,R0-P0|T0]=Points0pr,
	[0.0-P0,R0-P0|T0]=Points0prord,
	atom_concat(File,'After',File0),
%	atom_concat(File,'pr0',Pr0),
	write_p(Points0ord,File0,SR,Pos,Neg),
	% points in the ROC space obtained with the ROC method are printed to std 
	%output together with
	% the code for computing the convex hull and the area
	write_pr(Points0prord,File0,S,Pos,Neg),
/*
	points in the PR space obtained with the PR method are printed to a 
	matlab file together with the code for 
	1) interpolating in the PR space
	2) computing the area
*/	
	hull(Points0ord,0,0,0,AUC0),
	hull(Points0prord,0,0,0,AUC0pr),
	open('res_atoms_pr.csv',append,S1),
	format(S1,"~a;~d;~d;~f;~f;~f~n",[File,Pos,Neg,CLL0,AUC0,AUC0pr]),
	close(S1).

compute_cll([],C,C).

compute_cll([P- (\+ _)|T],C0,C1):-!,
	(P=:=1.0->
		C2 is C0 + log(0.00001)
	;
		C2 is C0 + log(1-P)
	),
	compute_cll(T,C2,C1).

compute_cll([P- _|T],C0,C1):-
	(P=:=0.0->
		C2 is C0 + log(0.00001)
	;
		C2 is C0 + log(P)
	),
	compute_cll(T,C2,C1).



write_p(P,A,S,_Pos,_Neg):-
	get_xy(P,PX,PY),
	format(S,"x=[",[]),
	writes(PX,S),
	format(S,"y=[",[]),
	writes(PY,S),
	format(S,"
	figure('Name','roc~a','NumberTitle','off')
	set(gca,'XLim',[0.0 1.0])
	set(gca,'YLim',[0.0 1.0])
	x=[x 1.0]
	y=[y 0.0]
	k=convhull(x,y)
  plot(x(k),y(k),'r-',x,y,'--b+')
	A = polyarea(x,y)~n~n
	save area_roc.csv  A -ascii -append
		",
	[A]).
/*
	figure('Name','pr~a','NumberTitle','off')
	set(gca,'XLim',[0.0 1.0])
	set(gca,'YLim',[0.0 1.0])
	tp=y(k)*~d
	fp=x(k)*~d
	recall=y(k)
	prec=tp./(tp+fp)
	prec(1)=prec(2)
  plot(recall,prec,'r-')
*/
write_pr(P,A,S,Pos,Neg):-
	get_xy(P,PX,PY),
	format(S,"rec=[",[A]),
	writes(PX,S),
	format(S,"prec=[",[A]),
	writes(PY,S),
	format(S,"
	pos=~d
	neg=~d
  tpa=0
  fpa=0
  rec=[rec 1.0]
  prec=[prec pos/(pos+neg)]
	  irec=[]
  	iprec=[]
  for j = 1:length(rec)-1
    if (rec(j)==rec(j+1) && prec(j)<prec(j+1))
	  irec=[irec rec(j)]
  	iprec=[iprec prec(j)]
    else
	    tpb=rec(j)*pos %Pos
	    if (prec(j)==0)
	    fpb=0
	    else
  	  fpb=tpb*(1-prec(j))/prec(j)
  	  end
    	for i=1:tpb-tpa
        irec=[irec (tpa+i)/pos] %Pos
       	iprec=[iprec (tpa+i)/(tpa+i+fpa+(fpb-fpa)/(tpb-tpa)*i)]
    	end
    tpa=tpb
    fpa=fpb
  irec=[irec rec(j)]
  iprec=[iprec prec(j)]
    end
  end
	figure('Name','pr_~a','NumberTitle','off')
	set(gca,'XLim',[0.0 1.0])
	set(gca,'YLim',[0.0 1.0])
	irecl=[0.0 0.0 irec 1.0 1.0]
	iprecl=[0.0 iprec(1) iprec pos/(pos+neg) 0.0]
	irecs=[irec(1) irec 1.0 1.0]
	iprecs=[0.0  iprec pos/(pos+neg) 0.0]
  plot(rec,prec,'--+k',irecl,iprecl,'-*r',irecs,iprecs,'-*b')
  Al=polyarea(irecl,iprecl)
  As=polyarea(irecs,iprecs)
  save area_prs.csv  As -ascii -append
  Al=polyarea(irecl,iprecl)
  save area_prl.csv  Al -ascii -append
~n~n",
	[Pos,Neg,A]).
	

writes([H],S):-
	format(S,"~f]~n",[H]).

writes([H|T],S):-
	format(S,"~f ",[H]),
	writes(T,S).
	
get_xy([],[],[]).

get_xy([X-Y|T],[X|TX],[Y|TY]):-
	get_xy(T,TX,TY).

compute_points([_],_TP,_FP,_FN,_TN,P,P,PR,PR):-!.
%	AUC1 is AUC0+Rec.

compute_points([_P- (\+ _),P0-(\+ H)|T],TP,FP,FN,TN,Po0,Po1,Pr0,Pr1):-!,
	FP1 is FP-1,
	TN1 is TN+1,
	compute_points([P0-(\+ H)|T],TP,FP1,FN,TN1,Po0,Po1,Pr0,Pr1).

compute_points([P- (\+ _),P0-H|T],TP,FP,FN,TN,Po0,Po1,Pr0,Pr1):-!,
	FP1 is FP-1,
	TN1 is TN+1,
	(P<P0->
	Prec is TP/(TP+FP1),
	Rec is TP/(TP+FN),
		FPR is FP1/(FP1+TN1),
		TPR is TP/(TP+FN),
		append(Po0,[(FPR-TPR)],Po2),
		append(Pr0,[Rec-Prec],Pr2)
	;		
		Po2=Po0,
		Pr2=Pr0
	),
	compute_points([P0-H|T],TP,FP1,FN,TN1,Po2,Po1,Pr2,Pr1).

compute_points([P- _,P0-(\+ H)|T],TP,FP,FN,TN,Po0,Po1,Pr0,Pr1):-!,
	TP1 is TP-1,
	FN1 is FN+1,
	(P<P0->
	Prec is TP1/(TP1+FP),
	Rec is TP1/(TP1+FN1),
		FPR is FP/(FP+TN),
		TPR is TP1/(TP1+FN1),
%	AUC2 is AUC0+(Rec-Rec0)*Prec,
		append(Po0,[FPR-TPR],Po2),
		append(Pr0,[Rec-Prec],Pr2)
	;
		Po2=Po0,
		Pr2=Pr0
	),
	compute_points([P0-(\+ H)|T],TP1,FP,FN1,TN,Po2,Po1,Pr2,Pr1).

compute_points([_P- _,P0- H|T],TP,FP,FN,TN,Po0,Po1,Pr0,Pr1):-!,
	TP1 is TP-1,
	FN1 is FN+1,
	compute_points([P0- H|T],TP1,FP,FN1,TN,Po0,Po1,Pr0,Pr1).

compute_pointsroc([],_P0,TP,FP,_FN,_TN,P0,P1,PR0,PR1):-!,
	append(P0,[1.0-1.0],P1),
	Prec is TP/(TP+FP),
	append(PR0,[1.0-Prec],PR1).
%	AUC1 is AUC0+Rec.

compute_pointsroc([P- (\+ _)|T],P0,TP,FP,FN,TN,Po0,Po1,Pr0,Pr1):-!,
	(P<P0->
		FPR is FP/(FP+TN),
		TPR is TP/(TP+FN),
	Prec is TP/(TP+FP),
	Rec is TP/(TP+FN),
		append(Po0,[(FPR-TPR)],Po2),
		append(Pr0,[Rec-Prec],Pr2),
		P1=P
	;		
		Po2=Po0,
		Pr2=Pr0,
		P1=P0
	),
	FP1 is FP+1,
	TN1 is TN-1,
	compute_pointsroc(T,P1,TP,FP1,FN,TN1,Po2,Po1,Pr2,Pr1).

compute_pointsroc([P- _|T],P0,TP,FP,FN,TN,Po0,Po1,Pr0,Pr1):-!,
	(P<P0->
		FPR is FP/(FP+TN),
		TPR is TP/(TP+FN),
%	AUC2 is AUC0+(Rec-Rec0)*Prec,
	Prec is TP/(TP+FP),
	Rec is TP/(TP+FN),
		append(Po0,[FPR-TPR],Po2),
		append(Pr0,[Rec-Prec],Pr2),
		P1=P
	;
		Po2=Po0,
		Pr2=Pr0,
		P1=P0
	),
	TP1 is TP+1,
	FN1 is FN-1,
	compute_pointsroc(T,P1,TP1,FP,FN1,TN,Po2,Po1,Pr2,Pr1).

hull([],FPR,TPR,AUC0,AUC1):-
	AUC1 is AUC0+(1-FPR)*(1+TPR)/2.


hull([FPR1-TPR1|T],FPR,TPR,AUC0,AUC1):-
	AUC2 is AUC0+(FPR1-FPR)*(TPR1+TPR)/2,
	hull(T,FPR1,TPR1,AUC2,AUC1).

sample(0,_List,[]):-!.

sample(N,List,[El|List1]):-
	length(List,L),
	random(0,L,Pos),
	nth0(Pos,List,El,Rest),
	N1 is N-1,
	sample(N1,Rest,List1).



find_gasm(LG,Pos,Neg):-
	setof(A,(Y,B)^samemovie(Y,A,B),LA),
	setof(B,(Y,A)^samemovie(Y,A,B),LB),
	append(LA,LB,L),
	remove_duplicates(L,L1),
	findall(samemovie(Y,A,B),(member(A,L1),member(B,L1),samemovie(Y,A,B)),LGP),
	findall(\+ samemovie(Y,A,B),(member(A,L1),member(B,L1),\+ samemovie(Y,A,B)),LGN),
%	sample(200,LGN0,LGN),
	length(LGP,Pos),
	length(LGN,Neg),
	append(LGP,LGN,LG).

find_ga(LG,Pos,Neg):-
	setof(A,(Y,B)^sameperson(Y,A,B),LA),
	setof(B,(Y,A)^sameperson(Y,A,B),LB),
	append(LA,LB,L),
	remove_duplicates(L,L1),
	findall(sameperson(Y,A,B),(member(A,L1),member(B,L1),sameperson(Y,A,B)),LGP),
	findall(\+ sameperson(Y,A,B),(member(A,L1),member(B,L1),\+ sameperson(Y,A,B)),LGN),
	length(LGP,Pos),
	length(LGN,Neg),
%	sample(50,LGN0,LGN),
	length(LGP,Pos),
	length(LGN,Neg),
	append(LGP,LGN,LG).


	
compute_CLL_atoms([],CLL,CLL,[]):-!.
	
compute_CLL_atoms([H|T],CLL0,CLL1,[PG1-H|T1]):-
	solve_CLL([H],PG),!,
		(PG=0.0->
			CLL2 is CLL0-10
%			format("-inf~n",[])
		;	
			CLL2 is CLL0+ log(PG)
%			format("~f~n",[PG])
		),
		(H= (\+ _)->
			PG1 is 1-PG
		;
			PG1 is PG
		),
	compute_CLL_atoms(T,CLL2,CLL1,T1).	
