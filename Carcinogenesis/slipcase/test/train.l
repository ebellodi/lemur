output(active/1).

input(ames/1).
input(has_property/3).
input(ashby_alert/3).
input(mutagenic/1).
input(ind/3).
input(atm/5).
input(nitro/2).
input(sulfo/2).
input(methyl/2).
input(methoxy/2).
input(amine/2).
input(ketone/2).
input(ether/2).
input(sulfide/2).
input(alcohol/2).
input(phenol/2).
input(ester/2).
input(imine/2).
input(alkyl_halide/2).
input(ar_halide/2).
input(non_ar_6c_ring/2).
input(non_ar_hetero_6_ring/2).
input(six_ring/2).
input(non_ar_5c_ring/2).
input(non_ar_hetero_5_ring/2).
input(five_ring/2).
input(connected/2).
input(symbond/4).


modeh(1,active(+drug)).

/*
slipcase doesn't support them
modeb(1,(+charge) >= (#charge)).
modeb(1,(+charge) =< (#charge)).
modeb(1,(+charge) = (#charge)).
modeb(1,(+nalerts) >= (#nalerts)).
modeb(1,(+nalerts) =< (#nalerts)).
modeb(1,(+nalerts) = (#nalerts)).
*/
modeb(*,ames(+drug)).
modeb(*,mutagenic(+drug)).
modeb(*,has_property(+drug,-property,-propval)).
modeb(*,ashby_alert(-alert,+drug,-ring)).
modeb(*,ind(+drug,-alert,-nalerts)).
modeb(*,atm(+drug,-atomid,-element,-atmtype,-charge)).
modeb(*,nitro(+drug,-ring)).
modeb(*,sulfo(+drug,-ring)).
modeb(*,methyl(+drug,-ring)).
modeb(*,methoxy(+drug,-ring)).
modeb(*,amine(+drug,-ring)).
modeb(*,ketone(+drug,-ring)).
modeb(*,ether(+drug,-ring)).
modeb(*,sulfide(+drug,-ring)).
modeb(*,alcohol(+drug,-ring)).
modeb(*,phenol(+drug,-ring)).
modeb(*,ester(+drug,-ring)).
modeb(*,imine(+drug,-ring)).
modeb(*,alkyl_halide(+drug,-ring)).
modeb(*,ar_halide(+drug,-ring)).
modeb(*,non_ar_6c_ring(+drug,-ring)).
modeb(*,non_ar_hetero_6_ring(+drug,-ring)).
modeb(*,six_ring(+drug,-ring)).
modeb(*,non_ar_5c_ring(+drug,-ring)).
modeb(*,non_ar_hetero_5_ring(+drug,-ring)).
modeb(*,five_ring(+drug,-ring)).
modeb(1,connected(+ring,+ring)).
modeb(1,symbond(+ring,+ring)).

connected(_M,Ring1,Ring2):-
        Ring1 \= Ring2,
        member(A,Ring1),
        member(A,Ring2), !.

symbond(Mod,M,A,B,T):- bond(Mod,M,A,B,T).
symbond(Mod,M,A,B,T):- bond(Mod,M,B,A,T).

