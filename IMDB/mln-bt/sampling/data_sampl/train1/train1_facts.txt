useStdLogicVariables: true.
actor(Amichaeldouglas).
actor(Arobyndouglass).
actor(Alewissmith).
actor(Abillyconnolly).
actor(Adanielvonbargen).
actor(Adenisarndt).
actor(Afloramontgomery).
actor(Asheilapaterson).
actor(Astancollymore).
actor(Abenjaminmouton).
actor(Ajeannetripplehorn).
actor(Aneilmaskell).
actor(Asharonstone).
actor(Aellenthomas).
actor(Adavidarnett).
actor(Ajackmcgee).
actor(Awayneknight).
actor(Ajustinmonjo).
actor(Ajaimzwoolvett).
actor(Atimberrington).
actor(Astephentobolowsky).
actor(Agilbellows).
actor(Adavidthewlis).
actor(Aianholm).
actor(Abenjohnson).
actor(Adannflorek).
actor(Aheathcotewilliams).
actor(Acharlotterampling).
actor(Achelcieross).
actor(Astevekuhn).
actor(Amarksangster).
actor(Aindiravarma).
actor(Ahughkeaysbyrne).
actor(Aginachiarelli).
actor(Abruceayoung).
actor(Adavidmorrissey).
actor(Amiguelferrer).
actor(Adorothymalone).
actor(Ageorgedzundza).
actor(Ahughdancy).
actor(Abillcable).
actor(Amarccaleb).
actor(Afrankcturner).
actor(Aalancpeterson).
actor(Anormanarmour).
actor(Ajurneesmollett).
actor(Ajanchappell).
actor(Agusmercurio).
actor(Acaitlinoheaney).
actor(Aterenceharvey).
actor(Adillonmoen).
actor(Aleilanisarelle).
actor(Alloydalan).
actor(Aconnorwiddows).
actor(Adebraengle).
director(Ametcalfestephen).
director(Averhoevenpauli).
director(Amillergeorgei).
director(Acatonjonesmichael).
female(Arobyndouglass).
female(Afloramontgomery).
female(Asheilapaterson).
female(Ajeannetripplehorn).
female(Asharonstone).
female(Aellenthomas).
female(Atimberrington).
female(Acharlotterampling).
female(Achelcieross).
female(Amarksangster).
female(Aginachiarelli).
female(Adorothymalone).
female(Ajurneesmollett).
female(Ajanchappell).
female(Acaitlinoheaney).
female(Aleilanisarelle).
female(Adebraengle).
movie(Abeautifuljoe, Abillyconnolly).
movie(Abeautifuljoe, Asheilapaterson).
movie(Abeautifuljoe, Asharonstone).
movie(Abeautifuljoe, Ajaimzwoolvett).
movie(Abeautifuljoe, Agilbellows).
movie(Abeautifuljoe, Aianholm).
movie(Abeautifuljoe, Abenjohnson).
movie(Abeautifuljoe, Adannflorek).
movie(Abeautifuljoe, Aginachiarelli).
movie(Abeautifuljoe, Afrankcturner).
movie(Abeautifuljoe, Aalancpeterson).
movie(Abeautifuljoe, Anormanarmour).
movie(Abeautifuljoe, Ajurneesmollett).
movie(Abeautifuljoe, Adillonmoen).
movie(Abeautifuljoe, Aconnorwiddows).
movie(Abeautifuljoe, Ametcalfestephen).
movie(Abasicinstinct2, Afloramontgomery).
movie(Abasicinstinct2, Astancollymore).
movie(Abasicinstinct2, Aneilmaskell).
movie(Abasicinstinct2, Asharonstone).
movie(Abasicinstinct2, Aellenthomas).
movie(Abasicinstinct2, Atimberrington).
movie(Abasicinstinct2, Adavidthewlis).
movie(Abasicinstinct2, Aheathcotewilliams).
movie(Abasicinstinct2, Acharlotterampling).
movie(Abasicinstinct2, Amarksangster).
movie(Abasicinstinct2, Aindiravarma).
movie(Abasicinstinct2, Adavidmorrissey).
movie(Abasicinstinct2, Ahughdancy).
movie(Abasicinstinct2, Ajanchappell).
movie(Abasicinstinct2, Aterenceharvey).
movie(Abasicinstinct2, Acatonjonesmichael).
movie(Abasicinstinct, Amichaeldouglas).
movie(Abasicinstinct, Adanielvonbargen).
movie(Abasicinstinct, Adenisarndt).
movie(Abasicinstinct, Abenjaminmouton).
movie(Abasicinstinct, Ajeannetripplehorn).
movie(Abasicinstinct, Asharonstone).
movie(Abasicinstinct, Ajackmcgee).
movie(Abasicinstinct, Awayneknight).
movie(Abasicinstinct, Astephentobolowsky).
movie(Abasicinstinct, Achelcieross).
movie(Abasicinstinct, Abruceayoung).
movie(Abasicinstinct, Adorothymalone).
movie(Abasicinstinct, Ageorgedzundza).
movie(Abasicinstinct, Abillcable).
movie(Abasicinstinct, Aleilanisarelle).
movie(Abasicinstinct, Averhoevenpauli).
movie(Abadlands2005, Arobyndouglass).
movie(Abadlands2005, Alewissmith).
movie(Abadlands2005, Asharonstone).
movie(Abadlands2005, Adavidarnett).
movie(Abadlands2005, Ajustinmonjo).
movie(Abadlands2005, Astevekuhn).
movie(Abadlands2005, Ahughkeaysbyrne).
movie(Abadlands2005, Amiguelferrer).
movie(Abadlands2005, Amarccaleb).
movie(Abadlands2005, Agusmercurio).
movie(Abadlands2005, Acaitlinoheaney).
movie(Abadlands2005, Alloydalan).
movie(Abadlands2005, Adebraengle).
movie(Abadlands2005, Amillergeorgei).
genre(Ametcalfestephen, Adrama).
genre(Ametcalfestephen, Acomedy).
genre(Ametcalfestephen, Aromance).
genre(Averhoevenpauli, Athriller).
genre(Averhoevenpauli, Adrama).
genre(Averhoevenpauli, Acrime).
genre(Averhoevenpauli, Amystery).
genre(Amillergeorgei, Ascifi).
genre(Acatonjonesmichael, Athriller).
genre(Acatonjonesmichael, Adrama).
genre(Acatonjonesmichael, Acrime).
genre(Acatonjonesmichael, Amystery).
actor(Avolkerranisch).
actor(Aludwigbriand).
actor(Agerdlohmeyer).
actor(Astevekalfa).
actor(Aguillaumeromain).
actor(Agabriellelazure).
actor(Ahorstkummeth).
actor(Aloccorbery).
actor(Ajuliedray).
actor(Afirminerichard).
actor(Ahubertmulzer).
actor(Asamantharnier).
actor(Atommikulla).
actor(Acoralyzahonero).
actor(Abrnicebejo).
actor(Abrigittefossey).
actor(Amaxmller).
actor(Amarenschumacher).
actor(Avincentlecoeur).
actor(Aestefanacastro).
actor(Ajuttawachowiak).
actor(Aarmelledeutsch).
actor(Athomasstielner).
actor(Afranoisechristophe).
actor(Apierrearditi).
actor(Aclaudiamessner).
actor(Amarisaburger).
actor(Afrancishuster).
actor(Atheresascholze).
actor(Aalexandrawinisky).
actor(Aandreasschwaiger).
actor(Akarinthaler).
actor(Arenateschroeter).
actor(Adidierdijoux).
actor(Agesinecukrowski).
actor(Akonstanzebreitebner).
actor(Amarkusbker).
actor(Ajrggudzuhn).
actor(Adietermann).
actor(Apatricejuiff).
actor(Accilecassel).
actor(Aleonardlansink).
actor(Aulrichmhe).
director(Akinkelmartin).
director(Akronthalerthomas).
director(Aengelhardtwilhelm).
director(Astephanbernhard).
director(Aromeclaudemichel).
director(Akappesstphane).
director(Aapprederisfranck).
director(Agutjahrrainer).
director(Akrgnther).
director(Abonnetchristiani).
director(Asummereric).
director(Azensmichael).
director(Atonetticlaudio).
director(Avergnejeanpierre).
director(Abraunbettinai).
director(Agustemmanuelii).
director(Aklischstefan).
director(Aladogedominique).
female(Agabriellelazure).
female(Ajuliedray).
female(Afirminerichard).
female(Asamantharnier).
female(Acoralyzahonero).
female(Abrnicebejo).
female(Abrigittefossey).
female(Amarenschumacher).
female(Aestefanacastro).
female(Ajuttawachowiak).
female(Aarmelledeutsch).
female(Afranoisechristophe).
female(Aclaudiamessner).
female(Amarisaburger).
female(Afrancishuster).
female(Atheresascholze).
female(Aalexandrawinisky).
female(Akarinthaler).
female(Arenateschroeter).
female(Agesinecukrowski).
female(Akonstanzebreitebner).
female(Accilecassel).
movie(Aunetunfontsix, Aludwigbriand).
movie(Aunetunfontsix, Aguillaumeromain).
movie(Aunetunfontsix, Aloccorbery).
movie(Aunetunfontsix, Ajuliedray).
movie(Aunetunfontsix, Asamantharnier).
movie(Aunetunfontsix, Abrnicebejo).
movie(Aunetunfontsix, Abrigittefossey).
movie(Aunetunfontsix, Avincentlecoeur).
movie(Aunetunfontsix, Aestefanacastro).
movie(Aunetunfontsix, Aarmelledeutsch).
movie(Aunetunfontsix, Apierrearditi).
movie(Aunetunfontsix, Aalexandrawinisky).
movie(Aunetunfontsix, Adidierdijoux).
movie(Aunetunfontsix, Apatricejuiff).
movie(Aunetunfontsix, Aapprederisfranck).
movie(Aunetunfontsix, Avergnejeanpierre).
movie(Aletztezeugeder, Avolkerranisch).
movie(Aletztezeugeder, Ajuttawachowiak).
movie(Aletztezeugeder, Aclaudiamessner).
movie(Aletztezeugeder, Atheresascholze).
movie(Aletztezeugeder, Aandreasschwaiger).
movie(Aletztezeugeder, Arenateschroeter).
movie(Aletztezeugeder, Agesinecukrowski).
movie(Aletztezeugeder, Akonstanzebreitebner).
movie(Aletztezeugeder, Ajrggudzuhn).
movie(Aletztezeugeder, Adietermann).
movie(Aletztezeugeder, Aleonardlansink).
movie(Aletztezeugeder, Aulrichmhe).
movie(Aletztezeugeder, Astephanbernhard).
movie(Aletztezeugeder, Azensmichael).
movie(Agrandpatronle, Astevekalfa).
movie(Agrandpatronle, Agabriellelazure).
movie(Agrandpatronle, Afirminerichard).
movie(Agrandpatronle, Acoralyzahonero).
movie(Agrandpatronle, Afranoisechristophe).
movie(Agrandpatronle, Afrancishuster).
movie(Agrandpatronle, Accilecassel).
movie(Agrandpatronle, Aromeclaudemichel).
movie(Agrandpatronle, Akappesstphane).
movie(Agrandpatronle, Abonnetchristiani).
movie(Agrandpatronle, Asummereric).
movie(Agrandpatronle, Atonetticlaudio).
movie(Agrandpatronle, Agustemmanuelii).
movie(Agrandpatronle, Aladogedominique).
movie(Arosenheimcopsdie, Agerdlohmeyer).
movie(Arosenheimcopsdie, Ahorstkummeth).
movie(Arosenheimcopsdie, Ahubertmulzer).
movie(Arosenheimcopsdie, Atommikulla).
movie(Arosenheimcopsdie, Amaxmller).
movie(Arosenheimcopsdie, Amarenschumacher).
movie(Arosenheimcopsdie, Athomasstielner).
movie(Arosenheimcopsdie, Amarisaburger).
movie(Arosenheimcopsdie, Aandreasschwaiger).
movie(Arosenheimcopsdie, Akarinthaler).
movie(Arosenheimcopsdie, Amarkusbker).
movie(Arosenheimcopsdie, Akinkelmartin).
movie(Arosenheimcopsdie, Akronthalerthomas).
movie(Arosenheimcopsdie, Aengelhardtwilhelm).
movie(Arosenheimcopsdie, Agutjahrrainer).
movie(Arosenheimcopsdie, Akrgnther).
movie(Arosenheimcopsdie, Abraunbettinai).
movie(Arosenheimcopsdie, Aklischstefan).
genre(Akinkelmartin, Acomedy).
genre(Akinkelmartin, Acrime).
genre(Akronthalerthomas, Acomedy).
genre(Akronthalerthomas, Acrime).
genre(Aengelhardtwilhelm, Acomedy).
genre(Aengelhardtwilhelm, Acrime).
genre(Astephanbernhard, Acrime).
genre(Aapprederisfranck, Acomedy).
genre(Agutjahrrainer, Acomedy).
genre(Agutjahrrainer, Acrime).
genre(Akrgnther, Acomedy).
genre(Akrgnther, Acrime).
genre(Azensmichael, Acrime).
genre(Avergnejeanpierre, Acomedy).
genre(Abraunbettinai, Acomedy).
genre(Abraunbettinai, Acrime).
genre(Aklischstefan, Acomedy).
genre(Aklischstefan, Acrime).
actor(Aangelinasarova).
actor(Aivankondov).
actor(Akirilyanev).
actor(Astoychomazgalov).
actor(Amikhailmikhajlov).
actor(Aganchoganchev).
actor(Amichalbajor).
actor(Ahristodinev).
actor(Avyarakovacheva).
actor(Apetardespotov).
actor(Asavahashamov).
actor(Alyubomirbobchevski).
actor(Amariashopova).
actor(Akostatsonev).
actor(Astefandanailov).
actor(Aivanbratanov).
actor(Aemiliaradeva).
actor(Ageorgistamatov).
actor(Astefanpejchev).
actor(Adimitarhadzhiyanev).
actor(Ajannowicki).
actor(Aalicjajachiewicz).
actor(Asonyadjulgerova).
actor(Aviktordanchenko).
actor(Akunkabaeva).
actor(Aantonradichev).
actor(Aemilmarkov).
actor(Astefanpetrov).
actor(Atzenokandov).
actor(Alyubomirbachvarov).
actor(Aivandimov).
actor(Aivangrigorov).
actor(Alyubomirkanev).
actor(Atzvetolyubrakovski).
actor(Asotirmaynolovski).
actor(Aivantonev).
actor(Abogomilsimeonov).
actor(Anevenakokanova).
actor(Anikoladadov).
actor(Apetyasilyanova).
director(Atraykovatanas).
director(Apunchevborislav).
director(Amarinovichanton).
director(Asurchadzhievstefani).
female(Aangelinasarova).
female(Avyarakovacheva).
female(Amariashopova).
female(Aemiliaradeva).
female(Aalicjajachiewicz).
female(Asonyadjulgerova).
female(Akunkabaeva).
female(Anevenakokanova).
female(Apetyasilyanova).
movie(Ageratzite, Aangelinasarova).
movie(Ageratzite, Aganchoganchev).
movie(Ageratzite, Avyarakovacheva).
movie(Ageratzite, Amariashopova).
movie(Ageratzite, Aivanbratanov).
movie(Ageratzite, Ageorgistamatov).
movie(Ageratzite, Astefanpejchev).
movie(Ageratzite, Akunkabaeva).
movie(Ageratzite, Astefanpetrov).
movie(Ageratzite, Aivandimov).
movie(Ageratzite, Atzvetolyubrakovski).
movie(Ageratzite, Aivantonev).
movie(Ageratzite, Anikoladadov).
movie(Ageratzite, Amarinovichanton).
movie(Aspasenieto, Astoychomazgalov).
movie(Aspasenieto, Amichalbajor).
movie(Aspasenieto, Akostatsonev).
movie(Aspasenieto, Ajannowicki).
movie(Aspasenieto, Aalicjajachiewicz).
movie(Aspasenieto, Aantonradichev).
movie(Aspasenieto, Aemilmarkov).
movie(Aspasenieto, Alyubomirbachvarov).
movie(Aspasenieto, Alyubomirkanev).
movie(Aspasenieto, Asotirmaynolovski).
movie(Aspasenieto, Abogomilsimeonov).
movie(Aspasenieto, Anevenakokanova).
movie(Aspasenieto, Apunchevborislav).
movie(Akristali, Astoychomazgalov).
movie(Akristali, Apetardespotov).
movie(Akristali, Akostatsonev).
movie(Akristali, Astefandanailov).
movie(Akristali, Aemiliaradeva).
movie(Akristali, Adimitarhadzhiyanev).
movie(Akristali, Asonyadjulgerova).
movie(Akristali, Aivangrigorov).
movie(Akristali, Anevenakokanova).
movie(Akristali, Apetyasilyanova).
movie(Akristali, Atraykovatanas).
movie(Alegendazapaisiy, Aangelinasarova).
movie(Alegendazapaisiy, Aivankondov).
movie(Alegendazapaisiy, Akirilyanev).
movie(Alegendazapaisiy, Astoychomazgalov).
movie(Alegendazapaisiy, Amikhailmikhajlov).
movie(Alegendazapaisiy, Ahristodinev).
movie(Alegendazapaisiy, Asavahashamov).
movie(Alegendazapaisiy, Alyubomirbobchevski).
movie(Alegendazapaisiy, Aviktordanchenko).
movie(Alegendazapaisiy, Atzenokandov).
movie(Alegendazapaisiy, Asurchadzhievstefani).
genre(Atraykovatanas, Adrama).
genre(Apunchevborislav, Adrama).
genre(Amarinovichanton, Adrama).
genre(Asurchadzhievstefani, Adrama).
actor(Aluciacammalleri).
actor(Agiancarloscuderi).
actor(Araffaeladavi).
actor(Aenzodimartino).
actor(Aconsuelolupo).
actor(Afrancomirabella).
actor(Abiagiobarone).
actor(Acesareapolito).
actor(Astefanodionisi).
actor(Afabiolobello).
actor(Amanueladolcemascolo).
actor(Acorradofortuna).
actor(Aannamariagherardi).
actor(Amarcocavicchioli).
actor(Adonatellafinocchiaro).
actor(Adanielegalea).
actor(Apaolaciampi).
actor(Amanliosgalambro).
actor(Agabrieleferzetti).
actor(Aantoniettacarbonetti).
actor(Aantoninobruschetta).
actor(Amaurizionicolosi).
actor(Amaurolenares).
actor(Asalvatorelazzaro).
actor(Apieradegliesposti).
actor(Amarcoleonardi).
actor(Aserenaautieri).
actor(Aradarassimov).
actor(Aileanarigano).
actor(Avincenzocrivello).
actor(Amarcospicuglia).
actor(Asaromiano).
actor(Atizianalodato).
actor(Anicolegrimaudo).
actor(Alaurabetti).
actor(Aluciasardo).
actor(Acarmelogalati).
actor(Avannifois).
actor(Aauroraquattrocchi).
actor(Abarbaratabita).
actor(Alucavitrano).
actor(Apenlopecruz).
actor(Aeddasabatini).
director(Asciveresmarianna).
director(Agrimaldiaurelioi).
director(Abattiatofranco).
female(Aluciacammalleri).
female(Araffaeladavi).
female(Afrancomirabella).
female(Amanueladolcemascolo).
female(Aannamariagherardi).
female(Adonatellafinocchiaro).
female(Apaolaciampi).
female(Agabrieleferzetti).
female(Aantoniettacarbonetti).
female(Apieradegliesposti).
female(Aserenaautieri).
female(Aradarassimov).
female(Aileanarigano).
female(Atizianalodato).
female(Anicolegrimaudo).
female(Alaurabetti).
female(Aluciasardo).
female(Aauroraquattrocchi).
female(Abarbaratabita).
female(Apenlopecruz).
female(Aeddasabatini).
movie(Anerolio, Agiancarloscuderi).
movie(Anerolio, Aenzodimartino).
movie(Anerolio, Afrancomirabella).
movie(Anerolio, Afabiolobello).
movie(Anerolio, Amarcocavicchioli).
movie(Anerolio, Aantoniettacarbonetti).
movie(Anerolio, Amaurizionicolosi).
movie(Anerolio, Amaurolenares).
movie(Anerolio, Asalvatorelazzaro).
movie(Anerolio, Apieradegliesposti).
movie(Anerolio, Avincenzocrivello).
movie(Anerolio, Amarcospicuglia).
movie(Anerolio, Asaromiano).
movie(Anerolio, Aluciasardo).
movie(Anerolio, Agrimaldiaurelioi).
movie(Aribellela, Araffaeladavi).
movie(Aribellela, Acesareapolito).
movie(Aribellela, Astefanodionisi).
movie(Aribellela, Adanielegalea).
movie(Aribellela, Apaolaciampi).
movie(Aribellela, Amarcoleonardi).
movie(Aribellela, Alaurabetti).
movie(Aribellela, Aauroraquattrocchi).
movie(Aribellela, Apenlopecruz).
movie(Aribellela, Aeddasabatini).
movie(Aribellela, Agrimaldiaurelioi).
movie(Aperdutoamor, Acorradofortuna).
movie(Aperdutoamor, Aannamariagherardi).
movie(Aperdutoamor, Adonatellafinocchiaro).
movie(Aperdutoamor, Amanliosgalambro).
movie(Aperdutoamor, Agabrieleferzetti).
movie(Aperdutoamor, Aantoninobruschetta).
movie(Aperdutoamor, Aradarassimov).
movie(Aperdutoamor, Atizianalodato).
movie(Aperdutoamor, Anicolegrimaudo).
movie(Aperdutoamor, Aluciasardo).
movie(Aperdutoamor, Alucavitrano).
movie(Aperdutoamor, Abattiatofranco).
movie(Asaramay, Aluciacammalleri).
movie(Asaramay, Aconsuelolupo).
movie(Asaramay, Abiagiobarone).
movie(Asaramay, Amanueladolcemascolo).
movie(Asaramay, Aserenaautieri).
movie(Asaramay, Aileanarigano).
movie(Asaramay, Aluciasardo).
movie(Asaramay, Acarmelogalati).
movie(Asaramay, Avannifois).
movie(Asaramay, Abarbaratabita).
movie(Asaramay, Asciveresmarianna).
genre(Agrimaldiaurelioi, Adrama).
genre(Abattiatofranco, Adrama).
