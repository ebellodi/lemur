#change N with {1..5}

java -cp ../Boostr/WILL.jar edu.wisc.cs.will.Boosting.Common.RunBoostedModels \
-target workedunder \
-trees 20 \
-negPosRatio -1 \
-save -l -train ../data_wu/trainN/ \
-mln \
1> risN.txt 2>stderrN.txt
