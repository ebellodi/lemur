useStdLogicVariables: true.
actor(Aeddiejemison).
actor(Atimothypaulperez).
actor(Acaseyaffleck).
actor(Ajorgerhernandez).
actor(Astanleyanderson).
actor(Atonygoldwyn).
actor(Afrankpatton).
actor(Anicholaswoodeson).
actor(Anelsonpeltz).
actor(Ajohnfinn).
actor(Alorigalinski).
actor(Atimrobbins).
actor(Agretascacchi).
actor(Adontiffany).
actor(Abradpitt).
actor(Ajohnheard).
actor(Aelliottgould).
actor(Aannejacques).
actor(Alylelovett).
actor(Acarolflorence).
actor(Aleahayres).
actor(Arobertculp).
actor(Adinaconnolly).
actor(Aandygarcia).
actor(Adenzelwashington).
actor(Aberniemac).
actor(Acynthiastevenson).
actor(Adeanstockwell).
actor(Aanthonyheald).
actor(Ahumecronyn).
actor(Adinamerrill).
actor(Apetergallagher).
actor(Aangelahall).
actor(Ajohnlithgow).
actor(Ajamessikking).
actor(Alarrysontag).
actor(Avincentdonofrio).
actor(Abrionjames).
actor(Afredward).
actor(Amarkgantt).
actor(Astanleytucci).
actor(Apaullnolan).
actor(Arichardegrant).
actor(Acatherinezetajones).
actor(Asamshepard).
actor(Awhoopigoldberg).
actor(Ajuliaroberts).
actor(Ageorgeclooney).
actor(Adavidsontag).
actor(Awilliamatherton).
actor(Asydneypollack).
actor(Aminianden).
actor(Ascottcaan).
actor(Aceceliaannbirt).
actor(Aedkross).
director(Asoderberghsteven).
director(Apakulaalanj).
director(Aaltmanroberti).
female(Aeddiejemison).
female(Atimothypaulperez).
female(Acaseyaffleck).
female(Ajorgerhernandez).
female(Afrankpatton).
female(Alorigalinski).
female(Agretascacchi).
female(Abradpitt).
female(Aelliottgould).
female(Aannejacques).
female(Acarolflorence).
female(Aleahayres).
female(Adinaconnolly).
female(Aberniemac).
female(Acynthiastevenson).
female(Adinamerrill).
female(Aangelahall).
female(Amarkgantt).
female(Apaullnolan).
female(Acatherinezetajones).
female(Awhoopigoldberg).
female(Ajuliaroberts).
female(Ageorgeclooney).
female(Aminianden).
female(Ascottcaan).
female(Aceceliaannbirt).
movie(Aoceanstwelve, Acaseyaffleck).
movie(Aoceanstwelve, Anelsonpeltz).
movie(Aoceanstwelve, Adontiffany).
movie(Aoceanstwelve, Abradpitt).
movie(Aoceanstwelve, Aannejacques).
movie(Aoceanstwelve, Adinaconnolly).
movie(Aoceanstwelve, Aandygarcia).
movie(Aoceanstwelve, Alarrysontag).
movie(Aoceanstwelve, Acatherinezetajones).
movie(Aoceanstwelve, Ajuliaroberts).
movie(Aoceanstwelve, Ageorgeclooney).
movie(Aoceanstwelve, Adavidsontag).
movie(Aoceanstwelve, Aminianden).
movie(Aoceanstwelve, Ascottcaan).
movie(Aoceanstwelve, Aedkross).
movie(Aoceanstwelve, Asoderberghsteven).
movie(Aplayerthe, Atimrobbins).
movie(Aplayerthe, Agretascacchi).
movie(Aplayerthe, Alylelovett).
movie(Aplayerthe, Aleahayres).
movie(Aplayerthe, Acynthiastevenson).
movie(Aplayerthe, Adeanstockwell).
movie(Aplayerthe, Adinamerrill).
movie(Aplayerthe, Apetergallagher).
movie(Aplayerthe, Aangelahall).
movie(Aplayerthe, Avincentdonofrio).
movie(Aplayerthe, Abrionjames).
movie(Aplayerthe, Afredward).
movie(Aplayerthe, Arichardegrant).
movie(Aplayerthe, Awhoopigoldberg).
movie(Aplayerthe, Asydneypollack).
movie(Aplayerthe, Aaltmanroberti).
movie(Apelicanbriefthe, Astanleyanderson).
movie(Apelicanbriefthe, Atonygoldwyn).
movie(Apelicanbriefthe, Anicholaswoodeson).
movie(Apelicanbriefthe, Ajohnfinn).
movie(Apelicanbriefthe, Ajohnheard).
movie(Apelicanbriefthe, Arobertculp).
movie(Apelicanbriefthe, Adenzelwashington).
movie(Apelicanbriefthe, Aanthonyheald).
movie(Apelicanbriefthe, Ahumecronyn).
movie(Apelicanbriefthe, Ajohnlithgow).
movie(Apelicanbriefthe, Ajamessikking).
movie(Apelicanbriefthe, Astanleytucci).
movie(Apelicanbriefthe, Asamshepard).
movie(Apelicanbriefthe, Ajuliaroberts).
movie(Apelicanbriefthe, Awilliamatherton).
movie(Apelicanbriefthe, Apakulaalanj).
movie(Aoceanseleven, Aeddiejemison).
movie(Aoceanseleven, Atimothypaulperez).
movie(Aoceanseleven, Acaseyaffleck).
movie(Aoceanseleven, Ajorgerhernandez).
movie(Aoceanseleven, Afrankpatton).
movie(Aoceanseleven, Alorigalinski).
movie(Aoceanseleven, Abradpitt).
movie(Aoceanseleven, Aelliottgould).
movie(Aoceanseleven, Acarolflorence).
movie(Aoceanseleven, Aberniemac).
movie(Aoceanseleven, Amarkgantt).
movie(Aoceanseleven, Apaullnolan).
movie(Aoceanseleven, Ageorgeclooney).
movie(Aoceanseleven, Ascottcaan).
movie(Aoceanseleven, Aceceliaannbirt).
movie(Aoceanseleven, Asoderberghsteven).
genre(Asoderberghsteven, Athriller).
genre(Asoderberghsteven, Acomedy).
genre(Asoderberghsteven, Adocumentary).
genre(Asoderberghsteven, Aaction).
genre(Asoderberghsteven, Acrime).
genre(Apakulaalanj, Athriller).
genre(Apakulaalanj, Adrama).
genre(Apakulaalanj, Amystery).
genre(Aaltmanroberti, Athriller).
genre(Aaltmanroberti, Acomedy).
genre(Aaltmanroberti, Adrama).
actor(Amichaeldouglas).
actor(Arobyndouglass).
actor(Alewissmith).
actor(Abillyconnolly).
actor(Adanielvonbargen).
actor(Adenisarndt).
actor(Afloramontgomery).
actor(Asheilapaterson).
actor(Astancollymore).
actor(Abenjaminmouton).
actor(Ajeannetripplehorn).
actor(Aneilmaskell).
actor(Asharonstone).
actor(Aellenthomas).
actor(Adavidarnett).
actor(Ajackmcgee).
actor(Awayneknight).
actor(Ajustinmonjo).
actor(Ajaimzwoolvett).
actor(Atimberrington).
actor(Astephentobolowsky).
actor(Agilbellows).
actor(Adavidthewlis).
actor(Aianholm).
actor(Abenjohnson).
actor(Adannflorek).
actor(Aheathcotewilliams).
actor(Acharlotterampling).
actor(Achelcieross).
actor(Astevekuhn).
actor(Amarksangster).
actor(Aindiravarma).
actor(Ahughkeaysbyrne).
actor(Aginachiarelli).
actor(Abruceayoung).
actor(Adavidmorrissey).
actor(Amiguelferrer).
actor(Adorothymalone).
actor(Ageorgedzundza).
actor(Ahughdancy).
actor(Abillcable).
actor(Amarccaleb).
actor(Afrankcturner).
actor(Aalancpeterson).
actor(Anormanarmour).
actor(Ajurneesmollett).
actor(Ajanchappell).
actor(Agusmercurio).
actor(Acaitlinoheaney).
actor(Aterenceharvey).
actor(Adillonmoen).
actor(Aleilanisarelle).
actor(Alloydalan).
actor(Aconnorwiddows).
actor(Adebraengle).
director(Ametcalfestephen).
director(Averhoevenpauli).
director(Amillergeorgei).
director(Acatonjonesmichael).
female(Arobyndouglass).
female(Afloramontgomery).
female(Asheilapaterson).
female(Ajeannetripplehorn).
female(Asharonstone).
female(Aellenthomas).
female(Atimberrington).
female(Acharlotterampling).
female(Achelcieross).
female(Amarksangster).
female(Aginachiarelli).
female(Adorothymalone).
female(Ajurneesmollett).
female(Ajanchappell).
female(Acaitlinoheaney).
female(Aleilanisarelle).
female(Adebraengle).
movie(Abeautifuljoe, Abillyconnolly).
movie(Abeautifuljoe, Asheilapaterson).
movie(Abeautifuljoe, Asharonstone).
movie(Abeautifuljoe, Ajaimzwoolvett).
movie(Abeautifuljoe, Agilbellows).
movie(Abeautifuljoe, Aianholm).
movie(Abeautifuljoe, Abenjohnson).
movie(Abeautifuljoe, Adannflorek).
movie(Abeautifuljoe, Aginachiarelli).
movie(Abeautifuljoe, Afrankcturner).
movie(Abeautifuljoe, Aalancpeterson).
movie(Abeautifuljoe, Anormanarmour).
movie(Abeautifuljoe, Ajurneesmollett).
movie(Abeautifuljoe, Adillonmoen).
movie(Abeautifuljoe, Aconnorwiddows).
movie(Abeautifuljoe, Ametcalfestephen).
movie(Abasicinstinct2, Afloramontgomery).
movie(Abasicinstinct2, Astancollymore).
movie(Abasicinstinct2, Aneilmaskell).
movie(Abasicinstinct2, Asharonstone).
movie(Abasicinstinct2, Aellenthomas).
movie(Abasicinstinct2, Atimberrington).
movie(Abasicinstinct2, Adavidthewlis).
movie(Abasicinstinct2, Aheathcotewilliams).
movie(Abasicinstinct2, Acharlotterampling).
movie(Abasicinstinct2, Amarksangster).
movie(Abasicinstinct2, Aindiravarma).
movie(Abasicinstinct2, Adavidmorrissey).
movie(Abasicinstinct2, Ahughdancy).
movie(Abasicinstinct2, Ajanchappell).
movie(Abasicinstinct2, Aterenceharvey).
movie(Abasicinstinct2, Acatonjonesmichael).
movie(Abasicinstinct, Amichaeldouglas).
movie(Abasicinstinct, Adanielvonbargen).
movie(Abasicinstinct, Adenisarndt).
movie(Abasicinstinct, Abenjaminmouton).
movie(Abasicinstinct, Ajeannetripplehorn).
movie(Abasicinstinct, Asharonstone).
movie(Abasicinstinct, Ajackmcgee).
movie(Abasicinstinct, Awayneknight).
movie(Abasicinstinct, Astephentobolowsky).
movie(Abasicinstinct, Achelcieross).
movie(Abasicinstinct, Abruceayoung).
movie(Abasicinstinct, Adorothymalone).
movie(Abasicinstinct, Ageorgedzundza).
movie(Abasicinstinct, Abillcable).
movie(Abasicinstinct, Aleilanisarelle).
movie(Abasicinstinct, Averhoevenpauli).
movie(Abadlands2005, Arobyndouglass).
movie(Abadlands2005, Alewissmith).
movie(Abadlands2005, Asharonstone).
movie(Abadlands2005, Adavidarnett).
movie(Abadlands2005, Ajustinmonjo).
movie(Abadlands2005, Astevekuhn).
movie(Abadlands2005, Ahughkeaysbyrne).
movie(Abadlands2005, Amiguelferrer).
movie(Abadlands2005, Amarccaleb).
movie(Abadlands2005, Agusmercurio).
movie(Abadlands2005, Acaitlinoheaney).
movie(Abadlands2005, Alloydalan).
movie(Abadlands2005, Adebraengle).
movie(Abadlands2005, Amillergeorgei).
genre(Ametcalfestephen, Adrama).
genre(Ametcalfestephen, Acomedy).
genre(Ametcalfestephen, Aromance).
genre(Averhoevenpauli, Athriller).
genre(Averhoevenpauli, Adrama).
genre(Averhoevenpauli, Acrime).
genre(Averhoevenpauli, Amystery).
genre(Amillergeorgei, Ascifi).
genre(Acatonjonesmichael, Athriller).
genre(Acatonjonesmichael, Adrama).
genre(Acatonjonesmichael, Acrime).
genre(Acatonjonesmichael, Amystery).
actor(Aangelinasarova).
actor(Aivankondov).
actor(Akirilyanev).
actor(Astoychomazgalov).
actor(Amikhailmikhajlov).
actor(Aganchoganchev).
actor(Amichalbajor).
actor(Ahristodinev).
actor(Avyarakovacheva).
actor(Apetardespotov).
actor(Asavahashamov).
actor(Alyubomirbobchevski).
actor(Amariashopova).
actor(Akostatsonev).
actor(Astefandanailov).
actor(Aivanbratanov).
actor(Aemiliaradeva).
actor(Ageorgistamatov).
actor(Astefanpejchev).
actor(Adimitarhadzhiyanev).
actor(Ajannowicki).
actor(Aalicjajachiewicz).
actor(Asonyadjulgerova).
actor(Aviktordanchenko).
actor(Akunkabaeva).
actor(Aantonradichev).
actor(Aemilmarkov).
actor(Astefanpetrov).
actor(Atzenokandov).
actor(Alyubomirbachvarov).
actor(Aivandimov).
actor(Aivangrigorov).
actor(Alyubomirkanev).
actor(Atzvetolyubrakovski).
actor(Asotirmaynolovski).
actor(Aivantonev).
actor(Abogomilsimeonov).
actor(Anevenakokanova).
actor(Anikoladadov).
actor(Apetyasilyanova).
director(Atraykovatanas).
director(Apunchevborislav).
director(Amarinovichanton).
director(Asurchadzhievstefani).
female(Aangelinasarova).
female(Avyarakovacheva).
female(Amariashopova).
female(Aemiliaradeva).
female(Aalicjajachiewicz).
female(Asonyadjulgerova).
female(Akunkabaeva).
female(Anevenakokanova).
female(Apetyasilyanova).
movie(Ageratzite, Aangelinasarova).
movie(Ageratzite, Aganchoganchev).
movie(Ageratzite, Avyarakovacheva).
movie(Ageratzite, Amariashopova).
movie(Ageratzite, Aivanbratanov).
movie(Ageratzite, Ageorgistamatov).
movie(Ageratzite, Astefanpejchev).
movie(Ageratzite, Akunkabaeva).
movie(Ageratzite, Astefanpetrov).
movie(Ageratzite, Aivandimov).
movie(Ageratzite, Atzvetolyubrakovski).
movie(Ageratzite, Aivantonev).
movie(Ageratzite, Anikoladadov).
movie(Ageratzite, Amarinovichanton).
movie(Aspasenieto, Astoychomazgalov).
movie(Aspasenieto, Amichalbajor).
movie(Aspasenieto, Akostatsonev).
movie(Aspasenieto, Ajannowicki).
movie(Aspasenieto, Aalicjajachiewicz).
movie(Aspasenieto, Aantonradichev).
movie(Aspasenieto, Aemilmarkov).
movie(Aspasenieto, Alyubomirbachvarov).
movie(Aspasenieto, Alyubomirkanev).
movie(Aspasenieto, Asotirmaynolovski).
movie(Aspasenieto, Abogomilsimeonov).
movie(Aspasenieto, Anevenakokanova).
movie(Aspasenieto, Apunchevborislav).
movie(Akristali, Astoychomazgalov).
movie(Akristali, Apetardespotov).
movie(Akristali, Akostatsonev).
movie(Akristali, Astefandanailov).
movie(Akristali, Aemiliaradeva).
movie(Akristali, Adimitarhadzhiyanev).
movie(Akristali, Asonyadjulgerova).
movie(Akristali, Aivangrigorov).
movie(Akristali, Anevenakokanova).
movie(Akristali, Apetyasilyanova).
movie(Akristali, Atraykovatanas).
movie(Alegendazapaisiy, Aangelinasarova).
movie(Alegendazapaisiy, Aivankondov).
movie(Alegendazapaisiy, Akirilyanev).
movie(Alegendazapaisiy, Astoychomazgalov).
movie(Alegendazapaisiy, Amikhailmikhajlov).
movie(Alegendazapaisiy, Ahristodinev).
movie(Alegendazapaisiy, Asavahashamov).
movie(Alegendazapaisiy, Alyubomirbobchevski).
movie(Alegendazapaisiy, Aviktordanchenko).
movie(Alegendazapaisiy, Atzenokandov).
movie(Alegendazapaisiy, Asurchadzhievstefani).
genre(Atraykovatanas, Adrama).
genre(Apunchevborislav, Adrama).
genre(Amarinovichanton, Adrama).
genre(Asurchadzhievstefani, Adrama).
actor(Aluciacammalleri).
actor(Agiancarloscuderi).
actor(Araffaeladavi).
actor(Aenzodimartino).
actor(Aconsuelolupo).
actor(Afrancomirabella).
actor(Abiagiobarone).
actor(Acesareapolito).
actor(Astefanodionisi).
actor(Afabiolobello).
actor(Amanueladolcemascolo).
actor(Acorradofortuna).
actor(Aannamariagherardi).
actor(Amarcocavicchioli).
actor(Adonatellafinocchiaro).
actor(Adanielegalea).
actor(Apaolaciampi).
actor(Amanliosgalambro).
actor(Agabrieleferzetti).
actor(Aantoniettacarbonetti).
actor(Aantoninobruschetta).
actor(Amaurizionicolosi).
actor(Amaurolenares).
actor(Asalvatorelazzaro).
actor(Apieradegliesposti).
actor(Amarcoleonardi).
actor(Aserenaautieri).
actor(Aradarassimov).
actor(Aileanarigano).
actor(Avincenzocrivello).
actor(Amarcospicuglia).
actor(Asaromiano).
actor(Atizianalodato).
actor(Anicolegrimaudo).
actor(Alaurabetti).
actor(Aluciasardo).
actor(Acarmelogalati).
actor(Avannifois).
actor(Aauroraquattrocchi).
actor(Abarbaratabita).
actor(Alucavitrano).
actor(Apenlopecruz).
actor(Aeddasabatini).
director(Asciveresmarianna).
director(Agrimaldiaurelioi).
director(Abattiatofranco).
female(Aluciacammalleri).
female(Araffaeladavi).
female(Afrancomirabella).
female(Amanueladolcemascolo).
female(Aannamariagherardi).
female(Adonatellafinocchiaro).
female(Apaolaciampi).
female(Agabrieleferzetti).
female(Aantoniettacarbonetti).
female(Apieradegliesposti).
female(Aserenaautieri).
female(Aradarassimov).
female(Aileanarigano).
female(Atizianalodato).
female(Anicolegrimaudo).
female(Alaurabetti).
female(Aluciasardo).
female(Aauroraquattrocchi).
female(Abarbaratabita).
female(Apenlopecruz).
female(Aeddasabatini).
movie(Anerolio, Agiancarloscuderi).
movie(Anerolio, Aenzodimartino).
movie(Anerolio, Afrancomirabella).
movie(Anerolio, Afabiolobello).
movie(Anerolio, Amarcocavicchioli).
movie(Anerolio, Aantoniettacarbonetti).
movie(Anerolio, Amaurizionicolosi).
movie(Anerolio, Amaurolenares).
movie(Anerolio, Asalvatorelazzaro).
movie(Anerolio, Apieradegliesposti).
movie(Anerolio, Avincenzocrivello).
movie(Anerolio, Amarcospicuglia).
movie(Anerolio, Asaromiano).
movie(Anerolio, Aluciasardo).
movie(Anerolio, Agrimaldiaurelioi).
movie(Aribellela, Araffaeladavi).
movie(Aribellela, Acesareapolito).
movie(Aribellela, Astefanodionisi).
movie(Aribellela, Adanielegalea).
movie(Aribellela, Apaolaciampi).
movie(Aribellela, Amarcoleonardi).
movie(Aribellela, Alaurabetti).
movie(Aribellela, Aauroraquattrocchi).
movie(Aribellela, Apenlopecruz).
movie(Aribellela, Aeddasabatini).
movie(Aribellela, Agrimaldiaurelioi).
movie(Aperdutoamor, Acorradofortuna).
movie(Aperdutoamor, Aannamariagherardi).
movie(Aperdutoamor, Adonatellafinocchiaro).
movie(Aperdutoamor, Amanliosgalambro).
movie(Aperdutoamor, Agabrieleferzetti).
movie(Aperdutoamor, Aantoninobruschetta).
movie(Aperdutoamor, Aradarassimov).
movie(Aperdutoamor, Atizianalodato).
movie(Aperdutoamor, Anicolegrimaudo).
movie(Aperdutoamor, Aluciasardo).
movie(Aperdutoamor, Alucavitrano).
movie(Aperdutoamor, Abattiatofranco).
movie(Asaramay, Aluciacammalleri).
movie(Asaramay, Aconsuelolupo).
movie(Asaramay, Abiagiobarone).
movie(Asaramay, Amanueladolcemascolo).
movie(Asaramay, Aserenaautieri).
movie(Asaramay, Aileanarigano).
movie(Asaramay, Aluciasardo).
movie(Asaramay, Acarmelogalati).
movie(Asaramay, Avannifois).
movie(Asaramay, Abarbaratabita).
movie(Asaramay, Asciveresmarianna).
genre(Agrimaldiaurelioi, Adrama).
genre(Abattiatofranco, Adrama).
