#change N with {1..5} (5-fold cv)

java -cp ../Boostr/WILL.jar edu.wisc.cs.will.Boosting.Common.RunBoostedModels \
-aucJarPath "../Boostr" \
-target workedunder \
-trees 20 \
-model ../data_wu/trainN/models \
-testNegPosRatio -1 \
-save -i -test ../data_wu/testN/ \
1>inferN.txt
