%Predizione di workedUnder

output(workedunder/2).

input(female/1).
input(genre/2).
input(actor/1).
input(director/1).
input(movie/2).

determination(workedunder/2,female/1).
determination(workedunder/2,genre/2).
determination(workedunder/2,actor/1).
determination(workedunder/2,director/1).
determination(workedunder/2,movie/2).

modeh(*,workedunder(+p,+p)).

/*
modeb(*,workedunder(+p,+p)).
modeb(*,workedunder(-p,+p)).
modeb(*,workedunder(+p,-p)).
*/
modeb(*,movie(-m,+p)).
modeb(*,movie(+m,-p)).
modeb(*,female(+p)).
modeb(*,genre(+p,-g)).
/*
modeb(*,genre(+p,ascifi)).
modeb(*,genre(+p,athriller)).
modeb(*,genre(+p,adrama)).
modeb(*,genre(+p,acrime)).
modeb(*,genre(+p,acomedy)).
modeb(*,genre(+p,amystery)).
modeb(*,genre(+p,aromance)).
*/
modeb(*,actor(+p)).
modeb(*,director(+p)).
