

:-[aleph].
:-read_all(imdb2f).   %refers to m2f.b,.f,.n

:- set(clauselength, 5).%  V is a positive integer (default 4). Sets upper bound on number of literals in an acceptable clause.
:- set(evalfn, auto_m). %V is one of: coverage, compression, posonly, pbayes, accuracy, laplace, auto_m, mestimate, entropy, gini, sd, wracc, or user (default coverage). Sets the evaluation function for a search. 
:- set(minpos, 2).%V is a positive integer (default 1). Set a lower bound on the number of positive examples to be covered by an acceptable clause. If the best clause covers positive examples below this number, then it is not added to the current theory. This can be used to prevent Aleph from adding ground unit clauses to the theory (by setting the value to 2).
:- set(minscore, 0.6).%V is an floating point number (default -inf). Set a lower bound on the utility of of an acceptable clause. When constructing clauses, If the best clause has utility below this number, then it is not added to the current theory. Beware: you can get counter-intuitive results in conjunction with the minpos setting.
%%:- set(nodes, 5000).
:- set(noise, 300).%V is an integer >= 0 (default 0). Set an upper bound on the number of negative examples allowed to be covered by an acceptable clause.
%%:- set(openlist, 100).

:- induce_cover.
:- write_rules('imdb2.rules').

