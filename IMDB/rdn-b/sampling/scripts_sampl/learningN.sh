#change N with {1..5} (5-fold cv)

java -cp ../Boostr/WILL.jar edu.wisc.cs.will.Boosting.Common.RunBoostedModels \
-target workedunder \
-trees 20 \
-save -l -train ../data_sampl/trainN/ \
1> risN.txt 2>stderrN.txt
