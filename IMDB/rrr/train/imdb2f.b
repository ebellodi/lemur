%Predizione di workedUnder

:- modeh(1,workedunder(+p,+p)).

:- modeb(*,movie(-m,+p)).
:- modeb(*,movie(+m,-p)).
:- modeb(*,female(+p)).
:- modeb(*,genre(+p,-g)).
:- modeb(*,actor(+p)).
:- modeb(*,director(+p)).

:- determination(workedunder/2,movie/2).
:- determination(workedunder/2,female/1).
:- determination(workedunder/2,genre/2).
:- determination(workedunder/2,actor/1).
:- determination(workedunder/2,director/1).

%background di training

actor(aeddiejemison).
actor(atimothypaulperez).
actor(acaseyaffleck).
actor(ajorgerhernandez).
actor(astanleyanderson).
actor(atonygoldwyn).
actor(afrankpatton).
actor(anicholaswoodeson).
actor(anelsonpeltz).
actor(ajohnfinn).
actor(alorigalinski).
actor(atimrobbins).
actor(agretascacchi).
actor(adontiffany).
actor(abradpitt).
actor(ajohnheard).
actor(aelliottgould).
actor(aannejacques).
actor(alylelovett).
actor(acarolflorence).
actor(aleahayres).
actor(arobertculp).
actor(adinaconnolly).
actor(aandygarcia).
actor(adenzelwashington).
actor(aberniemac).
actor(acynthiastevenson).
actor(adeanstockwell).
actor(aanthonyheald).
actor(ahumecronyn).
actor(adinamerrill).
actor(apetergallagher).
actor(aangelahall).
actor(ajohnlithgow).
actor(ajamessikking).
actor(alarrysontag).
actor(avincentdonofrio).
actor(abrionjames).
actor(afredward).
actor(amarkgantt).
actor(astanleytucci).
actor(apaullnolan).
actor(arichardegrant).
actor(acatherinezetajones).
actor(asamshepard).
actor(awhoopigoldberg).
actor(ajuliaroberts).
actor(ageorgeclooney).
actor(adavidsontag).
actor(awilliamatherton).
actor(asydneypollack).
actor(aminianden).
actor(ascottcaan).
actor(aceceliaannbirt).
actor(aedkross).
director(asoderberghsteven).
director(apakulaalanj).
director(aaltmanroberti).
female(aeddiejemison).
female(atimothypaulperez).
female(acaseyaffleck).
female(ajorgerhernandez).
female(afrankpatton).
female(alorigalinski).
female(agretascacchi).
female(abradpitt).
female(aelliottgould).
female(aannejacques).
female(acarolflorence).
female(aleahayres).
female(adinaconnolly).
female(aberniemac).
female(acynthiastevenson).
female(adinamerrill).
female(aangelahall).
female(amarkgantt).
female(apaullnolan).
female(acatherinezetajones).
female(awhoopigoldberg).
female(ajuliaroberts).
female(ageorgeclooney).
female(aminianden).
female(ascottcaan).
female(aceceliaannbirt).
movie(aoceanstwelve,acaseyaffleck).
movie(aoceanstwelve,anelsonpeltz).
movie(aoceanstwelve,adontiffany).
movie(aoceanstwelve,abradpitt).
movie(aoceanstwelve,aannejacques).
movie(aoceanstwelve,adinaconnolly).
movie(aoceanstwelve,aandygarcia).
movie(aoceanstwelve,alarrysontag).
movie(aoceanstwelve,acatherinezetajones).
movie(aoceanstwelve,ajuliaroberts).
movie(aoceanstwelve,ageorgeclooney).
movie(aoceanstwelve,adavidsontag).
movie(aoceanstwelve,aminianden).
movie(aoceanstwelve,ascottcaan).
movie(aoceanstwelve,aedkross).
movie(aoceanstwelve,asoderberghsteven).
movie(aplayerthe,atimrobbins).
movie(aplayerthe,agretascacchi).
movie(aplayerthe,alylelovett).
movie(aplayerthe,aleahayres).
movie(aplayerthe,acynthiastevenson).
movie(aplayerthe,adeanstockwell).
movie(aplayerthe,adinamerrill).
movie(aplayerthe,apetergallagher).
movie(aplayerthe,aangelahall).
movie(aplayerthe,avincentdonofrio).
movie(aplayerthe,abrionjames).
movie(aplayerthe,afredward).
movie(aplayerthe,arichardegrant).
movie(aplayerthe,awhoopigoldberg).
movie(aplayerthe,asydneypollack).
movie(aplayerthe,aaltmanroberti).
movie(apelicanbriefthe,astanleyanderson).
movie(apelicanbriefthe,atonygoldwyn).
movie(apelicanbriefthe,anicholaswoodeson).
movie(apelicanbriefthe,ajohnfinn).
movie(apelicanbriefthe,ajohnheard).
movie(apelicanbriefthe,arobertculp).
movie(apelicanbriefthe,adenzelwashington).
movie(apelicanbriefthe,aanthonyheald).
movie(apelicanbriefthe,ahumecronyn).
movie(apelicanbriefthe,ajohnlithgow).
movie(apelicanbriefthe,ajamessikking).
movie(apelicanbriefthe,astanleytucci).
movie(apelicanbriefthe,asamshepard).
movie(apelicanbriefthe,ajuliaroberts).
movie(apelicanbriefthe,awilliamatherton).
movie(apelicanbriefthe,apakulaalanj).
movie(aoceanseleven,aeddiejemison).
movie(aoceanseleven,atimothypaulperez).
movie(aoceanseleven,acaseyaffleck).
movie(aoceanseleven,ajorgerhernandez).
movie(aoceanseleven,afrankpatton).
movie(aoceanseleven,alorigalinski).
movie(aoceanseleven,abradpitt).
movie(aoceanseleven,aelliottgould).
movie(aoceanseleven,acarolflorence).
movie(aoceanseleven,aberniemac).
movie(aoceanseleven,amarkgantt).
movie(aoceanseleven,apaullnolan).
movie(aoceanseleven,ageorgeclooney).
movie(aoceanseleven,ascottcaan).
movie(aoceanseleven,aceceliaannbirt).
movie(aoceanseleven,asoderberghsteven).
genre(asoderberghsteven,athriller).
genre(asoderberghsteven,acomedy).
genre(asoderberghsteven,adocumentary).
genre(asoderberghsteven,aaction).
genre(asoderberghsteven,acrime).
genre(apakulaalanj,athriller).
genre(apakulaalanj,adrama).
genre(apakulaalanj,amystery).
genre(aaltmanroberti,athriller).
genre(aaltmanroberti,acomedy).
genre(aaltmanroberti,adrama).
actor(avolkerranisch).
actor(aludwigbriand).
actor(agerdlohmeyer).
actor(astevekalfa).
actor(aguillaumeromain).
actor(agabriellelazure).
actor(ahorstkummeth).
actor(aloccorbery).
actor(ajuliedray).
actor(afirminerichard).
actor(ahubertmulzer).
actor(asamantharnier).
actor(atommikulla).
actor(acoralyzahonero).
actor(abrnicebejo).
actor(abrigittefossey).
actor(amaxmller).
actor(amarenschumacher).
actor(avincentlecoeur).
actor(aestefanacastro).
actor(ajuttawachowiak).
actor(aarmelledeutsch).
actor(athomasstielner).
actor(afranoisechristophe).
actor(apierrearditi).
actor(aclaudiamessner).
actor(amarisaburger).
actor(afrancishuster).
actor(atheresascholze).
actor(aalexandrawinisky).
actor(aandreasschwaiger).
actor(akarinthaler).
actor(arenateschroeter).
actor(adidierdijoux).
actor(agesinecukrowski).
actor(akonstanzebreitebner).
actor(amarkusbker).
actor(ajrggudzuhn).
actor(adietermann).
actor(apatricejuiff).
actor(accilecassel).
actor(aleonardlansink).
actor(aulrichmhe).
director(akinkelmartin).
director(akronthalerthomas).
director(aengelhardtwilhelm).
director(astephanbernhard).
director(aromeclaudemichel).
director(akappesstphane).
director(aapprederisfranck).
director(agutjahrrainer).
director(akrgnther).
director(abonnetchristiani).
director(asummereric).
director(azensmichael).
director(atonetticlaudio).
director(avergnejeanpierre).
director(abraunbettinai).
director(agustemmanuelii).
director(aklischstefan).
director(aladogedominique).
female(agabriellelazure).
female(ajuliedray).
female(afirminerichard).
female(asamantharnier).
female(acoralyzahonero).
female(abrnicebejo).
female(abrigittefossey).
female(amarenschumacher).
female(aestefanacastro).
female(ajuttawachowiak).
female(aarmelledeutsch).
female(afranoisechristophe).
female(aclaudiamessner).
female(amarisaburger).
female(afrancishuster).
female(atheresascholze).
female(aalexandrawinisky).
female(akarinthaler).
female(arenateschroeter).
female(agesinecukrowski).
female(akonstanzebreitebner).
female(accilecassel).
movie(aunetunfontsix,aludwigbriand).
movie(aunetunfontsix,aguillaumeromain).
movie(aunetunfontsix,aloccorbery).
movie(aunetunfontsix,ajuliedray).
movie(aunetunfontsix,asamantharnier).
movie(aunetunfontsix,abrnicebejo).
movie(aunetunfontsix,abrigittefossey).
movie(aunetunfontsix,avincentlecoeur).
movie(aunetunfontsix,aestefanacastro).
movie(aunetunfontsix,aarmelledeutsch).
movie(aunetunfontsix,apierrearditi).
movie(aunetunfontsix,aalexandrawinisky).
movie(aunetunfontsix,adidierdijoux).
movie(aunetunfontsix,apatricejuiff).
movie(aunetunfontsix,aapprederisfranck).
movie(aunetunfontsix,avergnejeanpierre).
movie(aletztezeugeder,avolkerranisch).
movie(aletztezeugeder,ajuttawachowiak).
movie(aletztezeugeder,aclaudiamessner).
movie(aletztezeugeder,atheresascholze).
movie(aletztezeugeder,aandreasschwaiger).
movie(aletztezeugeder,arenateschroeter).
movie(aletztezeugeder,agesinecukrowski).
movie(aletztezeugeder,akonstanzebreitebner).
movie(aletztezeugeder,ajrggudzuhn).
movie(aletztezeugeder,adietermann).
movie(aletztezeugeder,aleonardlansink).
movie(aletztezeugeder,aulrichmhe).
movie(aletztezeugeder,astephanbernhard).
movie(aletztezeugeder,azensmichael).
movie(agrandpatronle,astevekalfa).
movie(agrandpatronle,agabriellelazure).
movie(agrandpatronle,afirminerichard).
movie(agrandpatronle,acoralyzahonero).
movie(agrandpatronle,afranoisechristophe).
movie(agrandpatronle,afrancishuster).
movie(agrandpatronle,accilecassel).
movie(agrandpatronle,aromeclaudemichel).
movie(agrandpatronle,akappesstphane).
movie(agrandpatronle,abonnetchristiani).
movie(agrandpatronle,asummereric).
movie(agrandpatronle,atonetticlaudio).
movie(agrandpatronle,agustemmanuelii).
movie(agrandpatronle,aladogedominique).
movie(arosenheimcopsdie,agerdlohmeyer).
movie(arosenheimcopsdie,ahorstkummeth).
movie(arosenheimcopsdie,ahubertmulzer).
movie(arosenheimcopsdie,atommikulla).
movie(arosenheimcopsdie,amaxmller).
movie(arosenheimcopsdie,amarenschumacher).
movie(arosenheimcopsdie,athomasstielner).
movie(arosenheimcopsdie,amarisaburger).
movie(arosenheimcopsdie,aandreasschwaiger).
movie(arosenheimcopsdie,akarinthaler).
movie(arosenheimcopsdie,amarkusbker).
movie(arosenheimcopsdie,akinkelmartin).
movie(arosenheimcopsdie,akronthalerthomas).
movie(arosenheimcopsdie,aengelhardtwilhelm).
movie(arosenheimcopsdie,agutjahrrainer).
movie(arosenheimcopsdie,akrgnther).
movie(arosenheimcopsdie,abraunbettinai).
movie(arosenheimcopsdie,aklischstefan).
genre(akinkelmartin,acomedy).
genre(akinkelmartin,acrime).
genre(akronthalerthomas,acomedy).
genre(akronthalerthomas,acrime).
genre(aengelhardtwilhelm,acomedy).
genre(aengelhardtwilhelm,acrime).
genre(astephanbernhard,acrime).
genre(aapprederisfranck,acomedy).
genre(agutjahrrainer,acomedy).
genre(agutjahrrainer,acrime).
genre(akrgnther,acomedy).
genre(akrgnther,acrime).
genre(azensmichael,acrime).
genre(avergnejeanpierre,acomedy).
genre(abraunbettinai,acomedy).
genre(abraunbettinai,acrime).
genre(aklischstefan,acomedy).
genre(aklischstefan,acrime).
actor(aangelinasarova).
actor(aivankondov).
actor(akirilyanev).
actor(astoychomazgalov).
actor(amikhailmikhajlov).
actor(aganchoganchev).
actor(amichalbajor).
actor(ahristodinev).
actor(avyarakovacheva).
actor(apetardespotov).
actor(asavahashamov).
actor(alyubomirbobchevski).
actor(amariashopova).
actor(akostatsonev).
actor(astefandanailov).
actor(aivanbratanov).
actor(aemiliaradeva).
actor(ageorgistamatov).
actor(astefanpejchev).
actor(adimitarhadzhiyanev).
actor(ajannowicki).
actor(aalicjajachiewicz).
actor(asonyadjulgerova).
actor(aviktordanchenko).
actor(akunkabaeva).
actor(aantonradichev).
actor(aemilmarkov).
actor(astefanpetrov).
actor(atzenokandov).
actor(alyubomirbachvarov).
actor(aivandimov).
actor(aivangrigorov).
actor(alyubomirkanev).
actor(atzvetolyubrakovski).
actor(asotirmaynolovski).
actor(aivantonev).
actor(abogomilsimeonov).
actor(anevenakokanova).
actor(anikoladadov).
actor(apetyasilyanova).
director(atraykovatanas).
director(apunchevborislav).
director(amarinovichanton).
director(asurchadzhievstefani).
female(aangelinasarova).
female(avyarakovacheva).
female(amariashopova).
female(aemiliaradeva).
female(aalicjajachiewicz).
female(asonyadjulgerova).
female(akunkabaeva).
female(anevenakokanova).
female(apetyasilyanova).
movie(ageratzite,aangelinasarova).
movie(ageratzite,aganchoganchev).
movie(ageratzite,avyarakovacheva).
movie(ageratzite,amariashopova).
movie(ageratzite,aivanbratanov).
movie(ageratzite,ageorgistamatov).
movie(ageratzite,astefanpejchev).
movie(ageratzite,akunkabaeva).
movie(ageratzite,astefanpetrov).
movie(ageratzite,aivandimov).
movie(ageratzite,atzvetolyubrakovski).
movie(ageratzite,aivantonev).
movie(ageratzite,anikoladadov).
movie(ageratzite,amarinovichanton).
movie(aspasenieto,astoychomazgalov).
movie(aspasenieto,amichalbajor).
movie(aspasenieto,akostatsonev).
movie(aspasenieto,ajannowicki).
movie(aspasenieto,aalicjajachiewicz).
movie(aspasenieto,aantonradichev).
movie(aspasenieto,aemilmarkov).
movie(aspasenieto,alyubomirbachvarov).
movie(aspasenieto,alyubomirkanev).
movie(aspasenieto,asotirmaynolovski).
movie(aspasenieto,abogomilsimeonov).
movie(aspasenieto,anevenakokanova).
movie(aspasenieto,apunchevborislav).
movie(akristali,astoychomazgalov).
movie(akristali,apetardespotov).
movie(akristali,akostatsonev).
movie(akristali,astefandanailov).
movie(akristali,aemiliaradeva).
movie(akristali,adimitarhadzhiyanev).
movie(akristali,asonyadjulgerova).
movie(akristali,aivangrigorov).
movie(akristali,anevenakokanova).
movie(akristali,apetyasilyanova).
movie(akristali,atraykovatanas).
genre(atraykovatanas,adrama).
genre(apunchevborislav,adrama).
genre(amarinovichanton,adrama).
genre(asurchadzhievstefani,adrama).
actor(aluciacammalleri).
actor(agiancarloscuderi).
actor(araffaeladavi).
actor(aenzodimartino).
actor(aconsuelolupo).
actor(afrancomirabella).
actor(abiagiobarone).
actor(acesareapolito).
actor(astefanodionisi).
actor(afabiolobello).
actor(amanueladolcemascolo).
actor(acorradofortuna).
actor(aannamariagherardi).
actor(amarcocavicchioli).
actor(adonatellafinocchiaro).
actor(adanielegalea).
actor(apaolaciampi).
actor(amanliosgalambro).
actor(agabrieleferzetti).
actor(aantoniettacarbonetti).
actor(aantoninobruschetta).
actor(amaurizionicolosi).
actor(amaurolenares).
actor(asalvatorelazzaro).
actor(apieradegliesposti).
actor(amarcoleonardi).
actor(aserenaautieri).
actor(aradarassimov).
actor(aileanarigano).
actor(avincenzocrivello).
actor(amarcospicuglia).
actor(asaromiano).
actor(atizianalodato).
actor(anicolegrimaudo).
actor(alaurabetti).
actor(aluciasardo).
actor(acarmelogalati).
actor(avannifois).
actor(aauroraquattrocchi).
actor(abarbaratabita).
actor(alucavitrano).
actor(apenlopecruz).
actor(aeddasabatini).
director(asciveresmarianna).
director(agrimaldiaurelioi).
director(abattiatofranco).
female(aluciacammalleri).
female(araffaeladavi).
female(afrancomirabella).
female(amanueladolcemascolo).
female(aannamariagherardi).
female(adonatellafinocchiaro).
female(apaolaciampi).
female(agabrieleferzetti).
female(aantoniettacarbonetti).
female(apieradegliesposti).
female(aserenaautieri).
female(aradarassimov).
female(aileanarigano).
female(atizianalodato).
female(anicolegrimaudo).
female(alaurabetti).
female(aluciasardo).
female(aauroraquattrocchi).
female(abarbaratabita).
female(apenlopecruz).
female(aeddasabatini).
movie(anerolio,agiancarloscuderi).
movie(anerolio,aenzodimartino).
movie(anerolio,afrancomirabella).
movie(anerolio,afabiolobello).
movie(anerolio,amarcocavicchioli).
movie(anerolio,aantoniettacarbonetti).
movie(anerolio,amaurizionicolosi).
movie(anerolio,amaurolenares).
movie(anerolio,asalvatorelazzaro).
movie(anerolio,apieradegliesposti).
movie(anerolio,avincenzocrivello).
movie(anerolio,amarcospicuglia).
movie(anerolio,asaromiano).
movie(anerolio,aluciasardo).
movie(anerolio,agrimaldiaurelioi).
movie(aribellela,araffaeladavi).
movie(aribellela,acesareapolito).
movie(aribellela,astefanodionisi).
movie(aribellela,adanielegalea).
movie(aribellela,apaolaciampi).
movie(aribellela,amarcoleonardi).
movie(aribellela,alaurabetti).
movie(aribellela,aauroraquattrocchi).
movie(aribellela,apenlopecruz).
movie(aribellela,aeddasabatini).
movie(aribellela,agrimaldiaurelioi).
movie(aperdutoamor,acorradofortuna).
movie(aperdutoamor,aannamariagherardi).
movie(aperdutoamor,adonatellafinocchiaro).
movie(aperdutoamor,amanliosgalambro).
movie(aperdutoamor,agabrieleferzetti).
movie(aperdutoamor,aantoninobruschetta).
movie(aperdutoamor,aradarassimov).
movie(aperdutoamor,atizianalodato).
movie(aperdutoamor,anicolegrimaudo).
movie(aperdutoamor,aluciasardo).
movie(aperdutoamor,alucavitrano).
movie(aperdutoamor,abattiatofranco).
movie(asaramay,aluciacammalleri).
movie(asaramay,aconsuelolupo).
movie(asaramay,abiagiobarone).
movie(asaramay,amanueladolcemascolo).
movie(asaramay,aserenaautieri).
movie(asaramay,aileanarigano).
movie(asaramay,aluciasardo).
movie(asaramay,acarmelogalati).
movie(asaramay,avannifois).
movie(asaramay,abarbaratabita).
movie(asaramay,asciveresmarianna).
genre(agrimaldiaurelioi,adrama).
genre(abattiatofranco,adrama).
