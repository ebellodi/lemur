%Predizione di workedUnder

:- modeh(1,workedunder(+p,+p)).

:- modeb(*,movie(-m,+p)).
:- modeb(*,movie(+m,-p)).
:- modeb(*,female(+p)).
:- modeb(*,genre(+p,-g)).
:- modeb(*,actor(+p)).
:- modeb(*,director(+p)).

:- determination(workedunder/2,movie/2).
:- determination(workedunder/2,female/1).
:- determination(workedunder/2,genre/2).
:- determination(workedunder/2,actor/1).
:- determination(workedunder/2,director/1).

%background di training

actor(amichaeldouglas).
actor(arobyndouglass).
actor(alewissmith).
actor(abillyconnolly).
actor(adanielvonbargen).
actor(adenisarndt).
actor(afloramontgomery).
actor(asheilapaterson).
actor(astancollymore).
actor(abenjaminmouton).
actor(ajeannetripplehorn).
actor(aneilmaskell).
actor(asharonstone).
actor(aellenthomas).
actor(adavidarnett).
actor(ajackmcgee).
actor(awayneknight).
actor(ajustinmonjo).
actor(ajaimzwoolvett).
actor(atimberrington).
actor(astephentobolowsky).
actor(agilbellows).
actor(adavidthewlis).
actor(aianholm).
actor(abenjohnson).
actor(adannflorek).
actor(aheathcotewilliams).
actor(acharlotterampling).
actor(achelcieross).
actor(astevekuhn).
actor(amarksangster).
actor(aindiravarma).
actor(ahughkeaysbyrne).
actor(aginachiarelli).
actor(abruceayoung).
actor(adavidmorrissey).
actor(amiguelferrer).
actor(adorothymalone).
actor(ageorgedzundza).
actor(ahughdancy).
actor(abillcable).
actor(amarccaleb).
actor(afrankcturner).
actor(aalancpeterson).
actor(anormanarmour).
actor(ajurneesmollett).
actor(ajanchappell).
actor(agusmercurio).
actor(acaitlinoheaney).
actor(aterenceharvey).
actor(adillonmoen).
actor(aleilanisarelle).
actor(alloydalan).
actor(aconnorwiddows).
actor(adebraengle).
director(ametcalfestephen).
director(averhoevenpauli).
director(amillergeorgei).
director(acatonjonesmichael).
female(arobyndouglass).
female(afloramontgomery).
female(asheilapaterson).
female(ajeannetripplehorn).
female(asharonstone).
female(aellenthomas).
female(atimberrington).
female(acharlotterampling).
female(achelcieross).
female(amarksangster).
female(aginachiarelli).
female(adorothymalone).
female(ajurneesmollett).
female(ajanchappell).
female(acaitlinoheaney).
female(aleilanisarelle).
female(adebraengle).
movie(abeautifuljoe,abillyconnolly).
movie(abeautifuljoe,asheilapaterson).
movie(abeautifuljoe,asharonstone).
movie(abeautifuljoe,ajaimzwoolvett).
movie(abeautifuljoe,agilbellows).
movie(abeautifuljoe,aianholm).
movie(abeautifuljoe,abenjohnson).
movie(abeautifuljoe,adannflorek).
movie(abeautifuljoe,aginachiarelli).
movie(abeautifuljoe,afrankcturner).
movie(abeautifuljoe,aalancpeterson).
movie(abeautifuljoe,anormanarmour).
movie(abeautifuljoe,ajurneesmollett).
movie(abeautifuljoe,adillonmoen).
movie(abeautifuljoe,aconnorwiddows).
movie(abeautifuljoe,ametcalfestephen).
movie(abasicinstinct2,afloramontgomery).
movie(abasicinstinct2,astancollymore).
movie(abasicinstinct2,aneilmaskell).
movie(abasicinstinct2,asharonstone).
movie(abasicinstinct2,aellenthomas).
movie(abasicinstinct2,atimberrington).
movie(abasicinstinct2,adavidthewlis).
movie(abasicinstinct2,aheathcotewilliams).
movie(abasicinstinct2,acharlotterampling).
movie(abasicinstinct2,amarksangster).
movie(abasicinstinct2,aindiravarma).
movie(abasicinstinct2,adavidmorrissey).
movie(abasicinstinct2,ahughdancy).
movie(abasicinstinct2,ajanchappell).
movie(abasicinstinct2,aterenceharvey).
movie(abasicinstinct2,acatonjonesmichael).
movie(abasicinstinct,amichaeldouglas).
movie(abasicinstinct,adanielvonbargen).
movie(abasicinstinct,adenisarndt).
movie(abasicinstinct,abenjaminmouton).
movie(abasicinstinct,ajeannetripplehorn).
movie(abasicinstinct,asharonstone).
movie(abasicinstinct,ajackmcgee).
movie(abasicinstinct,awayneknight).
movie(abasicinstinct,astephentobolowsky).
movie(abasicinstinct,achelcieross).
movie(abasicinstinct,abruceayoung).
movie(abasicinstinct,adorothymalone).
movie(abasicinstinct,ageorgedzundza).
movie(abasicinstinct,abillcable).
movie(abasicinstinct,aleilanisarelle).
movie(abasicinstinct,averhoevenpauli).
movie(abadlands2005,arobyndouglass).
movie(abadlands2005,alewissmith).
movie(abadlands2005,asharonstone).
movie(abadlands2005,adavidarnett).
movie(abadlands2005,ajustinmonjo).
movie(abadlands2005,astevekuhn).
movie(abadlands2005,ahughkeaysbyrne).
movie(abadlands2005,amiguelferrer).
movie(abadlands2005,amarccaleb).
movie(abadlands2005,agusmercurio).
movie(abadlands2005,acaitlinoheaney).
movie(abadlands2005,alloydalan).
movie(abadlands2005,adebraengle).
movie(abadlands2005,amillergeorgei).
genre(ametcalfestephen,acomedy).
genre(ametcalfestephen,adrama).
genre(ametcalfestephen,aromance).
genre(averhoevenpauli,athriller).
genre(averhoevenpauli,acrime).
genre(averhoevenpauli,adrama).
genre(averhoevenpauli,amystery).
genre(amillergeorgei,ascifi).
genre(acatonjonesmichael,athriller).
genre(acatonjonesmichael,acrime).
genre(acatonjonesmichael,adrama).
genre(acatonjonesmichael,amystery).
actor(avolkerranisch).
actor(aludwigbriand).
actor(agerdlohmeyer).
actor(astevekalfa).
actor(aguillaumeromain).
actor(agabriellelazure).
actor(ahorstkummeth).
actor(aloccorbery).
actor(ajuliedray).
actor(afirminerichard).
actor(ahubertmulzer).
actor(asamantharnier).
actor(atommikulla).
actor(acoralyzahonero).
actor(abrnicebejo).
actor(abrigittefossey).
actor(amaxmller).
actor(amarenschumacher).
actor(avincentlecoeur).
actor(aestefanacastro).
actor(ajuttawachowiak).
actor(aarmelledeutsch).
actor(athomasstielner).
actor(afranoisechristophe).
actor(apierrearditi).
actor(aclaudiamessner).
actor(amarisaburger).
actor(afrancishuster).
actor(atheresascholze).
actor(aalexandrawinisky).
actor(aandreasschwaiger).
actor(akarinthaler).
actor(arenateschroeter).
actor(adidierdijoux).
actor(agesinecukrowski).
actor(akonstanzebreitebner).
actor(amarkusbker).
actor(ajrggudzuhn).
actor(adietermann).
actor(apatricejuiff).
actor(accilecassel).
actor(aleonardlansink).
actor(aulrichmhe).
director(akinkelmartin).
director(akronthalerthomas).
director(aengelhardtwilhelm).
director(astephanbernhard).
director(aromeclaudemichel).
director(akappesstphane).
director(aapprederisfranck).
director(agutjahrrainer).
director(akrgnther).
director(abonnetchristiani).
director(asummereric).
director(azensmichael).
director(atonetticlaudio).
director(avergnejeanpierre).
director(abraunbettinai).
director(agustemmanuelii).
director(aklischstefan).
director(aladogedominique).
female(agabriellelazure).
female(ajuliedray).
female(afirminerichard).
female(asamantharnier).
female(acoralyzahonero).
female(abrnicebejo).
female(abrigittefossey).
female(amarenschumacher).
female(aestefanacastro).
female(ajuttawachowiak).
female(aarmelledeutsch).
female(afranoisechristophe).
female(aclaudiamessner).
female(amarisaburger).
female(afrancishuster).
female(atheresascholze).
female(aalexandrawinisky).
female(akarinthaler).
female(arenateschroeter).
female(agesinecukrowski).
female(akonstanzebreitebner).
female(accilecassel).
movie(aunetunfontsix,aludwigbriand).
movie(aunetunfontsix,aguillaumeromain).
movie(aunetunfontsix,aloccorbery).
movie(aunetunfontsix,ajuliedray).
movie(aunetunfontsix,asamantharnier).
movie(aunetunfontsix,abrnicebejo).
movie(aunetunfontsix,abrigittefossey).
movie(aunetunfontsix,avincentlecoeur).
movie(aunetunfontsix,aestefanacastro).
movie(aunetunfontsix,aarmelledeutsch).
movie(aunetunfontsix,apierrearditi).
movie(aunetunfontsix,aalexandrawinisky).
movie(aunetunfontsix,adidierdijoux).
movie(aunetunfontsix,apatricejuiff).
movie(aunetunfontsix,aapprederisfranck).
movie(aunetunfontsix,avergnejeanpierre).
movie(aletztezeugeder,avolkerranisch).
movie(aletztezeugeder,ajuttawachowiak).
movie(aletztezeugeder,aclaudiamessner).
movie(aletztezeugeder,atheresascholze).
movie(aletztezeugeder,aandreasschwaiger).
movie(aletztezeugeder,arenateschroeter).
movie(aletztezeugeder,agesinecukrowski).
movie(aletztezeugeder,akonstanzebreitebner).
movie(aletztezeugeder,ajrggudzuhn).
movie(aletztezeugeder,adietermann).
movie(aletztezeugeder,aleonardlansink).
movie(aletztezeugeder,aulrichmhe).
movie(aletztezeugeder,astephanbernhard).
movie(aletztezeugeder,azensmichael).
movie(agrandpatronle,astevekalfa).
movie(agrandpatronle,agabriellelazure).
movie(agrandpatronle,afirminerichard).
movie(agrandpatronle,acoralyzahonero).
movie(agrandpatronle,afranoisechristophe).
movie(agrandpatronle,afrancishuster).
movie(agrandpatronle,accilecassel).
movie(agrandpatronle,aromeclaudemichel).
movie(agrandpatronle,akappesstphane).
movie(agrandpatronle,abonnetchristiani).
movie(agrandpatronle,asummereric).
movie(agrandpatronle,atonetticlaudio).
movie(agrandpatronle,agustemmanuelii).
movie(agrandpatronle,aladogedominique).
movie(arosenheimcopsdie,agerdlohmeyer).
movie(arosenheimcopsdie,ahorstkummeth).
movie(arosenheimcopsdie,ahubertmulzer).
movie(arosenheimcopsdie,atommikulla).
movie(arosenheimcopsdie,amaxmller).
movie(arosenheimcopsdie,amarenschumacher).
movie(arosenheimcopsdie,athomasstielner).
movie(arosenheimcopsdie,amarisaburger).
movie(arosenheimcopsdie,aandreasschwaiger).
movie(arosenheimcopsdie,akarinthaler).
movie(arosenheimcopsdie,amarkusbker).
movie(arosenheimcopsdie,akinkelmartin).
movie(arosenheimcopsdie,akronthalerthomas).
movie(arosenheimcopsdie,aengelhardtwilhelm).
movie(arosenheimcopsdie,agutjahrrainer).
movie(arosenheimcopsdie,akrgnther).
movie(arosenheimcopsdie,abraunbettinai).
movie(arosenheimcopsdie,aklischstefan).
genre(akinkelmartin,acomedy).
genre(akinkelmartin,acrime).
genre(akronthalerthomas,acomedy).
genre(akronthalerthomas,acrime).
genre(aengelhardtwilhelm,acomedy).
genre(aengelhardtwilhelm,acrime).
genre(astephanbernhard,acrime).
genre(aapprederisfranck,acomedy).
genre(agutjahrrainer,acomedy).
genre(agutjahrrainer,acrime).
genre(akrgnther,acomedy).
genre(akrgnther,acrime).
genre(azensmichael,acrime).
genre(avergnejeanpierre,acomedy).
genre(abraunbettinai,acomedy).
genre(abraunbettinai,acrime).
genre(aklischstefan,acomedy).
genre(aklischstefan,acrime).
actor(aangelinasarova).
actor(aivankondov).
actor(akirilyanev).
actor(astoychomazgalov).
actor(amikhailmikhajlov).
actor(aganchoganchev).
actor(amichalbajor).
actor(ahristodinev).
actor(avyarakovacheva).
actor(apetardespotov).
actor(asavahashamov).
actor(alyubomirbobchevski).
actor(amariashopova).
actor(akostatsonev).
actor(astefandanailov).
actor(aivanbratanov).
actor(aemiliaradeva).
actor(ageorgistamatov).
actor(astefanpejchev).
actor(adimitarhadzhiyanev).
actor(ajannowicki).
actor(aalicjajachiewicz).
actor(asonyadjulgerova).
actor(aviktordanchenko).
actor(akunkabaeva).
actor(aantonradichev).
actor(aemilmarkov).
actor(astefanpetrov).
actor(atzenokandov).
actor(alyubomirbachvarov).
actor(aivandimov).
actor(aivangrigorov).
actor(alyubomirkanev).
actor(atzvetolyubrakovski).
actor(asotirmaynolovski).
actor(aivantonev).
actor(abogomilsimeonov).
actor(anevenakokanova).
actor(anikoladadov).
actor(apetyasilyanova).
director(atraykovatanas).
director(apunchevborislav).
director(amarinovichanton).
director(asurchadzhievstefani).
female(aangelinasarova).
female(avyarakovacheva).
female(amariashopova).
female(aemiliaradeva).
female(aalicjajachiewicz).
female(asonyadjulgerova).
female(akunkabaeva).
female(anevenakokanova).
female(apetyasilyanova).
movie(ageratzite,aangelinasarova).
movie(ageratzite,aganchoganchev).
movie(ageratzite,avyarakovacheva).
movie(ageratzite,amariashopova).
movie(ageratzite,aivanbratanov).
movie(ageratzite,ageorgistamatov).
movie(ageratzite,astefanpejchev).
movie(ageratzite,akunkabaeva).
movie(ageratzite,astefanpetrov).
movie(ageratzite,aivandimov).
movie(ageratzite,atzvetolyubrakovski).
movie(ageratzite,aivantonev).
movie(ageratzite,anikoladadov).
movie(ageratzite,amarinovichanton).
movie(aspasenieto,astoychomazgalov).
movie(aspasenieto,amichalbajor).
movie(aspasenieto,akostatsonev).
movie(aspasenieto,ajannowicki).
movie(aspasenieto,aalicjajachiewicz).
movie(aspasenieto,aantonradichev).
movie(aspasenieto,aemilmarkov).
movie(aspasenieto,alyubomirbachvarov).
movie(aspasenieto,alyubomirkanev).
movie(aspasenieto,asotirmaynolovski).
movie(aspasenieto,abogomilsimeonov).
movie(aspasenieto,anevenakokanova).
movie(aspasenieto,apunchevborislav).
movie(akristali,astoychomazgalov).
movie(akristali,apetardespotov).
movie(akristali,akostatsonev).
movie(akristali,astefandanailov).
movie(akristali,aemiliaradeva).
movie(akristali,adimitarhadzhiyanev).
movie(akristali,asonyadjulgerova).
movie(akristali,aivangrigorov).
movie(akristali,anevenakokanova).
movie(akristali,apetyasilyanova).
movie(akristali,atraykovatanas).
genre(atraykovatanas,adrama).
genre(apunchevborislav,adrama).
genre(amarinovichanton,adrama).
genre(asurchadzhievstefani,adrama).
actor(aluciacammalleri).
actor(agiancarloscuderi).
actor(araffaeladavi).
actor(aenzodimartino).
actor(aconsuelolupo).
actor(afrancomirabella).
actor(abiagiobarone).
actor(acesareapolito).
actor(astefanodionisi).
actor(afabiolobello).
actor(amanueladolcemascolo).
actor(acorradofortuna).
actor(aannamariagherardi).
actor(amarcocavicchioli).
actor(adonatellafinocchiaro).
actor(adanielegalea).
actor(apaolaciampi).
actor(amanliosgalambro).
actor(agabrieleferzetti).
actor(aantoniettacarbonetti).
actor(aantoninobruschetta).
actor(amaurizionicolosi).
actor(amaurolenares).
actor(asalvatorelazzaro).
actor(apieradegliesposti).
actor(amarcoleonardi).
actor(aserenaautieri).
actor(aradarassimov).
actor(aileanarigano).
actor(avincenzocrivello).
actor(amarcospicuglia).
actor(asaromiano).
actor(atizianalodato).
actor(anicolegrimaudo).
actor(alaurabetti).
actor(aluciasardo).
actor(acarmelogalati).
actor(avannifois).
actor(aauroraquattrocchi).
actor(abarbaratabita).
actor(alucavitrano).
actor(apenlopecruz).
actor(aeddasabatini).
director(asciveresmarianna).
director(agrimaldiaurelioi).
director(abattiatofranco).
female(aluciacammalleri).
female(araffaeladavi).
female(afrancomirabella).
female(amanueladolcemascolo).
female(aannamariagherardi).
female(adonatellafinocchiaro).
female(apaolaciampi).
female(agabrieleferzetti).
female(aantoniettacarbonetti).
female(apieradegliesposti).
female(aserenaautieri).
female(aradarassimov).
female(aileanarigano).
female(atizianalodato).
female(anicolegrimaudo).
female(alaurabetti).
female(aluciasardo).
female(aauroraquattrocchi).
female(abarbaratabita).
female(apenlopecruz).
female(aeddasabatini).
movie(anerolio,agiancarloscuderi).
movie(anerolio,aenzodimartino).
movie(anerolio,afrancomirabella).
movie(anerolio,afabiolobello).
movie(anerolio,amarcocavicchioli).
movie(anerolio,aantoniettacarbonetti).
movie(anerolio,amaurizionicolosi).
movie(anerolio,amaurolenares).
movie(anerolio,asalvatorelazzaro).
movie(anerolio,apieradegliesposti).
movie(anerolio,avincenzocrivello).
movie(anerolio,amarcospicuglia).
movie(anerolio,asaromiano).
movie(anerolio,aluciasardo).
movie(anerolio,agrimaldiaurelioi).
movie(aribellela,araffaeladavi).
movie(aribellela,acesareapolito).
movie(aribellela,astefanodionisi).
movie(aribellela,adanielegalea).
movie(aribellela,apaolaciampi).
movie(aribellela,amarcoleonardi).
movie(aribellela,alaurabetti).
movie(aribellela,aauroraquattrocchi).
movie(aribellela,apenlopecruz).
movie(aribellela,aeddasabatini).
movie(aribellela,agrimaldiaurelioi).
movie(aperdutoamor,acorradofortuna).
movie(aperdutoamor,aannamariagherardi).
movie(aperdutoamor,adonatellafinocchiaro).
movie(aperdutoamor,amanliosgalambro).
movie(aperdutoamor,agabrieleferzetti).
movie(aperdutoamor,aantoninobruschetta).
movie(aperdutoamor,aradarassimov).
movie(aperdutoamor,atizianalodato).
movie(aperdutoamor,anicolegrimaudo).
movie(aperdutoamor,aluciasardo).
movie(aperdutoamor,alucavitrano).
movie(aperdutoamor,abattiatofranco).
movie(asaramay,aluciacammalleri).
movie(asaramay,aconsuelolupo).
movie(asaramay,abiagiobarone).
movie(asaramay,amanueladolcemascolo).
movie(asaramay,aserenaautieri).
movie(asaramay,aileanarigano).
movie(asaramay,aluciasardo).
movie(asaramay,acarmelogalati).
movie(asaramay,avannifois).
movie(asaramay,abarbaratabita).
movie(asaramay,asciveresmarianna).
genre(agrimaldiaurelioi,adrama).
genre(abattiatofranco,adrama).
