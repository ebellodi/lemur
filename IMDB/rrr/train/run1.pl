:-[aleph].
:-read_all(imdb1f).   %refers to imdb1f.b,.f,.n

:- set(search, rls).
:- set(rls_type, rrr).
:- set(tries, 10).

:- set(evalfn, accuracy). 	%V is one of: coverage, compression, posonly, pbayes, accuracy, laplace, auto_m, mestimate, entropy, gini, sd, wracc, or user (default coverage). Sets the evaluation function for a search. 
:- set(minacc, 0.7).
:- set(clauselength, 5). 	%Sets upper bound on number of literals in an acceptable clause.
:- set(minpos, 2).		%V is a positive integer (default 1). Set a lower bound on the number of positive examples to be covered by an acceptable clause. If the best clause covers positive examples below this number, then it is not added to the current theory. This can be used to prevent Aleph from adding ground unit clauses to the theory (by setting the value to 2).

:- induce_cover.
:- write_rules('imdb1.rules').

