# README #

* This repository makes available data and scripts for running experiments with the following statistical relational learning (SRL) algorithms: LEMUR, SLIPCASE (Bellodi and Riguzzi, 2012), SLIPCOVER (Bellodi and Riguzzi, 2014), BUSL (Mihalkova and Mooney (2007), LSM (Kok and Domingos, 2010), Aleph++ExactL1 (Huynh and Mooney, 2008), RDN-B (Natarajan et al, 2012), MLN-B (clause and tree versions) (Khot et al., 2011), RRR (Zelezny et al., 2002) and SLS (Paes et al., 2008).
* More information on LEMUR can be found at [https://sites.google.com/a/unife.it/ml/lemur](https://sites.google.com/a/unife.it/ml/lemur).
* The Yap implementation of LEMUR can be found at https://bitbucket.org/ebellodi/yap-lemur.

DATA

* For each of the seven data sets, a sub-directory for each algorithm is present.
* For each algorithm, data are divided into training and test sets. All data sets have been tested with 5-fold cross-validation except for Carcinogenesis (no cross-validation) and Mutagenesis (10 folds).
* To run the algorithms, follow the instructions in run.sh or run.pl (Prolog file) files.

REFERENCES

* Bellodi E, Riguzzi F (2012) Learning the structure of probabilistic logic programs. In: Muggleton S, Tamaddoni-Nezhad A, Lisi F (eds) Inductive Logic Programming, Springer Berlin Heidelberg, LNCS, vol 7207, pp 61-75.
* Bellodi E, Riguzzi F (2014) Structure learning of probabilistic logic programs by searching the clause space. Theory and Practice of Logic Programming FirstView Articles, doi:10.1017/S1471068413000689.
* Mihalkova L, Mooney RJ (2007) Bottom-up learning of markov logic network structure. In: Proceedings of the 24th International Conference on Machine Learning, ACM,pp 625-632.
* Kok S, Domingos P (2010) Learning Markov Logic Networks using structural motifs. In: Furnkranz J, Joachims T (eds) Proceedings of the 27th International Conference on Machine Learning, Omnipress, pp 551-558.
* Huynh TN, Mooney RJ (2008) Discriminative structure and parameter learning for markov logic networks. In: Cohen WW, McCallum A, Roweis ST (eds) Proceedings of the 25th international conference on Machine learning, ACM, pp 416-423.
* Natarajan S, Khot T, Kersting K, Gutmann B, Shavlik J (2012) Gradient-based boosting for statistical relational learning: The relational dependency network case. Machine Learning 86(1):25-56.
* Khot T, Natarajan S, Kersting K, Shavlik JW (2011) Learning Markov Logic Networks via functional gradient boosting. In: Cook DJ, Pei J, 0010 WW, Zaane OR, Wu X (eds) Proceedings of the 11th IEEE International Conference on Data Mining, IEEE, pp 320-329.
* Zelezny F, Srinivasan A, (2002) Lattice-search runtime distributions may be heavy-tailed. In: Proceedings of the 12th International Conference on Inductive Logic Programming, Springer.
* Paes A, Zaverucha G, Costa VS (2008) Revising 
first-order logic theories from examples through stochastic local search. In: Proceedings of the 17th International Conference on Inductive Logic Programming, Springer-Verlag, Berlin, Heidelberg, ILP'07, pp 200-210.