/* test according to the method of Davis and Goadrich*/

:-source.
:-consult(slipcover).

t:-
	open('cll.pl',append,S), %AI fold 
	open('res_atoms.csv',append,S1),  %replace with the other correspodent folds
	set(single_var,false),			
	reconsult('s1.l'),
	ta(s1, s1, 's1.kb', S, 1, S1),
	ta(s2, s2, 's2.kb', S, 2, S1),
	ta(s3, s3, 's3.kb', S, 3, S1),
	ta(s4, s4, 's2.kb', S, 4, S1),
	ta(s5, s5, 's5.kb', S, 5, S1),
	ta(s6, s6, 's6.kb', S, 6, S1),
	ta(s7, s7, 's7.kb', S, 7, S1),
	ta(s8, s8, 's8.kb', S, 8, S1),
	ta(s9, s9, 's9.kb', S, 9, S1),
	ta(s10, s10, 's10.kb', S, 10, S1),
	close(S),
	close(S1).

% test a single fold, Fold = 1..10    
% cll goes to cll[Fold].pl
t(Fold):-
    name(Fold, Fstr),
    append("cll", Fstr, FPl0),
    append(FPl0, ".pl", FPlstr),
    name(FPl, FPlstr),
    open(FPl,write,S), %AI fold 
    open('res_atoms.csv',write,S1),  %replace with the other correspodent folds
    set(single_var,false),			
/*	ta(ai,ai_train,'ai.kb',S,1,S1),
	ta(graphics,graphics_train,'graphics.kb',S,2,S1),
	ta(language,language_train,'language.kb',S,3,S1),
	ta(systems,systems_train,'systems.kb',S,4,S1),
	ta(theory,theory_train,'theory.kb',S,5,S1),
    */
    append("s", Fstr, Fnstr),
    name(Fn, Fnstr),
    append(Fnstr, ".kb", Kbfstr),
    name(Kbf, Kbfstr),
    ta(Fn, Fn, Kbf, S, Fold, S1),
   % ta(s1, s1, 's1.kb', S, 1, S1),
   % ta(s2, s2, 's2.kb', S, 2, S1),
    %ta(s3, s3, 's3.kb', S, 3, S1),
    %ta(s4, s4, 's4.kb', S, 4, S1),
   % ta(s5, s5, 's5.kb', S, 5, S1),
   % ta(s6, s6, 's6.kb', S, 6, S1),
   % ta(s7, s7, 's7.kb', S, 7, S1),
    %ta(s8, s8, 's8.kb', S, 8, S1),
   % ta(s9, s9, 's9.kb', S, 9, S1),
%    ta(s10, s10, 's10.kb', S, 10, S1),
     close(S),
     close(S1).

ta(Model,File,FileKB,S,N,S1):-
	generate_file_names(File,_FileKB,FileOut,FileL,FileLPAD,FileBG),
	format("~a~n",[File]),
%	load_models(FileKB,_),
        load_models(FileKB,DB),%assert all facts in kb
       
	(file_exists(FileBG)->
		set(compiling,on),
		load(FileBG,_ThBG,RBG),
		set(compiling,off),
		generate_clauses(RBG,_RBG1,0,[],ThBG), 
		assert_all(ThBG)
	;
		true
	),

	find_ga(Model,LG,Pos,Neg),

/*        set(compiling,on),
	load(FileLPAD,Th,R),		%file CPL
	set(compiling,off),
        assert_all(Th),  
	assert_all(R),
	compute_CLL_atoms(LG,0,0,CLL0,LG0),
        retract_all(Th),
	retract_all(R),
	retract(rule_n(_)),
        assert(rule_n(0)),
	keysort(LG0,LGOrd0),
	format(S,"cll(~a,pre,~d,~d,[",[File,Pos,Neg]),
	writes(LGOrd0,S),
	%abolish_rules,
*/  
	set(compiling,on),
	load(FileOut,Th1,R1),	%FILE RULES
	%format("loaded rules~n"),
	set(compiling,off),
%	generate_clauses(R1,R2,0,[],Th),  FALLISCE
%        %assert_all(R2),
%	assert_all(Th),
        assert_all(Th1),
        assert_all(R1),
	compute_CLL_atoms(LG,0,0,CLL1,LG1),
        retract_all(Th1),
        retract_all(R1),
	keysort(LG1,LGOrd1),
	format(S,"cll(~a,post,~d,~d,[\n",[File,Pos,Neg]),
	writes(LGOrd1,S),
	format(S1,"~a;~f~n",[File,CLL1]).
    
/*
ta(Model,File,FileKB,S,N,S1):-
	generate_file_names(File,_FileKB,FileOut,FileL,FileLPAD),
	format("~a~n",[File]),
	reconsult(FileL),
	load_models(FileKB,_),
	find_ga(Model,LG,Pos,Neg),
        set(compiling,on),
	load(FileLPAD,Th,R),		%file CPL
	set(compiling,off),
        assert_all(Th),  
	assert_all(R),
	compute_CLL_atoms(LG,0,0,CLL0,LG0),
        retract_all(Th),
	retract_all(R),
	keysort(LG0,LGOrd0),
	format(S,"cll(~a,pre,~d,~d,[",[File,Pos,Neg]),
	writes(LGOrd0,S),
	abolish_rules,
	set(compiling,on),
	load(FileOut,Th1,R1),	%FILE RULES
	set(compiling,off),
	assert_all(Th1),  
        assert_all(R1),
	compute_CLL_atoms(LG,0,0,CLL1,LG1),
        retract_all(Th1),
	retract_all(R1),
	keysort(LG1,LGOrd1),
	format(S,"cll(~a,post,~d,~d,[",[File,Pos,Neg]),
	writes(LGOrd1,S),
	format(S1,"~a;~f~f~n",[File,CLL0,CLL1]).
    */


writes([H-H1],S):-
	format(S,"~f - (~p)]).~n~n",[H,H1]).

writes([H-H1|T],S):-
	format(S,"~f - (~p),~n",[H,H1]),
	writes(T,S).


load_exaset(File, L) :-
    open(File, read, S),
    read_all_exas(S, [], L),
    close(S).

read_all_exas(S, L0, L) :-
    read(S, At),
    (At \= end_of_file
    -> append(L0, [At], L1), read_all_exas(S, L1, L) 
    ; L = L0
    ).

find_ga(M,LG,Pos,Neg):-                   
    name(M, Fold),
    append(Fold, ".f", PFileStr),
    append(Fold, ".n", NFileStr),
    name(PFile, PFileStr),
    name(NFile, NFileStr),
    load_exaset(PFile, LGP0),
    load_exaset(NFile, LGN0),
    findall(\+ At1,( member(At, LGN0), At =.. [P|Args], At1 =..[P,M|Args]), LGN),
    findall(At1,( member(At, LGP0),At =.. [P|Args],At1=..[P,M|Args]), LGP),
    %    writeln(LGP),
    % writeln(LGN),
    length(LGP,Pos),
    length(LGN,Neg),
%	sample(50,LGN0,LGN),
%	length(LGP,Pos),
%	length(LGN,Neg),
	append(LGP,LGN,LG).


               

write_list([],_).

write_list([H|T],S):-
	format(S,"~p~n",[H]),
	write_list(T,S).

generate_file_names(File,FileKB,FileOut,FileL, FileLPAD,FileBG):-
    name(File,FileString),
    append(FileString,"f",FileString1),
    name(FileExt,FileString1),
    generate_file_name(File,".kb",FileKB),
    generate_file_name(FileExt,".cpl",FileLPAD),
    generate_file_name(FileExt,".rules",FileOut),
    generate_file_name(FileExt,".bg",FileBG),
    generate_file_name(FileExt,".l",FileL).


compute_CLL_atoms([],_N,CLL,CLL,[]):-!.


compute_CLL_atoms([\+ H|T],N,CLL0,CLL1,[PG- (\+ H)|T1]):-!,
	rule_n(NR),
        init_test(NR),
	write((\+ H)),nl,
	get_node(H,BDD),!,
	ret_prob(BDD,PG),
	end_test,!,
		%
		PG1 is 1-PG,
			
		(PG1=:=0.0->
			CLL2 is CLL0-10
%			format("-inf~n",[])
		;
			CLL2 is CLL0+ log(PG1)
%			format("~f~n",[PG])
		),		
		N1 is N+1,
	compute_CLL_atoms(T,N1,CLL2,CLL1,T1).	

compute_CLL_atoms([H|T],N,CLL0,CLL1,[PG-H|T1]):-
       	rule_n(NR),
 	init_test(NR),
	write(H),nl,
	get_node(H,BDD),!,
	ret_prob(BDD,PG),
	end_test,!,
		(PG=:=0.0->
			CLL2 is CLL0-10
%			format("-inf~n",[])
		;	
			CLL2 is CLL0+ log(PG)
%			format("~f~n",[PG])
		),
		N1 is N+1,
	compute_CLL_atoms(T,N1,CLL2,CLL1,T1).		

