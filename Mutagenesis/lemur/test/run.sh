#!/bin/bash

for i in 1 2 3 4 5 6 7 8 9 10
do
		yap -l tst.pl -g "t($i),halt."
		java -jar auc.jar cll$i.pl list > $i.auclist.out 
		grep Area $i.auclist.out >> Areas
done