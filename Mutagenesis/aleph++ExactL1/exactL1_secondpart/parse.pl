:-source.


c:-
	c('s1.result'),
	c('s2.result'),
	c('s3.result'),
	c('s4.result'),
	c('s5.result'),
	c('s6.result'),
	c('s7.result'),
	c('s8.result'),
	c('s9.result'),
	c('s10.result').

c(File):-
	open(File,read,S),
	atom_concat(File,'.out',FileOut),
	open(FileOut,write,SO),
	read_sum(S,SO),
	close(S),
	close(SO).
	
read_sum(S,SO):-
	get_code(S,A),
	A\= -1,!,
	([A]=")"->
		put_char(SO,')'),
		put_char(SO,'.')
	;
		(A=10->
			put_code(SO,46),
			put_code(SO,10)
		;
			((A>=65,A=<90)->
				A1 is A+32,
				put_code(SO,A1)
			;
				put_code(SO,A)
			)
		)
	),	
	read_sum(S,SO).
	
read_sum(_S,_SO).	
	
