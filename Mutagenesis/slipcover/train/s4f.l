
output(active/1).

input(lumo/2).
input(logp/2).
input(bond/4).
input(atm/5).
input(benzene/2).
input(carbon_5_aromatic_ring/2).
input(carbon_6_ring/2).
input(hetero_aromatic_6_ring/2).
input(hetero_aromatic_5_ring/2).
input(ring_size_6/2).
input(ring_size_5/2).
input(nitro/2).
input(methyl/2).
input(anthracene/2).
input(phenanthrene/2).
input(ball3/2).


modeh(1,active(+drug)).

modeb(1,lumo(+drug,-energy)).
modeb(1,logp(+drug,-hydrophob)).
modeb(*,atm(+drug,-atomid,-#element,-#int,-charge)).
modeb(*,bond(+drug,-atomid,-atomid,-#int)).
modeb(1,(+charge) >= (#charge)).
modeb(1,(+charge) =< (#charge)).
modeb(1,(+charge)= #charge).
modeb(1,(+hydrophob) >= (#hydrophob)).
modeb(1,(+hydrophob) =< (#hydrophob)).
modeb(1,(+hydrophob)= #hydrophob).
modeb(1,(+energy) >= (#energy)).
modeb(1,(+energy) =< (#energy)).
modeb(1,(+energy)= #energy).

modeb(*,benzene(+drug,-ring)).
modeb(*,carbon_5_aromatic_ring(+drug,-ring)).
modeb(*,carbon_6_ring(+drug,-ring)).
modeb(*,hetero_aromatic_6_ring(+drug,-ring)).
modeb(*,hetero_aromatic_5_ring(+drug,-ring)).
modeb(*,ring_size_6(+drug,-ring)).
modeb(*,ring_size_5(+drug,-ring)).
modeb(*,nitro(+drug,-ring)).
modeb(*,methyl(+drug,-ring)).
modeb(*,anthracene(+drug,-ringlist)).
modeb(*,phenanthrene(+drug,-ringlist)).
modeb(*,ball3(+drug,-ringlist)).

modeb(*,member(-ring,+ringlist)).
modeb(1,member(+ring,+ringlist)).


lookahead(logp(_A,_B),[(_B=_C)]).
lookahead(logp(_A,_B),[>=(_B,_C)]).
lookahead(logp(_A,_B),[=<(_B,_C)]).
lookahead(lumo(_A,_B),[(_B=_C)]).
lookahead(lumo(_A,_B),[>=(_B,_C)]).
lookahead(lumo(_A,_B),[=<(_B,_C)]).
%lookahead(atm(_,_,_,_,C),[>=(C,_)]).
%lookahead(atm(_,_,_,_,C),[=<(C,_)]).
%lookahead(atm(_,_,_,_,C),[(C=_)]).


