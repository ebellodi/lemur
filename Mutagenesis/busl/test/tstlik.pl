
:-source.

c:-
	open('cll.pl',write,S),     
	open('res_atoms.csv',write,S1),
	reconsult('s1.f'), 
	c('s1.result',S,S1),
	reconsult('s2.f'),  %file s2.pl (file di test) con esempi di query(vedi infer) solo positivi
	c('s2.result',S,S1),
	reconsult('s3.f'),
	c('s3.result',S,S1),
	reconsult('s4.f'),
	c('s4.result',S,S1),
	reconsult('s5.f'),
	c('s5.result',S,S1),
	reconsult('s6.f'),
	c('s6.result',S,S1),
	reconsult('s7.f'),
	c('s7.result',S,S1),
	reconsult('s8.f'),
	c('s8.result',S,S1),
	reconsult('s9.f'),
	c('s9.result',S,S1),
	reconsult('s10.f'),
	c('s10.result',S,S1).

	
writes([H-H1],S):-
	format(S,"~f - (~p)]).~n~n",[H,H1]).

writes([H-H1|T],S):-
	format(S,"~f - (~p),~n",[H,H1]),
	writes(T,S).

c(File,S,S1):-
	atom_concat(File,'.out',File1),
	open(File1,read,SI),
	read_sum(SI,0,N,0,Pos,0,Neg,0,Sum,[],L),
	keysort(L,L1),
	format(S,"cll('~a',post,~d,~d,[",[File,Pos,Neg]),
	writes(L1,S),
	format(S1,"~f~n",[Sum]),
	close(SI).

read_sum(S,N,N2,Pos0,Pos1,Neg0,Neg1,S0,S1,L0,[P-A1|L1]):-
	read(S,A),
	A\=end_of_file,!,
	read(S,P),
	(call(A)->
		A1=A,
		Pos2 is Pos0+1,
		Neg2 is Neg0
	;
		A1=(\+ A),
		Pos2 is Pos0,
		Neg2 is Neg0+1
	),
	LP is log(P),
	S2 is S0 + LP,
	N1 is N+1,	
	read_sum(S,N1,N2,Pos2,Pos1,Neg2,Neg1,S2,S1,L0,L1).
	
read_sum(_S,N,N,Pos,Pos,Neg,Neg,S,S,L,L).	
	
read_db(S,SO):-
	read_line(S,L,End),
	(L=[97,100|_]->
		true
	;
		format(SO,"~s",[L])
	),
	(End=f->
		read_db(S,SO)
	;
		true
	).
	
read_db(_S,_SO).		

read_line(S,[A1|T],End):-
	get_code(S,A),
	(A= -1->
		A1=10,
		T=[],
		End=t
	;
		(A=10->
			A1=10,
			T=[],
			End=f
		;
			A1=A,
			read_line(S,T,End)
		)
	).
	
read_line(_S,_SO).		
