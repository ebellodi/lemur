// Add this to indicate that first letter caps is constant and first letter lower-case is a variable.
// Use quotes(") to make sure words beginning with lower case are considered as constants.
// To totally reverse the syntax,(e.g. prolog format), set
// usePrologVariables:true.
useStdLogicVariables: true.


// Set the mode for ILP/Tree search.
// + indicates that this variable must already be grounded/appear before in the clause/tree.
// - indicates that a new variable could be used. 


mode: active(+Drug).

mode: lumo(+Drug,-Energy).
mode: logp(+Drug,-Hydrophob).
mode: atm(+Drug,-Atomid,#Element,#Atmtype,-Charge).
mode: bond(+Drug,-Atomid,-Atomid,#Bondtype).
mode: greaterCharge(+Charge,+Charge).
mode: lowerCharge(+Charge,+Charge).
mode: equalCharge(+Charge,+Charge).
mode: greaterHydro(+Hydrophob,+Hydrophob).
mode: lowerHydro(+Hydrophob,+Hydrophob).
mode: equalHydro(+Hydrophob,+Hydrophob).
mode: greaterEnergy(+Energy,+Energy).
mode: lowerEnergy(+Energy,+Energy).
mode: equalEnergy(+Energy,+Energy).

mode: benzene(+Drug,-Ring).
mode: carbon_5_aromatic_ring(+Drug,-Ring).
mode: carbon_6_ring(+Drug,-Ring).
mode: hetero_aromatic_6_ring(+Drug,-Ring).
mode: hetero_aromatic_5_ring(+Drug,-Ring).
mode: ring_size_6(+Drug,-Ring).
mode: ring_size_5(+Drug,-Ring).
mode: nitro(+Drug,-Ring).
mode: methyl(+Drug,-Ring).
mode: anthracene(+Drug,-Ringlist).
mode: phenanthrene(+Drug,-Ringlist).
mode: ball3(+Drug,-Ringlist).
mode: member(-Ring,+Ringlist).
mode: member(+Ring,+Ringlist).


greaterCharge(c1, c2) :- c1 >= c2.
lowerCharge(c1, c2) :-  c1 =< c2.
equalCharge(c1, c2) :-  c1 =:= c2.
greaterHydro(c1, c2) :- c1 >= c2.
lowerHydro(c1, c2) :-  c1 =< c2.
equalHydro(c1, c2) :-  c1 =:= c2.
greaterEnergy(c1, c2) :-  c1 >= c2.
lowerEnergy(c1, c2) :-  c1 =< c2.
equalEnergy(c1, c2) :-  c1 =:= c2.



// Specific to RDNBoost tree learning.
// Look at the advanced documentation on webpage for more details
setParam: treeDepth=4.
setParam: nodeSize=2.
setParam: numOfClauses=8.

