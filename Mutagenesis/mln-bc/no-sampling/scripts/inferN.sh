#change N with {1..10}

java -cp ../Boostr/WILL.jar edu.wisc.cs.will.Boosting.Common.RunBoostedModels \
-aucJarPath "../Boostr" \
-target active \
-trees 20 \
-model ../data/trainN/models \
-testNegPosRatio -1 \
-save -i -test ../data/testN/ \
1>inferN.txt
