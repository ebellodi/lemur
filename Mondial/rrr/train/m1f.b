:- modeh(1,christian_religion(+state)).


:- modeb(2,population(+state,-infmort)).
:- modeb(1,politics(+state,-type)).
:- modeb(1,=(+type,#type)).
:- modeb(1,economy(+state,-gdp)).
%:- modeb(*,(+gdp)>=(#gdp)).
%:- modeb(*,(+gdp)=<(#gdp)).
:- modeb(1,language(+state,-language)).
:- modeb(1,language(-state,+language)).
:- modeb(1,ethnicGroup(+state,-group,-perg)).
%:- modeb(2,(+group)=(#group)).
:- modeb(2,=(+group,#group)).
:- modeb(1,encompasses(+state,-cont)).
:- modeb(1,encompasses(-state,+cont)).
:- modeb(1,continent(+cont,-area)).
%:- modeb(1,(+cont)=(#cont)).
:- modeb(1,=(+cont,#cont)).
:- modeb(1,borders(+state,-state)).
:- modeb(1,borders(-state,+state)).
:- modeb(2,organization(+org,-state)).
:- modeb(2,organization(-org,+state)).
:- modeb(1,isMember(+state,-org,-mtype)).
%:- modeb(1,(+mtype)=(#mtype)).
:- modeb(1,=(+mtype,#mtype)).


:- determination(christian_religion/1, '='/2).
%:- determination(christian_religion/1, '>='/2).  %INSTANTIATION ERROR- =:=/2: expected bound value 
%:- determination(christian_religion/1, '=<'/2).   %INSTANTIATION ERROR- =:=/2: expected bound value
:- determination(christian_religion/1,politics/2).
:- determination(christian_religion/1,population/2).
:- determination(christian_religion/1,economy/2).
:- determination(christian_religion/1,language/2).
:- determination(christian_religion/1,ethnicGroup/3).
:- determination(christian_religion/1,borders/2).
:- determination(christian_religion/1,continent/2).
:- determination(christian_religion/1,encompasses/2).
:- determination(christian_religion/1,organization/2).
:- determination(christian_religion/1,isMember/3).


country('AL').
country('GR').
country('MK').
country('SRB').
country('MNE').
country('KOS').
country('AND').
country('F').
country('E').
country('A').
country('CZ').
country('D').
country('H').
country('I').
country('FL').
country('SK').
country('SLO').
country('CH').
country('BY').
country('LV').
country('LT').
country('PL').
country('UA').
country('R').
country('B').
country('L').
country('NL').
country('BIH').
country('HR').
country('BG').
country('RO').
country('TR').
country('DK').
country('EW').
country('FARX').
country('SF').
country('N').
country('S').
country('MC').
country('GBZ').
country('GBG').
country('V').
country('CEU').
country('MEL').
country('IS').
country('IRL').
country('RSM').
country('GBJ').
country('M').
country('GBM').
country('MD').
country('P').
country('SVAX').
country('GB').
country('AFG').
country('CN').
country('IR').
country('PK').
country('TAD').
country('TM').
country('UZB').
country('ARM').
country('GE').
country('AZ').
country('BRN').
country('BD').
country('MYA').
country('IND').
country('BHT').
country('BRU').
country('MAL').
country('LAO').
country('THA').
country('K').
country('VN').
country('KAZ').
country('NOK').
country('KGZ').
country('HONX').
country('MACX').
country('MNG').
country('NEP').
country('XMAS').
country('COCO').
country('CY').
country('GAZA').
country('IL').
country('ET').
country('RI').
country('TL').
country('PNG').
country('IRQ').
country('JOR').
country('KWT').
country('SA').
country('SYR').
country('RL').
country('WEST').
country('J').
country('ROK').
country('MV').
country('OM').
country('UAE').
country('YE').
country('RP').
country('Q').
country('SGP').
country('CL').
country('RC').
country('AXA').
country('AG').
country('ARU').
country('BS').
country('BDS').
country('BZ').
country('GCA').
country('MEX').
country('BERM').
country('BVIR').
country('CDN').
country('USA').
country('CAYM').
country('CR').
country('NIC').
country('PA').
country('C').
country('WD').
country('DOM').
country('RH').
country('ES').
country('HCA').
country('GROX').
country('WG').
country('GUAD').
country('JA').
country('MART').
country('MNTS').
country('NA').
country('SMAR').
country('CO').
country('PR').
country('KN').
country('WL').
country('SPMI').
country('WV').
country('TT').
country('TUCA').
country('VIRG').
country('AMSA').
country('AUS').
country('COOK').
country('FJI').
country('FPOL').
country('GUAM').
country('KIR').
country('MH').
country('FSM').
country('NAU').
country('NCA').
country('NZ').
country('NIUE').
country('NORF').
country('NMIS').
country('PAL').
country('PITC').
country('SLB').
country('TO').
country('TUV').
country('VU').
country('WAFU').
country('WS').
country('RA').
country('BOL').
country('BR').
country('RCH').
country('PY').
country('ROU').
country('PE').
country('FGU').
country('GUY').
country('SME').
country('YV').
country('EC').
country('FALK').
country('DZ').
country('LAR').
country('RMM').
country('RIM').
country('MA').
country('RN').
country('TN').
country('WSA').
country('ANG').
country('RCB').
country('NAM').
country('ZRE').
country('Z').
country('BEN').
country('BF').
country('WAN').
country('RT').
country('RB').
country('RSA').
country('ZW').
country('CI').
country('GH').
country('BI').
country('RWA').
country('EAT').
country('CAM').
country('RCA').
country('TCH').
country('GQ').
country('G').
country('CV').
country('SUD').
country('SSD').
country('COM').
country('RG').
country('LB').
country('DJI').
country('ER').
country('ETH').
country('SP').
country('EAK').
country('WAG').
country('SN').
country('GNB').
country('WAL').
country('EAU').
country('LS').
country('RM').
country('MW').
country('MOC').
country('MS').
country('MAYO').
country('SD').
country('REUN').
country('HELX').
country('STP').
country('SY').
population('AL',49200).
population('GR',7400).
population('MK',29700).
population('SRB',6750).
population('MNE',-1).
population('KOS',-1).
population('AND',2200).
population('F',5300).
population('E',6300).
population('A',6200).
population('CZ',8400).
population('D',6000).
population('H',12300).
population('I',6900).
population('FL',5300).
population('SK',10700).
population('SLO',7300).
population('CH',5400).
population('BY',13400).
population('LV',21200).
population('LT',17000).
population('PL',12400).
population('UA',22500).
population('R',24700).
population('B',6400).
population('L',4700).
population('NL',4900).
population('BIH',43200).
population('HR',10200).
population('BG',15700).
population('RO',23200).
population('TR',43200).
population('DK',4800).
population('EW',17400).
population('FARX',7200).
population('SF',4900).
population('N',4900).
population('S',4500).
population('MC',6900).
population('GBZ',6900).
population('GBG',9200).
population('V',-1).
population('CEU',-1).
population('MEL',-1).
population('IS',4300).
population('IRL',6400).
population('RSM',5500).
population('GBJ',2700).
population('M',6900).
population('GBM',2400).
population('MD',47600).
population('P',7600).
population('SVAX',-1).
population('GB',6400).
population('AFG',149700).
population('CN',39600).
population('IR',52700).
population('PK',96800).
population('TAD',113100).
population('TM',81600).
population('UZB',79600).
population('ARM',38900).
population('GE',22500).
population('AZ',74500).
population('BRN',17100).
population('BD',102300).
population('MYA',80700).
population('IND',71100).
population('BHT',116300).
population('BRU',24200).
population('MAL',24000).
population('LAO',96800).
population('THA',33400).
population('K',107800).
population('VN',38400).
population('KAZ',63200).
population('NOK',25900).
population('KGZ',77800).
population('HONX',2900).
population('MACX',5300).
population('MNG',69700).
population('NEP',79000).
population('XMAS',-1).
population('COCO',-1).
population('CY',8400).
population('GAZA',30600).
population('IL',8500).
population('ET',72800).
population('RI',63100).
population('TL',40650).
population('PNG',60100).
population('IRQ',60000).
population('JOR',31500).
population('KWT',11100).
population('SA',46400).
population('SYR',40000).
population('RL',36700).
population('WEST',28600).
population('J',4400).
population('ROK',8200).
population('MV',47000).
population('OM',27300).
population('UAE',20400).
population('YE',71500).
population('RP',35900).
population('Q',19600).
population('SGP',4700).
population('CL',20800).
population('RC',7000).
population('AXA',3520).
population('AG',17200).
population('ARU',13790).
population('BS',23300).
population('BDS',18700).
population('BZ',33900).
population('GCA',50700).
population('MEX',25000).
population('BERM',2460).
population('BVIR',14650).
population('CDN',6100).
population('USA',6700).
population('CAYM',6940).
population('CR',13500).
population('NIC',45800).
population('PA',29700).
population('C',9000).
population('WD',9600).
population('DOM',47700).
population('RH',103800).
population('ES',31900).
population('HCA',41800).
population('GROX',23800).
population('WG',11900).
population('GUAD',8300).
population('JA',15600).
population('MART',7100).
population('MNTS',16079).
population('NA',9090).
population('SMAR',-1).
population('CO',25800).
population('PR',8420).
population('KN',18900).
population('WL',20000).
population('SPMI',9950).
population('WV',16800).
population('TT',18200).
population('TUCA',13890).
population('VIRG',12540).
population('AMSA',10180).
population('AUS',5500).
population('COOK',16900).
population('FJI',17400).
population('FPOL',7550).
population('GUAM',6050).
population('KIR',52900).
population('MH',46900).
population('FSM',35800).
population('NAU',40600).
population('NCA',7050).
population('NZ',6700).
population('NIUE',-1).
population('NORF',-1).
population('NMIS',6700).
population('PAL',25070).
population('PITC',-1).
population('SLB',25800).
population('TO',40260).
population('TUV',27600).
population('VU',64599).
population('WAFU',5020).
population('WS',34300).
population('RA',28300).
population('BOL',67500).
population('BR',55300).
population('RCH',13600).
population('PY',23200).
population('ROU',15400).
population('PE',52200).
population('FGU',14600).
population('GUY',51400).
population('SME',29300).
population('YV',29500).
population('EC',34800).
population('FALK',-1).
population('DZ',48700).
population('LAR',59500).
population('RMM',102700).
population('RIM',81700).
population('MA',43200).
population('RN',117600).
population('TN',35100).
population('WSA',145820).
population('ANG',138900).
population('RCB',108100).
population('NAM',47200).
population('ZRE',108000).
population('Z',96100).
population('BEN',105100).
population('BF',117800).
population('WAN',72400).
population('RT',84300).
population('RB',54200).
population('RSA',48800).
population('ZW',72800).
population('CI',82400).
population('GH',80300).
population('BI',102200).
population('RWA',118800).
population('EAT',105900).
population('CAM',78700).
population('RCA',111700).
population('TCH',120400).
population('GQ',98000).
population('G',90100).
population('CV',54300).
population('SUD',68000).
population('SSD',102000).
population('COM',75300).
population('RG',134100).
population('LB',108100).
population('DJI',106700).
population('ER',118900).
population('ETH',122800).
population('SP',121100).
population('EAK',55300).
population('WAG',80500).
population('SN',64000).
population('GNB',115800).
population('WAL',135600).
population('EAU',99400).
population('LS',81600).
population('RM',93500).
population('MW',139900).
population('MOC',125600).
population('MS',17200).
population('MAYO',56290).
population('SD',88400).
population('REUN',7500).
population('HELX',17630).
population('STP',61100).
population('SY',12500).
politics('AL','emerging democracy').
politics('GR','parliamentary republic').
politics('MK','emerging democracy').
politics('SRB','parliamentary democracy').
politics('MNE','parliamentary democracy').
politics('KOS',republic).
politics('AND','parliamentary democracy that retains as its heads of state a coprincipality').
politics('F',republic).
politics('E','parliamentary monarchy').
politics('A','federal republic').
politics('CZ','parliamentary democracy').
politics('D','federal republic').
politics('H',republic).
politics('I',republic).
politics('FL','hereditary constitutional monarchy').
politics('SK','parliamentary democracy').
politics('SLO','emerging democracy').
politics('CH','federal republic').
politics('BY',republic).
politics('LV',republic).
politics('LT',republic).
politics('PL','democratic state').
politics('UA',republic).
politics('R',federation).
politics('B','constitutional monarchy').
politics('L','constitutional monarchy').
politics('NL','constitutional monarchy').
politics('BIH','emerging democracy').
politics('HR','parliamentary democracy').
politics('BG','emerging democracy').
politics('RO',republic).
politics('TR','republican parliamentary democracy').
politics('DK','constitutional monarchy').
politics('EW',republic).
politics('FARX','part of the Danish realm').
politics('SF',republic).
politics('N','constitutional monarchy').
politics('S','constitutional monarchy').
politics('MC','constitutional monarchy').
politics('GBZ','dependent territory of the UK').
politics('GBG','British crown dependency').
politics('V','monarchical sacerdotal state').
politics('CEU','dependent territory of Spain').
politics('MEL','dependent territory of Spain').
politics('IS',republic).
politics('IRL',republic).
politics('RSM',republic).
politics('GBJ','British crown dependency').
politics('M','parliamentary democracy').
politics('GBM','British crown dependency').
politics('MD',republic).
politics('P',republic).
politics('SVAX','territory of Norway administered by the Ministry of Industry').
politics('GB','constitutional monarchy').
politics('AFG','transitional government').
politics('CN','Communist state').
politics('IR','theocratic republic').
politics('PK',republic).
politics('TAD',republic).
politics('TM',republic).
politics('UZB',republic).
politics('ARM',republic).
politics('GE',republic).
politics('AZ',republic).
politics('BRN','traditional monarchy').
politics('BD',republic).
politics('MYA','military regime').
politics('IND','federal republic').
politics('BHT',monarchy).
politics('BRU','constitutional sultanate').
politics('MAL','constitutional monarchy').
politics('LAO','Communist state').
politics('THA','constitutional monarchy').
politics('K','multiparty liberal democracy under a constitutional monarchy established in September 1993').
politics('VN','Communist state').
politics('KAZ',republic).
politics('NOK','Communist state').
politics('KGZ',republic).
politics('HONX','special administrative area in China').
politics('MACX','special administrative area in China').
politics('MNG',republic).
politics('NEP','parliamentary democracy as of 12 May 1991').
politics('XMAS','territory of Australia').
politics('COCO','territory of Australia').
politics('CY',republic).
politics('GAZA',pnull).
politics('IL',republic).
politics('ET',republic).
politics('RI',republic).
politics('TL','parliamentary democracy').
politics('PNG','parliamentary democracy').
politics('IRQ',republic).
politics('JOR','constitutional monarchy').
politics('KWT','nominal constitutional monarchy').
politics('SA',monarchy).
politics('SYR','republic under military regime since March 1963').
politics('RL',republic).
politics('WEST',pnull).
politics('J','constitutional monarchy').
politics('ROK',republic).
politics('MV',republic).
politics('OM',monarchy).
politics('UAE','federation with specified powers delegated to the UAE central government and other powers reserved to member emirates').
politics('YE',republic).
politics('RP',republic).
politics('Q','traditional monarchy').
politics('SGP','republic within Commonwealth').
politics('CL',republic).
politics('RC','multiparty democratic regime').
politics('AXA','British Overseas Territories').
politics('AG','parliamentary democracy').
politics('ARU','part of the Dutch realm').
politics('BS',commonwealth).
politics('BDS','parliamentary democracy').
politics('BZ','parliamentary democracy').
politics('GCA',republic).
politics('MEX','federal republic operating under a centralized government').
politics('BERM','British Overseas Territories').
politics('BVIR','British Overseas Territories').
politics('CDN','confederation with parliamentary democracy').
politics('USA','federal republic').
politics('CAYM','British Overseas Territories').
politics('CR','democratic republic').
politics('NIC',republic).
politics('PA','constitutional republic').
politics('C','Communist state').
politics('WD','parliamentary democracy').
politics('DOM',republic).
politics('RH',republic).
politics('ES',republic).
politics('HCA',republic).
politics('GROX','part of the Danish realm').
politics('WG','parliamentary democracy').
politics('GUAD','overseas department of France').
politics('JA','parliamentary democracy').
politics('MART','overseas department of France').
politics('MNTS','British Overseas Territories').
politics('NA','part of the Dutch realm').
politics('SMAR','overseas collectivity of France').
politics('CO',republic).
politics('PR','commonwealth associated with the US').
politics('KN','constitutional monarchy').
politics('WL','parliamentary democracy').
politics('SPMI','territorial collectivity of France').
politics('WV','constitutional monarchy').
politics('TT','parliamentary democracy').
politics('TUCA','British Overseas Territories').
politics('VIRG',organized).
politics('AMSA','unincorporated and unorganized territory of the US').
politics('AUS','federal parliamentary state').
politics('COOK','self governing parliamentary government in free association with New Zealand').
politics('FJI',republic).
politics('FPOL','overseas territory of France since 1946').
politics('GUAM',organized).
politics('KIR',republic).
politics('MH','constitutional government in free association with the US').
politics('FSM','constitutional government in free association with the US').
politics('NAU',republic).
politics('NCA','overseas territory of France since 1956').
politics('NZ','parliamentary democracy').
politics('NIUE','self governing territory in free association with New Zealand').
politics('NORF','territory of Australia').
politics('NMIS','commonwealth in political union with the US').
politics('PAL','constitutional government in free association with the US').
politics('PITC','British Overseas Territories').
politics('SLB','parliamentary democracy').
politics('TO','hereditary constitutional monarchy').
politics('TUV',democracy).
politics('VU',republic).
politics('WAFU','overseas territory of France').
politics('WS','constitutional monarchy under native chief').
politics('RA',republic).
politics('BOL',republic).
politics('BR','federal republic').
politics('RCH',republic).
politics('PY',republic).
politics('ROU',republic).
politics('PE',republic).
politics('FGU','overseas department of France').
politics('GUY',republic).
politics('SME',republic).
politics('YV',republic).
politics('EC',republic).
politics('FALK','British Overseas Territories').
politics('DZ',republic).
politics('LAR','Jamahiriya in theory').
politics('RMM',republic).
politics('RIM',republic).
politics('MA','constitutional monarchy').
politics('RN',republic).
politics('TN',republic).
politics('WSA','legal status of territory and question of sovereignty unresolved').
politics('ANG','transitional government nominally a multiparty democracy with a strong presidential system').
politics('RCB',republic).
politics('NAM',republic).
politics('ZRE','republic with a strong presidential system').
politics('Z',republic).
politics('BEN','republic under multiparty democratic rule').
politics('BF',parliamentary).
politics('WAN','military government').
politics('RT','republic under transition to multiparty democratic rule').
politics('RB','parliamentary republic').
politics('RSA',republic).
politics('ZW','parliamentary democracy').
politics('CI',republic).
politics('GH','constitutional democracy').
politics('BI',republic).
politics('RWA',republic).
politics('EAT',republic).
politics('CAM','unitary republic').
politics('RCA',republic).
politics('TCH',republic).
politics('GQ','republic in transition to multiparty democracy').
politics('G',republic).
politics('CV',republic).
politics('SUD','transitional previously ruling military junta').
politics('SSD',republic).
politics('COM','independent republic').
politics('RG',republic).
politics('LB',republic).
politics('DJI',republic).
politics('ER','transitional government').
politics('ETH','federal republic').
politics('SP',none).
politics('EAK',republic).
politics('WAG','republic under multiparty democratic rule').
politics('SN','republic under multiparty democratic rule').
politics('GNB',republic).
politics('WAL','constitutional democracy').
politics('EAU',republic).
politics('LS','modified constitutional monarchy').
politics('RM',republic).
politics('MW','multiparty democracy').
politics('MOC',republic).
politics('MS','parliamentary democracy').
politics('MAYO','territorial collectivity of France').
politics('SD',monarchy).
politics('REUN','overseas department of France').
politics('HELX','British Overseas Territories').
politics('STP',republic).
politics('SY',republic).
economy('AL',41000000).
economy('GR',1017000000).
economy('MK',19000000).
economy('SRB',521800000).
economy('MNE',45150000).
economy('KOS',32370000).
economy('AND',10000000).
economy('F',11730000000).
economy('E',5650000000).
economy('A',1520000000).
economy('CZ',1062000000).
economy('D',14522000000).
economy('H',725000000).
economy('I',10886000000).
economy('FL',6300000).
economy('SK',390000000).
economy('SLO',226000000).
economy('CH',1585000000).
economy('BY',492000000).
economy('LV',147000000).
economy('LT',133000000).
economy('PL',2267000000).
economy('UA',1746000000).
economy('R',7960000000).
economy('B',1970000000).
economy('L',100000000).
economy('NL',3019000000).
economy('BIH',10000000).
economy('HR',201000000).
economy('BG',432000000).
economy('RO',1057000000).
economy('TR',3457000000).
economy('DK',1128000000).
economy('EW',123000000).
economy('FARX',7330000).
economy('SF',924000000).
economy('N',1062000000).
economy('S',1773000000).
economy('MC',7880000).
economy('GBZ',2050000).
economy('GBG',enull).
economy('V',enull).
economy('CEU',enull).
economy('MEL',enull).
economy('IS',50000000).
economy('IRL',546000000).
economy('RSM',3800000).
economy('GBJ',enull).
economy('M',44000000).
economy('GBM',7800000).
economy('MD',104000000).
economy('P',1162000000).
economy('SVAX',enull).
economy('GB',11384000000).
economy('AFG',128000000).
economy('CN',35000000000).
economy('IR',3235000000).
economy('PK',2742000000).
economy('TAD',64000000).
economy('TM',115000000).
economy('UZB',547000000).
economy('ARM',91000000).
economy('GE',62000000).
economy('AZ',115000000).
economy('BRN',73000000).
economy('BD',1445000000).
economy('MYA',470000000).
economy('IND',14087000000).
economy('BHT',13000000).
economy('BRU',46000000).
economy('MAL',1936000000).
economy('LAO',52000000).
economy('THA',4167000000).
economy('K',70000000).
economy('VN',970000000).
economy('KAZ',469000000).
economy('NOK',215000000).
economy('KGZ',54000000).
economy('HONX',3076000000).
economy('MACX',2204000000).
economy('MNG',49000000).
economy('NEP',252000000).
economy('XMAS',enull).
economy('COCO',enull).
economy('CY',78000000).
economy('GAZA',17000000).
economy('IL',801000000).
economy('ET',1710000000).
economy('RI',7109000000).
economy('TL',4890000).
economy('PNG',102000000).
economy('IRQ',411000000).
economy('JOR',193000000).
economy('KWT',308000000).
economy('SA',1893000000).
economy('SYR',912000000).
economy('RL',183000000).
economy('WEST',37000000).
economy('J',26792000000).
economy('ROK',5907000000).
economy('MV',3900000).
economy('OM',191000000).
economy('UAE',701000000).
economy('YE',371000000).
economy('RP',1797000000).
economy('Q',107000000).
economy('SGP',661000000).
economy('CL',656000000).
economy('RC',2905000000).
economy('AXA',530000).
economy('AG',4250000).
economy('ARU',22580000).
economy('BS',48000000).
economy('BDS',25000000).
economy('BZ',5750000).
economy('GCA',367000000).
economy('MEX',7214000000).
economy('BERM',45000000).
economy('BVIR',1330000).
economy('CDN',6940000000).
economy('USA',72477000000).
economy('CAYM',19390000).
economy('CR',184000000).
economy('NIC',71000000).
economy('PA',136000000).
economy('C',147000000).
economy('WD',2000000).
economy('DOM',268000000).
economy('RH',65000000).
economy('ES',114000000).
economy('HCA',108000000).
economy('GROX',8920000).
economy('WG',2840000).
economy('GUAD',37000000).
economy('JA',82000000).
economy('MART',39500000).
economy('MNTS',290000).
economy('NA',28000000).
economy('SMAR',enull).
economy('CO',1925000000).
economy('PR',880000000).
economy('KN',2200000).
economy('WL',6400000).
economy('SPMI',680000).
economy('WV',2400000).
economy('TT',162000000).
economy('TUCA',2160000).
economy('VIRG',12000000).
economy('AMSA',4622000).
economy('AUS',4054000000).
economy('COOK',1832000).
economy('FJI',47000000).
economy('FPOL',61000000).
economy('GUAM',25000000).
economy('KIR',680000).
economy('MH',940000).
economy('FSM',2050000).
economy('NAU',1000000).
economy('NCA',31580000).
economy('NZ',623000000).
economy('NIUE',100100).
economy('NORF',enull).
economy('NMIS',6334000).
economy('PAL',818000).
economy('PITC',enull).
economy('SLB',10000000).
economy('TO',2280000).
economy('TUV',78000).
economy('VU',2100000).
economy('WAFU',600000).
economy('WS',4150000).
economy('RA',2785000000).
economy('BOL',200000000).
economy('BR',9768000000).
economy('RCH',1132000000).
economy('PY',170000000).
economy('ROU',244000000).
economy('PE',870000000).
economy('FGU',8000000).
economy('GUY',16000000).
economy('SME',13000000).
economy('YV',1955000000).
economy('EC',446000000).
economy('FALK',1051000).
economy('DZ',1087000000).
economy('LAR',329000000).
economy('RMM',54000000).
economy('RIM',28000000).
economy('MA',874000000).
economy('RN',55000000).
economy('TN',371000000).
economy('WSA',enull).
economy('ANG',74000000).
economy('RCB',77000000).
economy('NAM',58000000).
economy('ZRE',165000000).
economy('Z',89000000).
economy('BEN',76000000).
economy('BF',74000000).
economy('WAN',1359000000).
economy('RT',41000000).
economy('RB',45000000).
economy('RSA',2150000000).
economy('ZW',181000000).
economy('CI',219000000).
economy('GH',251000000).
economy('BI',40000000).
economy('RWA',38000000).
economy('EAT',231000000).
economy('CAM',165000000).
economy('RCA',25000000).
economy('TCH',33000000).
economy('GQ',3250000).
economy('G',60000000).
economy('CV',4400000).
economy('SUD',684000000).
economy('SSD',enull).
economy('COM',3700000).
economy('RG',65000000).
economy('LB',23000000).
economy('DJI',5000000).
economy('ER',20000000).
economy('ETH',242000000).
economy('SP',36000000).
economy('EAK',368000000).
economy('WAG',11000000).
economy('SN',145000000).
economy('GNB',10000000).
economy('WAL',44000000).
economy('EAU',168000000).
economy('LS',28000000).
economy('RM',114000000).
economy('MW',69000000).
economy('MOC',122000000).
economy('MS',109000000).
economy('MAYO',9536000).
economy('SD',36000000).
economy('REUN',29000000).
economy('HELX',180000).
economy('STP',1380000).
economy('SY',4300000).
ethnicGroup('AL','Greeks',30).
ethnicGroup('AL','Albanian',950).
ethnicGroup('GR','Greek',980).
ethnicGroup('MK','Albanian',220).
ethnicGroup('MK','Serb',20).
ethnicGroup('MK','Macedonian',650).
ethnicGroup('MK','Turkish',40).
ethnicGroup('MK','Gypsy',30).
ethnicGroup('SRB','Serb',829).
ethnicGroup('SRB','Montenegrin',9).
ethnicGroup('SRB','Hungarian',39).
ethnicGroup('SRB','Roma',14).
ethnicGroup('SRB','Bosniak',18).
ethnicGroup('SRB','Croat',11).
ethnicGroup('MNE','Montenegrin',430).
ethnicGroup('MNE','Serb',320).
ethnicGroup('MNE','Bosniak',80).
ethnicGroup('MNE','Albanian',50).
ethnicGroup('KOS','Albanian',880).
ethnicGroup('KOS','Serb',70).
ethnicGroup('AND','French',60).
ethnicGroup('AND','Spanish',610).
ethnicGroup('AND','Andorran',300).
ethnicGroup('E','Mediterranean Nordic',1000).
ethnicGroup('A','German',994).
ethnicGroup('A','Slovene',2).
ethnicGroup('A','Croat',3).
ethnicGroup('CZ','German',5).
ethnicGroup('CZ','Polish',6).
ethnicGroup('CZ','Roma',3).
ethnicGroup('CZ','Hungarian',2).
ethnicGroup('CZ','Czech',944).
ethnicGroup('CZ','Slovak',30).
ethnicGroup('D','German',915).
ethnicGroup('D','Turkish',24).
ethnicGroup('H','Serb',20).
ethnicGroup('H','German',26).
ethnicGroup('H','Roma',40).
ethnicGroup('H','Hungarian',899).
ethnicGroup('H','Slovak',8).
ethnicGroup('H','Romanian',7).
ethnicGroup('FL','Italian',50).
ethnicGroup('FL','Alemannic',950).
ethnicGroup('SK','German',1).
ethnicGroup('SK','Polish',1).
ethnicGroup('SK','Roma',15).
ethnicGroup('SK','Hungarian',107).
ethnicGroup('SK','Czech',10).
ethnicGroup('SK','Slovak',857).
ethnicGroup('SK','Ukrainian',3).
ethnicGroup('SK','Ruthenian',3).
ethnicGroup('SLO','Serb',20).
ethnicGroup('SLO','Slovene',910).
ethnicGroup('SLO','Muslim',10).
ethnicGroup('SLO','Croat',30).
ethnicGroup('BY','Belorussian',812).
ethnicGroup('BY','Russian',114).
ethnicGroup('BY','Polish',39).
ethnicGroup('BY','Ukrainian',24).
ethnicGroup('LV','Latvian',593).
ethnicGroup('LV','Russian',278).
ethnicGroup('LV','Belorussian',36).
ethnicGroup('LV','Ukrainian',25).
ethnicGroup('LV','Polish',24).
ethnicGroup('LV','Lithuanian',13).
ethnicGroup('LT','Lithuanian',840).
ethnicGroup('LT','Polish',61).
ethnicGroup('LT','Russian',49).
ethnicGroup('LT','Belorussian',11).
ethnicGroup('PL','German',13).
ethnicGroup('PL','Polish',976).
ethnicGroup('PL','Ukrainian',6).
ethnicGroup('PL','Byelorussian',5).
ethnicGroup('UA','Ukrainian',778).
ethnicGroup('UA','Russian',173).
ethnicGroup('UA','Belorussian',6).
ethnicGroup('UA','Moldovan',5).
ethnicGroup('UA','Crimean Tatar',5).
ethnicGroup('UA','Bulgarian',4).
ethnicGroup('UA','Hungarian',3).
ethnicGroup('UA','Romanian',3).
ethnicGroup('UA','Polish',3).
ethnicGroup('UA','Jewish',2).
ethnicGroup('R','Russian',798).
ethnicGroup('R','Tatar',38).
ethnicGroup('R','Ukrainian',20).
ethnicGroup('R','Bashkir',12).
ethnicGroup('R','Chuvash',11).
ethnicGroup('B','Fleming',550).
ethnicGroup('B','Walloon',330).
ethnicGroup('NL','Dutch',960).
ethnicGroup('BIH','Serb',400).
ethnicGroup('BIH','Muslim',380).
ethnicGroup('BIH','Croat',220).
ethnicGroup('HR','Serb',120).
ethnicGroup('HR','Hungarian',5).
ethnicGroup('HR','Muslim',9).
ethnicGroup('HR','Croat',780).
ethnicGroup('HR','Slovene',5).
ethnicGroup('BG','Macedonian',25).
ethnicGroup('BG','Roma',26).
ethnicGroup('BG','Russian',2).
ethnicGroup('BG','Turk',85).
ethnicGroup('BG','Bulgarian',853).
ethnicGroup('BG','Armenian',3).
ethnicGroup('RO','German',4).
ethnicGroup('RO','Roma',16).
ethnicGroup('RO','Hungarian',89).
ethnicGroup('RO','Romanian',891).
ethnicGroup('TR','Turkish',800).
ethnicGroup('TR','Kurdish',200).
ethnicGroup('EW','Estonian',687).
ethnicGroup('EW','Russian',256).
ethnicGroup('EW','Ukrainian',21).
ethnicGroup('EW','Belorussian',12).
ethnicGroup('EW','Finn',8).
ethnicGroup('FARX','Scandinavian',1000).
ethnicGroup('S','foreign-born first-generation',120).
ethnicGroup('MC','French',470).
ethnicGroup('MC','Italian',160).
ethnicGroup('MC','Monegasque',160).
ethnicGroup('GBG','Norman-French',1000).
ethnicGroup('IS','Celt',1000).
ethnicGroup('GBJ','Norman-French',1000).
ethnicGroup('MD','Moldavian/Romanian',782).
ethnicGroup('MD','Ukrainian',84).
ethnicGroup('MD','Russian',58).
ethnicGroup('MD','Gagauz',344).
ethnicGroup('MD','Bulgarian',19).
ethnicGroup('SVAX','Norwegian',554).
ethnicGroup('SVAX','Russian Ukrainian',443).
ethnicGroup('GB','English',770).
ethnicGroup('GB','Scottish',79).
ethnicGroup('GB','Welsh',44).
ethnicGroup('GB','Northern Irish',26).
ethnicGroup('GB','Indian',18).
ethnicGroup('GB','Pakistani',13).
ethnicGroup('AFG','Tajik',250).
ethnicGroup('AFG','Pashtun',380).
ethnicGroup('AFG','Uzbek',60).
ethnicGroup('AFG','Hazara',190).
ethnicGroup('CN','Han Chinese',915).
ethnicGroup('IR','Arab',30).
ethnicGroup('IR','Persian',510).
ethnicGroup('IR','Baloch',20).
ethnicGroup('IR','Azerbaijani',240).
ethnicGroup('IR','Kurd',70).
ethnicGroup('IR','Lur',20).
ethnicGroup('IR','Turkmen',20).
ethnicGroup('IR','Gilaki Mazandarani',80).
ethnicGroup('TAD','Tajik',799).
ethnicGroup('TAD','Uzbek',153).
ethnicGroup('TAD','Russian',11).
ethnicGroup('TAD','Kyrgyz',11).
ethnicGroup('TM','Turkmen',850).
ethnicGroup('TM','Uzbek',50).
ethnicGroup('TM','Russian',40).
ethnicGroup('UZB','Uzbek',800).
ethnicGroup('UZB','Russian',55).
ethnicGroup('UZB','Tajik',50).
ethnicGroup('UZB','Kazak',30).
ethnicGroup('UZB','Karakalpak',25).
ethnicGroup('UZB','Tatar',15).
ethnicGroup('ARM','Armenian',977).
ethnicGroup('ARM','Russian',5).
ethnicGroup('ARM','Yezidi',13).
ethnicGroup('GE','Georgian',838).
ethnicGroup('GE','Azeri',65).
ethnicGroup('GE','Russian',15).
ethnicGroup('AZ','Azeri',906).
ethnicGroup('AZ','Dagestani',22).
ethnicGroup('AZ','Russian',18).
ethnicGroup('AZ','Armenian',15).
ethnicGroup('BRN','Arab',100).
ethnicGroup('BRN','Asian',130).
ethnicGroup('BRN','Bahraini',630).
ethnicGroup('BRN','Iranian',80).
ethnicGroup('BD','Bengali',980).
ethnicGroup('MYA','Indian',20).
ethnicGroup('MYA','Chinese',30).
ethnicGroup('MYA','Burman',680).
ethnicGroup('MYA','Shan',90).
ethnicGroup('MYA','Karen',70).
ethnicGroup('MYA','Rakhine',40).
ethnicGroup('MYA','Mon',20).
ethnicGroup('IND','Dravidian',250).
ethnicGroup('IND','Indo-Aryan',720).
ethnicGroup('IND','Mongol',30).
ethnicGroup('BHT','Bhote',500).
ethnicGroup('BHT','Nepalese',350).
ethnicGroup('BRU','Chinese',200).
ethnicGroup('BRU','Malay',640).
ethnicGroup('MAL','Malay',504).
ethnicGroup('MAL','Chinese',237).
ethnicGroup('MAL','Indian',71).
ethnicGroup('MAL','Borneo indigenous',110).
ethnicGroup('LAO','Lao Loum',680).
ethnicGroup('LAO','Lao Theung',220).
ethnicGroup('LAO','Lao Soung Hmong Yao',90).
ethnicGroup('LAO','Vietnamese/Chinese',10).
ethnicGroup('THA','Chinese',140).
ethnicGroup('THA','Thai',750).
ethnicGroup('K','Chinese',10).
ethnicGroup('K','Khmer',900).
ethnicGroup('K','Vietnamese',50).
ethnicGroup('VN','Viet/Kinh',857).
ethnicGroup('VN','Tay',19).
ethnicGroup('VN','Thai',18).
ethnicGroup('VN','Muong',15).
ethnicGroup('VN','Khmer',15).
ethnicGroup('VN','Mong',12).
ethnicGroup('VN','Nung',11).
ethnicGroup('KAZ','Kazakh',631).
ethnicGroup('KAZ','Russian',237).
ethnicGroup('KAZ','Uzbek',28).
ethnicGroup('KAZ','Ukrainian',21).
ethnicGroup('KAZ','Uighur',14).
ethnicGroup('KAZ','Tatar',13).
ethnicGroup('KAZ','German',11).
ethnicGroup('KGZ','Kyrgyz',649).
ethnicGroup('KGZ','Uzbek',138).
ethnicGroup('KGZ','Russian',125).
ethnicGroup('KGZ','Dungan',11).
ethnicGroup('KGZ','Ukrainian',10).
ethnicGroup('KGZ','Uighur',10).
ethnicGroup('HONX','Chinese',950).
ethnicGroup('HONX','Filipino',16).
ethnicGroup('HONX','Indonesian',13).
ethnicGroup('MACX','Chinese',950).
ethnicGroup('MACX','Portuguese',30).
ethnicGroup('MNG','Mongol',949).
ethnicGroup('MNG','Kazak',50).
ethnicGroup('XMAS','Chinese',700).
ethnicGroup('XMAS','Malay',100).
ethnicGroup('XMAS','European',200).
ethnicGroup('GAZA','Jewish',6).
ethnicGroup('GAZA','Palestinian Arab',994).
ethnicGroup('IL','Jewish',820).
ethnicGroup('ET','European',10).
ethnicGroup('ET','Eastern Hamitic',990).
ethnicGroup('RI','Javanese',450).
ethnicGroup('RI','Sundanese',140).
ethnicGroup('RI','Madurese',75).
ethnicGroup('RI','Malay',75).
ethnicGroup('IRQ','Kurdish',150).
ethnicGroup('IRQ','Arab',750).
ethnicGroup('IRQ','Assyrian',50).
ethnicGroup('JOR','Armenian',10).
ethnicGroup('JOR','Arab',980).
ethnicGroup('JOR','Circassian',10).
ethnicGroup('KWT','Arab',350).
ethnicGroup('KWT','Iranian',40).
ethnicGroup('KWT','South Asian',90).
ethnicGroup('KWT','Kuwaiti',450).
ethnicGroup('SA','Arab',900).
ethnicGroup('SA','Afro-Asian',100).
ethnicGroup('SYR','Arab',903).
ethnicGroup('RL','Armenian',40).
ethnicGroup('RL','Arab',950).
ethnicGroup('WEST','Jewish',170).
ethnicGroup('WEST','Palestinian Arab',830).
ethnicGroup('J','Japanese',994).
ethnicGroup('UAE','South Asian',500).
ethnicGroup('UAE','Emiri',190).
ethnicGroup('UAE','Arab Iranian',230).
ethnicGroup('RP','Chinese',15).
ethnicGroup('RP','Malay',955).
ethnicGroup('Q','Indian',180).
ethnicGroup('Q','Pakistani',180).
ethnicGroup('Q','Arab',400).
ethnicGroup('Q','Iranian',100).
ethnicGroup('SGP','Indian',64).
ethnicGroup('SGP','Chinese',764).
ethnicGroup('SGP','Malay',149).
ethnicGroup('CL','Sinhalese',740).
ethnicGroup('CL','Tamil',180).
ethnicGroup('CL','Arab',70).
ethnicGroup('CL','Vedda',10).
ethnicGroup('RC','Chinese',140).
ethnicGroup('RC','Taiwanese',840).
ethnicGroup('AXA','Black',901).
ethnicGroup('AXA','Mulatto',46).
ethnicGroup('AXA','White',37).
ethnicGroup('ARU','European/Caribbean Indian',800).
ethnicGroup('BDS','European',40).
ethnicGroup('BDS','African',800).
ethnicGroup('BZ','Creole',300).
ethnicGroup('BZ','Garifuna',70).
ethnicGroup('BZ','Maya',110).
ethnicGroup('GCA','Mestizo - Amerindian-Spanish',594).
ethnicGroup('GCA','Amerindian',405).
ethnicGroup('MEX','Amerindian',300).
ethnicGroup('MEX','Caucasian',90).
ethnicGroup('BERM','Black',540).
ethnicGroup('BERM','White',341).
ethnicGroup('BERM','Mixed',64).
ethnicGroup('BVIR','Black',834).
ethnicGroup('BVIR','White',70).
ethnicGroup('BVIR','Indian',34).
ethnicGroup('BVIR','Mixed',54).
ethnicGroup('CDN','French',270).
ethnicGroup('CDN','Asian',115).
ethnicGroup('CDN','European',200).
ethnicGroup('CDN','British Isles',400).
ethnicGroup('CDN','Inuit',15).
ethnicGroup('USA','Asian',33).
ethnicGroup('USA','Native American',8).
ethnicGroup('CAYM','Black',200).
ethnicGroup('CAYM','White',200).
ethnicGroup('CAYM','Mixed',400).
ethnicGroup('CR','Indian',10).
ethnicGroup('CR','Chinese',10).
ethnicGroup('NIC','Indian',50).
ethnicGroup('PA','West Indian',140).
ethnicGroup('PA','Indian',60).
ethnicGroup('C','Chinese',10).
ethnicGroup('WD','Carib Indians',1000).
ethnicGroup('RH','European',50).
ethnicGroup('ES','Indian',50).
ethnicGroup('HCA','Indian',70).
ethnicGroup('GROX','Danish',140).
ethnicGroup('GROX','Greenlander',860).
ethnicGroup('WG','African',1000).
ethnicGroup('GUAD','Chinese',50).
ethnicGroup('JA','African',763).
ethnicGroup('JA','Afro-European',151).
ethnicGroup('JA','Afro-East Indian',30).
ethnicGroup('JA','Afro-Chinese',12).
ethnicGroup('MART','Chinese',50).
ethnicGroup('MART','African-white-Indian',900).
ethnicGroup('NA','Mixed Black',850).
ethnicGroup('CO','Indian',10).
ethnicGroup('CO','black-Indian',30).
ethnicGroup('PR','White',802).
ethnicGroup('PR','Black',80).
ethnicGroup('PR','Amerindian',4).
ethnicGroup('PR','Asian',4).
ethnicGroup('KN','African',1000).
ethnicGroup('WL','African',903).
ethnicGroup('WL','East Indian',32).
ethnicGroup('SPMI','Basques Bretons',1000).
ethnicGroup('TT','Chinese',10).
ethnicGroup('TT','East Indian',400).
ethnicGroup('TUCA','African',900).
ethnicGroup('AMSA','Caucasian',20).
ethnicGroup('AMSA','Tongan',40).
ethnicGroup('AMSA','Samoan',890).
ethnicGroup('AUS','Asian',40).
ethnicGroup('AUS','Caucasian',950).
ethnicGroup('COOK','Polynesian',935).
ethnicGroup('FJI','Indian',460).
ethnicGroup('FJI','Fijian',490).
ethnicGroup('FPOL','French',100).
ethnicGroup('FPOL','Chinese',120).
ethnicGroup('FPOL','Polynesian',780).
ethnicGroup('GUAM','Chamorro',371).
ethnicGroup('GUAM','Filipino',263).
ethnicGroup('KIR','Micronesian',1000).
ethnicGroup('MH','Micronesian',1000).
ethnicGroup('FSM','Polynesian',1000).
ethnicGroup('NAU','Chinese',80).
ethnicGroup('NAU','European',80).
ethnicGroup('NAU','Nauruan',580).
ethnicGroup('NAU','Pacific Islander',260).
ethnicGroup('NCA','Vietnamese',14).
ethnicGroup('NCA','European',341).
ethnicGroup('NCA','Melanesian',441).
ethnicGroup('NCA','Wallisian',90).
ethnicGroup('NCA','Indonesian',25).
ethnicGroup('NZ','European',880).
ethnicGroup('NZ','Pacific Islander',29).
ethnicGroup('NZ','Maori',89).
ethnicGroup('NIUE','Polynesian',1000).
ethnicGroup('NMIS','Asian',563).
ethnicGroup('NMIS','Pacific Islander',363).
ethnicGroup('NMIS','Caucasian',18).
ethnicGroup('PITC','Bounty',1000).
ethnicGroup('SLB','Chinese',3).
ethnicGroup('SLB','European',8).
ethnicGroup('SLB','Polynesian',40).
ethnicGroup('SLB','Micronesian',15).
ethnicGroup('SLB','Melanesian',930).
ethnicGroup('TUV','Polynesian',960).
ethnicGroup('VU','French',40).
ethnicGroup('VU','Melanesian',940).
ethnicGroup('WAFU','Polynesian',1000).
ethnicGroup('WS','Europeans',4).
ethnicGroup('WS','Samoan',926).
ethnicGroup('WS','Euronesians',70).
ethnicGroup('RA','Indian',1000).
ethnicGroup('BOL','European',50).
ethnicGroup('BOL','Quechua',300).
ethnicGroup('BOL','Aymara',250).
ethnicGroup('BR','African',440).
ethnicGroup('RCH','Indian',30).
ethnicGroup('RCH','European European-Indian',950).
ethnicGroup('PY','Amerindians',50).
ethnicGroup('PE','Indian',450).
ethnicGroup('FGU','Amerindian',120).
ethnicGroup('GUY','East Indian',510).
ethnicGroup('GUY','Amerindian',40).
ethnicGroup('GUY','European Chinese',20).
ethnicGroup('SME','Chinese',17).
ethnicGroup('SME','Javanese',153).
ethnicGroup('SME','Creole',310).
ethnicGroup('SME','Europeans',10).
ethnicGroup('SME','Amerindian',26).
ethnicGroup('SME','Hindustani',370).
ethnicGroup('YV','Amerindian',20).
ethnicGroup('EC','Spanish',100).
ethnicGroup('EC','Indian',250).
ethnicGroup('FALK','Christian',672).
ethnicGroup('FALK','None',315).
ethnicGroup('DZ','European',10).
ethnicGroup('DZ','Arab-Berber',990).
ethnicGroup('LAR','Berber Arab',970).
ethnicGroup('RMM','Mande',500).
ethnicGroup('RMM','Peuhl',170).
ethnicGroup('RMM','Voltaic',120).
ethnicGroup('RMM','Songhai',60).
ethnicGroup('RMM','Tuareg',100).
ethnicGroup('RIM','Maur',300).
ethnicGroup('RIM','Black Maur',400).
ethnicGroup('MA','Jewish',2).
ethnicGroup('MA','Arab-Berber',991).
ethnicGroup('RN','Fula',85).
ethnicGroup('RN','Hausa',560).
ethnicGroup('RN','Djerma',220).
ethnicGroup('RN','Tuareg',80).
ethnicGroup('RN','Beri Beri',43).
ethnicGroup('RN','Gourmantche',12).
ethnicGroup('TN','Jewish',10).
ethnicGroup('TN','European',10).
ethnicGroup('TN','Arab-Berber',980).
ethnicGroup('ANG','European',10).
ethnicGroup('ANG','Ovimbundu',370).
ethnicGroup('ANG','Kimbundu',250).
ethnicGroup('ANG','Bakongo',130).
ethnicGroup('ZRE','Mangbetu-Azande',450).
ethnicGroup('Z','European',11).
ethnicGroup('Z','African',987).
ethnicGroup('BEN','African',990).
ethnicGroup('BF','Mossi',240).
ethnicGroup('WAN','African',990).
ethnicGroup('RT','African',990).
ethnicGroup('RT','European Syrian-Lebanese',10).
ethnicGroup('RB','Batswana',950).
ethnicGroup('RB','Kgalagadi',40).
ethnicGroup('RSA','Indian',26).
ethnicGroup('RSA','Colored',86).
ethnicGroup('ZW','Asian',10).
ethnicGroup('ZW','African',980).
ethnicGroup('CI','Malinke',110).
ethnicGroup('CI','Baoule',230).
ethnicGroup('CI','Bete',180).
ethnicGroup('CI','Senoufou',150).
ethnicGroup('GH','European',2).
ethnicGroup('GH','African',998).
ethnicGroup('RWA','Hutu',800).
ethnicGroup('RWA','Tutsi',190).
ethnicGroup('RWA','Twa',10).
ethnicGroup('EAT','Bantu',950).
ethnicGroup('CAM','African',130).
ethnicGroup('CAM','Fulani',100).
ethnicGroup('CAM','Cameroon Highlanders',310).
ethnicGroup('CAM','Equatorial Bantu',190).
ethnicGroup('CAM','Kirdi',110).
ethnicGroup('CAM','Northwestern Bantu',80).
ethnicGroup('CAM','Eastern Nigritic',70).
ethnicGroup('RCA','Baya',340).
ethnicGroup('RCA','Banda',270).
ethnicGroup('RCA','Sara',100).
ethnicGroup('RCA','Mandjia',210).
ethnicGroup('RCA','Mboum',40).
ethnicGroup('RCA','MBaka',40).
ethnicGroup('CV','European',10).
ethnicGroup('CV','African',280).
ethnicGroup('CV','Creole',710).
ethnicGroup('SUD','Arab',700).
ethnicGroup('SUD','Beja',60).
ethnicGroup('RG','Malinke',300).
ethnicGroup('RG','Peuhl',400).
ethnicGroup('RG','Soussou',200).
ethnicGroup('LB','African',950).
ethnicGroup('LB','Americo-Liberians',50).
ethnicGroup('DJI','Italian',50).
ethnicGroup('DJI','Afar',350).
ethnicGroup('DJI','Somali',600).
ethnicGroup('ER','Tigrinya',550).
ethnicGroup('ER','Tigre',300).
ethnicGroup('ER','Saho',40).
ethnicGroup('ER','Kunama',20).
ethnicGroup('ER','Rashaida',20).
ethnicGroup('ER','Bilen',20).
ethnicGroup('ETH','Oromo',345).
ethnicGroup('ETH','Amhara',269).
ethnicGroup('ETH','Somali',62).
ethnicGroup('ETH','Tigraway',61).
ethnicGroup('ETH','Sidama',40).
ethnicGroup('ETH','Gurage',25).
ethnicGroup('ETH','Welaita',23).
ethnicGroup('ETH','Hadiya',17).
ethnicGroup('ETH','Afar',17).
ethnicGroup('SP','Somali',850).
ethnicGroup('EAK','Arab',10).
ethnicGroup('EAK','Kikuyu',220).
ethnicGroup('EAK','Luhya',140).
ethnicGroup('EAK','Luo',130).
ethnicGroup('EAK','Kalenjin',120).
ethnicGroup('EAK','Kamba',110).
ethnicGroup('EAK','Kisii',60).
ethnicGroup('EAK','Meru',60).
ethnicGroup('WAG','African',990).
ethnicGroup('SN','Fulani',170).
ethnicGroup('SN','Wolof',360).
ethnicGroup('SN','Diola',90).
ethnicGroup('SN','Mandingo',90).
ethnicGroup('SN','Serer',170).
ethnicGroup('SN','Toucouleur',90).
ethnicGroup('SN','European Lebanese',10).
ethnicGroup('GNB','European',10).
ethnicGroup('GNB','African',990).
ethnicGroup('WAL','Asian',10).
ethnicGroup('WAL','African',990).
ethnicGroup('EAU','Rwanda',60).
ethnicGroup('EAU','Arab',10).
ethnicGroup('EAU','Baganda',170).
ethnicGroup('EAU','Karamojong',120).
ethnicGroup('EAU','Basogo',80).
ethnicGroup('EAU','Iteso',80).
ethnicGroup('EAU','Langi',60).
ethnicGroup('EAU','Bagisu',50).
ethnicGroup('EAU','Acholi',40).
ethnicGroup('EAU','Lugbara',40).
ethnicGroup('EAU','Bunyoro',30).
ethnicGroup('EAU','Batobo',30).
ethnicGroup('LS','Sotho',997).
ethnicGroup('MOC','European',0).
ethnicGroup('MOC','Indian',0).
ethnicGroup('MOC','Euro-African',2).
ethnicGroup('MS','Creole',270).
ethnicGroup('MS','Indo-Mauritian',680).
ethnicGroup('MS','Sino-Mauritian',30).
ethnicGroup('MS','Franco-Mauritian',20).
ethnicGroup('SD','European',30).
ethnicGroup('SD','African',970).
ethnicGroup('HELX','African descent',500).
ethnicGroup('HELX','White',250).
ethnicGroup('HELX','Chinese',250).
ethnicGroup('STP','Europeans',1000).
ethnicGroup('SY','Seychellois',1000).
language('MK','Albanian').
language('MK','Macedonian').
language('MK','Turkish').
language('MK','Serbian').
language('SRB','Serbian').
language('SRB','Hungarian').
language('SRB','Bosnian').
language('SRB','Roma').
language('MNE','Serbian').
language('MNE','Montenegrin').
language('MNE','Bosnian').
language('MNE','Albanian').
language('F','French').
language('E','Catalan').
language('E','Galician').
language('E','Basque').
language('A','German').
language('D','German').
language('H','Hungarian').
language('SLO','Slovenian').
language('SLO','Croatian').
language('CH','French').
language('CH','German').
language('CH','Italian').
language('CH','Romansch').
language('BY','Russian').
language('BY','Belorussian').
language('LV','Latvian').
language('LV','Russian').
language('LT','Lithuanian').
language('LT','Russian').
language('LT','Polish').
language('PL','Polish').
language('UA','Ukrainian').
language('UA','Russian').
language('R','Russian').
language('B','French').
language('B','German').
language('B','Dutch').
language('NL','Dutch').
language('BIH','Serbo-Croatian').
language('HR','Serbo-Croatian').
language('BG','Bulgarian').
language('EW','Estonian').
language('EW','Russian').
language('SF','Swedish').
language('SF','Finnish').
language('N','Norwegian').
language('S','Swedish').
language('IS','Icelandic').
language('RSM','Italian').
language('P','Portuguese').
language('AFG','Turkic').
language('AFG','Pashtu').
language('AFG','Afghan Persian').
language('IR','Turkish').
language('IR','Kurdish').
language('IR','Balochi').
language('IR','Arabic').
language('IR','Luri').
language('IR','Persian').
language('IR','Turkic').
language('PK','Pashtu').
language('PK','Urdu').
language('PK','Punjabi').
language('PK','Sindhi').
language('PK','Balochi').
language('PK','Hindko').
language('PK','Brahui').
language('PK','Siraiki').
language('TM','Turkmen').
language('TM','Russian').
language('TM','Uzbek').
language('UZB','Uzbek').
language('UZB','Russian').
language('UZB','Tajik').
language('ARM','Armenian').
language('ARM','Kurd').
language('ARM','Russian').
language('GE','Georgian').
language('GE','Russian').
language('GE','Armenian').
language('GE','Azeri').
language('AZ','Azeri').
language('AZ','Lezgi').
language('AZ','Russian').
language('AZ','Armenian').
language('MYA','Burmese').
language('IND','Hindi').
language('KAZ','Kazakh').
language('NOK','Korean').
language('KGZ','Kyrgyz').
language('KGZ','Uzbek').
language('KGZ','Russian').
language('KGZ','Dungun').
language('HONX','Chinese').
language('HONX','English').
language('MACX','Portuguese').
language('MNG','Khalkha Mongol').
language('NEP','Nepali').
language('PNG','English').
language('SA','Arabic').
language('J','Japanese').
language('CL','Tamil').
language('CL','Sinhala').
language('AXA','English').
language('AG','English').
language('BDS','English').
language('GCA','Spanish').
language('GCA','Amerindian').
language('BVIR','English').
language('CAYM','English').
language('CAYM','Spanish').
language('NIC','Spanish').
language('PA','English').
language('C','Spanish').
language('DOM','Spanish').
language('RH','French').
language('GUAD','French').
language('MNTS','English').
language('NA','Papiamento').
language('NA','English').
language('NA','Dutch').
language('NA','Spanish').
language('NA','Creole').
language('CO','Spanish').
language('KN','English').
language('SPMI','French').
language('TUCA','English').
language('AMSA','Samoan').
language('AMSA','English').
language('AMSA','Tongan').
language('AUS','English').
language('FPOL','French').
language('FPOL','Polynesian').
language('GUAM','English').
language('GUAM','Chamorro').
language('GUAM','Phillipine Languages').
language('NMIS','Phillipine Languages').
language('NMIS','Chinese Languages').
language('NMIS','Chamorro').
language('NMIS','English').
language('NMIS','Other Pacific Island Languages').
language('PITC','Pitkern').
language('SLB','English').
language('WAFU','Wallisian').
language('WAFU','Futunian').
language('WAFU','French').
language('RCH','Spanish').
language('FGU','French').
language('FALK','English').
language('RMM','Bambara').
language('NAM','German').
language('NAM','English').
language('NAM','Afrikaans').
language('RG','French').
language('LB','English').
language('MOC','Portuguese').
language('HELX','English').
language('STP','Portuguese').
borders('AL','GR').
borders('AL','MK').
borders('AL','MNE').
borders('AL','KOS').
borders('GR','MK').
borders('GR','TR').
borders('MK','SRB').
borders('MNE','SRB').
borders('KOS','MNE').
borders('KOS','SRB').
borders('KOS','MK').
borders('AND','F').
borders('AND','E').
borders('F','I').
borders('F','L').
borders('F','MC').
borders('E','F').
borders('E','GBZ').
borders('E','P').
borders('A','CZ').
borders('A','D').
borders('A','H').
borders('A','I').
borders('A','FL').
borders('A','SK').
borders('A','SLO').
borders('A','CH').
borders('CZ','D').
borders('CZ','SK').
borders('CZ','PL').
borders('D','F').
borders('D','PL').
borders('D','L').
borders('D','NL').
borders('D','DK').
borders('H','SRB').
borders('H','SK').
borders('H','SLO').
borders('H','UA').
borders('H','HR').
borders('H','RO').
borders('I','SLO').
borders('I','V').
borders('I','RSM').
borders('SK','UA').
borders('CH','F').
borders('CH','D').
borders('CH','I').
borders('CH','FL').
borders('BY','LV').
borders('BY','LT').
borders('BY','PL').
borders('BY','UA').
borders('BY','R').
borders('LV','R').
borders('LT','LV').
borders('LT','PL').
borders('LT','R').
borders('PL','SK').
borders('PL','UA').
borders('PL','R').
borders('R','UA').
borders('R','SF').
borders('B','F').
borders('B','D').
borders('B','L').
borders('B','NL').
borders('BIH','SRB').
borders('BIH','MNE').
borders('BIH','HR').
borders('HR','SRB').
borders('HR','MNE').
borders('HR','SLO').
borders('BG','GR').
borders('BG','MK').
borders('BG','SRB').
borders('BG','RO').
borders('BG','TR').
borders('RO','SRB').
borders('RO','UA').
borders('EW','LV').
borders('EW','R').
borders('N','R').
borders('N','SF').
borders('N','S').
borders('S','SF').
borders('CEU','MA').
borders('MD','UA').
borders('MD','RO').
borders('GB','IRL').
borders('AFG','CN').
borders('AFG','IR').
borders('AFG','PK').
borders('AFG','TAD').
borders('AFG','TM').
borders('AFG','UZB').
borders('CN','R').
borders('CN','PK').
borders('CN','TAD').
borders('CN','MYA').
borders('CN','IND').
borders('CN','LAO').
borders('CN','VN').
borders('CN','KAZ').
borders('CN','NOK').
borders('CN','KGZ').
borders('CN','MNG').
borders('CN','MACX').
borders('CN','HONX').
borders('IR','TR').
borders('IR','PK').
borders('IR','TM').
borders('IR','IRQ').
borders('TAD','UZB').
borders('TM','UZB').
borders('ARM','TR').
borders('ARM','IR').
borders('ARM','GE').
borders('ARM','AZ').
borders('GE','R').
borders('GE','TR').
borders('AZ','R').
borders('AZ','TR').
borders('AZ','IR').
borders('AZ','GE').
borders('BD','MYA').
borders('BD','IND').
borders('MYA','THA').
borders('IND','PK').
borders('IND','MYA').
borders('IND','NEP').
borders('BHT','CN').
borders('BHT','IND').
borders('BRU','MAL').
borders('MAL','THA').
borders('MAL','RI').
borders('LAO','MYA').
borders('LAO','THA').
borders('LAO','VN').
borders('K','LAO').
borders('K','THA').
borders('K','VN').
borders('KAZ','R').
borders('KAZ','TM').
borders('KAZ','UZB').
borders('KAZ','KGZ').
borders('NOK','R').
borders('NOK','ROK').
borders('KGZ','TAD').
borders('KGZ','UZB').
borders('MNG','R').
borders('GAZA','IL').
borders('IL','JOR').
borders('IL','SYR').
borders('IL','RL').
borders('IL','WEST').
borders('ET','GAZA').
borders('ET','IL').
borders('ET','LAR').
borders('ET','SUD').
borders('RI','TL').
borders('PNG','RI').
borders('IRQ','TR').
borders('IRQ','JOR').
borders('IRQ','KWT').
borders('IRQ','SA').
borders('IRQ','SYR').
borders('JOR','SA').
borders('JOR','SYR').
borders('JOR','WEST').
borders('KWT','SA').
borders('SA','UAE').
borders('SA','YE').
borders('SYR','TR').
borders('RL','SYR').
borders('OM','SA').
borders('OM','UAE').
borders('OM','YE').
borders('Q','SA').
borders('BZ','GCA').
borders('BZ','MEX').
borders('GCA','MEX').
borders('GCA','HCA').
borders('MEX','USA').
borders('CDN','USA').
borders('CR','NIC').
borders('CR','PA').
borders('DOM','RH').
borders('ES','GCA').
borders('ES','HCA').
borders('HCA','NIC').
borders('NA','SMAR').
borders('CO','PA').
borders('CO','PE').
borders('CO','YV').
borders('CO','EC').
borders('RA','RCH').
borders('RA','ROU').
borders('BOL','RA').
borders('BOL','BR').
borders('BOL','RCH').
borders('BOL','PY').
borders('BOL','PE').
borders('BR','CO').
borders('BR','RA').
borders('BR','PY').
borders('BR','ROU').
borders('BR','PE').
borders('BR','FGU').
borders('BR','GUY').
borders('BR','SME').
borders('BR','YV').
borders('PY','RA').
borders('PE','RCH').
borders('FGU','SME').
borders('GUY','SME').
borders('GUY','YV').
borders('EC','PE').
borders('DZ','LAR').
borders('DZ','RMM').
borders('DZ','RIM').
borders('DZ','MA').
borders('DZ','RN').
borders('DZ','TN').
borders('DZ','WSA').
borders('LAR','RN').
borders('LAR','TN').
borders('LAR','TCH').
borders('LAR','SUD').
borders('RMM','RN').
borders('RMM','SN').
borders('RIM','RMM').
borders('RIM','WSA').
borders('RIM','SN').
borders('MA','WSA').
borders('MA','MEL').
borders('RN','WAN').
borders('RN','TCH').
borders('ANG','RCB').
borders('ANG','NAM').
borders('ANG','ZRE').
borders('ANG','Z').
borders('RCB','ZRE').
borders('NAM','Z').
borders('NAM','RB').
borders('NAM','RSA').
borders('Z','ZRE').
borders('Z','ZW').
borders('BEN','RN').
borders('BEN','BF').
borders('BEN','WAN').
borders('BEN','RT').
borders('BF','RMM').
borders('BF','RN').
borders('BF','RT').
borders('BF','CI').
borders('BF','GH').
borders('RB','RSA').
borders('RB','ZW').
borders('RSA','ZW').
borders('RSA','SD').
borders('CI','RMM').
borders('CI','GH').
borders('CI','RG').
borders('CI','LB').
borders('GH','RT').
borders('BI','ZRE').
borders('BI','RWA').
borders('BI','EAT').
borders('RWA','ZRE').
borders('EAT','ZRE').
borders('EAT','Z').
borders('EAT','RWA').
borders('EAT','EAU').
borders('EAT','MW').
borders('EAT','MOC').
borders('CAM','RCB').
borders('CAM','WAN').
borders('CAM','RCA').
borders('CAM','TCH').
borders('CAM','GQ').
borders('CAM','G').
borders('RCA','RCB').
borders('RCA','ZRE').
borders('RCA','TCH').
borders('RCA','SUD').
borders('RCA','SSD').
borders('TCH','WAN').
borders('G','RCB').
borders('G','GQ').
borders('SUD','TCH').
borders('SSD','ZRE').
borders('SSD','SUD').
borders('RG','RMM').
borders('RG','SN').
borders('RG','WAL').
borders('LB','RG').
borders('LB','WAL').
borders('DJI','ER').
borders('DJI','ETH').
borders('DJI','SP').
borders('ER','SUD').
borders('ER','ETH').
borders('ETH','SUD').
borders('ETH','SSD').
borders('ETH','SP').
borders('EAK','EAT').
borders('EAK','SSD').
borders('EAK','ETH').
borders('EAK','SP').
borders('EAK','EAU').
borders('SN','WAG').
borders('GNB','RG').
borders('GNB','SN').
borders('EAU','ZRE').
borders('EAU','RWA').
borders('EAU','SSD').
borders('LS','RSA').
borders('MW','Z').
borders('MOC','Z').
borders('MOC','RSA').
borders('MOC','ZW').
borders('MOC','MW').
borders('MOC','SD').
continent('Europe',9562488).
continent('Asia',45095292).
continent('Australia/Oceania',8503474).
continent('Africa',30254708).
continent('America',39872000).
encompasses('AL','Europe').
encompasses('GR','Europe').
encompasses('MK','Europe').
encompasses('SRB','Europe').
encompasses('MNE','Europe').
encompasses('KOS','Europe').
encompasses('AND','Europe').
encompasses('F','Europe').
encompasses('E','Europe').
encompasses('A','Europe').
encompasses('CZ','Europe').
encompasses('D','Europe').
encompasses('H','Europe').
encompasses('I','Europe').
encompasses('FL','Europe').
encompasses('SK','Europe').
encompasses('SLO','Europe').
encompasses('CH','Europe').
encompasses('BY','Europe').
encompasses('LV','Europe').
encompasses('LT','Europe').
encompasses('PL','Europe').
encompasses('UA','Europe').
encompasses('R','Europe').
encompasses('R','Asia').
encompasses('B','Europe').
encompasses('L','Europe').
encompasses('NL','Europe').
encompasses('BIH','Europe').
encompasses('HR','Europe').
encompasses('BG','Europe').
encompasses('RO','Europe').
encompasses('TR','Europe').
encompasses('TR','Asia').
encompasses('DK','Europe').
encompasses('EW','Europe').
encompasses('FARX','Europe').
encompasses('SF','Europe').
encompasses('N','Europe').
encompasses('S','Europe').
encompasses('MC','Europe').
encompasses('GBZ','Europe').
encompasses('GBG','Europe').
encompasses('V','Europe').
encompasses('CEU','Africa').
encompasses('MEL','Africa').
encompasses('IS','Europe').
encompasses('IRL','Europe').
encompasses('RSM','Europe').
encompasses('GBJ','Europe').
encompasses('M','Europe').
encompasses('GBM','Europe').
encompasses('MD','Europe').
encompasses('P','Europe').
encompasses('SVAX','Europe').
encompasses('GB','Europe').
encompasses('AFG','Asia').
encompasses('CN','Asia').
encompasses('IR','Asia').
encompasses('PK','Asia').
encompasses('TAD','Asia').
encompasses('TM','Asia').
encompasses('UZB','Asia').
encompasses('ARM','Asia').
encompasses('GE','Asia').
encompasses('AZ','Asia').
encompasses('BRN','Asia').
encompasses('BD','Asia').
encompasses('MYA','Asia').
encompasses('IND','Asia').
encompasses('BHT','Asia').
encompasses('BRU','Asia').
encompasses('MAL','Asia').
encompasses('LAO','Asia').
encompasses('THA','Asia').
encompasses('K','Asia').
encompasses('VN','Asia').
encompasses('KAZ','Asia').
encompasses('NOK','Asia').
encompasses('KGZ','Asia').
encompasses('HONX','Asia').
encompasses('MACX','Asia').
encompasses('MNG','Asia').
encompasses('NEP','Asia').
encompasses('XMAS','Australia/Oceania').
encompasses('COCO','Australia/Oceania').
encompasses('CY','Europe').
encompasses('GAZA','Asia').
encompasses('IL','Asia').
encompasses('ET','Asia').
encompasses('ET','Africa').
encompasses('RI','Asia').
encompasses('RI','Australia/Oceania').
encompasses('TL','Asia').
encompasses('PNG','Australia/Oceania').
encompasses('IRQ','Asia').
encompasses('JOR','Asia').
encompasses('KWT','Asia').
encompasses('SA','Asia').
encompasses('SYR','Asia').
encompasses('RL','Asia').
encompasses('WEST','Asia').
encompasses('J','Asia').
encompasses('ROK','Asia').
encompasses('MV','Asia').
encompasses('OM','Asia').
encompasses('UAE','Asia').
encompasses('YE','Asia').
encompasses('RP','Asia').
encompasses('Q','Asia').
encompasses('SGP','Asia').
encompasses('CL','Asia').
encompasses('RC','Asia').
encompasses('AXA','America').
encompasses('AG','America').
encompasses('ARU','America').
encompasses('BS','America').
encompasses('BDS','America').
encompasses('BZ','America').
encompasses('GCA','America').
encompasses('MEX','America').
encompasses('BERM','America').
encompasses('BVIR','America').
encompasses('CDN','America').
encompasses('USA','America').
encompasses('CAYM','America').
encompasses('CR','America').
encompasses('NIC','America').
encompasses('PA','America').
encompasses('C','America').
encompasses('WD','America').
encompasses('DOM','America').
encompasses('RH','America').
encompasses('ES','America').
encompasses('HCA','America').
encompasses('GROX','America').
encompasses('WG','America').
encompasses('GUAD','America').
encompasses('JA','America').
encompasses('MART','America').
encompasses('MNTS','America').
encompasses('NA','America').
encompasses('SMAR','America').
encompasses('CO','America').
encompasses('PR','America').
encompasses('KN','America').
encompasses('WL','America').
encompasses('SPMI','America').
encompasses('WV','America').
encompasses('TT','America').
encompasses('TUCA','America').
encompasses('VIRG','America').
encompasses('AMSA','Australia/Oceania').
encompasses('AUS','Australia/Oceania').
encompasses('COOK','Australia/Oceania').
encompasses('FJI','Australia/Oceania').
encompasses('FPOL','Australia/Oceania').
encompasses('GUAM','Australia/Oceania').
encompasses('KIR','Australia/Oceania').
encompasses('MH','Australia/Oceania').
encompasses('FSM','Australia/Oceania').
encompasses('NAU','Australia/Oceania').
encompasses('NCA','Australia/Oceania').
encompasses('NZ','Australia/Oceania').
encompasses('NIUE','Australia/Oceania').
encompasses('NORF','Australia/Oceania').
encompasses('NMIS','Australia/Oceania').
encompasses('PAL','Australia/Oceania').
encompasses('PITC','Australia/Oceania').
encompasses('SLB','Australia/Oceania').
encompasses('TO','Australia/Oceania').
encompasses('TUV','Australia/Oceania').
encompasses('VU','Australia/Oceania').
encompasses('WAFU','Australia/Oceania').
encompasses('WS','Australia/Oceania').
encompasses('RA','America').
encompasses('BOL','America').
encompasses('BR','America').
encompasses('RCH','America').
encompasses('PY','America').
encompasses('ROU','America').
encompasses('PE','America').
encompasses('FGU','America').
encompasses('GUY','America').
encompasses('SME','America').
encompasses('YV','America').
encompasses('EC','America').
encompasses('FALK','America').
encompasses('DZ','Africa').
encompasses('LAR','Africa').
encompasses('RMM','Africa').
encompasses('RIM','Africa').
encompasses('MA','Africa').
encompasses('RN','Africa').
encompasses('TN','Africa').
encompasses('WSA','Africa').
encompasses('ANG','Africa').
encompasses('RCB','Africa').
encompasses('NAM','Africa').
encompasses('ZRE','Africa').
encompasses('Z','Africa').
encompasses('BEN','Africa').
encompasses('BF','Africa').
encompasses('WAN','Africa').
encompasses('RT','Africa').
encompasses('RB','Africa').
encompasses('RSA','Africa').
encompasses('ZW','Africa').
encompasses('CI','Africa').
encompasses('GH','Africa').
encompasses('BI','Africa').
encompasses('RWA','Africa').
encompasses('EAT','Africa').
encompasses('CAM','Africa').
encompasses('RCA','Africa').
encompasses('TCH','Africa').
encompasses('GQ','Africa').
encompasses('G','Africa').
encompasses('CV','Africa').
encompasses('SUD','Africa').
encompasses('SSD','Africa').
encompasses('COM','Africa').
encompasses('RG','Africa').
encompasses('LB','Africa').
encompasses('DJI','Africa').
encompasses('ER','Africa').
encompasses('ETH','Africa').
encompasses('SP','Africa').
encompasses('EAK','Africa').
encompasses('WAG','Africa').
encompasses('SN','Africa').
encompasses('GNB','Africa').
encompasses('WAL','Africa').
encompasses('EAU','Africa').
encompasses('LS','Africa').
encompasses('RM','Africa').
encompasses('MW','Africa').
encompasses('MOC','Africa').
encompasses('MS','Africa').
encompasses('MAYO','Africa').
encompasses('SD','Africa').
encompasses('REUN','Australia/Oceania').
encompasses('HELX','Australia/Oceania').
encompasses('STP','Africa').
encompasses('SY','Africa').
organization('OACP','B').
organization('OAfDB','CI').
organization('OACCT','F').
organization('OOPANAL','MEX').
organization('OAG','PE').
organization('OABEDA','SUD').
organization('OACC',onull).
organization('OAFESD',onull).
organization('OAL','ET').
organization('OAMU','MA').
organization('OAMF','UAE').
organization('OAPEC','SGP').
organization('OAsDB','RP').
organization('OASEAN','RI').
organization('OMekong Group',onull).
organization('OANZUS','AUS').
organization('OBIS','CH').
organization('OBenelux','B').
organization('OBSEC',onull).
organization('OCaricom','GUY').
organization('OCDB',onull).
organization('OUDEAC','RCA').
organization('OBDEAC','RCB').
organization('OBCIE','HCA').
organization('OCACM','GCA').
organization('OCEI','H').
organization('OCP','CL').
organization('OC','GB').
organization('OCIS','BY').
organization('OCAEU','JOR').
organization('OCE','F').
organization('OCBSS',onull).
organization('OEntente','CI').
organization('OCCC','B').
organization('OEADB','EAU').
organization('OESCAP','THA').
organization('OESCWA','JOR').
organization('OECA','ETH').
organization('OECE','CH').
organization('OECLAC','RCH').
organization('OCEEAC','G').
organization('OCEPGL',onull).
organization('OECOWAS','WAN').
organization('OECO','IR').
organization('OEBRD','GB').
organization('OEFTA','CH').
organization('OEIB','L').
organization('OCERN','CH').
organization('OESA','F').
organization('OEU','B').
organization('OFAO','I').
organization('OFZ','F').
organization('OG-2',onull).
organization('OG-3',onull).
organization('OG-5',onull).
organization('OG-6',onull).
organization('OG-7',onull).
organization('OG-8',onull).
organization('OG-9',onull).
organization('OG-10','F').
organization('OG-11',onull).
organization('OG-15','CH').
organization('OG-19',onull).
organization('OG-24','B').
organization('OG-77',onull).
organization('OGCC','SA').
organization('OIADB','USA').
organization('OIGADD','DJI').
organization('OIAEA','A').
organization('OIBRD','USA').
organization('OICC','F').
organization('OICAO','CDN').
organization('OICFTU','B').
organization('OInterpol','F').
organization('OIDA','USA').
organization('OIEA','F').
organization('OIFRCS','CH').
organization('OIFC','USA').
organization('OIFAD','I').
organization('OILO','CH').
organization('OIMO','GB').
organization('OInmarsat','GB').
organization('OIMF','USA').
organization('OIOC','CH').
organization('OIOM','CH').
organization('OISO','CH').
organization('OICRM','CH').
organization('OITU','CH').
organization('OIntelsat','USA').
organization('OIDB','SA').
organization('OLAES','YV').
organization('OLAIA','ROU').
organization('OMTCR',onull).
organization('ONAM','RI').
organization('ONC','S').
organization('ONIB','SF').
organization('OANC','B').
organization('ONATO','B').
organization('OEN',onull).
organization('ONSG','A').
organization('OOECD','F').
organization('OOSCE','A').
organization('OOAU','ETH').
organization('OOAS','USA').
organization('OOAPEC',onull).
organization('OOECS','WL').
organization('OOPEC','A').
organization('OOIC','SA').
organization('OPFP',onull).
organization('OPCA',onull).
organization('ORG',onull).
organization('OSAARC','NEP').
organization('OSPC','NCA').
organization('OSPF','FJI').
organization('OSparteca',onull).
organization('OSACU','RSA').
organization('OSADC','RB').
organization('OMercosur','RA').
organization('OUN','USA').
organization('OUNAVEM III','USA').
organization('OUNAMIR','USA').
organization('OUNCRO','USA').
organization('OUNDOF','SYR').
organization('OUNESCO','F').
organization('OUNFICYP','USA').
organization('OUNIDO','A').
organization('OUNITAR',onull).
organization('OUNIFIL','USA').
organization('OUNIKOM','USA').
organization('OUNMOGIP','USA').
organization('OMINURSO','USA').
organization('OUNMIH','USA').
organization('OUNMOT','USA').
organization('OUNOMIG','USA').
organization('OUNOMIL','USA').
organization('OUNHCR','CH').
organization('OUNPREDEP',onull).
organization('OUNPROFOR','USA').
organization('OUNRWA','A').
organization('OUNTSO','IL').
organization('OUNU',onull).
organization('OUPU','CH').
organization('OWADB','RT').
organization('OWEU','B').
organization('OWCL','B').
organization('OWFTU','CZ').
organization('OWHO','CH').
organization('OWIPO','CH').
organization('OWMO','CH').
organization('OWToO','E').
organization('OWTrO',onull).
organization('OZC',onull).
isMember('TL','OACP',member).
isMember('PNG','OACP',member).
isMember('AG','OACP',member).
isMember('BS','OACP',member).
isMember('BDS','OACP',member).
isMember('BZ','OACP',member).
isMember('WD','OACP',member).
isMember('DOM','OACP',member).
isMember('RH','OACP',member).
isMember('WG','OACP',member).
isMember('JA','OACP',member).
isMember('KN','OACP',member).
isMember('WL','OACP',member).
isMember('WV','OACP',member).
isMember('TT','OACP',member).
isMember('COOK','OACP',member).
isMember('FJI','OACP',member).
isMember('KIR','OACP',member).
isMember('NIUE','OACP',member).
isMember('SLB','OACP',member).
isMember('TO','OACP',member).
isMember('TUV','OACP',member).
isMember('VU','OACP',member).
isMember('WS','OACP',member).
isMember('GUY','OACP',member).
isMember('SME','OACP',member).
isMember('RMM','OACP',member).
isMember('RIM','OACP',member).
isMember('RN','OACP',member).
isMember('ANG','OACP',member).
isMember('RCB','OACP',member).
isMember('NAM','OACP',member).
isMember('ZRE','OACP',member).
isMember('Z','OACP',member).
isMember('BEN','OACP',member).
isMember('BF','OACP',member).
isMember('WAN','OACP',member).
isMember('RT','OACP',member).
isMember('RB','OACP',member).
isMember('ZW','OACP',member).
isMember('CI','OACP',member).
isMember('GH','OACP',member).
isMember('BI','OACP',member).
isMember('RWA','OACP',member).
isMember('EAT','OACP',member).
isMember('CAM','OACP',member).
isMember('RCA','OACP',member).
isMember('TCH','OACP',member).
isMember('GQ','OACP',member).
isMember('G','OACP',member).
isMember('CV','OACP',member).
isMember('SUD','OACP',member).
isMember('COM','OACP',member).
isMember('RG','OACP',member).
isMember('LB','OACP',member).
isMember('DJI','OACP',member).
isMember('ER','OACP',member).
isMember('ETH','OACP',member).
isMember('SP','OACP',member).
isMember('EAK','OACP',member).
isMember('WAG','OACP',member).
isMember('SN','OACP',member).
isMember('GNB','OACP',member).
isMember('WAL','OACP',member).
isMember('EAU','OACP',member).
isMember('LS','OACP',member).
isMember('RM','OACP',member).
isMember('MW','OACP',member).
isMember('MOC','OACP',member).
isMember('MS','OACP',member).
isMember('SD','OACP',member).
isMember('STP','OACP',member).
isMember('SY','OACP',member).
isMember('ET','OAfDB','regional member').
isMember('DZ','OAfDB','regional member').
isMember('LAR','OAfDB','regional member').
isMember('RMM','OAfDB','regional member').
isMember('RIM','OAfDB','regional member').
isMember('MA','OAfDB','regional member').
isMember('RN','OAfDB','regional member').
isMember('TN','OAfDB','regional member').
isMember('ANG','OAfDB','regional member').
isMember('RCB','OAfDB','regional member').
isMember('NAM','OAfDB','regional member').
isMember('ZRE','OAfDB','regional member').
isMember('Z','OAfDB','regional member').
isMember('BEN','OAfDB','regional member').
isMember('BF','OAfDB','regional member').
isMember('WAN','OAfDB','regional member').
isMember('RT','OAfDB','regional member').
isMember('RB','OAfDB','regional member').
isMember('ZW','OAfDB','regional member').
isMember('CI','OAfDB','regional member').
isMember('GH','OAfDB','regional member').
isMember('BI','OAfDB','regional member').
isMember('RWA','OAfDB','regional member').
isMember('EAT','OAfDB','regional member').
isMember('CAM','OAfDB','regional member').
isMember('RCA','OAfDB','regional member').
isMember('TCH','OAfDB','regional member').
isMember('GQ','OAfDB','regional member').
isMember('G','OAfDB','regional member').
isMember('CV','OAfDB','regional member').
isMember('SUD','OAfDB','regional member').
isMember('COM','OAfDB','regional member').
isMember('RG','OAfDB','regional member').
isMember('LB','OAfDB','regional member').
isMember('DJI','OAfDB','regional member').
isMember('ER','OAfDB','regional member').
isMember('ETH','OAfDB','regional member').
isMember('SP','OAfDB','regional member').
isMember('EAK','OAfDB','regional member').
isMember('WAG','OAfDB','regional member').
isMember('SN','OAfDB','regional member').
isMember('GNB','OAfDB','regional member').
isMember('WAL','OAfDB','regional member').
isMember('EAU','OAfDB','regional member').
isMember('LS','OAfDB','regional member').
isMember('RM','OAfDB','regional member').
isMember('MW','OAfDB','regional member').
isMember('MOC','OAfDB','regional member').
isMember('MS','OAfDB','regional member').
isMember('SD','OAfDB','regional member').
isMember('STP','OAfDB','regional member').
isMember('SY','OAfDB','regional member').
isMember('F','OAfDB','nonregional member').
isMember('E','OAfDB','nonregional member').
isMember('A','OAfDB','nonregional member').
isMember('D','OAfDB','nonregional member').
isMember('I','OAfDB','nonregional member').
isMember('CH','OAfDB','nonregional member').
isMember('B','OAfDB','nonregional member').
isMember('NL','OAfDB','nonregional member').
isMember('DK','OAfDB','nonregional member').
isMember('SF','OAfDB','nonregional member').
isMember('N','OAfDB','nonregional member').
isMember('S','OAfDB','nonregional member').
isMember('P','OAfDB','nonregional member').
isMember('GB','OAfDB','nonregional member').
isMember('CN','OAfDB','nonregional member').
isMember('IND','OAfDB','nonregional member').
isMember('KWT','OAfDB','nonregional member').
isMember('SA','OAfDB','nonregional member').
isMember('J','OAfDB','nonregional member').
isMember('ROK','OAfDB','nonregional member').
isMember('UAE','OAfDB','nonregional member').
isMember('CDN','OAfDB','nonregional member').
isMember('USA','OAfDB','nonregional member').
isMember('RA','OAfDB','nonregional member').
isMember('BR','OAfDB','nonregional member').
isMember('F','OACCT',member).
isMember('B','OACCT',member).
isMember('L','OACCT',member).
isMember('BG','OACCT',member).
isMember('RO','OACCT',member).
isMember('MC','OACCT',member).
isMember('LAO','OACCT',member).
isMember('K','OACCT',member).
isMember('VN','OACCT',member).
isMember('RL','OACCT',member).
isMember('CDN','OACCT',member).
isMember('WD','OACCT',member).
isMember('RH','OACCT',member).
isMember('VU','OACCT',member).
isMember('RMM','OACCT',member).
isMember('RN','OACCT',member).
isMember('TN','OACCT',member).
isMember('RCB','OACCT',member).
isMember('ZRE','OACCT',member).
isMember('BEN','OACCT',member).
isMember('BF','OACCT',member).
isMember('RT','OACCT',member).
isMember('CI','OACCT',member).
isMember('BI','OACCT',member).
isMember('RWA','OACCT',member).
isMember('CAM','OACCT',member).
isMember('RCA','OACCT',member).
isMember('TCH','OACCT',member).
isMember('GQ','OACCT',member).
isMember('G','OACCT',member).
isMember('COM','OACCT',member).
isMember('RG','OACCT',member).
isMember('DJI','OACCT',member).
isMember('SN','OACCT',member).
isMember('RM','OACCT',member).
isMember('MS','OACCT',member).
isMember('SY','OACCT',member).
isMember('ET','OACCT','associate member').
isMember('WL','OACCT','associate member').
isMember('RIM','OACCT','associate member').
isMember('MA','OACCT','associate member').
isMember('GNB','OACCT','associate member').
isMember('AG','OOPANAL',member).
isMember('BS','OOPANAL',member).
isMember('BDS','OOPANAL',member).
isMember('BZ','OOPANAL',member).
isMember('GCA','OOPANAL',member).
isMember('MEX','OOPANAL',member).
isMember('CR','OOPANAL',member).
isMember('NIC','OOPANAL',member).
isMember('PA','OOPANAL',member).
isMember('WD','OOPANAL',member).
isMember('DOM','OOPANAL',member).
isMember('RH','OOPANAL',member).
isMember('ES','OOPANAL',member).
isMember('HCA','OOPANAL',member).
isMember('WG','OOPANAL',member).
isMember('JA','OOPANAL',member).
isMember('CO','OOPANAL',member).
isMember('WV','OOPANAL',member).
isMember('TT','OOPANAL',member).
isMember('RA','OOPANAL',member).
isMember('BOL','OOPANAL',member).
isMember('BR','OOPANAL',member).
isMember('RCH','OOPANAL',member).
isMember('PY','OOPANAL',member).
isMember('ROU','OOPANAL',member).
isMember('PE','OOPANAL',member).
isMember('SME','OOPANAL',member).
isMember('YV','OOPANAL',member).
isMember('EC','OOPANAL',member).
isMember('CO','OAG',member).
isMember('BOL','OAG',member).
isMember('PE','OAG',member).
isMember('YV','OAG',member).
isMember('EC','OAG',member).
isMember('PA','OAG','associate member').
isMember('F','OAG',observer).
isMember('E','OAG',observer).
isMember('A','OAG',observer).
isMember('D','OAG',observer).
isMember('I','OAG',observer).
isMember('CH','OAG',observer).
isMember('B','OAG',observer).
isMember('NL','OAG',observer).
isMember('DK','OAG',observer).
isMember('SF','OAG',observer).
isMember('S','OAG',observer).
isMember('GB','OAG',observer).
isMember('IND','OAG',observer).
isMember('IL','OAG',observer).
isMember('ET','OAG',observer).
isMember('J','OAG',observer).
isMember('MEX','OAG',observer).
isMember('CDN','OAG',observer).
isMember('USA','OAG',observer).
isMember('CR','OAG',observer).
isMember('AUS','OAG',observer).
isMember('RA','OAG',observer).
isMember('BR','OAG',observer).
isMember('PY','OAG',observer).
isMember('ROU','OAG',observer).
isMember('BRN','OABEDA',member).
isMember('ET','OABEDA',member).
isMember('IRQ','OABEDA',member).
isMember('JOR','OABEDA',member).
isMember('KWT','OABEDA',member).
isMember('SA','OABEDA',member).
isMember('SYR','OABEDA',member).
isMember('RL','OABEDA',member).
isMember('OM','OABEDA',member).
isMember('UAE','OABEDA',member).
isMember('Q','OABEDA',member).
isMember('DZ','OABEDA',member).
isMember('LAR','OABEDA',member).
isMember('RIM','OABEDA',member).
isMember('MA','OABEDA',member).
isMember('TN','OABEDA',member).
isMember('SUD','OABEDA',member).
isMember('ET','OACC',member).
isMember('IRQ','OACC',member).
isMember('JOR','OACC',member).
isMember('YE','OACC',member).
isMember('BRN','OAFESD',member).
isMember('ET','OAFESD',member).
isMember('IRQ','OAFESD',member).
isMember('JOR','OAFESD',member).
isMember('KWT','OAFESD',member).
isMember('SA','OAFESD',member).
isMember('SYR','OAFESD',member).
isMember('RL','OAFESD',member).
isMember('OM','OAFESD',member).
isMember('UAE','OAFESD',member).
isMember('YE','OAFESD',member).
isMember('Q','OAFESD',member).
isMember('DZ','OAFESD',member).
isMember('LAR','OAFESD',member).
isMember('RIM','OAFESD',member).
isMember('MA','OAFESD',member).
isMember('TN','OAFESD',member).
isMember('SUD','OAFESD',member).
isMember('DJI','OAFESD',member).
isMember('SP','OAFESD',member).
isMember('BRN','OAL',member).
isMember('ET','OAL',member).
isMember('IRQ','OAL',member).
isMember('JOR','OAL',member).
isMember('KWT','OAL',member).
isMember('SA','OAL',member).
isMember('SYR','OAL',member).
isMember('RL','OAL',member).
isMember('OM','OAL',member).
isMember('UAE','OAL',member).
isMember('YE','OAL',member).
isMember('Q','OAL',member).
isMember('DZ','OAL',member).
isMember('LAR','OAL',member).
isMember('RIM','OAL',member).
isMember('MA','OAL',member).
isMember('TN','OAL',member).
isMember('SUD','OAL',member).
isMember('COM','OAL',member).
isMember('DJI','OAL',member).
isMember('SP','OAL',member).
isMember('DZ','OAMU',member).
isMember('LAR','OAMU',member).
isMember('RIM','OAMU',member).
isMember('MA','OAMU',member).
isMember('TN','OAMU',member).
isMember('BRN','OAMF',member).
isMember('ET','OAMF',member).
isMember('IRQ','OAMF',member).
isMember('JOR','OAMF',member).
isMember('KWT','OAMF',member).
isMember('SA','OAMF',member).
isMember('SYR','OAMF',member).
isMember('RL','OAMF',member).
isMember('OM','OAMF',member).
isMember('UAE','OAMF',member).
isMember('YE','OAMF',member).
isMember('Q','OAMF',member).
isMember('DZ','OAMF',member).
isMember('LAR','OAMF',member).
isMember('RIM','OAMF',member).
isMember('MA','OAMF',member).
isMember('TN','OAMF',member).
isMember('SUD','OAMF',member).
isMember('SP','OAMF',member).
isMember('CN','OAPEC',member).
isMember('HONX','OAPEC',member).
isMember('PNG','OAPEC',member).
isMember('J','OAPEC',member).
isMember('ROK','OAPEC',member).
isMember('RC','OAPEC',member).
isMember('MEX','OAPEC',member).
isMember('CDN','OAPEC',member).
isMember('USA','OAPEC',member).
isMember('NZ','OAPEC',member).
isMember('RCH','OAPEC',member).
isMember('AFG','OAsDB','regional member').
isMember('CN','OAsDB','regional member').
isMember('PK','OAsDB','regional member').
isMember('UZB','OAsDB','regional member').
isMember('BD','OAsDB','regional member').
isMember('MYA','OAsDB','regional member').
isMember('IND','OAsDB','regional member').
isMember('BHT','OAsDB','regional member').
isMember('MAL','OAsDB','regional member').
isMember('LAO','OAsDB','regional member').
isMember('THA','OAsDB','regional member').
isMember('K','OAsDB','regional member').
isMember('VN','OAsDB','regional member').
isMember('KAZ','OAsDB','regional member').
isMember('KGZ','OAsDB','regional member').
isMember('MNG','OAsDB','regional member').
isMember('NEP','OAsDB','regional member').
isMember('RI','OAsDB','regional member').
isMember('PNG','OAsDB','regional member').
isMember('J','OAsDB','regional member').
isMember('ROK','OAsDB','regional member').
isMember('MV','OAsDB','regional member').
isMember('RP','OAsDB','regional member').
isMember('SGP','OAsDB','regional member').
isMember('CL','OAsDB','regional member').
isMember('RC','OAsDB','regional member').
isMember('AUS','OAsDB','regional member').
isMember('FJI','OAsDB','regional member').
isMember('KIR','OAsDB','regional member').
isMember('MH','OAsDB','regional member').
isMember('NAU','OAsDB','regional member').
isMember('NZ','OAsDB','regional member').
isMember('SLB','OAsDB','regional member').
isMember('TO','OAsDB','regional member').
isMember('TUV','OAsDB','regional member').
isMember('VU','OAsDB','regional member').
isMember('WS','OAsDB','regional member').
isMember('F','OAsDB','nonregional member').
isMember('E','OAsDB','nonregional member').
isMember('A','OAsDB','nonregional member').
isMember('D','OAsDB','nonregional member').
isMember('I','OAsDB','nonregional member').
isMember('CH','OAsDB','nonregional member').
isMember('B','OAsDB','nonregional member').
isMember('NL','OAsDB','nonregional member').
isMember('TR','OAsDB','nonregional member').
isMember('DK','OAsDB','nonregional member').
isMember('SF','OAsDB','nonregional member').
isMember('N','OAsDB','nonregional member').
isMember('S','OAsDB','nonregional member').
isMember('GB','OAsDB','nonregional member').
isMember('CDN','OAsDB','nonregional member').
isMember('USA','OAsDB','nonregional member').
isMember('BRU','OASEAN',member).
isMember('MAL','OASEAN',member).
isMember('THA','OASEAN',member).
isMember('VN','OASEAN',member).
isMember('RI','OASEAN',member).
isMember('RP','OASEAN',member).
isMember('SGP','OASEAN',member).
isMember('LAO','OASEAN',observer).
isMember('CN','OMekong Group',member).
isMember('MYA','OMekong Group',member).
isMember('BRU','OMekong Group',member).
isMember('MAL','OMekong Group',member).
isMember('LAO','OMekong Group',member).
isMember('THA','OMekong Group',member).
isMember('K','OMekong Group',member).
isMember('VN','OMekong Group',member).
isMember('RI','OMekong Group',member).
isMember('RP','OMekong Group',member).
isMember('SGP','OMekong Group',member).
isMember('USA','OANZUS',member).
isMember('AUS','OANZUS',member).
isMember('NZ','OANZUS',member).
isMember('GR','OBIS',member).
isMember('F','OBIS',member).
isMember('E','OBIS',member).
isMember('A','OBIS',member).
isMember('CZ','OBIS',member).
isMember('D','OBIS',member).
isMember('H','OBIS',member).
isMember('I','OBIS',member).
isMember('SK','OBIS',member).
isMember('CH','OBIS',member).
isMember('LV','OBIS',member).
isMember('LT','OBIS',member).
isMember('PL','OBIS',member).
isMember('B','OBIS',member).
isMember('NL','OBIS',member).
isMember('BG','OBIS',member).
isMember('RO','OBIS',member).
isMember('TR','OBIS',member).
isMember('DK','OBIS',member).
isMember('EW','OBIS',member).
isMember('SF','OBIS',member).
isMember('N','OBIS',member).
isMember('S','OBIS',member).
isMember('IS','OBIS',member).
isMember('IRL','OBIS',member).
isMember('P','OBIS',member).
isMember('GB','OBIS',member).
isMember('HONX','OBIS',member).
isMember('J','OBIS',member).
isMember('CDN','OBIS',member).
isMember('USA','OBIS',member).
isMember('AUS','OBIS',member).
isMember('RSA','OBIS',member).
isMember('B','OBenelux',member).
isMember('L','OBenelux',member).
isMember('NL','OBenelux',member).
isMember('AL','OBSEC',member).
isMember('GR','OBSEC',member).
isMember('UA','OBSEC',member).
isMember('R','OBSEC',member).
isMember('BG','OBSEC',member).
isMember('RO','OBSEC',member).
isMember('TR','OBSEC',member).
isMember('MD','OBSEC',member).
isMember('ARM','OBSEC',member).
isMember('GE','OBSEC',member).
isMember('AZ','OBSEC',member).
isMember('SRB','OBSEC',observer).
isMember('SK','OBSEC',observer).
isMember('PL','OBSEC',observer).
isMember('IL','OBSEC',observer).
isMember('ET','OBSEC',observer).
isMember('TN','OBSEC',observer).
isMember('AG','OCaricom',member).
isMember('BS','OCaricom',member).
isMember('BDS','OCaricom',member).
isMember('BZ','OCaricom',member).
isMember('WD','OCaricom',member).
isMember('WG','OCaricom',member).
isMember('JA','OCaricom',member).
isMember('MNTS','OCaricom',member).
isMember('KN','OCaricom',member).
isMember('WL','OCaricom',member).
isMember('WV','OCaricom',member).
isMember('TT','OCaricom',member).
isMember('GUY','OCaricom',member).
isMember('SME','OCaricom',member).
isMember('BVIR','OCaricom','associate member').
isMember('TUCA','OCaricom','associate member').
isMember('AXA','OCaricom',observer).
isMember('ARU','OCaricom',observer).
isMember('MEX','OCaricom',observer).
isMember('BERM','OCaricom',observer).
isMember('CAYM','OCaricom',observer).
isMember('DOM','OCaricom',observer).
isMember('RH','OCaricom',observer).
isMember('NA','OCaricom',observer).
isMember('PR','OCaricom',observer).
isMember('YV','OCaricom',observer).
isMember('AXA','OCDB','regional member').
isMember('AG','OCDB','regional member').
isMember('BS','OCDB','regional member').
isMember('BDS','OCDB','regional member').
isMember('BZ','OCDB','regional member').
isMember('MEX','OCDB','regional member').
isMember('BVIR','OCDB','regional member').
isMember('CAYM','OCDB','regional member').
isMember('WD','OCDB','regional member').
isMember('WG','OCDB','regional member').
isMember('JA','OCDB','regional member').
isMember('MNTS','OCDB','regional member').
isMember('CO','OCDB','regional member').
isMember('KN','OCDB','regional member').
isMember('WL','OCDB','regional member').
isMember('WV','OCDB','regional member').
isMember('TT','OCDB','regional member').
isMember('TUCA','OCDB','regional member').
isMember('GUY','OCDB','regional member').
isMember('YV','OCDB','regional member').
isMember('F','OCDB','nonregional member').
isMember('D','OCDB','nonregional member').
isMember('I','OCDB','nonregional member').
isMember('GB','OCDB','nonregional member').
isMember('CDN','OCDB','nonregional member').
isMember('RCB','OUDEAC',member).
isMember('CAM','OUDEAC',member).
isMember('RCA','OUDEAC',member).
isMember('TCH','OUDEAC',member).
isMember('GQ','OUDEAC',member).
isMember('G','OUDEAC',member).
isMember('F','OBDEAC',member).
isMember('D','OBDEAC',member).
isMember('KWT','OBDEAC',member).
isMember('RCB','OBDEAC',member).
isMember('CAM','OBDEAC',member).
isMember('RCA','OBDEAC',member).
isMember('TCH','OBDEAC',member).
isMember('GQ','OBDEAC',member).
isMember('G','OBDEAC',member).
isMember('GCA','OBCIE',member).
isMember('CR','OBCIE',member).
isMember('NIC','OBCIE',member).
isMember('ES','OBCIE',member).
isMember('HCA','OBCIE',member).
isMember('RC','OBCIE','nonregional member').
isMember('MEX','OBCIE','nonregional member').
isMember('RA','OBCIE','nonregional member').
isMember('YV','OBCIE','nonregional member').
isMember('GCA','OCACM',member).
isMember('CR','OCACM',member).
isMember('NIC','OCACM',member).
isMember('ES','OCACM',member).
isMember('HCA','OCACM',member).
isMember('MK','OCEI',member).
isMember('A','OCEI',member).
isMember('CZ','OCEI',member).
isMember('H','OCEI',member).
isMember('I','OCEI',member).
isMember('SK','OCEI',member).
isMember('SLO','OCEI',member).
isMember('PL','OCEI',member).
isMember('BIH','OCEI',member).
isMember('HR','OCEI',member).
isMember('SRB','OCEI','associate member').
isMember('MNE','OCEI','associate member').
isMember('BY','OCEI','associate member').
isMember('UA','OCEI','associate member').
isMember('BG','OCEI','associate member').
isMember('RO','OCEI','associate member').
isMember('AFG','OCP',member).
isMember('IR','OCP',member).
isMember('PK','OCP',member).
isMember('BD','OCP',member).
isMember('MYA','OCP',member).
isMember('IND','OCP',member).
isMember('BHT','OCP',member).
isMember('MAL','OCP',member).
isMember('LAO','OCP',member).
isMember('THA','OCP',member).
isMember('K','OCP',member).
isMember('NEP','OCP',member).
isMember('RI','OCP',member).
isMember('PNG','OCP',member).
isMember('J','OCP',member).
isMember('ROK','OCP',member).
isMember('MV','OCP',member).
isMember('RP','OCP',member).
isMember('SGP','OCP',member).
isMember('CL','OCP',member).
isMember('USA','OCP',member).
isMember('AUS','OCP',member).
isMember('FJI','OCP',member).
isMember('NZ','OCP',member).
isMember('M','OC',member).
isMember('GB','OC',member).
isMember('PK','OC',member).
isMember('BD','OC',member).
isMember('IND','OC',member).
isMember('BRU','OC',member).
isMember('MAL','OC',member).
isMember('CY','OC',member).
isMember('PNG','OC',member).
isMember('MV','OC',member).
isMember('SGP','OC',member).
isMember('CL','OC',member).
isMember('AG','OC',member).
isMember('BS','OC',member).
isMember('BDS','OC',member).
isMember('BZ','OC',member).
isMember('CDN','OC',member).
isMember('WD','OC',member).
isMember('WG','OC',member).
isMember('JA','OC',member).
isMember('KN','OC',member).
isMember('WL','OC',member).
isMember('WV','OC',member).
isMember('TT','OC',member).
isMember('AUS','OC',member).
isMember('KIR','OC',member).
isMember('NZ','OC',member).
isMember('SLB','OC',member).
isMember('TO','OC',member).
isMember('VU','OC',member).
isMember('WS','OC',member).
isMember('GUY','OC',member).
isMember('NAM','OC',member).
isMember('Z','OC',member).
isMember('WAN','OC',member).
isMember('RB','OC',member).
isMember('RSA','OC',member).
isMember('ZW','OC',member).
isMember('GH','OC',member).
isMember('EAT','OC',member).
isMember('EAK','OC',member).
isMember('WAG','OC',member).
isMember('WAL','OC',member).
isMember('EAU','OC',member).
isMember('LS','OC',member).
isMember('MW','OC',member).
isMember('MS','OC',member).
isMember('SD','OC',member).
isMember('SY','OC',member).
isMember('NAU','OC','special member').
isMember('TUV','OC','special member').
isMember('BY','OCIS',member).
isMember('UA','OCIS',member).
isMember('R','OCIS',member).
isMember('MD','OCIS',member).
isMember('TAD','OCIS',member).
isMember('TM','OCIS',member).
isMember('UZB','OCIS',member).
isMember('ARM','OCIS',member).
isMember('GE','OCIS',member).
isMember('AZ','OCIS',member).
isMember('KAZ','OCIS',member).
isMember('KGZ','OCIS',member).
isMember('ET','OCAEU',member).
isMember('IRQ','OCAEU',member).
isMember('JOR','OCAEU',member).
isMember('KWT','OCAEU',member).
isMember('SYR','OCAEU',member).
isMember('UAE','OCAEU',member).
isMember('YE','OCAEU',member).
isMember('LAR','OCAEU',member).
isMember('RIM','OCAEU',member).
isMember('SUD','OCAEU',member).
isMember('SP','OCAEU',member).
isMember('AL','OCE',member).
isMember('GR','OCE',member).
isMember('MK','OCE',member).
isMember('SRB','OCE',member).
isMember('MNE','OCE',member).
isMember('AND','OCE',member).
isMember('F','OCE',member).
isMember('E','OCE',member).
isMember('A','OCE',member).
isMember('CZ','OCE',member).
isMember('D','OCE',member).
isMember('H','OCE',member).
isMember('I','OCE',member).
isMember('FL','OCE',member).
isMember('SK','OCE',member).
isMember('SLO','OCE',member).
isMember('CH','OCE',member).
isMember('LV','OCE',member).
isMember('LT','OCE',member).
isMember('PL','OCE',member).
isMember('UA','OCE',member).
isMember('R','OCE',member).
isMember('B','OCE',member).
isMember('L','OCE',member).
isMember('NL','OCE',member).
isMember('BG','OCE',member).
isMember('RO','OCE',member).
isMember('TR','OCE',member).
isMember('DK','OCE',member).
isMember('EW','OCE',member).
isMember('SF','OCE',member).
isMember('N','OCE',member).
isMember('S','OCE',member).
isMember('IS','OCE',member).
isMember('IRL','OCE',member).
isMember('RSM','OCE',member).
isMember('M','OCE',member).
isMember('MD','OCE',member).
isMember('P','OCE',member).
isMember('GB','OCE',member).
isMember('CY','OCE',member).
isMember('IL','OCE',observer).
isMember('BY','OCE',guest).
isMember('BIH','OCE',guest).
isMember('HR','OCE',guest).
isMember('D','OCBSS',member).
isMember('LV','OCBSS',member).
isMember('LT','OCBSS',member).
isMember('PL','OCBSS',member).
isMember('R','OCBSS',member).
isMember('DK','OCBSS',member).
isMember('EW','OCBSS',member).
isMember('SF','OCBSS',member).
isMember('N','OCBSS',member).
isMember('S','OCBSS',member).
isMember('RN','OEntente',member).
isMember('BEN','OEntente',member).
isMember('BF','OEntente',member).
isMember('RT','OEntente',member).
isMember('CI','OEntente',member).
isMember('AL','OCCC',member).
isMember('GR','OCCC',member).
isMember('MK','OCCC',member).
isMember('F','OCCC',member).
isMember('E','OCCC',member).
isMember('A','OCCC',member).
isMember('CZ','OCCC',member).
isMember('D','OCCC',member).
isMember('H','OCCC',member).
isMember('I','OCCC',member).
isMember('SK','OCCC',member).
isMember('SLO','OCCC',member).
isMember('CH','OCCC',member).
isMember('BY','OCCC',member).
isMember('LV','OCCC',member).
isMember('LT','OCCC',member).
isMember('PL','OCCC',member).
isMember('UA','OCCC',member).
isMember('R','OCCC',member).
isMember('B','OCCC',member).
isMember('L','OCCC',member).
isMember('NL','OCCC',member).
isMember('HR','OCCC',member).
isMember('BG','OCCC',member).
isMember('RO','OCCC',member).
isMember('TR','OCCC',member).
isMember('DK','OCCC',member).
isMember('EW','OCCC',member).
isMember('SF','OCCC',member).
isMember('N','OCCC',member).
isMember('S','OCCC',member).
isMember('IS','OCCC',member).
isMember('IRL','OCCC',member).
isMember('M','OCCC',member).
isMember('MD','OCCC',member).
isMember('P','OCCC',member).
isMember('GB','OCCC',member).
isMember('CN','OCCC',member).
isMember('IR','OCCC',member).
isMember('PK','OCCC',member).
isMember('TM','OCCC',member).
isMember('UZB','OCCC',member).
isMember('ARM','OCCC',member).
isMember('GE','OCCC',member).
isMember('AZ','OCCC',member).
isMember('BD','OCCC',member).
isMember('MYA','OCCC',member).
isMember('IND','OCCC',member).
isMember('MAL','OCCC',member).
isMember('THA','OCCC',member).
isMember('VN','OCCC',member).
isMember('KAZ','OCCC',member).
isMember('MNG','OCCC',member).
isMember('NEP','OCCC',member).
isMember('CY','OCCC',member).
isMember('IL','OCCC',member).
isMember('ET','OCCC',member).
isMember('RI','OCCC',member).
isMember('IRQ','OCCC',member).
isMember('JOR','OCCC',member).
isMember('KWT','OCCC',member).
isMember('SA','OCCC',member).
isMember('SYR','OCCC',member).
isMember('RL','OCCC',member).
isMember('J','OCCC',member).
isMember('ROK','OCCC',member).
isMember('UAE','OCCC',member).
isMember('YE','OCCC',member).
isMember('RP','OCCC',member).
isMember('Q','OCCC',member).
isMember('SGP','OCCC',member).
isMember('CL','OCCC',member).
isMember('BS','OCCC',member).
isMember('GCA','OCCC',member).
isMember('MEX','OCCC',member).
isMember('CDN','OCCC',member).
isMember('USA','OCCC',member).
isMember('C','OCCC',member).
isMember('RH','OCCC',member).
isMember('JA','OCCC',member).
isMember('CO','OCCC',member).
isMember('TT','OCCC',member).
isMember('AUS','OCCC',member).
isMember('NZ','OCCC',member).
isMember('RA','OCCC',member).
isMember('BR','OCCC',member).
isMember('RCH','OCCC',member).
isMember('PY','OCCC',member).
isMember('ROU','OCCC',member).
isMember('PE','OCCC',member).
isMember('GUY','OCCC',member).
isMember('DZ','OCCC',member).
isMember('LAR','OCCC',member).
isMember('RMM','OCCC',member).
isMember('RIM','OCCC',member).
isMember('MA','OCCC',member).
isMember('RN','OCCC',member).
isMember('TN','OCCC',member).
isMember('ANG','OCCC',member).
isMember('RCB','OCCC',member).
isMember('NAM','OCCC',member).
isMember('ZRE','OCCC',member).
isMember('Z','OCCC',member).
isMember('BF','OCCC',member).
isMember('WAN','OCCC',member).
isMember('RT','OCCC',member).
isMember('RB','OCCC',member).
isMember('RSA','OCCC',member).
isMember('ZW','OCCC',member).
isMember('CI','OCCC',member).
isMember('GH','OCCC',member).
isMember('BI','OCCC',member).
isMember('RWA','OCCC',member).
isMember('EAT','OCCC',member).
isMember('CAM','OCCC',member).
isMember('RCA','OCCC',member).
isMember('G','OCCC',member).
isMember('CV','OCCC',member).
isMember('SUD','OCCC',member).
isMember('COM','OCCC',member).
isMember('RG','OCCC',member).
isMember('LB','OCCC',member).
isMember('ETH','OCCC',member).
isMember('EAK','OCCC',member).
isMember('WAG','OCCC',member).
isMember('SN','OCCC',member).
isMember('WAL','OCCC',member).
isMember('EAU','OCCC',member).
isMember('LS','OCCC',member).
isMember('RM','OCCC',member).
isMember('MW','OCCC',member).
isMember('MOC','OCCC',member).
isMember('MS','OCCC',member).
isMember('SD','OCCC',member).
isMember('EAT','OEADB',member).
isMember('EAK','OEADB',member).
isMember('EAU','OEADB',member).
isMember('F','OESCAP',member).
isMember('R','OESCAP',member).
isMember('NL','OESCAP',member).
isMember('GB','OESCAP',member).
isMember('AFG','OESCAP',member).
isMember('CN','OESCAP',member).
isMember('IR','OESCAP',member).
isMember('PK','OESCAP',member).
isMember('TAD','OESCAP',member).
isMember('TM','OESCAP',member).
isMember('UZB','OESCAP',member).
isMember('ARM','OESCAP',member).
isMember('AZ','OESCAP',member).
isMember('BD','OESCAP',member).
isMember('MYA','OESCAP',member).
isMember('IND','OESCAP',member).
isMember('BHT','OESCAP',member).
isMember('BRU','OESCAP',member).
isMember('MAL','OESCAP',member).
isMember('LAO','OESCAP',member).
isMember('THA','OESCAP',member).
isMember('K','OESCAP',member).
isMember('VN','OESCAP',member).
isMember('KAZ','OESCAP',member).
isMember('NOK','OESCAP',member).
isMember('KGZ','OESCAP',member).
isMember('MNG','OESCAP',member).
isMember('NEP','OESCAP',member).
isMember('RI','OESCAP',member).
isMember('PNG','OESCAP',member).
isMember('J','OESCAP',member).
isMember('ROK','OESCAP',member).
isMember('MV','OESCAP',member).
isMember('RP','OESCAP',member).
isMember('SGP','OESCAP',member).
isMember('CL','OESCAP',member).
isMember('USA','OESCAP',member).
isMember('AUS','OESCAP',member).
isMember('FJI','OESCAP',member).
isMember('KIR','OESCAP',member).
isMember('MH','OESCAP',member).
isMember('NAU','OESCAP',member).
isMember('NZ','OESCAP',member).
isMember('SLB','OESCAP',member).
isMember('TO','OESCAP',member).
isMember('TUV','OESCAP',member).
isMember('VU','OESCAP',member).
isMember('WS','OESCAP',member).
isMember('FPOL','OESCAP','associate member').
isMember('NMIS','OESCAP','associate member').
isMember('PAL','OESCAP','associate member').
isMember('BRN','OESCWA',member).
isMember('ET','OESCWA',member).
isMember('IRQ','OESCWA',member).
isMember('JOR','OESCWA',member).
isMember('KWT','OESCWA',member).
isMember('SA','OESCWA',member).
isMember('SYR','OESCWA',member).
isMember('RL','OESCWA',member).
isMember('OM','OESCWA',member).
isMember('UAE','OESCWA',member).
isMember('YE','OESCWA',member).
isMember('Q','OESCWA',member).
isMember('ET','OECA',member).
isMember('DZ','OECA',member).
isMember('LAR','OECA',member).
isMember('RMM','OECA',member).
isMember('RIM','OECA',member).
isMember('MA','OECA',member).
isMember('RN','OECA',member).
isMember('TN','OECA',member).
isMember('ANG','OECA',member).
isMember('RCB','OECA',member).
isMember('NAM','OECA',member).
isMember('ZRE','OECA',member).
isMember('Z','OECA',member).
isMember('BEN','OECA',member).
isMember('BF','OECA',member).
isMember('WAN','OECA',member).
isMember('RT','OECA',member).
isMember('RB','OECA',member).
isMember('RSA','OECA',member).
isMember('ZW','OECA',member).
isMember('CI','OECA',member).
isMember('GH','OECA',member).
isMember('BI','OECA',member).
isMember('RWA','OECA',member).
isMember('EAT','OECA',member).
isMember('CAM','OECA',member).
isMember('RCA','OECA',member).
isMember('TCH','OECA',member).
isMember('GQ','OECA',member).
isMember('G','OECA',member).
isMember('CV','OECA',member).
isMember('SUD','OECA',member).
isMember('COM','OECA',member).
isMember('RG','OECA',member).
isMember('LB','OECA',member).
isMember('DJI','OECA',member).
isMember('ER','OECA',member).
isMember('ETH','OECA',member).
isMember('SP','OECA',member).
isMember('EAK','OECA',member).
isMember('WAG','OECA',member).
isMember('SN','OECA',member).
isMember('GNB','OECA',member).
isMember('WAL','OECA',member).
isMember('EAU','OECA',member).
isMember('LS','OECA',member).
isMember('RM','OECA',member).
isMember('MW','OECA',member).
isMember('MOC','OECA',member).
isMember('MS','OECA',member).
isMember('SD','OECA',member).
isMember('STP','OECA',member).
isMember('SY','OECA',member).
isMember('F','OECA','associate member').
isMember('GB','OECA','associate member').
isMember('AL','OECE',member).
isMember('GR','OECE',member).
isMember('MK','OECE',member).
isMember('AND','OECE',member).
isMember('F','OECE',member).
isMember('E','OECE',member).
isMember('A','OECE',member).
isMember('CZ','OECE',member).
isMember('D','OECE',member).
isMember('H','OECE',member).
isMember('I','OECE',member).
isMember('FL','OECE',member).
isMember('SK','OECE',member).
isMember('SLO','OECE',member).
isMember('CH','OECE',member).
isMember('BY','OECE',member).
isMember('LV','OECE',member).
isMember('LT','OECE',member).
isMember('PL','OECE',member).
isMember('UA','OECE',member).
isMember('R','OECE',member).
isMember('B','OECE',member).
isMember('L','OECE',member).
isMember('NL','OECE',member).
isMember('BIH','OECE',member).
isMember('HR','OECE',member).
isMember('BG','OECE',member).
isMember('RO','OECE',member).
isMember('TR','OECE',member).
isMember('DK','OECE',member).
isMember('EW','OECE',member).
isMember('SF','OECE',member).
isMember('N','OECE',member).
isMember('S','OECE',member).
isMember('MC','OECE',member).
isMember('IS','OECE',member).
isMember('IRL','OECE',member).
isMember('RSM','OECE',member).
isMember('M','OECE',member).
isMember('MD','OECE',member).
isMember('P','OECE',member).
isMember('GB','OECE',member).
isMember('TAD','OECE',member).
isMember('TM','OECE',member).
isMember('UZB','OECE',member).
isMember('ARM','OECE',member).
isMember('GE','OECE',member).
isMember('AZ','OECE',member).
isMember('KAZ','OECE',member).
isMember('KGZ','OECE',member).
isMember('CY','OECE',member).
isMember('IL','OECE',member).
isMember('CDN','OECE',member).
isMember('USA','OECE',member).
isMember('F','OECLAC',member).
isMember('E','OECLAC',member).
isMember('I','OECLAC',member).
isMember('NL','OECLAC',member).
isMember('P','OECLAC',member).
isMember('GB','OECLAC',member).
isMember('AG','OECLAC',member).
isMember('BS','OECLAC',member).
isMember('BDS','OECLAC',member).
isMember('BZ','OECLAC',member).
isMember('GCA','OECLAC',member).
isMember('MEX','OECLAC',member).
isMember('CDN','OECLAC',member).
isMember('USA','OECLAC',member).
isMember('CR','OECLAC',member).
isMember('NIC','OECLAC',member).
isMember('PA','OECLAC',member).
isMember('C','OECLAC',member).
isMember('WD','OECLAC',member).
isMember('DOM','OECLAC',member).
isMember('RH','OECLAC',member).
isMember('ES','OECLAC',member).
isMember('HCA','OECLAC',member).
isMember('WG','OECLAC',member).
isMember('JA','OECLAC',member).
isMember('CO','OECLAC',member).
isMember('KN','OECLAC',member).
isMember('WL','OECLAC',member).
isMember('WV','OECLAC',member).
isMember('TT','OECLAC',member).
isMember('RA','OECLAC',member).
isMember('BOL','OECLAC',member).
isMember('BR','OECLAC',member).
isMember('RCH','OECLAC',member).
isMember('PY','OECLAC',member).
isMember('ROU','OECLAC',member).
isMember('PE','OECLAC',member).
isMember('GUY','OECLAC',member).
isMember('SME','OECLAC',member).
isMember('YV','OECLAC',member).
isMember('EC','OECLAC',member).
isMember('BVIR','OECLAC','associate member').
isMember('VIRG','OECLAC','associate member').
isMember('RCB','OCEEAC',member).
isMember('ZRE','OCEEAC',member).
isMember('BI','OCEEAC',member).
isMember('RWA','OCEEAC',member).
isMember('CAM','OCEEAC',member).
isMember('RCA','OCEEAC',member).
isMember('TCH','OCEEAC',member).
isMember('GQ','OCEEAC',member).
isMember('G','OCEEAC',member).
isMember('STP','OCEEAC',member).
isMember('ANG','OCEEAC',observer).
isMember('ZRE','OCEPGL',member).
isMember('BI','OCEPGL',member).
isMember('RWA','OCEPGL',member).
isMember('RMM','OECOWAS',member).
isMember('RIM','OECOWAS',member).
isMember('RN','OECOWAS',member).
isMember('BEN','OECOWAS',member).
isMember('BF','OECOWAS',member).
isMember('WAN','OECOWAS',member).
isMember('RT','OECOWAS',member).
isMember('CI','OECOWAS',member).
isMember('GH','OECOWAS',member).
isMember('CV','OECOWAS',member).
isMember('RG','OECOWAS',member).
isMember('LB','OECOWAS',member).
isMember('WAG','OECOWAS',member).
isMember('SN','OECOWAS',member).
isMember('GNB','OECOWAS',member).
isMember('WAL','OECOWAS',member).
isMember('TR','OECO',member).
isMember('AFG','OECO',member).
isMember('IR','OECO',member).
isMember('PK','OECO',member).
isMember('TAD','OECO',member).
isMember('TM','OECO',member).
isMember('UZB','OECO',member).
isMember('AZ','OECO',member).
isMember('KAZ','OECO',member).
isMember('KGZ','OECO',member).
isMember('AL','OEBRD',member).
isMember('GR','OEBRD',member).
isMember('MK','OEBRD',member).
isMember('SRB','OEBRD',member).
isMember('MNE','OEBRD',member).
isMember('F','OEBRD',member).
isMember('E','OEBRD',member).
isMember('A','OEBRD',member).
isMember('CZ','OEBRD',member).
isMember('D','OEBRD',member).
isMember('H','OEBRD',member).
isMember('I','OEBRD',member).
isMember('FL','OEBRD',member).
isMember('SK','OEBRD',member).
isMember('SLO','OEBRD',member).
isMember('CH','OEBRD',member).
isMember('BY','OEBRD',member).
isMember('LV','OEBRD',member).
isMember('LT','OEBRD',member).
isMember('PL','OEBRD',member).
isMember('UA','OEBRD',member).
isMember('R','OEBRD',member).
isMember('B','OEBRD',member).
isMember('L','OEBRD',member).
isMember('NL','OEBRD',member).
isMember('HR','OEBRD',member).
isMember('BG','OEBRD',member).
isMember('RO','OEBRD',member).
isMember('TR','OEBRD',member).
isMember('DK','OEBRD',member).
isMember('EW','OEBRD',member).
isMember('SF','OEBRD',member).
isMember('N','OEBRD',member).
isMember('S','OEBRD',member).
isMember('IS','OEBRD',member).
isMember('IRL','OEBRD',member).
isMember('M','OEBRD',member).
isMember('MD','OEBRD',member).
isMember('P','OEBRD',member).
isMember('GB','OEBRD',member).
isMember('TAD','OEBRD',member).
isMember('TM','OEBRD',member).
isMember('UZB','OEBRD',member).
isMember('ARM','OEBRD',member).
isMember('GE','OEBRD',member).
isMember('AZ','OEBRD',member).
isMember('KAZ','OEBRD',member).
isMember('KGZ','OEBRD',member).
isMember('CY','OEBRD',member).
isMember('IL','OEBRD',member).
isMember('ET','OEBRD',member).
isMember('J','OEBRD',member).
isMember('ROK','OEBRD',member).
isMember('MEX','OEBRD',member).
isMember('CDN','OEBRD',member).
isMember('USA','OEBRD',member).
isMember('AUS','OEBRD',member).
isMember('NZ','OEBRD',member).
isMember('MA','OEBRD',member).
isMember('FL','OEFTA',member).
isMember('CH','OEFTA',member).
isMember('N','OEFTA',member).
isMember('IS','OEFTA',member).
isMember('GR','OEIB',member).
isMember('F','OEIB',member).
isMember('E','OEIB',member).
isMember('A','OEIB',member).
isMember('D','OEIB',member).
isMember('I','OEIB',member).
isMember('B','OEIB',member).
isMember('L','OEIB',member).
isMember('NL','OEIB',member).
isMember('DK','OEIB',member).
isMember('SF','OEIB',member).
isMember('S','OEIB',member).
isMember('IRL','OEIB',member).
isMember('P','OEIB',member).
isMember('GB','OEIB',member).
isMember('GR','OCERN',member).
isMember('F','OCERN',member).
isMember('E','OCERN',member).
isMember('A','OCERN',member).
isMember('CZ','OCERN',member).
isMember('D','OCERN',member).
isMember('H','OCERN',member).
isMember('I','OCERN',member).
isMember('SK','OCERN',member).
isMember('CH','OCERN',member).
isMember('PL','OCERN',member).
isMember('B','OCERN',member).
isMember('NL','OCERN',member).
isMember('DK','OCERN',member).
isMember('SF','OCERN',member).
isMember('N','OCERN',member).
isMember('S','OCERN',member).
isMember('P','OCERN',member).
isMember('GB','OCERN',member).
isMember('R','OCERN',observer).
isMember('TR','OCERN',observer).
isMember('IL','OCERN',observer).
isMember('F','OESA',member).
isMember('E','OESA',member).
isMember('A','OESA',member).
isMember('D','OESA',member).
isMember('I','OESA',member).
isMember('CH','OESA',member).
isMember('B','OESA',member).
isMember('NL','OESA',member).
isMember('DK','OESA',member).
isMember('SF','OESA',member).
isMember('N','OESA',member).
isMember('S','OESA',member).
isMember('IRL','OESA',member).
isMember('GB','OESA',member).
isMember('CDN','OESA','cooperating state').
isMember('GR','OEU',member).
isMember('F','OEU',member).
isMember('E','OEU',member).
isMember('A','OEU',member).
isMember('CZ','OEU',member).
isMember('D','OEU',member).
isMember('H','OEU',member).
isMember('I','OEU',member).
isMember('SK','OEU',member).
isMember('SLO','OEU',member).
isMember('LV','OEU',member).
isMember('LT','OEU',member).
isMember('PL','OEU',member).
isMember('B','OEU',member).
isMember('L','OEU',member).
isMember('NL','OEU',member).
isMember('BG','OEU',member).
isMember('RO','OEU',member).
isMember('DK','OEU',member).
isMember('EW','OEU',member).
isMember('SF','OEU',member).
isMember('S','OEU',member).
isMember('IRL','OEU',member).
isMember('M','OEU',member).
isMember('P','OEU',member).
isMember('GB','OEU',member).
isMember('CY','OEU',member).
isMember('MK','OEU','membership applicant').
isMember('HR','OEU','membership applicant').
isMember('TR','OEU','membership applicant').
isMember('AL','OFAO',member).
isMember('GR','OFAO',member).
isMember('MK','OFAO',member).
isMember('SRB','OFAO',member).
isMember('MNE','OFAO',member).
isMember('F','OFAO',member).
isMember('E','OFAO',member).
isMember('A','OFAO',member).
isMember('CZ','OFAO',member).
isMember('D','OFAO',member).
isMember('H','OFAO',member).
isMember('I','OFAO',member).
isMember('SK','OFAO',member).
isMember('SLO','OFAO',member).
isMember('CH','OFAO',member).
isMember('LV','OFAO',member).
isMember('LT','OFAO',member).
isMember('PL','OFAO',member).
isMember('B','OFAO',member).
isMember('L','OFAO',member).
isMember('NL','OFAO',member).
isMember('BIH','OFAO',member).
isMember('HR','OFAO',member).
isMember('BG','OFAO',member).
isMember('RO','OFAO',member).
isMember('TR','OFAO',member).
isMember('DK','OFAO',member).
isMember('EW','OFAO',member).
isMember('SF','OFAO',member).
isMember('N','OFAO',member).
isMember('S','OFAO',member).
isMember('IS','OFAO',member).
isMember('IRL','OFAO',member).
isMember('M','OFAO',member).
isMember('MD','OFAO',member).
isMember('P','OFAO',member).
isMember('GB','OFAO',member).
isMember('AFG','OFAO',member).
isMember('CN','OFAO',member).
isMember('IR','OFAO',member).
isMember('PK','OFAO',member).
isMember('TAD','OFAO',member).
isMember('TM','OFAO',member).
isMember('ARM','OFAO',member).
isMember('GE','OFAO',member).
isMember('AZ','OFAO',member).
isMember('BRN','OFAO',member).
isMember('BD','OFAO',member).
isMember('MYA','OFAO',member).
isMember('IND','OFAO',member).
isMember('BHT','OFAO',member).
isMember('BRU','OFAO',member).
isMember('MAL','OFAO',member).
isMember('LAO','OFAO',member).
isMember('THA','OFAO',member).
isMember('K','OFAO',member).
isMember('VN','OFAO',member).
isMember('NOK','OFAO',member).
isMember('KGZ','OFAO',member).
isMember('MNG','OFAO',member).
isMember('NEP','OFAO',member).
isMember('CY','OFAO',member).
isMember('IL','OFAO',member).
isMember('ET','OFAO',member).
isMember('RI','OFAO',member).
isMember('TL','OFAO',member).
isMember('PNG','OFAO',member).
isMember('IRQ','OFAO',member).
isMember('JOR','OFAO',member).
isMember('KWT','OFAO',member).
isMember('SA','OFAO',member).
isMember('SYR','OFAO',member).
isMember('RL','OFAO',member).
isMember('J','OFAO',member).
isMember('ROK','OFAO',member).
isMember('MV','OFAO',member).
isMember('OM','OFAO',member).
isMember('UAE','OFAO',member).
isMember('YE','OFAO',member).
isMember('RP','OFAO',member).
isMember('Q','OFAO',member).
isMember('CL','OFAO',member).
isMember('AG','OFAO',member).
isMember('BS','OFAO',member).
isMember('BDS','OFAO',member).
isMember('BZ','OFAO',member).
isMember('GCA','OFAO',member).
isMember('MEX','OFAO',member).
isMember('CDN','OFAO',member).
isMember('USA','OFAO',member).
isMember('CR','OFAO',member).
isMember('NIC','OFAO',member).
isMember('PA','OFAO',member).
isMember('C','OFAO',member).
isMember('WD','OFAO',member).
isMember('DOM','OFAO',member).
isMember('RH','OFAO',member).
isMember('ES','OFAO',member).
isMember('HCA','OFAO',member).
isMember('WG','OFAO',member).
isMember('JA','OFAO',member).
isMember('CO','OFAO',member).
isMember('KN','OFAO',member).
isMember('WL','OFAO',member).
isMember('WV','OFAO',member).
isMember('TT','OFAO',member).
isMember('AUS','OFAO',member).
isMember('COOK','OFAO',member).
isMember('FJI','OFAO',member).
isMember('NZ','OFAO',member).
isMember('NIUE','OFAO',member).
isMember('SLB','OFAO',member).
isMember('TO','OFAO',member).
isMember('VU','OFAO',member).
isMember('WS','OFAO',member).
isMember('RA','OFAO',member).
isMember('BOL','OFAO',member).
isMember('BR','OFAO',member).
isMember('RCH','OFAO',member).
isMember('PY','OFAO',member).
isMember('ROU','OFAO',member).
isMember('PE','OFAO',member).
isMember('GUY','OFAO',member).
isMember('SME','OFAO',member).
isMember('YV','OFAO',member).
isMember('EC','OFAO',member).
isMember('DZ','OFAO',member).
isMember('LAR','OFAO',member).
isMember('RMM','OFAO',member).
isMember('RIM','OFAO',member).
isMember('MA','OFAO',member).
isMember('RN','OFAO',member).
isMember('TN','OFAO',member).
isMember('ANG','OFAO',member).
isMember('RCB','OFAO',member).
isMember('NAM','OFAO',member).
isMember('ZRE','OFAO',member).
isMember('Z','OFAO',member).
isMember('BEN','OFAO',member).
isMember('BF','OFAO',member).
isMember('WAN','OFAO',member).
isMember('RT','OFAO',member).
isMember('RB','OFAO',member).
isMember('RSA','OFAO',member).
isMember('ZW','OFAO',member).
isMember('CI','OFAO',member).
isMember('GH','OFAO',member).
isMember('BI','OFAO',member).
isMember('RWA','OFAO',member).
isMember('EAT','OFAO',member).
isMember('CAM','OFAO',member).
isMember('RCA','OFAO',member).
isMember('TCH','OFAO',member).
isMember('GQ','OFAO',member).
isMember('G','OFAO',member).
isMember('CV','OFAO',member).
isMember('SUD','OFAO',member).
isMember('COM','OFAO',member).
isMember('RG','OFAO',member).
isMember('LB','OFAO',member).
isMember('DJI','OFAO',member).
isMember('ER','OFAO',member).
isMember('ETH','OFAO',member).
isMember('SP','OFAO',member).
isMember('EAK','OFAO',member).
isMember('WAG','OFAO',member).
isMember('SN','OFAO',member).
isMember('GNB','OFAO',member).
isMember('WAL','OFAO',member).
isMember('EAU','OFAO',member).
isMember('LS','OFAO',member).
isMember('RM','OFAO',member).
isMember('MW','OFAO',member).
isMember('MOC','OFAO',member).
isMember('MS','OFAO',member).
isMember('SD','OFAO',member).
isMember('STP','OFAO',member).
isMember('SY','OFAO',member).
isMember('F','OFZ',member).
isMember('RMM','OFZ',member).
isMember('RN','OFZ',member).
isMember('RCB','OFZ',member).
isMember('BEN','OFZ',member).
isMember('BF','OFZ',member).
isMember('RT','OFZ',member).
isMember('CI','OFZ',member).
isMember('CAM','OFZ',member).
isMember('RCA','OFZ',member).
isMember('TCH','OFZ',member).
isMember('GQ','OFZ',member).
isMember('G','OFZ',member).
isMember('COM','OFZ',member).
isMember('SN','OFZ',member).
isMember('J','OG-2',member).
isMember('USA','OG-2',member).
isMember('MEX','OG-3',member).
isMember('CO','OG-3',member).
isMember('YV','OG-3',member).
isMember('F','OG-5',member).
isMember('D','OG-5',member).
isMember('GB','OG-5',member).
isMember('J','OG-5',member).
isMember('USA','OG-5',member).
isMember('GR','OG-6',member).
isMember('S','OG-6',member).
isMember('IND','OG-6',member).
isMember('MEX','OG-6',member).
isMember('RA','OG-6',member).
isMember('EAT','OG-6',member).
isMember('F','OG-7',member).
isMember('D','OG-7',member).
isMember('I','OG-7',member).
isMember('GB','OG-7',member).
isMember('J','OG-7',member).
isMember('CDN','OG-7',member).
isMember('USA','OG-7',member).
isMember('F','OG-8',member).
isMember('D','OG-8',member).
isMember('I','OG-8',member).
isMember('R','OG-8',member).
isMember('GB','OG-8',member).
isMember('J','OG-8',member).
isMember('CDN','OG-8',member).
isMember('USA','OG-8',member).
isMember('SRB','OG-9',member).
isMember('A','OG-9',member).
isMember('H','OG-9',member).
isMember('B','OG-9',member).
isMember('BG','OG-9',member).
isMember('RO','OG-9',member).
isMember('DK','OG-9',member).
isMember('SF','OG-9',member).
isMember('S','OG-9',member).
isMember('F','OG-10',member).
isMember('D','OG-10',member).
isMember('I','OG-10',member).
isMember('CH','OG-10',member).
isMember('B','OG-10',member).
isMember('NL','OG-10',member).
isMember('S','OG-10',member).
isMember('GB','OG-10',member).
isMember('J','OG-10',member).
isMember('CDN','OG-10',member).
isMember('USA','OG-10',member).
isMember('MEX','OG-11',member).
isMember('DOM','OG-11',member).
isMember('CO','OG-11',member).
isMember('RA','OG-11',member).
isMember('BOL','OG-11',member).
isMember('BR','OG-11',member).
isMember('RCH','OG-11',member).
isMember('ROU','OG-11',member).
isMember('PE','OG-11',member).
isMember('YV','OG-11',member).
isMember('EC','OG-11',member).
isMember('IND','OG-15',member).
isMember('MAL','OG-15',member).
isMember('ET','OG-15',member).
isMember('RI','OG-15',member).
isMember('MEX','OG-15',member).
isMember('JA','OG-15',member).
isMember('RA','OG-15',member).
isMember('BR','OG-15',member).
isMember('PE','OG-15',member).
isMember('YV','OG-15',member).
isMember('DZ','OG-15',member).
isMember('WAN','OG-15',member).
isMember('ZW','OG-15',member).
isMember('SN','OG-15',member).
isMember('IR','OG-19',member).
isMember('PK','OG-19',member).
isMember('IND','OG-19',member).
isMember('ET','OG-19',member).
isMember('RI','OG-19',member).
isMember('IRQ','OG-19',member).
isMember('SA','OG-19',member).
isMember('MEX','OG-19',member).
isMember('JA','OG-19',member).
isMember('RA','OG-19',member).
isMember('BR','OG-19',member).
isMember('PE','OG-19',member).
isMember('YV','OG-19',member).
isMember('DZ','OG-19',member).
isMember('ZRE','OG-19',member).
isMember('Z','OG-19',member).
isMember('WAN','OG-19',member).
isMember('CAM','OG-19',member).
isMember('IR','OG-24',member).
isMember('PK','OG-24',member).
isMember('IND','OG-24',member).
isMember('ET','OG-24',member).
isMember('SYR','OG-24',member).
isMember('RL','OG-24',member).
isMember('RP','OG-24',member).
isMember('CL','OG-24',member).
isMember('GCA','OG-24',member).
isMember('MEX','OG-24',member).
isMember('CO','OG-24',member).
isMember('TT','OG-24',member).
isMember('RA','OG-24',member).
isMember('BR','OG-24',member).
isMember('PE','OG-24',member).
isMember('YV','OG-24',member).
isMember('DZ','OG-24',member).
isMember('ZRE','OG-24',member).
isMember('WAN','OG-24',member).
isMember('CI','OG-24',member).
isMember('GH','OG-24',member).
isMember('G','OG-24',member).
isMember('ETH','OG-24',member).
isMember('RO','OG-77',member).
isMember('M','OG-77',member).
isMember('AFG','OG-77',member).
isMember('IR','OG-77',member).
isMember('PK','OG-77',member).
isMember('BRN','OG-77',member).
isMember('BD','OG-77',member).
isMember('MYA','OG-77',member).
isMember('IND','OG-77',member).
isMember('BHT','OG-77',member).
isMember('BRU','OG-77',member).
isMember('MAL','OG-77',member).
isMember('LAO','OG-77',member).
isMember('THA','OG-77',member).
isMember('K','OG-77',member).
isMember('VN','OG-77',member).
isMember('NOK','OG-77',member).
isMember('MNG','OG-77',member).
isMember('NEP','OG-77',member).
isMember('CY','OG-77',member).
isMember('ET','OG-77',member).
isMember('RI','OG-77',member).
isMember('TL','OG-77',member).
isMember('PNG','OG-77',member).
isMember('IRQ','OG-77',member).
isMember('JOR','OG-77',member).
isMember('KWT','OG-77',member).
isMember('SA','OG-77',member).
isMember('SYR','OG-77',member).
isMember('RL','OG-77',member).
isMember('ROK','OG-77',member).
isMember('MV','OG-77',member).
isMember('OM','OG-77',member).
isMember('UAE','OG-77',member).
isMember('YE','OG-77',member).
isMember('RP','OG-77',member).
isMember('Q','OG-77',member).
isMember('SGP','OG-77',member).
isMember('CL','OG-77',member).
isMember('AG','OG-77',member).
isMember('BS','OG-77',member).
isMember('BDS','OG-77',member).
isMember('BZ','OG-77',member).
isMember('GCA','OG-77',member).
isMember('MEX','OG-77',member).
isMember('CR','OG-77',member).
isMember('NIC','OG-77',member).
isMember('PA','OG-77',member).
isMember('C','OG-77',member).
isMember('WD','OG-77',member).
isMember('DOM','OG-77',member).
isMember('RH','OG-77',member).
isMember('ES','OG-77',member).
isMember('HCA','OG-77',member).
isMember('WG','OG-77',member).
isMember('JA','OG-77',member).
isMember('CO','OG-77',member).
isMember('KN','OG-77',member).
isMember('WL','OG-77',member).
isMember('WV','OG-77',member).
isMember('TT','OG-77',member).
isMember('FJI','OG-77',member).
isMember('SLB','OG-77',member).
isMember('TO','OG-77',member).
isMember('VU','OG-77',member).
isMember('WS','OG-77',member).
isMember('RA','OG-77',member).
isMember('BOL','OG-77',member).
isMember('BR','OG-77',member).
isMember('RCH','OG-77',member).
isMember('PY','OG-77',member).
isMember('ROU','OG-77',member).
isMember('PE','OG-77',member).
isMember('GUY','OG-77',member).
isMember('SME','OG-77',member).
isMember('YV','OG-77',member).
isMember('EC','OG-77',member).
isMember('DZ','OG-77',member).
isMember('LAR','OG-77',member).
isMember('RMM','OG-77',member).
isMember('RIM','OG-77',member).
isMember('MA','OG-77',member).
isMember('RN','OG-77',member).
isMember('TN','OG-77',member).
isMember('ANG','OG-77',member).
isMember('RCB','OG-77',member).
isMember('NAM','OG-77',member).
isMember('ZRE','OG-77',member).
isMember('Z','OG-77',member).
isMember('BEN','OG-77',member).
isMember('BF','OG-77',member).
isMember('WAN','OG-77',member).
isMember('RT','OG-77',member).
isMember('RB','OG-77',member).
isMember('RSA','OG-77',member).
isMember('ZW','OG-77',member).
isMember('CI','OG-77',member).
isMember('GH','OG-77',member).
isMember('BI','OG-77',member).
isMember('RWA','OG-77',member).
isMember('EAT','OG-77',member).
isMember('CAM','OG-77',member).
isMember('RCA','OG-77',member).
isMember('TCH','OG-77',member).
isMember('GQ','OG-77',member).
isMember('G','OG-77',member).
isMember('CV','OG-77',member).
isMember('SUD','OG-77',member).
isMember('COM','OG-77',member).
isMember('RG','OG-77',member).
isMember('LB','OG-77',member).
isMember('DJI','OG-77',member).
isMember('ETH','OG-77',member).
isMember('SP','OG-77',member).
isMember('EAK','OG-77',member).
isMember('WAG','OG-77',member).
isMember('SN','OG-77',member).
isMember('GNB','OG-77',member).
isMember('WAL','OG-77',member).
isMember('EAU','OG-77',member).
isMember('LS','OG-77',member).
isMember('RM','OG-77',member).
isMember('MW','OG-77',member).
isMember('MOC','OG-77',member).
isMember('MS','OG-77',member).
isMember('SD','OG-77',member).
isMember('STP','OG-77',member).
isMember('SY','OG-77',member).
isMember('BRN','OGCC',member).
isMember('KWT','OGCC',member).
isMember('SA','OGCC',member).
isMember('OM','OGCC',member).
isMember('UAE','OGCC',member).
isMember('Q','OGCC',member).
isMember('F','OIADB',member).
isMember('E','OIADB',member).
isMember('A','OIADB',member).
isMember('D','OIADB',member).
isMember('I','OIADB',member).
isMember('SLO','OIADB',member).
isMember('CH','OIADB',member).
isMember('B','OIADB',member).
isMember('NL','OIADB',member).
isMember('HR','OIADB',member).
isMember('DK','OIADB',member).
isMember('SF','OIADB',member).
isMember('N','OIADB',member).
isMember('S','OIADB',member).
isMember('P','OIADB',member).
isMember('GB','OIADB',member).
isMember('IL','OIADB',member).
isMember('J','OIADB',member).
isMember('BS','OIADB',member).
isMember('BDS','OIADB',member).
isMember('BZ','OIADB',member).
isMember('GCA','OIADB',member).
isMember('MEX','OIADB',member).
isMember('CDN','OIADB',member).
isMember('USA','OIADB',member).
isMember('CR','OIADB',member).
isMember('NIC','OIADB',member).
isMember('PA','OIADB',member).
isMember('DOM','OIADB',member).
isMember('RH','OIADB',member).
isMember('ES','OIADB',member).
isMember('HCA','OIADB',member).
isMember('JA','OIADB',member).
isMember('CO','OIADB',member).
isMember('TT','OIADB',member).
isMember('RA','OIADB',member).
isMember('BOL','OIADB',member).
isMember('BR','OIADB',member).
isMember('RCH','OIADB',member).
isMember('PY','OIADB',member).
isMember('ROU','OIADB',member).
isMember('PE','OIADB',member).
isMember('GUY','OIADB',member).
isMember('SME','OIADB',member).
isMember('YV','OIADB',member).
isMember('EC','OIADB',member).
isMember('SUD','OIGADD',member).
isMember('DJI','OIGADD',member).
isMember('ER','OIGADD',member).
isMember('ETH','OIGADD',member).
isMember('SP','OIGADD',member).
isMember('EAK','OIGADD',member).
isMember('EAU','OIGADD',member).
isMember('AL','OIAEA',member).
isMember('GR','OIAEA',member).
isMember('MK','OIAEA',member).
isMember('SRB','OIAEA',member).
isMember('MNE','OIAEA',member).
isMember('F','OIAEA',member).
isMember('E','OIAEA',member).
isMember('A','OIAEA',member).
isMember('CZ','OIAEA',member).
isMember('D','OIAEA',member).
isMember('H','OIAEA',member).
isMember('I','OIAEA',member).
isMember('FL','OIAEA',member).
isMember('SK','OIAEA',member).
isMember('SLO','OIAEA',member).
isMember('CH','OIAEA',member).
isMember('BY','OIAEA',member).
isMember('LT','OIAEA',member).
isMember('PL','OIAEA',member).
isMember('UA','OIAEA',member).
isMember('R','OIAEA',member).
isMember('B','OIAEA',member).
isMember('L','OIAEA',member).
isMember('NL','OIAEA',member).
isMember('HR','OIAEA',member).
isMember('BG','OIAEA',member).
isMember('RO','OIAEA',member).
isMember('TR','OIAEA',member).
isMember('DK','OIAEA',member).
isMember('EW','OIAEA',member).
isMember('SF','OIAEA',member).
isMember('N','OIAEA',member).
isMember('S','OIAEA',member).
isMember('MC','OIAEA',member).
isMember('V','OIAEA',member).
isMember('IS','OIAEA',member).
isMember('IRL','OIAEA',member).
isMember('P','OIAEA',member).
isMember('GB','OIAEA',member).
isMember('AFG','OIAEA',member).
isMember('CN','OIAEA',member).
isMember('IR','OIAEA',member).
isMember('PK','OIAEA',member).
isMember('UZB','OIAEA',member).
isMember('ARM','OIAEA',member).
isMember('BD','OIAEA',member).
isMember('MYA','OIAEA',member).
isMember('IND','OIAEA',member).
isMember('MAL','OIAEA',member).
isMember('THA','OIAEA',member).
isMember('K','OIAEA',member).
isMember('VN','OIAEA',member).
isMember('KAZ','OIAEA',member).
isMember('MNG','OIAEA',member).
isMember('CY','OIAEA',member).
isMember('IL','OIAEA',member).
isMember('ET','OIAEA',member).
isMember('RI','OIAEA',member).
isMember('IRQ','OIAEA',member).
isMember('JOR','OIAEA',member).
isMember('KWT','OIAEA',member).
isMember('SA','OIAEA',member).
isMember('SYR','OIAEA',member).
isMember('RL','OIAEA',member).
isMember('J','OIAEA',member).
isMember('ROK','OIAEA',member).
isMember('UAE','OIAEA',member).
isMember('YE','OIAEA',member).
isMember('RP','OIAEA',member).
isMember('Q','OIAEA',member).
isMember('SGP','OIAEA',member).
isMember('CL','OIAEA',member).
isMember('GCA','OIAEA',member).
isMember('MEX','OIAEA',member).
isMember('CDN','OIAEA',member).
isMember('USA','OIAEA',member).
isMember('CR','OIAEA',member).
isMember('NIC','OIAEA',member).
isMember('PA','OIAEA',member).
isMember('C','OIAEA',member).
isMember('DOM','OIAEA',member).
isMember('RH','OIAEA',member).
isMember('ES','OIAEA',member).
isMember('JA','OIAEA',member).
isMember('CO','OIAEA',member).
isMember('AUS','OIAEA',member).
isMember('MH','OIAEA',member).
isMember('NZ','OIAEA',member).
isMember('RA','OIAEA',member).
isMember('BOL','OIAEA',member).
isMember('BR','OIAEA',member).
isMember('RCH','OIAEA',member).
isMember('PY','OIAEA',member).
isMember('ROU','OIAEA',member).
isMember('PE','OIAEA',member).
isMember('YV','OIAEA',member).
isMember('EC','OIAEA',member).
isMember('DZ','OIAEA',member).
isMember('LAR','OIAEA',member).
isMember('RMM','OIAEA',member).
isMember('MA','OIAEA',member).
isMember('RN','OIAEA',member).
isMember('TN','OIAEA',member).
isMember('NAM','OIAEA',member).
isMember('ZRE','OIAEA',member).
isMember('Z','OIAEA',member).
isMember('WAN','OIAEA',member).
isMember('RSA','OIAEA',member).
isMember('ZW','OIAEA',member).
isMember('CI','OIAEA',member).
isMember('GH','OIAEA',member).
isMember('EAT','OIAEA',member).
isMember('CAM','OIAEA',member).
isMember('G','OIAEA',member).
isMember('SUD','OIAEA',member).
isMember('LB','OIAEA',member).
isMember('ETH','OIAEA',member).
isMember('EAK','OIAEA',member).
isMember('SN','OIAEA',member).
isMember('WAL','OIAEA',member).
isMember('EAU','OIAEA',member).
isMember('RM','OIAEA',member).
isMember('MS','OIAEA',member).
isMember('AL','OIBRD',member).
isMember('GR','OIBRD',member).
isMember('MK','OIBRD',member).
isMember('SRB','OIBRD',member).
isMember('MNE','OIBRD',member).
isMember('F','OIBRD',member).
isMember('E','OIBRD',member).
isMember('A','OIBRD',member).
isMember('CZ','OIBRD',member).
isMember('D','OIBRD',member).
isMember('H','OIBRD',member).
isMember('I','OIBRD',member).
isMember('SK','OIBRD',member).
isMember('SLO','OIBRD',member).
isMember('CH','OIBRD',member).
isMember('BY','OIBRD',member).
isMember('LV','OIBRD',member).
isMember('LT','OIBRD',member).
isMember('PL','OIBRD',member).
isMember('UA','OIBRD',member).
isMember('R','OIBRD',member).
isMember('B','OIBRD',member).
isMember('L','OIBRD',member).
isMember('NL','OIBRD',member).
isMember('HR','OIBRD',member).
isMember('BG','OIBRD',member).
isMember('RO','OIBRD',member).
isMember('TR','OIBRD',member).
isMember('DK','OIBRD',member).
isMember('EW','OIBRD',member).
isMember('SF','OIBRD',member).
isMember('N','OIBRD',member).
isMember('S','OIBRD',member).
isMember('IS','OIBRD',member).
isMember('IRL','OIBRD',member).
isMember('M','OIBRD',member).
isMember('MD','OIBRD',member).
isMember('P','OIBRD',member).
isMember('GB','OIBRD',member).
isMember('AFG','OIBRD',member).
isMember('CN','OIBRD',member).
isMember('IR','OIBRD',member).
isMember('PK','OIBRD',member).
isMember('TAD','OIBRD',member).
isMember('TM','OIBRD',member).
isMember('UZB','OIBRD',member).
isMember('ARM','OIBRD',member).
isMember('GE','OIBRD',member).
isMember('AZ','OIBRD',member).
isMember('BRN','OIBRD',member).
isMember('BD','OIBRD',member).
isMember('MYA','OIBRD',member).
isMember('IND','OIBRD',member).
isMember('BHT','OIBRD',member).
isMember('BRU','OIBRD',member).
isMember('MAL','OIBRD',member).
isMember('LAO','OIBRD',member).
isMember('THA','OIBRD',member).
isMember('K','OIBRD',member).
isMember('VN','OIBRD',member).
isMember('KAZ','OIBRD',member).
isMember('KGZ','OIBRD',member).
isMember('MNG','OIBRD',member).
isMember('NEP','OIBRD',member).
isMember('CY','OIBRD',member).
isMember('IL','OIBRD',member).
isMember('ET','OIBRD',member).
isMember('RI','OIBRD',member).
isMember('TL','OIBRD',member).
isMember('PNG','OIBRD',member).
isMember('IRQ','OIBRD',member).
isMember('JOR','OIBRD',member).
isMember('KWT','OIBRD',member).
isMember('SA','OIBRD',member).
isMember('SYR','OIBRD',member).
isMember('RL','OIBRD',member).
isMember('J','OIBRD',member).
isMember('ROK','OIBRD',member).
isMember('MV','OIBRD',member).
isMember('OM','OIBRD',member).
isMember('UAE','OIBRD',member).
isMember('YE','OIBRD',member).
isMember('RP','OIBRD',member).
isMember('Q','OIBRD',member).
isMember('SGP','OIBRD',member).
isMember('CL','OIBRD',member).
isMember('AG','OIBRD',member).
isMember('BS','OIBRD',member).
isMember('BDS','OIBRD',member).
isMember('BZ','OIBRD',member).
isMember('GCA','OIBRD',member).
isMember('MEX','OIBRD',member).
isMember('CDN','OIBRD',member).
isMember('USA','OIBRD',member).
isMember('CR','OIBRD',member).
isMember('NIC','OIBRD',member).
isMember('PA','OIBRD',member).
isMember('WD','OIBRD',member).
isMember('DOM','OIBRD',member).
isMember('RH','OIBRD',member).
isMember('ES','OIBRD',member).
isMember('HCA','OIBRD',member).
isMember('WG','OIBRD',member).
isMember('JA','OIBRD',member).
isMember('CO','OIBRD',member).
isMember('KN','OIBRD',member).
isMember('WL','OIBRD',member).
isMember('WV','OIBRD',member).
isMember('TT','OIBRD',member).
isMember('AUS','OIBRD',member).
isMember('FJI','OIBRD',member).
isMember('KIR','OIBRD',member).
isMember('MH','OIBRD',member).
isMember('NZ','OIBRD',member).
isMember('SLB','OIBRD',member).
isMember('TO','OIBRD',member).
isMember('VU','OIBRD',member).
isMember('WS','OIBRD',member).
isMember('RA','OIBRD',member).
isMember('BOL','OIBRD',member).
isMember('BR','OIBRD',member).
isMember('RCH','OIBRD',member).
isMember('PY','OIBRD',member).
isMember('ROU','OIBRD',member).
isMember('PE','OIBRD',member).
isMember('GUY','OIBRD',member).
isMember('SME','OIBRD',member).
isMember('YV','OIBRD',member).
isMember('EC','OIBRD',member).
isMember('DZ','OIBRD',member).
isMember('LAR','OIBRD',member).
isMember('RMM','OIBRD',member).
isMember('RIM','OIBRD',member).
isMember('MA','OIBRD',member).
isMember('RN','OIBRD',member).
isMember('TN','OIBRD',member).
isMember('ANG','OIBRD',member).
isMember('RCB','OIBRD',member).
isMember('NAM','OIBRD',member).
isMember('ZRE','OIBRD',member).
isMember('Z','OIBRD',member).
isMember('BEN','OIBRD',member).
isMember('BF','OIBRD',member).
isMember('WAN','OIBRD',member).
isMember('RT','OIBRD',member).
isMember('RB','OIBRD',member).
isMember('RSA','OIBRD',member).
isMember('ZW','OIBRD',member).
isMember('CI','OIBRD',member).
isMember('GH','OIBRD',member).
isMember('BI','OIBRD',member).
isMember('RWA','OIBRD',member).
isMember('EAT','OIBRD',member).
isMember('CAM','OIBRD',member).
isMember('RCA','OIBRD',member).
isMember('TCH','OIBRD',member).
isMember('GQ','OIBRD',member).
isMember('G','OIBRD',member).
isMember('CV','OIBRD',member).
isMember('SUD','OIBRD',member).
isMember('COM','OIBRD',member).
isMember('RG','OIBRD',member).
isMember('LB','OIBRD',member).
isMember('DJI','OIBRD',member).
isMember('ER','OIBRD',member).
isMember('ETH','OIBRD',member).
isMember('SP','OIBRD',member).
isMember('EAK','OIBRD',member).
isMember('WAG','OIBRD',member).
isMember('SN','OIBRD',member).
isMember('GNB','OIBRD',member).
isMember('WAL','OIBRD',member).
isMember('EAU','OIBRD',member).
isMember('LS','OIBRD',member).
isMember('RM','OIBRD',member).
isMember('MW','OIBRD',member).
isMember('MOC','OIBRD',member).
isMember('MS','OIBRD',member).
isMember('SD','OIBRD',member).
isMember('STP','OIBRD',member).
isMember('SY','OIBRD',member).
isMember('GR','OICC',member).
isMember('SRB','OICC',member).
isMember('F','OICC',member).
isMember('E','OICC',member).
isMember('A','OICC',member).
isMember('D','OICC',member).
isMember('I','OICC',member).
isMember('CH','OICC',member).
isMember('B','OICC',member).
isMember('L','OICC',member).
isMember('NL','OICC',member).
isMember('TR','OICC',member).
isMember('DK','OICC',member).
isMember('SF','OICC',member).
isMember('N','OICC',member).
isMember('S','OICC',member).
isMember('IS','OICC',member).
isMember('IRL','OICC',member).
isMember('P','OICC',member).
isMember('GB','OICC',member).
isMember('IR','OICC',member).
isMember('PK','OICC',member).
isMember('IND','OICC',member).
isMember('HONX','OICC',member).
isMember('CY','OICC',member).
isMember('IL','OICC',member).
isMember('ET','OICC',member).
isMember('RI','OICC',member).
isMember('JOR','OICC',member).
isMember('KWT','OICC',member).
isMember('SA','OICC',member).
isMember('SYR','OICC',member).
isMember('RL','OICC',member).
isMember('J','OICC',member).
isMember('ROK','OICC',member).
isMember('SGP','OICC',member).
isMember('CL','OICC',member).
isMember('RC','OICC',member).
isMember('MEX','OICC',member).
isMember('CDN','OICC',member).
isMember('USA','OICC',member).
isMember('CO','OICC',member).
isMember('AUS','OICC',member).
isMember('RA','OICC',member).
isMember('BR','OICC',member).
isMember('ROU','OICC',member).
isMember('YV','OICC',member).
isMember('EC','OICC',member).
isMember('MA','OICC',member).
isMember('TN','OICC',member).
isMember('ZRE','OICC',member).
isMember('BF','OICC',member).
isMember('WAN','OICC',member).
isMember('RT','OICC',member).
isMember('RSA','OICC',member).
isMember('CI','OICC',member).
isMember('CAM','OICC',member).
isMember('G','OICC',member).
isMember('SN','OICC',member).
isMember('RM','OICC',member).
isMember('AL','OICAO',member).
isMember('GR','OICAO',member).
isMember('MK','OICAO',member).
isMember('SRB','OICAO',member).
isMember('MNE','OICAO',member).
isMember('F','OICAO',member).
isMember('E','OICAO',member).
isMember('A','OICAO',member).
isMember('CZ','OICAO',member).
isMember('D','OICAO',member).
isMember('H','OICAO',member).
isMember('I','OICAO',member).
isMember('SK','OICAO',member).
isMember('SLO','OICAO',member).
isMember('CH','OICAO',member).
isMember('BY','OICAO',member).
isMember('LV','OICAO',member).
isMember('LT','OICAO',member).
isMember('PL','OICAO',member).
isMember('UA','OICAO',member).
isMember('R','OICAO',member).
isMember('B','OICAO',member).
isMember('L','OICAO',member).
isMember('NL','OICAO',member).
isMember('BIH','OICAO',member).
isMember('HR','OICAO',member).
isMember('BG','OICAO',member).
isMember('RO','OICAO',member).
isMember('TR','OICAO',member).
isMember('DK','OICAO',member).
isMember('EW','OICAO',member).
isMember('SF','OICAO',member).
isMember('N','OICAO',member).
isMember('S','OICAO',member).
isMember('MC','OICAO',member).
isMember('IS','OICAO',member).
isMember('IRL','OICAO',member).
isMember('RSM','OICAO',member).
isMember('M','OICAO',member).
isMember('MD','OICAO',member).
isMember('P','OICAO',member).
isMember('GB','OICAO',member).
isMember('AFG','OICAO',member).
isMember('CN','OICAO',member).
isMember('IR','OICAO',member).
isMember('PK','OICAO',member).
isMember('TAD','OICAO',member).
isMember('TM','OICAO',member).
isMember('UZB','OICAO',member).
isMember('ARM','OICAO',member).
isMember('GE','OICAO',member).
isMember('AZ','OICAO',member).
isMember('BRN','OICAO',member).
isMember('BD','OICAO',member).
isMember('MYA','OICAO',member).
isMember('IND','OICAO',member).
isMember('BHT','OICAO',member).
isMember('BRU','OICAO',member).
isMember('MAL','OICAO',member).
isMember('LAO','OICAO',member).
isMember('THA','OICAO',member).
isMember('K','OICAO',member).
isMember('VN','OICAO',member).
isMember('KAZ','OICAO',member).
isMember('NOK','OICAO',member).
isMember('KGZ','OICAO',member).
isMember('MNG','OICAO',member).
isMember('NEP','OICAO',member).
isMember('CY','OICAO',member).
isMember('IL','OICAO',member).
isMember('ET','OICAO',member).
isMember('RI','OICAO',member).
isMember('TL','OICAO',member).
isMember('PNG','OICAO',member).
isMember('IRQ','OICAO',member).
isMember('JOR','OICAO',member).
isMember('KWT','OICAO',member).
isMember('SA','OICAO',member).
isMember('SYR','OICAO',member).
isMember('RL','OICAO',member).
isMember('J','OICAO',member).
isMember('ROK','OICAO',member).
isMember('MV','OICAO',member).
isMember('OM','OICAO',member).
isMember('UAE','OICAO',member).
isMember('YE','OICAO',member).
isMember('RP','OICAO',member).
isMember('Q','OICAO',member).
isMember('SGP','OICAO',member).
isMember('CL','OICAO',member).
isMember('AG','OICAO',member).
isMember('BS','OICAO',member).
isMember('BDS','OICAO',member).
isMember('BZ','OICAO',member).
isMember('GCA','OICAO',member).
isMember('MEX','OICAO',member).
isMember('CDN','OICAO',member).
isMember('USA','OICAO',member).
isMember('CR','OICAO',member).
isMember('NIC','OICAO',member).
isMember('PA','OICAO',member).
isMember('C','OICAO',member).
isMember('DOM','OICAO',member).
isMember('RH','OICAO',member).
isMember('ES','OICAO',member).
isMember('HCA','OICAO',member).
isMember('WG','OICAO',member).
isMember('JA','OICAO',member).
isMember('CO','OICAO',member).
isMember('WL','OICAO',member).
isMember('WV','OICAO',member).
isMember('TT','OICAO',member).
isMember('AUS','OICAO',member).
isMember('COOK','OICAO',member).
isMember('FJI','OICAO',member).
isMember('KIR','OICAO',member).
isMember('MH','OICAO',member).
isMember('NAU','OICAO',member).
isMember('NZ','OICAO',member).
isMember('SLB','OICAO',member).
isMember('TO','OICAO',member).
isMember('VU','OICAO',member).
isMember('RA','OICAO',member).
isMember('BOL','OICAO',member).
isMember('BR','OICAO',member).
isMember('RCH','OICAO',member).
isMember('PY','OICAO',member).
isMember('ROU','OICAO',member).
isMember('PE','OICAO',member).
isMember('GUY','OICAO',member).
isMember('SME','OICAO',member).
isMember('YV','OICAO',member).
isMember('EC','OICAO',member).
isMember('DZ','OICAO',member).
isMember('LAR','OICAO',member).
isMember('RMM','OICAO',member).
isMember('RIM','OICAO',member).
isMember('MA','OICAO',member).
isMember('RN','OICAO',member).
isMember('TN','OICAO',member).
isMember('ANG','OICAO',member).
isMember('RCB','OICAO',member).
isMember('NAM','OICAO',member).
isMember('ZRE','OICAO',member).
isMember('Z','OICAO',member).
isMember('BEN','OICAO',member).
isMember('BF','OICAO',member).
isMember('WAN','OICAO',member).
isMember('RT','OICAO',member).
isMember('RB','OICAO',member).
isMember('RSA','OICAO',member).
isMember('ZW','OICAO',member).
isMember('CI','OICAO',member).
isMember('GH','OICAO',member).
isMember('BI','OICAO',member).
isMember('RWA','OICAO',member).
isMember('EAT','OICAO',member).
isMember('CAM','OICAO',member).
isMember('RCA','OICAO',member).
isMember('TCH','OICAO',member).
isMember('GQ','OICAO',member).
isMember('G','OICAO',member).
isMember('CV','OICAO',member).
isMember('SUD','OICAO',member).
isMember('COM','OICAO',member).
isMember('RG','OICAO',member).
isMember('LB','OICAO',member).
isMember('DJI','OICAO',member).
isMember('ER','OICAO',member).
isMember('ETH','OICAO',member).
isMember('SP','OICAO',member).
isMember('EAK','OICAO',member).
isMember('WAG','OICAO',member).
isMember('SN','OICAO',member).
isMember('GNB','OICAO',member).
isMember('WAL','OICAO',member).
isMember('EAU','OICAO',member).
isMember('LS','OICAO',member).
isMember('RM','OICAO',member).
isMember('MW','OICAO',member).
isMember('MOC','OICAO',member).
isMember('MS','OICAO',member).
isMember('SD','OICAO',member).
isMember('STP','OICAO',member).
isMember('SY','OICAO',member).
isMember('GR','OICFTU',member).
isMember('F','OICFTU',member).
isMember('E','OICFTU',member).
isMember('A','OICFTU',member).
isMember('CZ','OICFTU',member).
isMember('D','OICFTU',member).
isMember('I','OICFTU',member).
isMember('SK','OICFTU',member).
isMember('CH','OICFTU',member).
isMember('PL','OICFTU',member).
isMember('B','OICFTU',member).
isMember('L','OICFTU',member).
isMember('NL','OICFTU',member).
isMember('BG','OICFTU',member).
isMember('RO','OICFTU',member).
isMember('TR','OICFTU',member).
isMember('DK','OICFTU',member).
isMember('EW','OICFTU',member).
isMember('SF','OICFTU',member).
isMember('N','OICFTU',member).
isMember('S','OICFTU',member).
isMember('V','OICFTU',member).
isMember('IS','OICFTU',member).
isMember('RSM','OICFTU',member).
isMember('M','OICFTU',member).
isMember('P','OICFTU',member).
isMember('GB','OICFTU',member).
isMember('CN','OICFTU',member).
isMember('PK','OICFTU',member).
isMember('BD','OICFTU',member).
isMember('IND','OICFTU',member).
isMember('MAL','OICFTU',member).
isMember('THA','OICFTU',member).
isMember('CY','OICFTU',member).
isMember('IL','OICFTU',member).
isMember('RI','OICFTU',member).
isMember('PNG','OICFTU',member).
isMember('RL','OICFTU',member).
isMember('J','OICFTU',member).
isMember('ROK','OICFTU',member).
isMember('RP','OICFTU',member).
isMember('SGP','OICFTU',member).
isMember('CL','OICFTU',member).
isMember('AG','OICFTU',member).
isMember('BS','OICFTU',member).
isMember('BDS','OICFTU',member).
isMember('BZ','OICFTU',member).
isMember('GCA','OICFTU',member).
isMember('MEX','OICFTU',member).
isMember('BERM','OICFTU',member).
isMember('CDN','OICFTU',member).
isMember('USA','OICFTU',member).
isMember('CR','OICFTU',member).
isMember('NIC','OICFTU',member).
isMember('PA','OICFTU',member).
isMember('WD','OICFTU',member).
isMember('DOM','OICFTU',member).
isMember('ES','OICFTU',member).
isMember('HCA','OICFTU',member).
isMember('WG','OICFTU',member).
isMember('JA','OICFTU',member).
isMember('CO','OICFTU',member).
isMember('KN','OICFTU',member).
isMember('WL','OICFTU',member).
isMember('WV','OICFTU',member).
isMember('TT','OICFTU',member).
isMember('AUS','OICFTU',member).
isMember('FJI','OICFTU',member).
isMember('FPOL','OICFTU',member).
isMember('KIR','OICFTU',member).
isMember('NZ','OICFTU',member).
isMember('TO','OICFTU',member).
isMember('WS','OICFTU',member).
isMember('RA','OICFTU',member).
isMember('BR','OICFTU',member).
isMember('RCH','OICFTU',member).
isMember('PY','OICFTU',member).
isMember('PE','OICFTU',member).
isMember('GUY','OICFTU',member).
isMember('SME','OICFTU',member).
isMember('YV','OICFTU',member).
isMember('EC','OICFTU',member).
isMember('RMM','OICFTU',member).
isMember('MA','OICFTU',member).
isMember('TN','OICFTU',member).
isMember('Z','OICFTU',member).
isMember('BEN','OICFTU',member).
isMember('BF','OICFTU',member).
isMember('RT','OICFTU',member).
isMember('RB','OICFTU',member).
isMember('ZW','OICFTU',member).
isMember('GH','OICFTU',member).
isMember('RWA','OICFTU',member).
isMember('CAM','OICFTU',member).
isMember('TCH','OICFTU',member).
isMember('G','OICFTU',member).
isMember('CV','OICFTU',member).
isMember('LB','OICFTU',member).
isMember('EAK','OICFTU',member).
isMember('WAG','OICFTU',member).
isMember('SN','OICFTU',member).
isMember('WAL','OICFTU',member).
isMember('EAU','OICFTU',member).
isMember('LS','OICFTU',member).
isMember('RM','OICFTU',member).
isMember('MW','OICFTU',member).
isMember('MS','OICFTU',member).
isMember('SD','OICFTU',member).
isMember('HELX','OICFTU',member).
isMember('SY','OICFTU',member).
isMember('AL','OInterpol',member).
isMember('GR','OInterpol',member).
isMember('MK','OInterpol',member).
isMember('SRB','OInterpol',member).
isMember('MNE','OInterpol',member).
isMember('AND','OInterpol',member).
isMember('F','OInterpol',member).
isMember('E','OInterpol',member).
isMember('A','OInterpol',member).
isMember('CZ','OInterpol',member).
isMember('D','OInterpol',member).
isMember('H','OInterpol',member).
isMember('I','OInterpol',member).
isMember('FL','OInterpol',member).
isMember('SK','OInterpol',member).
isMember('SLO','OInterpol',member).
isMember('CH','OInterpol',member).
isMember('BY','OInterpol',member).
isMember('LV','OInterpol',member).
isMember('LT','OInterpol',member).
isMember('PL','OInterpol',member).
isMember('UA','OInterpol',member).
isMember('R','OInterpol',member).
isMember('B','OInterpol',member).
isMember('L','OInterpol',member).
isMember('NL','OInterpol',member).
isMember('BIH','OInterpol',member).
isMember('HR','OInterpol',member).
isMember('BG','OInterpol',member).
isMember('RO','OInterpol',member).
isMember('TR','OInterpol',member).
isMember('DK','OInterpol',member).
isMember('EW','OInterpol',member).
isMember('SF','OInterpol',member).
isMember('N','OInterpol',member).
isMember('S','OInterpol',member).
isMember('MC','OInterpol',member).
isMember('IS','OInterpol',member).
isMember('IRL','OInterpol',member).
isMember('M','OInterpol',member).
isMember('MD','OInterpol',member).
isMember('P','OInterpol',member).
isMember('GB','OInterpol',member).
isMember('CN','OInterpol',member).
isMember('IR','OInterpol',member).
isMember('PK','OInterpol',member).
isMember('UZB','OInterpol',member).
isMember('ARM','OInterpol',member).
isMember('GE','OInterpol',member).
isMember('AZ','OInterpol',member).
isMember('BRN','OInterpol',member).
isMember('BD','OInterpol',member).
isMember('MYA','OInterpol',member).
isMember('IND','OInterpol',member).
isMember('BRU','OInterpol',member).
isMember('MAL','OInterpol',member).
isMember('LAO','OInterpol',member).
isMember('THA','OInterpol',member).
isMember('K','OInterpol',member).
isMember('VN','OInterpol',member).
isMember('KAZ','OInterpol',member).
isMember('MNG','OInterpol',member).
isMember('NEP','OInterpol',member).
isMember('CY','OInterpol',member).
isMember('IL','OInterpol',member).
isMember('ET','OInterpol',member).
isMember('RI','OInterpol',member).
isMember('TL','OInterpol',member).
isMember('PNG','OInterpol',member).
isMember('IRQ','OInterpol',member).
isMember('JOR','OInterpol',member).
isMember('KWT','OInterpol',member).
isMember('SA','OInterpol',member).
isMember('SYR','OInterpol',member).
isMember('RL','OInterpol',member).
isMember('J','OInterpol',member).
isMember('ROK','OInterpol',member).
isMember('MV','OInterpol',member).
isMember('OM','OInterpol',member).
isMember('UAE','OInterpol',member).
isMember('YE','OInterpol',member).
isMember('RP','OInterpol',member).
isMember('Q','OInterpol',member).
isMember('SGP','OInterpol',member).
isMember('CL','OInterpol',member).
isMember('AG','OInterpol',member).
isMember('ARU','OInterpol',member).
isMember('BS','OInterpol',member).
isMember('BDS','OInterpol',member).
isMember('BZ','OInterpol',member).
isMember('GCA','OInterpol',member).
isMember('MEX','OInterpol',member).
isMember('CDN','OInterpol',member).
isMember('USA','OInterpol',member).
isMember('CR','OInterpol',member).
isMember('NIC','OInterpol',member).
isMember('PA','OInterpol',member).
isMember('C','OInterpol',member).
isMember('WD','OInterpol',member).
isMember('DOM','OInterpol',member).
isMember('RH','OInterpol',member).
isMember('ES','OInterpol',member).
isMember('HCA','OInterpol',member).
isMember('WG','OInterpol',member).
isMember('JA','OInterpol',member).
isMember('NA','OInterpol',member).
isMember('CO','OInterpol',member).
isMember('KN','OInterpol',member).
isMember('WL','OInterpol',member).
isMember('WV','OInterpol',member).
isMember('TT','OInterpol',member).
isMember('AUS','OInterpol',member).
isMember('FJI','OInterpol',member).
isMember('KIR','OInterpol',member).
isMember('MH','OInterpol',member).
isMember('NAU','OInterpol',member).
isMember('NZ','OInterpol',member).
isMember('TO','OInterpol',member).
isMember('RA','OInterpol',member).
isMember('BOL','OInterpol',member).
isMember('BR','OInterpol',member).
isMember('RCH','OInterpol',member).
isMember('PY','OInterpol',member).
isMember('ROU','OInterpol',member).
isMember('PE','OInterpol',member).
isMember('GUY','OInterpol',member).
isMember('SME','OInterpol',member).
isMember('YV','OInterpol',member).
isMember('EC','OInterpol',member).
isMember('DZ','OInterpol',member).
isMember('LAR','OInterpol',member).
isMember('RMM','OInterpol',member).
isMember('RIM','OInterpol',member).
isMember('MA','OInterpol',member).
isMember('RN','OInterpol',member).
isMember('TN','OInterpol',member).
isMember('ANG','OInterpol',member).
isMember('RCB','OInterpol',member).
isMember('NAM','OInterpol',member).
isMember('ZRE','OInterpol',member).
isMember('Z','OInterpol',member).
isMember('BEN','OInterpol',member).
isMember('BF','OInterpol',member).
isMember('WAN','OInterpol',member).
isMember('RT','OInterpol',member).
isMember('RB','OInterpol',member).
isMember('RSA','OInterpol',member).
isMember('ZW','OInterpol',member).
isMember('CI','OInterpol',member).
isMember('GH','OInterpol',member).
isMember('BI','OInterpol',member).
isMember('RWA','OInterpol',member).
isMember('EAT','OInterpol',member).
isMember('CAM','OInterpol',member).
isMember('RCA','OInterpol',member).
isMember('TCH','OInterpol',member).
isMember('GQ','OInterpol',member).
isMember('G','OInterpol',member).
isMember('CV','OInterpol',member).
isMember('SUD','OInterpol',member).
isMember('SSD','OInterpol',member).
isMember('RG','OInterpol',member).
isMember('LB','OInterpol',member).
isMember('DJI','OInterpol',member).
isMember('ETH','OInterpol',member).
isMember('SP','OInterpol',member).
isMember('EAK','OInterpol',member).
isMember('WAG','OInterpol',member).
isMember('SN','OInterpol',member).
isMember('GNB','OInterpol',member).
isMember('WAL','OInterpol',member).
isMember('EAU','OInterpol',member).
isMember('LS','OInterpol',member).
isMember('RM','OInterpol',member).
isMember('MW','OInterpol',member).
isMember('MOC','OInterpol',member).
isMember('MS','OInterpol',member).
isMember('SD','OInterpol',member).
isMember('STP','OInterpol',member).
isMember('SY','OInterpol',member).
isMember('GBZ','OInterpol',subbureau).
isMember('AXA','OInterpol',subbureau).
isMember('BERM','OInterpol',subbureau).
isMember('BVIR','OInterpol',subbureau).
isMember('CAYM','OInterpol',subbureau).
isMember('MNTS','OInterpol',subbureau).
isMember('PR','OInterpol',subbureau).
isMember('TUCA','OInterpol',subbureau).
isMember('AMSA','OInterpol',subbureau).
isMember('F','OIDA','Part I').
isMember('A','OIDA','Part I').
isMember('D','OIDA','Part I').
isMember('I','OIDA','Part I').
isMember('CH','OIDA','Part I').
isMember('R','OIDA','Part I').
isMember('B','OIDA','Part I').
isMember('L','OIDA','Part I').
isMember('NL','OIDA','Part I').
isMember('DK','OIDA','Part I').
isMember('SF','OIDA','Part I').
isMember('N','OIDA','Part I').
isMember('S','OIDA','Part I').
isMember('IS','OIDA','Part I').
isMember('IRL','OIDA','Part I').
isMember('GB','OIDA','Part I').
isMember('KWT','OIDA','Part I').
isMember('J','OIDA','Part I').
isMember('UAE','OIDA','Part I').
isMember('CDN','OIDA','Part I').
isMember('USA','OIDA','Part I').
isMember('AUS','OIDA','Part I').
isMember('NZ','OIDA','Part I').
isMember('RSA','OIDA','Part I').
isMember('AL','OIDA','Part II').
isMember('GR','OIDA','Part II').
isMember('MK','OIDA','Part II').
isMember('SRB','OIDA','Part II').
isMember('MNE','OIDA','Part II').
isMember('E','OIDA','Part II').
isMember('CZ','OIDA','Part II').
isMember('H','OIDA','Part II').
isMember('SK','OIDA','Part II').
isMember('SLO','OIDA','Part II').
isMember('LV','OIDA','Part II').
isMember('PL','OIDA','Part II').
isMember('HR','OIDA','Part II').
isMember('TR','OIDA','Part II').
isMember('MD','OIDA','Part II').
isMember('P','OIDA','Part II').
isMember('AFG','OIDA','Part II').
isMember('CN','OIDA','Part II').
isMember('IR','OIDA','Part II').
isMember('PK','OIDA','Part II').
isMember('TAD','OIDA','Part II').
isMember('UZB','OIDA','Part II').
isMember('ARM','OIDA','Part II').
isMember('GE','OIDA','Part II').
isMember('AZ','OIDA','Part II').
isMember('BD','OIDA','Part II').
isMember('MYA','OIDA','Part II').
isMember('IND','OIDA','Part II').
isMember('BHT','OIDA','Part II').
isMember('MAL','OIDA','Part II').
isMember('LAO','OIDA','Part II').
isMember('THA','OIDA','Part II').
isMember('K','OIDA','Part II').
isMember('VN','OIDA','Part II').
isMember('KAZ','OIDA','Part II').
isMember('KGZ','OIDA','Part II').
isMember('MNG','OIDA','Part II').
isMember('NEP','OIDA','Part II').
isMember('CY','OIDA','Part II').
isMember('IL','OIDA','Part II').
isMember('ET','OIDA','Part II').
isMember('RI','OIDA','Part II').
isMember('TL','OIDA','Part II').
isMember('PNG','OIDA','Part II').
isMember('IRQ','OIDA','Part II').
isMember('JOR','OIDA','Part II').
isMember('SA','OIDA','Part II').
isMember('SYR','OIDA','Part II').
isMember('RL','OIDA','Part II').
isMember('ROK','OIDA','Part II').
isMember('MV','OIDA','Part II').
isMember('OM','OIDA','Part II').
isMember('YE','OIDA','Part II').
isMember('RP','OIDA','Part II').
isMember('CL','OIDA','Part II').
isMember('BZ','OIDA','Part II').
isMember('GCA','OIDA','Part II').
isMember('MEX','OIDA','Part II').
isMember('CR','OIDA','Part II').
isMember('NIC','OIDA','Part II').
isMember('PA','OIDA','Part II').
isMember('WD','OIDA','Part II').
isMember('DOM','OIDA','Part II').
isMember('RH','OIDA','Part II').
isMember('ES','OIDA','Part II').
isMember('HCA','OIDA','Part II').
isMember('WG','OIDA','Part II').
isMember('CO','OIDA','Part II').
isMember('KN','OIDA','Part II').
isMember('WL','OIDA','Part II').
isMember('WV','OIDA','Part II').
isMember('TT','OIDA','Part II').
isMember('FJI','OIDA','Part II').
isMember('KIR','OIDA','Part II').
isMember('MH','OIDA','Part II').
isMember('SLB','OIDA','Part II').
isMember('TO','OIDA','Part II').
isMember('VU','OIDA','Part II').
isMember('WS','OIDA','Part II').
isMember('RA','OIDA','Part II').
isMember('BOL','OIDA','Part II').
isMember('BR','OIDA','Part II').
isMember('RCH','OIDA','Part II').
isMember('PY','OIDA','Part II').
isMember('PE','OIDA','Part II').
isMember('GUY','OIDA','Part II').
isMember('EC','OIDA','Part II').
isMember('DZ','OIDA','Part II').
isMember('LAR','OIDA','Part II').
isMember('RMM','OIDA','Part II').
isMember('RIM','OIDA','Part II').
isMember('MA','OIDA','Part II').
isMember('RN','OIDA','Part II').
isMember('TN','OIDA','Part II').
isMember('ANG','OIDA','Part II').
isMember('RCB','OIDA','Part II').
isMember('ZRE','OIDA','Part II').
isMember('Z','OIDA','Part II').
isMember('BEN','OIDA','Part II').
isMember('BF','OIDA','Part II').
isMember('WAN','OIDA','Part II').
isMember('RT','OIDA','Part II').
isMember('RB','OIDA','Part II').
isMember('ZW','OIDA','Part II').
isMember('CI','OIDA','Part II').
isMember('GH','OIDA','Part II').
isMember('BI','OIDA','Part II').
isMember('RWA','OIDA','Part II').
isMember('EAT','OIDA','Part II').
isMember('CAM','OIDA','Part II').
isMember('RCA','OIDA','Part II').
isMember('TCH','OIDA','Part II').
isMember('GQ','OIDA','Part II').
isMember('G','OIDA','Part II').
isMember('CV','OIDA','Part II').
isMember('SUD','OIDA','Part II').
isMember('COM','OIDA','Part II').
isMember('RG','OIDA','Part II').
isMember('LB','OIDA','Part II').
isMember('DJI','OIDA','Part II').
isMember('ER','OIDA','Part II').
isMember('ETH','OIDA','Part II').
isMember('SP','OIDA','Part II').
isMember('EAK','OIDA','Part II').
isMember('WAG','OIDA','Part II').
isMember('SN','OIDA','Part II').
isMember('GNB','OIDA','Part II').
isMember('WAL','OIDA','Part II').
isMember('EAU','OIDA','Part II').
isMember('LS','OIDA','Part II').
isMember('RM','OIDA','Part II').
isMember('MW','OIDA','Part II').
isMember('MOC','OIDA','Part II').
isMember('MS','OIDA','Part II').
isMember('SD','OIDA','Part II').
isMember('STP','OIDA','Part II').
isMember('GR','OIEA',member).
isMember('F','OIEA',member).
isMember('E','OIEA',member).
isMember('A','OIEA',member).
isMember('D','OIEA',member).
isMember('I','OIEA',member).
isMember('CH','OIEA',member).
isMember('B','OIEA',member).
isMember('L','OIEA',member).
isMember('NL','OIEA',member).
isMember('TR','OIEA',member).
isMember('DK','OIEA',member).
isMember('SF','OIEA',member).
isMember('N','OIEA',member).
isMember('S','OIEA',member).
isMember('IRL','OIEA',member).
isMember('P','OIEA',member).
isMember('GB','OIEA',member).
isMember('J','OIEA',member).
isMember('CDN','OIEA',member).
isMember('USA','OIEA',member).
isMember('AUS','OIEA',member).
isMember('NZ','OIEA',member).
isMember('AL','OIFRCS',member).
isMember('GR','OIFRCS',member).
isMember('MK','OIFRCS',member).
isMember('SRB','OIFRCS',member).
isMember('MNE','OIFRCS',member).
isMember('AND','OIFRCS',member).
isMember('F','OIFRCS',member).
isMember('E','OIFRCS',member).
isMember('A','OIFRCS',member).
isMember('CZ','OIFRCS',member).
isMember('D','OIFRCS',member).
isMember('H','OIFRCS',member).
isMember('I','OIFRCS',member).
isMember('FL','OIFRCS',member).
isMember('SK','OIFRCS',member).
isMember('SLO','OIFRCS',member).
isMember('CH','OIFRCS',member).
isMember('BY','OIFRCS',member).
isMember('LV','OIFRCS',member).
isMember('LT','OIFRCS',member).
isMember('PL','OIFRCS',member).
isMember('UA','OIFRCS',member).
isMember('R','OIFRCS',member).
isMember('B','OIFRCS',member).
isMember('L','OIFRCS',member).
isMember('NL','OIFRCS',member).
isMember('HR','OIFRCS',member).
isMember('BG','OIFRCS',member).
isMember('RO','OIFRCS',member).
isMember('TR','OIFRCS',member).
isMember('DK','OIFRCS',member).
isMember('EW','OIFRCS',member).
isMember('SF','OIFRCS',member).
isMember('N','OIFRCS',member).
isMember('S','OIFRCS',member).
isMember('MC','OIFRCS',member).
isMember('IS','OIFRCS',member).
isMember('IRL','OIFRCS',member).
isMember('RSM','OIFRCS',member).
isMember('M','OIFRCS',member).
isMember('P','OIFRCS',member).
isMember('GB','OIFRCS',member).
isMember('AFG','OIFRCS',member).
isMember('CN','OIFRCS',member).
isMember('IR','OIFRCS',member).
isMember('PK','OIFRCS',member).
isMember('TM','OIFRCS',member).
isMember('UZB','OIFRCS',member).
isMember('ARM','OIFRCS',member).
isMember('AZ','OIFRCS',member).
isMember('BRN','OIFRCS',member).
isMember('BD','OIFRCS',member).
isMember('MYA','OIFRCS',member).
isMember('IND','OIFRCS',member).
isMember('MAL','OIFRCS',member).
isMember('LAO','OIFRCS',member).
isMember('THA','OIFRCS',member).
isMember('K','OIFRCS',member).
isMember('VN','OIFRCS',member).
isMember('NOK','OIFRCS',member).
isMember('MNG','OIFRCS',member).
isMember('NEP','OIFRCS',member).
isMember('ET','OIFRCS',member).
isMember('RI','OIFRCS',member).
isMember('TL','OIFRCS',member).
isMember('PNG','OIFRCS',member).
isMember('IRQ','OIFRCS',member).
isMember('JOR','OIFRCS',member).
isMember('KWT','OIFRCS',member).
isMember('SA','OIFRCS',member).
isMember('SYR','OIFRCS',member).
isMember('RL','OIFRCS',member).
isMember('J','OIFRCS',member).
isMember('ROK','OIFRCS',member).
isMember('UAE','OIFRCS',member).
isMember('YE','OIFRCS',member).
isMember('RP','OIFRCS',member).
isMember('Q','OIFRCS',member).
isMember('SGP','OIFRCS',member).
isMember('CL','OIFRCS',member).
isMember('AG','OIFRCS',member).
isMember('BS','OIFRCS',member).
isMember('BDS','OIFRCS',member).
isMember('BZ','OIFRCS',member).
isMember('GCA','OIFRCS',member).
isMember('MEX','OIFRCS',member).
isMember('CDN','OIFRCS',member).
isMember('USA','OIFRCS',member).
isMember('CR','OIFRCS',member).
isMember('NIC','OIFRCS',member).
isMember('PA','OIFRCS',member).
isMember('C','OIFRCS',member).
isMember('WD','OIFRCS',member).
isMember('DOM','OIFRCS',member).
isMember('RH','OIFRCS',member).
isMember('ES','OIFRCS',member).
isMember('HCA','OIFRCS',member).
isMember('WG','OIFRCS',member).
isMember('JA','OIFRCS',member).
isMember('CO','OIFRCS',member).
isMember('KN','OIFRCS',member).
isMember('WL','OIFRCS',member).
isMember('WV','OIFRCS',member).
isMember('TT','OIFRCS',member).
isMember('AUS','OIFRCS',member).
isMember('COOK','OIFRCS',member).
isMember('FJI','OIFRCS',member).
isMember('NZ','OIFRCS',member).
isMember('SLB','OIFRCS',member).
isMember('TO','OIFRCS',member).
isMember('VU','OIFRCS',member).
isMember('WS','OIFRCS',member).
isMember('RA','OIFRCS',member).
isMember('BOL','OIFRCS',member).
isMember('BR','OIFRCS',member).
isMember('RCH','OIFRCS',member).
isMember('PY','OIFRCS',member).
isMember('ROU','OIFRCS',member).
isMember('PE','OIFRCS',member).
isMember('GUY','OIFRCS',member).
isMember('SME','OIFRCS',member).
isMember('YV','OIFRCS',member).
isMember('EC','OIFRCS',member).
isMember('DZ','OIFRCS',member).
isMember('LAR','OIFRCS',member).
isMember('RMM','OIFRCS',member).
isMember('RIM','OIFRCS',member).
isMember('MA','OIFRCS',member).
isMember('RN','OIFRCS',member).
isMember('TN','OIFRCS',member).
isMember('ANG','OIFRCS',member).
isMember('RCB','OIFRCS',member).
isMember('NAM','OIFRCS',member).
isMember('ZRE','OIFRCS',member).
isMember('Z','OIFRCS',member).
isMember('BEN','OIFRCS',member).
isMember('BF','OIFRCS',member).
isMember('WAN','OIFRCS',member).
isMember('RT','OIFRCS',member).
isMember('RB','OIFRCS',member).
isMember('RSA','OIFRCS',member).
isMember('ZW','OIFRCS',member).
isMember('CI','OIFRCS',member).
isMember('GH','OIFRCS',member).
isMember('BI','OIFRCS',member).
isMember('RWA','OIFRCS',member).
isMember('EAT','OIFRCS',member).
isMember('CAM','OIFRCS',member).
isMember('RCA','OIFRCS',member).
isMember('TCH','OIFRCS',member).
isMember('GQ','OIFRCS',member).
isMember('CV','OIFRCS',member).
isMember('SUD','OIFRCS',member).
isMember('RG','OIFRCS',member).
isMember('LB','OIFRCS',member).
isMember('DJI','OIFRCS',member).
isMember('ETH','OIFRCS',member).
isMember('SP','OIFRCS',member).
isMember('EAK','OIFRCS',member).
isMember('WAG','OIFRCS',member).
isMember('SN','OIFRCS',member).
isMember('GNB','OIFRCS',member).
isMember('WAL','OIFRCS',member).
isMember('EAU','OIFRCS',member).
isMember('LS','OIFRCS',member).
isMember('RM','OIFRCS',member).
isMember('MW','OIFRCS',member).
isMember('MOC','OIFRCS',member).
isMember('MS','OIFRCS',member).
isMember('SD','OIFRCS',member).
isMember('STP','OIFRCS',member).
isMember('SY','OIFRCS',member).
isMember('CY','OIFRCS','associate member').
isMember('KIR','OIFRCS','associate member').
isMember('TUV','OIFRCS','associate member').
isMember('G','OIFRCS','associate member').
isMember('COM','OIFRCS','associate member').
isMember('AL','OIFC',member).
isMember('GR','OIFC',member).
isMember('MK','OIFC',member).
isMember('SRB','OIFC',member).
isMember('MNE','OIFC',member).
isMember('F','OIFC',member).
isMember('E','OIFC',member).
isMember('A','OIFC',member).
isMember('CZ','OIFC',member).
isMember('D','OIFC',member).
isMember('H','OIFC',member).
isMember('I','OIFC',member).
isMember('SK','OIFC',member).
isMember('SLO','OIFC',member).
isMember('CH','OIFC',member).
isMember('BY','OIFC',member).
isMember('LV','OIFC',member).
isMember('LT','OIFC',member).
isMember('PL','OIFC',member).
isMember('UA','OIFC',member).
isMember('R','OIFC',member).
isMember('B','OIFC',member).
isMember('L','OIFC',member).
isMember('NL','OIFC',member).
isMember('HR','OIFC',member).
isMember('BG','OIFC',member).
isMember('RO','OIFC',member).
isMember('TR','OIFC',member).
isMember('DK','OIFC',member).
isMember('EW','OIFC',member).
isMember('SF','OIFC',member).
isMember('N','OIFC',member).
isMember('S','OIFC',member).
isMember('IS','OIFC',member).
isMember('IRL','OIFC',member).
isMember('MD','OIFC',member).
isMember('P','OIFC',member).
isMember('GB','OIFC',member).
isMember('AFG','OIFC',member).
isMember('CN','OIFC',member).
isMember('IR','OIFC',member).
isMember('PK','OIFC',member).
isMember('TAD','OIFC',member).
isMember('UZB','OIFC',member).
isMember('ARM','OIFC',member).
isMember('GE','OIFC',member).
isMember('BD','OIFC',member).
isMember('MYA','OIFC',member).
isMember('IND','OIFC',member).
isMember('MAL','OIFC',member).
isMember('LAO','OIFC',member).
isMember('THA','OIFC',member).
isMember('VN','OIFC',member).
isMember('KAZ','OIFC',member).
isMember('KGZ','OIFC',member).
isMember('MNG','OIFC',member).
isMember('NEP','OIFC',member).
isMember('CY','OIFC',member).
isMember('IL','OIFC',member).
isMember('ET','OIFC',member).
isMember('RI','OIFC',member).
isMember('TL','OIFC',member).
isMember('PNG','OIFC',member).
isMember('IRQ','OIFC',member).
isMember('JOR','OIFC',member).
isMember('KWT','OIFC',member).
isMember('SA','OIFC',member).
isMember('SYR','OIFC',member).
isMember('RL','OIFC',member).
isMember('J','OIFC',member).
isMember('ROK','OIFC',member).
isMember('MV','OIFC',member).
isMember('OM','OIFC',member).
isMember('UAE','OIFC',member).
isMember('YE','OIFC',member).
isMember('RP','OIFC',member).
isMember('SGP','OIFC',member).
isMember('CL','OIFC',member).
isMember('AG','OIFC',member).
isMember('BS','OIFC',member).
isMember('BDS','OIFC',member).
isMember('BZ','OIFC',member).
isMember('GCA','OIFC',member).
isMember('MEX','OIFC',member).
isMember('CDN','OIFC',member).
isMember('USA','OIFC',member).
isMember('CR','OIFC',member).
isMember('NIC','OIFC',member).
isMember('PA','OIFC',member).
isMember('WD','OIFC',member).
isMember('DOM','OIFC',member).
isMember('RH','OIFC',member).
isMember('ES','OIFC',member).
isMember('HCA','OIFC',member).
isMember('WG','OIFC',member).
isMember('JA','OIFC',member).
isMember('CO','OIFC',member).
isMember('WL','OIFC',member).
isMember('TT','OIFC',member).
isMember('AUS','OIFC',member).
isMember('FJI','OIFC',member).
isMember('KIR','OIFC',member).
isMember('MH','OIFC',member).
isMember('NZ','OIFC',member).
isMember('SLB','OIFC',member).
isMember('TO','OIFC',member).
isMember('VU','OIFC',member).
isMember('WS','OIFC',member).
isMember('RA','OIFC',member).
isMember('BOL','OIFC',member).
isMember('BR','OIFC',member).
isMember('RCH','OIFC',member).
isMember('PY','OIFC',member).
isMember('ROU','OIFC',member).
isMember('PE','OIFC',member).
isMember('GUY','OIFC',member).
isMember('YV','OIFC',member).
isMember('EC','OIFC',member).
isMember('DZ','OIFC',member).
isMember('LAR','OIFC',member).
isMember('RMM','OIFC',member).
isMember('RIM','OIFC',member).
isMember('MA','OIFC',member).
isMember('RN','OIFC',member).
isMember('TN','OIFC',member).
isMember('ANG','OIFC',member).
isMember('RCB','OIFC',member).
isMember('NAM','OIFC',member).
isMember('ZRE','OIFC',member).
isMember('Z','OIFC',member).
isMember('BEN','OIFC',member).
isMember('BF','OIFC',member).
isMember('WAN','OIFC',member).
isMember('RT','OIFC',member).
isMember('RB','OIFC',member).
isMember('RSA','OIFC',member).
isMember('ZW','OIFC',member).
isMember('CI','OIFC',member).
isMember('GH','OIFC',member).
isMember('BI','OIFC',member).
isMember('RWA','OIFC',member).
isMember('EAT','OIFC',member).
isMember('CAM','OIFC',member).
isMember('RCA','OIFC',member).
isMember('GQ','OIFC',member).
isMember('G','OIFC',member).
isMember('CV','OIFC',member).
isMember('SUD','OIFC',member).
isMember('COM','OIFC',member).
isMember('RG','OIFC',member).
isMember('LB','OIFC',member).
isMember('DJI','OIFC',member).
isMember('ETH','OIFC',member).
isMember('SP','OIFC',member).
isMember('EAK','OIFC',member).
isMember('WAG','OIFC',member).
isMember('SN','OIFC',member).
isMember('GNB','OIFC',member).
isMember('WAL','OIFC',member).
isMember('EAU','OIFC',member).
isMember('LS','OIFC',member).
isMember('RM','OIFC',member).
isMember('MW','OIFC',member).
isMember('MOC','OIFC',member).
isMember('MS','OIFC',member).
isMember('SD','OIFC',member).
isMember('SY','OIFC',member).
isMember('GR','OIFAD','Category I').
isMember('F','OIFAD','Category I').
isMember('E','OIFAD','Category I').
isMember('A','OIFAD','Category I').
isMember('D','OIFAD','Category I').
isMember('I','OIFAD','Category I').
isMember('CH','OIFAD','Category I').
isMember('B','OIFAD','Category I').
isMember('L','OIFAD','Category I').
isMember('NL','OIFAD','Category I').
isMember('DK','OIFAD','Category I').
isMember('SF','OIFAD','Category I').
isMember('N','OIFAD','Category I').
isMember('S','OIFAD','Category I').
isMember('IRL','OIFAD','Category I').
isMember('GB','OIFAD','Category I').
isMember('J','OIFAD','Category I').
isMember('CDN','OIFAD','Category I').
isMember('USA','OIFAD','Category I').
isMember('AUS','OIFAD','Category I').
isMember('NZ','OIFAD','Category I').
isMember('IR','OIFAD','Category II').
isMember('RI','OIFAD','Category II').
isMember('IRQ','OIFAD','Category II').
isMember('KWT','OIFAD','Category II').
isMember('SA','OIFAD','Category II').
isMember('UAE','OIFAD','Category II').
isMember('Q','OIFAD','Category II').
isMember('YV','OIFAD','Category II').
isMember('DZ','OIFAD','Category II').
isMember('LAR','OIFAD','Category II').
isMember('WAN','OIFAD','Category II').
isMember('G','OIFAD','Category II').
isMember('AL','OIFAD','Category III').
isMember('MK','OIFAD','Category III').
isMember('BIH','OIFAD','Category III').
isMember('HR','OIFAD','Category III').
isMember('RO','OIFAD','Category III').
isMember('TR','OIFAD','Category III').
isMember('M','OIFAD','Category III').
isMember('P','OIFAD','Category III').
isMember('AFG','OIFAD','Category III').
isMember('CN','OIFAD','Category III').
isMember('PK','OIFAD','Category III').
isMember('TAD','OIFAD','Category III').
isMember('ARM','OIFAD','Category III').
isMember('GE','OIFAD','Category III').
isMember('AZ','OIFAD','Category III').
isMember('BD','OIFAD','Category III').
isMember('MYA','OIFAD','Category III').
isMember('IND','OIFAD','Category III').
isMember('BHT','OIFAD','Category III').
isMember('MAL','OIFAD','Category III').
isMember('LAO','OIFAD','Category III').
isMember('THA','OIFAD','Category III').
isMember('K','OIFAD','Category III').
isMember('VN','OIFAD','Category III').
isMember('NOK','OIFAD','Category III').
isMember('KGZ','OIFAD','Category III').
isMember('MNG','OIFAD','Category III').
isMember('NEP','OIFAD','Category III').
isMember('CY','OIFAD','Category III').
isMember('IL','OIFAD','Category III').
isMember('ET','OIFAD','Category III').
isMember('TL','OIFAD','Category III').
isMember('PNG','OIFAD','Category III').
isMember('JOR','OIFAD','Category III').
isMember('SYR','OIFAD','Category III').
isMember('RL','OIFAD','Category III').
isMember('ROK','OIFAD','Category III').
isMember('MV','OIFAD','Category III').
isMember('OM','OIFAD','Category III').
isMember('YE','OIFAD','Category III').
isMember('RP','OIFAD','Category III').
isMember('CL','OIFAD','Category III').
isMember('AG','OIFAD','Category III').
isMember('BDS','OIFAD','Category III').
isMember('BZ','OIFAD','Category III').
isMember('GCA','OIFAD','Category III').
isMember('MEX','OIFAD','Category III').
isMember('CR','OIFAD','Category III').
isMember('NIC','OIFAD','Category III').
isMember('PA','OIFAD','Category III').
isMember('C','OIFAD','Category III').
isMember('WD','OIFAD','Category III').
isMember('DOM','OIFAD','Category III').
isMember('RH','OIFAD','Category III').
isMember('ES','OIFAD','Category III').
isMember('HCA','OIFAD','Category III').
isMember('WG','OIFAD','Category III').
isMember('JA','OIFAD','Category III').
isMember('CO','OIFAD','Category III').
isMember('KN','OIFAD','Category III').
isMember('WL','OIFAD','Category III').
isMember('WV','OIFAD','Category III').
isMember('TT','OIFAD','Category III').
isMember('COOK','OIFAD','Category III').
isMember('FJI','OIFAD','Category III').
isMember('NIUE','OIFAD','Category III').
isMember('SLB','OIFAD','Category III').
isMember('TO','OIFAD','Category III').
isMember('WS','OIFAD','Category III').
isMember('RA','OIFAD','Category III').
isMember('BOL','OIFAD','Category III').
isMember('BR','OIFAD','Category III').
isMember('RCH','OIFAD','Category III').
isMember('PY','OIFAD','Category III').
isMember('ROU','OIFAD','Category III').
isMember('PE','OIFAD','Category III').
isMember('GUY','OIFAD','Category III').
isMember('SME','OIFAD','Category III').
isMember('EC','OIFAD','Category III').
isMember('RMM','OIFAD','Category III').
isMember('RIM','OIFAD','Category III').
isMember('MA','OIFAD','Category III').
isMember('RN','OIFAD','Category III').
isMember('TN','OIFAD','Category III').
isMember('ANG','OIFAD','Category III').
isMember('RCB','OIFAD','Category III').
isMember('NAM','OIFAD','Category III').
isMember('ZRE','OIFAD','Category III').
isMember('Z','OIFAD','Category III').
isMember('BEN','OIFAD','Category III').
isMember('BF','OIFAD','Category III').
isMember('RT','OIFAD','Category III').
isMember('RB','OIFAD','Category III').
isMember('ZW','OIFAD','Category III').
isMember('CI','OIFAD','Category III').
isMember('GH','OIFAD','Category III').
isMember('BI','OIFAD','Category III').
isMember('RWA','OIFAD','Category III').
isMember('EAT','OIFAD','Category III').
isMember('CAM','OIFAD','Category III').
isMember('RCA','OIFAD','Category III').
isMember('TCH','OIFAD','Category III').
isMember('GQ','OIFAD','Category III').
isMember('CV','OIFAD','Category III').
isMember('SUD','OIFAD','Category III').
isMember('COM','OIFAD','Category III').
isMember('RG','OIFAD','Category III').
isMember('LB','OIFAD','Category III').
isMember('DJI','OIFAD','Category III').
isMember('ER','OIFAD','Category III').
isMember('ETH','OIFAD','Category III').
isMember('SP','OIFAD','Category III').
isMember('EAK','OIFAD','Category III').
isMember('WAG','OIFAD','Category III').
isMember('SN','OIFAD','Category III').
isMember('GNB','OIFAD','Category III').
isMember('WAL','OIFAD','Category III').
isMember('EAU','OIFAD','Category III').
isMember('LS','OIFAD','Category III').
isMember('RM','OIFAD','Category III').
isMember('MW','OIFAD','Category III').
isMember('MOC','OIFAD','Category III').
isMember('MS','OIFAD','Category III').
isMember('SD','OIFAD','Category III').
isMember('STP','OIFAD','Category III').
isMember('SY','OIFAD','Category III').
isMember('AL','OILO',member).
isMember('GR','OILO',member).
isMember('MK','OILO',member).
isMember('SRB','OILO',member).
isMember('MNE','OILO',member).
isMember('F','OILO',member).
isMember('E','OILO',member).
isMember('A','OILO',member).
isMember('CZ','OILO',member).
isMember('D','OILO',member).
isMember('H','OILO',member).
isMember('I','OILO',member).
isMember('SK','OILO',member).
isMember('SLO','OILO',member).
isMember('CH','OILO',member).
isMember('BY','OILO',member).
isMember('LV','OILO',member).
isMember('LT','OILO',member).
isMember('PL','OILO',member).
isMember('UA','OILO',member).
isMember('R','OILO',member).
isMember('B','OILO',member).
isMember('L','OILO',member).
isMember('NL','OILO',member).
isMember('BIH','OILO',member).
isMember('HR','OILO',member).
isMember('BG','OILO',member).
isMember('RO','OILO',member).
isMember('TR','OILO',member).
isMember('DK','OILO',member).
isMember('EW','OILO',member).
isMember('SF','OILO',member).
isMember('N','OILO',member).
isMember('S','OILO',member).
isMember('IS','OILO',member).
isMember('IRL','OILO',member).
isMember('RSM','OILO',member).
isMember('M','OILO',member).
isMember('MD','OILO',member).
isMember('P','OILO',member).
isMember('GB','OILO',member).
isMember('AFG','OILO',member).
isMember('CN','OILO',member).
isMember('IR','OILO',member).
isMember('PK','OILO',member).
isMember('TAD','OILO',member).
isMember('TM','OILO',member).
isMember('UZB','OILO',member).
isMember('ARM','OILO',member).
isMember('GE','OILO',member).
isMember('AZ','OILO',member).
isMember('BRN','OILO',member).
isMember('BD','OILO',member).
isMember('MYA','OILO',member).
isMember('IND','OILO',member).
isMember('MAL','OILO',member).
isMember('LAO','OILO',member).
isMember('THA','OILO',member).
isMember('K','OILO',member).
isMember('VN','OILO',member).
isMember('KAZ','OILO',member).
isMember('KGZ','OILO',member).
isMember('MNG','OILO',member).
isMember('NEP','OILO',member).
isMember('CY','OILO',member).
isMember('IL','OILO',member).
isMember('ET','OILO',member).
isMember('RI','OILO',member).
isMember('TL','OILO',member).
isMember('PNG','OILO',member).
isMember('IRQ','OILO',member).
isMember('JOR','OILO',member).
isMember('KWT','OILO',member).
isMember('SA','OILO',member).
isMember('SYR','OILO',member).
isMember('RL','OILO',member).
isMember('J','OILO',member).
isMember('ROK','OILO',member).
isMember('OM','OILO',member).
isMember('UAE','OILO',member).
isMember('YE','OILO',member).
isMember('RP','OILO',member).
isMember('Q','OILO',member).
isMember('SGP','OILO',member).
isMember('CL','OILO',member).
isMember('AG','OILO',member).
isMember('ARU','OILO',member).
isMember('BS','OILO',member).
isMember('BDS','OILO',member).
isMember('BZ','OILO',member).
isMember('GCA','OILO',member).
isMember('MEX','OILO',member).
isMember('CDN','OILO',member).
isMember('USA','OILO',member).
isMember('CR','OILO',member).
isMember('NIC','OILO',member).
isMember('PA','OILO',member).
isMember('C','OILO',member).
isMember('WD','OILO',member).
isMember('DOM','OILO',member).
isMember('RH','OILO',member).
isMember('ES','OILO',member).
isMember('HCA','OILO',member).
isMember('WG','OILO',member).
isMember('JA','OILO',member).
isMember('NA','OILO',member).
isMember('CO','OILO',member).
isMember('WL','OILO',member).
isMember('WV','OILO',member).
isMember('TT','OILO',member).
isMember('AUS','OILO',member).
isMember('FJI','OILO',member).
isMember('NZ','OILO',member).
isMember('SLB','OILO',member).
isMember('RA','OILO',member).
isMember('BOL','OILO',member).
isMember('BR','OILO',member).
isMember('RCH','OILO',member).
isMember('PY','OILO',member).
isMember('ROU','OILO',member).
isMember('PE','OILO',member).
isMember('GUY','OILO',member).
isMember('SME','OILO',member).
isMember('YV','OILO',member).
isMember('EC','OILO',member).
isMember('DZ','OILO',member).
isMember('LAR','OILO',member).
isMember('RMM','OILO',member).
isMember('RIM','OILO',member).
isMember('MA','OILO',member).
isMember('RN','OILO',member).
isMember('TN','OILO',member).
isMember('ANG','OILO',member).
isMember('RCB','OILO',member).
isMember('NAM','OILO',member).
isMember('ZRE','OILO',member).
isMember('Z','OILO',member).
isMember('BEN','OILO',member).
isMember('BF','OILO',member).
isMember('WAN','OILO',member).
isMember('RT','OILO',member).
isMember('RB','OILO',member).
isMember('RSA','OILO',member).
isMember('ZW','OILO',member).
isMember('CI','OILO',member).
isMember('GH','OILO',member).
isMember('BI','OILO',member).
isMember('RWA','OILO',member).
isMember('EAT','OILO',member).
isMember('CAM','OILO',member).
isMember('RCA','OILO',member).
isMember('TCH','OILO',member).
isMember('GQ','OILO',member).
isMember('G','OILO',member).
isMember('CV','OILO',member).
isMember('SUD','OILO',member).
isMember('COM','OILO',member).
isMember('RG','OILO',member).
isMember('LB','OILO',member).
isMember('DJI','OILO',member).
isMember('ER','OILO',member).
isMember('ETH','OILO',member).
isMember('SP','OILO',member).
isMember('EAK','OILO',member).
isMember('WAG','OILO',member).
isMember('SN','OILO',member).
isMember('GNB','OILO',member).
isMember('WAL','OILO',member).
isMember('EAU','OILO',member).
isMember('LS','OILO',member).
isMember('RM','OILO',member).
isMember('MW','OILO',member).
isMember('MOC','OILO',member).
isMember('MS','OILO',member).
isMember('SD','OILO',member).
isMember('STP','OILO',member).
isMember('SY','OILO',member).
isMember('AL','OIMO',member).
isMember('GR','OIMO',member).
isMember('MK','OIMO',member).
isMember('SRB','OIMO',member).
isMember('MNE','OIMO',member).
isMember('F','OIMO',member).
isMember('E','OIMO',member).
isMember('A','OIMO',member).
isMember('CZ','OIMO',member).
isMember('D','OIMO',member).
isMember('H','OIMO',member).
isMember('I','OIMO',member).
isMember('SK','OIMO',member).
isMember('SLO','OIMO',member).
isMember('CH','OIMO',member).
isMember('LV','OIMO',member).
isMember('PL','OIMO',member).
isMember('UA','OIMO',member).
isMember('R','OIMO',member).
isMember('B','OIMO',member).
isMember('L','OIMO',member).
isMember('NL','OIMO',member).
isMember('BIH','OIMO',member).
isMember('HR','OIMO',member).
isMember('BG','OIMO',member).
isMember('RO','OIMO',member).
isMember('TR','OIMO',member).
isMember('DK','OIMO',member).
isMember('EW','OIMO',member).
isMember('SF','OIMO',member).
isMember('N','OIMO',member).
isMember('S','OIMO',member).
isMember('MC','OIMO',member).
isMember('IS','OIMO',member).
isMember('IRL','OIMO',member).
isMember('M','OIMO',member).
isMember('P','OIMO',member).
isMember('GB','OIMO',member).
isMember('CN','OIMO',member).
isMember('IR','OIMO',member).
isMember('PK','OIMO',member).
isMember('TM','OIMO',member).
isMember('GE','OIMO',member).
isMember('AZ','OIMO',member).
isMember('BRN','OIMO',member).
isMember('BD','OIMO',member).
isMember('MYA','OIMO',member).
isMember('IND','OIMO',member).
isMember('BRU','OIMO',member).
isMember('MAL','OIMO',member).
isMember('THA','OIMO',member).
isMember('K','OIMO',member).
isMember('VN','OIMO',member).
isMember('KAZ','OIMO',member).
isMember('NOK','OIMO',member).
isMember('NEP','OIMO',member).
isMember('CY','OIMO',member).
isMember('IL','OIMO',member).
isMember('ET','OIMO',member).
isMember('RI','OIMO',member).
isMember('TL','OIMO',member).
isMember('PNG','OIMO',member).
isMember('IRQ','OIMO',member).
isMember('JOR','OIMO',member).
isMember('KWT','OIMO',member).
isMember('SA','OIMO',member).
isMember('SYR','OIMO',member).
isMember('RL','OIMO',member).
isMember('J','OIMO',member).
isMember('ROK','OIMO',member).
isMember('MV','OIMO',member).
isMember('OM','OIMO',member).
isMember('UAE','OIMO',member).
isMember('YE','OIMO',member).
isMember('RP','OIMO',member).
isMember('Q','OIMO',member).
isMember('SGP','OIMO',member).
isMember('CL','OIMO',member).
isMember('AG','OIMO',member).
isMember('BS','OIMO',member).
isMember('BDS','OIMO',member).
isMember('BZ','OIMO',member).
isMember('GCA','OIMO',member).
isMember('MEX','OIMO',member).
isMember('CDN','OIMO',member).
isMember('USA','OIMO',member).
isMember('CR','OIMO',member).
isMember('NIC','OIMO',member).
isMember('PA','OIMO',member).
isMember('C','OIMO',member).
isMember('WD','OIMO',member).
isMember('DOM','OIMO',member).
isMember('RH','OIMO',member).
isMember('ES','OIMO',member).
isMember('HCA','OIMO',member).
isMember('JA','OIMO',member).
isMember('CO','OIMO',member).
isMember('WL','OIMO',member).
isMember('WV','OIMO',member).
isMember('TT','OIMO',member).
isMember('AUS','OIMO',member).
isMember('FJI','OIMO',member).
isMember('NZ','OIMO',member).
isMember('SLB','OIMO',member).
isMember('VU','OIMO',member).
isMember('RA','OIMO',member).
isMember('BOL','OIMO',member).
isMember('BR','OIMO',member).
isMember('RCH','OIMO',member).
isMember('PY','OIMO',member).
isMember('ROU','OIMO',member).
isMember('PE','OIMO',member).
isMember('GUY','OIMO',member).
isMember('SME','OIMO',member).
isMember('YV','OIMO',member).
isMember('EC','OIMO',member).
isMember('DZ','OIMO',member).
isMember('LAR','OIMO',member).
isMember('RIM','OIMO',member).
isMember('MA','OIMO',member).
isMember('TN','OIMO',member).
isMember('ANG','OIMO',member).
isMember('RCB','OIMO',member).
isMember('NAM','OIMO',member).
isMember('ZRE','OIMO',member).
isMember('BEN','OIMO',member).
isMember('WAN','OIMO',member).
isMember('RT','OIMO',member).
isMember('RSA','OIMO',member).
isMember('CI','OIMO',member).
isMember('GH','OIMO',member).
isMember('EAT','OIMO',member).
isMember('CAM','OIMO',member).
isMember('GQ','OIMO',member).
isMember('G','OIMO',member).
isMember('CV','OIMO',member).
isMember('SUD','OIMO',member).
isMember('RG','OIMO',member).
isMember('LB','OIMO',member).
isMember('DJI','OIMO',member).
isMember('ER','OIMO',member).
isMember('ETH','OIMO',member).
isMember('SP','OIMO',member).
isMember('EAK','OIMO',member).
isMember('WAG','OIMO',member).
isMember('SN','OIMO',member).
isMember('GNB','OIMO',member).
isMember('WAL','OIMO',member).
isMember('RM','OIMO',member).
isMember('MW','OIMO',member).
isMember('MOC','OIMO',member).
isMember('MS','OIMO',member).
isMember('STP','OIMO',member).
isMember('SY','OIMO',member).
isMember('HONX','OIMO',associated).
isMember('MACX','OIMO',associated).
isMember('GR','OInmarsat',member).
isMember('F','OInmarsat',member).
isMember('E','OInmarsat',member).
isMember('CZ','OInmarsat',member).
isMember('D','OInmarsat',member).
isMember('I','OInmarsat',member).
isMember('SK','OInmarsat',member).
isMember('CH','OInmarsat',member).
isMember('BY','OInmarsat',member).
isMember('PL','OInmarsat',member).
isMember('UA','OInmarsat',member).
isMember('R','OInmarsat',member).
isMember('B','OInmarsat',member).
isMember('NL','OInmarsat',member).
isMember('HR','OInmarsat',member).
isMember('BG','OInmarsat',member).
isMember('RO','OInmarsat',member).
isMember('TR','OInmarsat',member).
isMember('DK','OInmarsat',member).
isMember('SF','OInmarsat',member).
isMember('N','OInmarsat',member).
isMember('S','OInmarsat',member).
isMember('MC','OInmarsat',member).
isMember('IS','OInmarsat',member).
isMember('M','OInmarsat',member).
isMember('P','OInmarsat',member).
isMember('GB','OInmarsat',member).
isMember('CN','OInmarsat',member).
isMember('IR','OInmarsat',member).
isMember('PK','OInmarsat',member).
isMember('GE','OInmarsat',member).
isMember('BRN','OInmarsat',member).
isMember('BD','OInmarsat',member).
isMember('IND','OInmarsat',member).
isMember('BRU','OInmarsat',member).
isMember('MAL','OInmarsat',member).
isMember('CY','OInmarsat',member).
isMember('IL','OInmarsat',member).
isMember('ET','OInmarsat',member).
isMember('RI','OInmarsat',member).
isMember('IRQ','OInmarsat',member).
isMember('KWT','OInmarsat',member).
isMember('SA','OInmarsat',member).
isMember('J','OInmarsat',member).
isMember('ROK','OInmarsat',member).
isMember('OM','OInmarsat',member).
isMember('UAE','OInmarsat',member).
isMember('RP','OInmarsat',member).
isMember('Q','OInmarsat',member).
isMember('SGP','OInmarsat',member).
isMember('CL','OInmarsat',member).
isMember('BS','OInmarsat',member).
isMember('MEX','OInmarsat',member).
isMember('CDN','OInmarsat',member).
isMember('USA','OInmarsat',member).
isMember('PA','OInmarsat',member).
isMember('C','OInmarsat',member).
isMember('CO','OInmarsat',member).
isMember('AUS','OInmarsat',member).
isMember('NZ','OInmarsat',member).
isMember('RA','OInmarsat',member).
isMember('BR','OInmarsat',member).
isMember('RCH','OInmarsat',member).
isMember('PE','OInmarsat',member).
isMember('DZ','OInmarsat',member).
isMember('TN','OInmarsat',member).
isMember('WAN','OInmarsat',member).
isMember('RSA','OInmarsat',member).
isMember('CAM','OInmarsat',member).
isMember('G','OInmarsat',member).
isMember('LB','OInmarsat',member).
isMember('SN','OInmarsat',member).
isMember('MOC','OInmarsat',member).
isMember('MS','OInmarsat',member).
isMember('AL','OIMF',member).
isMember('GR','OIMF',member).
isMember('MK','OIMF',member).
isMember('SRB','OIMF',member).
isMember('MNE','OIMF',member).
isMember('F','OIMF',member).
isMember('E','OIMF',member).
isMember('A','OIMF',member).
isMember('CZ','OIMF',member).
isMember('D','OIMF',member).
isMember('H','OIMF',member).
isMember('I','OIMF',member).
isMember('SK','OIMF',member).
isMember('SLO','OIMF',member).
isMember('CH','OIMF',member).
isMember('BY','OIMF',member).
isMember('LV','OIMF',member).
isMember('LT','OIMF',member).
isMember('PL','OIMF',member).
isMember('UA','OIMF',member).
isMember('R','OIMF',member).
isMember('B','OIMF',member).
isMember('L','OIMF',member).
isMember('NL','OIMF',member).
isMember('HR','OIMF',member).
isMember('BG','OIMF',member).
isMember('RO','OIMF',member).
isMember('TR','OIMF',member).
isMember('DK','OIMF',member).
isMember('EW','OIMF',member).
isMember('SF','OIMF',member).
isMember('N','OIMF',member).
isMember('S','OIMF',member).
isMember('IS','OIMF',member).
isMember('IRL','OIMF',member).
isMember('RSM','OIMF',member).
isMember('M','OIMF',member).
isMember('MD','OIMF',member).
isMember('P','OIMF',member).
isMember('GB','OIMF',member).
isMember('AFG','OIMF',member).
isMember('CN','OIMF',member).
isMember('IR','OIMF',member).
isMember('PK','OIMF',member).
isMember('TAD','OIMF',member).
isMember('TM','OIMF',member).
isMember('UZB','OIMF',member).
isMember('ARM','OIMF',member).
isMember('GE','OIMF',member).
isMember('AZ','OIMF',member).
isMember('BRN','OIMF',member).
isMember('BD','OIMF',member).
isMember('MYA','OIMF',member).
isMember('IND','OIMF',member).
isMember('BHT','OIMF',member).
isMember('BRU','OIMF',member).
isMember('MAL','OIMF',member).
isMember('LAO','OIMF',member).
isMember('THA','OIMF',member).
isMember('K','OIMF',member).
isMember('VN','OIMF',member).
isMember('KAZ','OIMF',member).
isMember('KGZ','OIMF',member).
isMember('HONX','OIMF',member).
isMember('MACX','OIMF',member).
isMember('MNG','OIMF',member).
isMember('NEP','OIMF',member).
isMember('CY','OIMF',member).
isMember('IL','OIMF',member).
isMember('ET','OIMF',member).
isMember('RI','OIMF',member).
isMember('TL','OIMF',member).
isMember('PNG','OIMF',member).
isMember('IRQ','OIMF',member).
isMember('JOR','OIMF',member).
isMember('KWT','OIMF',member).
isMember('SA','OIMF',member).
isMember('SYR','OIMF',member).
isMember('RL','OIMF',member).
isMember('J','OIMF',member).
isMember('ROK','OIMF',member).
isMember('MV','OIMF',member).
isMember('OM','OIMF',member).
isMember('UAE','OIMF',member).
isMember('YE','OIMF',member).
isMember('RP','OIMF',member).
isMember('Q','OIMF',member).
isMember('SGP','OIMF',member).
isMember('CL','OIMF',member).
isMember('AG','OIMF',member).
isMember('ARU','OIMF',member).
isMember('BS','OIMF',member).
isMember('BDS','OIMF',member).
isMember('BZ','OIMF',member).
isMember('GCA','OIMF',member).
isMember('MEX','OIMF',member).
isMember('CDN','OIMF',member).
isMember('USA','OIMF',member).
isMember('CR','OIMF',member).
isMember('NIC','OIMF',member).
isMember('PA','OIMF',member).
isMember('WD','OIMF',member).
isMember('DOM','OIMF',member).
isMember('RH','OIMF',member).
isMember('ES','OIMF',member).
isMember('HCA','OIMF',member).
isMember('WG','OIMF',member).
isMember('JA','OIMF',member).
isMember('NA','OIMF',member).
isMember('CO','OIMF',member).
isMember('KN','OIMF',member).
isMember('WL','OIMF',member).
isMember('WV','OIMF',member).
isMember('TT','OIMF',member).
isMember('AUS','OIMF',member).
isMember('FJI','OIMF',member).
isMember('KIR','OIMF',member).
isMember('MH','OIMF',member).
isMember('NZ','OIMF',member).
isMember('SLB','OIMF',member).
isMember('TO','OIMF',member).
isMember('VU','OIMF',member).
isMember('WS','OIMF',member).
isMember('RA','OIMF',member).
isMember('BOL','OIMF',member).
isMember('BR','OIMF',member).
isMember('RCH','OIMF',member).
isMember('PY','OIMF',member).
isMember('ROU','OIMF',member).
isMember('PE','OIMF',member).
isMember('GUY','OIMF',member).
isMember('SME','OIMF',member).
isMember('YV','OIMF',member).
isMember('EC','OIMF',member).
isMember('DZ','OIMF',member).
isMember('LAR','OIMF',member).
isMember('RMM','OIMF',member).
isMember('RIM','OIMF',member).
isMember('MA','OIMF',member).
isMember('RN','OIMF',member).
isMember('TN','OIMF',member).
isMember('ANG','OIMF',member).
isMember('RCB','OIMF',member).
isMember('NAM','OIMF',member).
isMember('ZRE','OIMF',member).
isMember('Z','OIMF',member).
isMember('BEN','OIMF',member).
isMember('BF','OIMF',member).
isMember('WAN','OIMF',member).
isMember('RT','OIMF',member).
isMember('RB','OIMF',member).
isMember('RSA','OIMF',member).
isMember('ZW','OIMF',member).
isMember('CI','OIMF',member).
isMember('GH','OIMF',member).
isMember('BI','OIMF',member).
isMember('RWA','OIMF',member).
isMember('EAT','OIMF',member).
isMember('CAM','OIMF',member).
isMember('RCA','OIMF',member).
isMember('TCH','OIMF',member).
isMember('GQ','OIMF',member).
isMember('G','OIMF',member).
isMember('CV','OIMF',member).
isMember('SUD','OIMF',member).
isMember('COM','OIMF',member).
isMember('RG','OIMF',member).
isMember('LB','OIMF',member).
isMember('DJI','OIMF',member).
isMember('ER','OIMF',member).
isMember('ETH','OIMF',member).
isMember('SP','OIMF',member).
isMember('EAK','OIMF',member).
isMember('WAG','OIMF',member).
isMember('SN','OIMF',member).
isMember('GNB','OIMF',member).
isMember('WAL','OIMF',member).
isMember('EAU','OIMF',member).
isMember('LS','OIMF',member).
isMember('RM','OIMF',member).
isMember('MW','OIMF',member).
isMember('MOC','OIMF',member).
isMember('MS','OIMF',member).
isMember('SD','OIMF',member).
isMember('STP','OIMF',member).
isMember('SY','OIMF',member).
isMember('AL','OIOC','National Olympic Committee').
isMember('GR','OIOC','National Olympic Committee').
isMember('MK','OIOC','National Olympic Committee').
isMember('SRB','OIOC','National Olympic Committee').
isMember('MNE','OIOC','National Olympic Committee').
isMember('AND','OIOC','National Olympic Committee').
isMember('F','OIOC','National Olympic Committee').
isMember('E','OIOC','National Olympic Committee').
isMember('A','OIOC','National Olympic Committee').
isMember('CZ','OIOC','National Olympic Committee').
isMember('D','OIOC','National Olympic Committee').
isMember('H','OIOC','National Olympic Committee').
isMember('I','OIOC','National Olympic Committee').
isMember('FL','OIOC','National Olympic Committee').
isMember('SK','OIOC','National Olympic Committee').
isMember('SLO','OIOC','National Olympic Committee').
isMember('CH','OIOC','National Olympic Committee').
isMember('BY','OIOC','National Olympic Committee').
isMember('LV','OIOC','National Olympic Committee').
isMember('LT','OIOC','National Olympic Committee').
isMember('PL','OIOC','National Olympic Committee').
isMember('UA','OIOC','National Olympic Committee').
isMember('R','OIOC','National Olympic Committee').
isMember('B','OIOC','National Olympic Committee').
isMember('L','OIOC','National Olympic Committee').
isMember('NL','OIOC','National Olympic Committee').
isMember('BIH','OIOC','National Olympic Committee').
isMember('HR','OIOC','National Olympic Committee').
isMember('BG','OIOC','National Olympic Committee').
isMember('RO','OIOC','National Olympic Committee').
isMember('TR','OIOC','National Olympic Committee').
isMember('DK','OIOC','National Olympic Committee').
isMember('EW','OIOC','National Olympic Committee').
isMember('SF','OIOC','National Olympic Committee').
isMember('N','OIOC','National Olympic Committee').
isMember('S','OIOC','National Olympic Committee').
isMember('MC','OIOC','National Olympic Committee').
isMember('IS','OIOC','National Olympic Committee').
isMember('IRL','OIOC','National Olympic Committee').
isMember('RSM','OIOC','National Olympic Committee').
isMember('M','OIOC','National Olympic Committee').
isMember('MD','OIOC','National Olympic Committee').
isMember('P','OIOC','National Olympic Committee').
isMember('GB','OIOC','National Olympic Committee').
isMember('AFG','OIOC','National Olympic Committee').
isMember('CN','OIOC','National Olympic Committee').
isMember('IR','OIOC','National Olympic Committee').
isMember('PK','OIOC','National Olympic Committee').
isMember('TAD','OIOC','National Olympic Committee').
isMember('TM','OIOC','National Olympic Committee').
isMember('UZB','OIOC','National Olympic Committee').
isMember('ARM','OIOC','National Olympic Committee').
isMember('GE','OIOC','National Olympic Committee').
isMember('AZ','OIOC','National Olympic Committee').
isMember('BRN','OIOC','National Olympic Committee').
isMember('BD','OIOC','National Olympic Committee').
isMember('MYA','OIOC','National Olympic Committee').
isMember('IND','OIOC','National Olympic Committee').
isMember('BHT','OIOC','National Olympic Committee').
isMember('BRU','OIOC','National Olympic Committee').
isMember('MAL','OIOC','National Olympic Committee').
isMember('LAO','OIOC','National Olympic Committee').
isMember('THA','OIOC','National Olympic Committee').
isMember('VN','OIOC','National Olympic Committee').
isMember('KAZ','OIOC','National Olympic Committee').
isMember('NOK','OIOC','National Olympic Committee').
isMember('KGZ','OIOC','National Olympic Committee').
isMember('HONX','OIOC','National Olympic Committee').
isMember('MNG','OIOC','National Olympic Committee').
isMember('NEP','OIOC','National Olympic Committee').
isMember('CY','OIOC','National Olympic Committee').
isMember('IL','OIOC','National Olympic Committee').
isMember('ET','OIOC','National Olympic Committee').
isMember('RI','OIOC','National Olympic Committee').
isMember('TL','OIOC','National Olympic Committee').
isMember('PNG','OIOC','National Olympic Committee').
isMember('IRQ','OIOC','National Olympic Committee').
isMember('JOR','OIOC','National Olympic Committee').
isMember('KWT','OIOC','National Olympic Committee').
isMember('SA','OIOC','National Olympic Committee').
isMember('SYR','OIOC','National Olympic Committee').
isMember('RL','OIOC','National Olympic Committee').
isMember('J','OIOC','National Olympic Committee').
isMember('ROK','OIOC','National Olympic Committee').
isMember('MV','OIOC','National Olympic Committee').
isMember('OM','OIOC','National Olympic Committee').
isMember('UAE','OIOC','National Olympic Committee').
isMember('YE','OIOC','National Olympic Committee').
isMember('RP','OIOC','National Olympic Committee').
isMember('Q','OIOC','National Olympic Committee').
isMember('SGP','OIOC','National Olympic Committee').
isMember('CL','OIOC','National Olympic Committee').
isMember('RC','OIOC','National Olympic Committee').
isMember('AG','OIOC','National Olympic Committee').
isMember('ARU','OIOC','National Olympic Committee').
isMember('BS','OIOC','National Olympic Committee').
isMember('BDS','OIOC','National Olympic Committee').
isMember('BZ','OIOC','National Olympic Committee').
isMember('GCA','OIOC','National Olympic Committee').
isMember('MEX','OIOC','National Olympic Committee').
isMember('BERM','OIOC','National Olympic Committee').
isMember('BVIR','OIOC','National Olympic Committee').
isMember('CDN','OIOC','National Olympic Committee').
isMember('USA','OIOC','National Olympic Committee').
isMember('CAYM','OIOC','National Olympic Committee').
isMember('CR','OIOC','National Olympic Committee').
isMember('NIC','OIOC','National Olympic Committee').
isMember('PA','OIOC','National Olympic Committee').
isMember('C','OIOC','National Olympic Committee').
isMember('WD','OIOC','National Olympic Committee').
isMember('DOM','OIOC','National Olympic Committee').
isMember('RH','OIOC','National Olympic Committee').
isMember('ES','OIOC','National Olympic Committee').
isMember('HCA','OIOC','National Olympic Committee').
isMember('WG','OIOC','National Olympic Committee').
isMember('JA','OIOC','National Olympic Committee').
isMember('NA','OIOC','National Olympic Committee').
isMember('CO','OIOC','National Olympic Committee').
isMember('PR','OIOC','National Olympic Committee').
isMember('KN','OIOC','National Olympic Committee').
isMember('WL','OIOC','National Olympic Committee').
isMember('WV','OIOC','National Olympic Committee').
isMember('TT','OIOC','National Olympic Committee').
isMember('VIRG','OIOC','National Olympic Committee').
isMember('AMSA','OIOC','National Olympic Committee').
isMember('AUS','OIOC','National Olympic Committee').
isMember('COOK','OIOC','National Olympic Committee').
isMember('FJI','OIOC','National Olympic Committee').
isMember('GUAM','OIOC','National Olympic Committee').
isMember('NZ','OIOC','National Olympic Committee').
isMember('SLB','OIOC','National Olympic Committee').
isMember('TO','OIOC','National Olympic Committee').
isMember('VU','OIOC','National Olympic Committee').
isMember('WS','OIOC','National Olympic Committee').
isMember('RA','OIOC','National Olympic Committee').
isMember('BOL','OIOC','National Olympic Committee').
isMember('BR','OIOC','National Olympic Committee').
isMember('RCH','OIOC','National Olympic Committee').
isMember('PY','OIOC','National Olympic Committee').
isMember('ROU','OIOC','National Olympic Committee').
isMember('PE','OIOC','National Olympic Committee').
isMember('GUY','OIOC','National Olympic Committee').
isMember('SME','OIOC','National Olympic Committee').
isMember('YV','OIOC','National Olympic Committee').
isMember('EC','OIOC','National Olympic Committee').
isMember('DZ','OIOC','National Olympic Committee').
isMember('LAR','OIOC','National Olympic Committee').
isMember('RMM','OIOC','National Olympic Committee').
isMember('RIM','OIOC','National Olympic Committee').
isMember('MA','OIOC','National Olympic Committee').
isMember('RN','OIOC','National Olympic Committee').
isMember('TN','OIOC','National Olympic Committee').
isMember('ANG','OIOC','National Olympic Committee').
isMember('RCB','OIOC','National Olympic Committee').
isMember('NAM','OIOC','National Olympic Committee').
isMember('ZRE','OIOC','National Olympic Committee').
isMember('Z','OIOC','National Olympic Committee').
isMember('BEN','OIOC','National Olympic Committee').
isMember('BF','OIOC','National Olympic Committee').
isMember('WAN','OIOC','National Olympic Committee').
isMember('RT','OIOC','National Olympic Committee').
isMember('RB','OIOC','National Olympic Committee').
isMember('RSA','OIOC','National Olympic Committee').
isMember('ZW','OIOC','National Olympic Committee').
isMember('CI','OIOC','National Olympic Committee').
isMember('GH','OIOC','National Olympic Committee').
isMember('BI','OIOC','National Olympic Committee').
isMember('RWA','OIOC','National Olympic Committee').
isMember('EAT','OIOC','National Olympic Committee').
isMember('CAM','OIOC','National Olympic Committee').
isMember('RCA','OIOC','National Olympic Committee').
isMember('TCH','OIOC','National Olympic Committee').
isMember('GQ','OIOC','National Olympic Committee').
isMember('G','OIOC','National Olympic Committee').
isMember('CV','OIOC','National Olympic Committee').
isMember('SUD','OIOC','National Olympic Committee').
isMember('COM','OIOC','National Olympic Committee').
isMember('RG','OIOC','National Olympic Committee').
isMember('LB','OIOC','National Olympic Committee').
isMember('DJI','OIOC','National Olympic Committee').
isMember('ETH','OIOC','National Olympic Committee').
isMember('SP','OIOC','National Olympic Committee').
isMember('EAK','OIOC','National Olympic Committee').
isMember('WAG','OIOC','National Olympic Committee').
isMember('SN','OIOC','National Olympic Committee').
isMember('WAL','OIOC','National Olympic Committee').
isMember('EAU','OIOC','National Olympic Committee').
isMember('LS','OIOC','National Olympic Committee').
isMember('RM','OIOC','National Olympic Committee').
isMember('MW','OIOC','National Olympic Committee').
isMember('MOC','OIOC','National Olympic Committee').
isMember('MS','OIOC','National Olympic Committee').
isMember('SD','OIOC','National Olympic Committee').
isMember('REUN','OIOC','National Olympic Committee').
isMember('STP','OIOC','National Olympic Committee').
isMember('SY','OIOC','National Olympic Committee').
isMember('AL','OIOM',member).
isMember('GR','OIOM',member).
isMember('SRB','OIOM',member).
isMember('MNE','OIOM',member).
isMember('F','OIOM',member).
isMember('A','OIOM',member).
isMember('D','OIOM',member).
isMember('H','OIOM',member).
isMember('I','OIOM',member).
isMember('CH','OIOM',member).
isMember('PL','OIOM',member).
isMember('B','OIOM',member).
isMember('L','OIOM',member).
isMember('NL','OIOM',member).
isMember('HR','OIOM',member).
isMember('BG','OIOM',member).
isMember('DK','OIOM',member).
isMember('SF','OIOM',member).
isMember('N','OIOM',member).
isMember('S','OIOM',member).
isMember('P','OIOM',member).
isMember('PK','OIOM',member).
isMember('TAD','OIOM',member).
isMember('ARM','OIOM',member).
isMember('BD','OIOM',member).
isMember('THA','OIOM',member).
isMember('CY','OIOM',member).
isMember('IL','OIOM',member).
isMember('ET','OIOM',member).
isMember('J','OIOM',member).
isMember('ROK','OIOM',member).
isMember('RP','OIOM',member).
isMember('CL','OIOM',member).
isMember('GCA','OIOM',member).
isMember('CDN','OIOM',member).
isMember('USA','OIOM',member).
isMember('CR','OIOM',member).
isMember('NIC','OIOM',member).
isMember('PA','OIOM',member).
isMember('DOM','OIOM',member).
isMember('RH','OIOM',member).
isMember('ES','OIOM',member).
isMember('HCA','OIOM',member).
isMember('CO','OIOM',member).
isMember('AUS','OIOM',member).
isMember('RA','OIOM',member).
isMember('BOL','OIOM',member).
isMember('RCH','OIOM',member).
isMember('PY','OIOM',member).
isMember('ROU','OIOM',member).
isMember('PE','OIOM',member).
isMember('YV','OIOM',member).
isMember('EC','OIOM',member).
isMember('ANG','OIOM',member).
isMember('Z','OIOM',member).
isMember('EAK','OIOM',member).
isMember('EAU','OIOM',member).
isMember('E','OIOM',observer).
isMember('CZ','OIOM',observer).
isMember('SK','OIOM',observer).
isMember('SLO','OIOM',observer).
isMember('BY','OIOM',observer).
isMember('LV','OIOM',observer).
isMember('UA','OIOM',observer).
isMember('R','OIOM',observer).
isMember('BIH','OIOM',observer).
isMember('RO','OIOM',observer).
isMember('TR','OIOM',observer).
isMember('V','OIOM',observer).
isMember('RSM','OIOM',observer).
isMember('M','OIOM',observer).
isMember('MD','OIOM',observer).
isMember('GB','OIOM',observer).
isMember('AFG','OIOM',observer).
isMember('IR','OIOM',observer).
isMember('GE','OIOM',observer).
isMember('IND','OIOM',observer).
isMember('VN','OIOM',observer).
isMember('KGZ','OIOM',observer).
isMember('RI','OIOM',observer).
isMember('JOR','OIOM',observer).
isMember('BZ','OIOM',observer).
isMember('MEX','OIOM',observer).
isMember('NZ','OIOM',observer).
isMember('BR','OIOM',observer).
isMember('MA','OIOM',observer).
isMember('NAM','OIOM',observer).
isMember('ZW','OIOM',observer).
isMember('GH','OIOM',observer).
isMember('RWA','OIOM',observer).
isMember('CV','OIOM',observer).
isMember('SUD','OIOM',observer).
isMember('LB','OIOM',observer).
isMember('SP','OIOM',observer).
isMember('GNB','OIOM',observer).
isMember('MOC','OIOM',observer).
isMember('STP','OIOM',observer).
isMember('AL','OISO',member).
isMember('GR','OISO',member).
isMember('SRB','OISO',member).
isMember('MNE','OISO',member).
isMember('F','OISO',member).
isMember('E','OISO',member).
isMember('A','OISO',member).
isMember('CZ','OISO',member).
isMember('D','OISO',member).
isMember('H','OISO',member).
isMember('I','OISO',member).
isMember('SK','OISO',member).
isMember('SLO','OISO',member).
isMember('CH','OISO',member).
isMember('BY','OISO',member).
isMember('PL','OISO',member).
isMember('UA','OISO',member).
isMember('R','OISO',member).
isMember('B','OISO',member).
isMember('NL','OISO',member).
isMember('HR','OISO',member).
isMember('BG','OISO',member).
isMember('RO','OISO',member).
isMember('TR','OISO',member).
isMember('DK','OISO',member).
isMember('SF','OISO',member).
isMember('N','OISO',member).
isMember('S','OISO',member).
isMember('IS','OISO',member).
isMember('IRL','OISO',member).
isMember('P','OISO',member).
isMember('GB','OISO',member).
isMember('CN','OISO',member).
isMember('IR','OISO',member).
isMember('PK','OISO',member).
isMember('UZB','OISO',member).
isMember('BD','OISO',member).
isMember('IND','OISO',member).
isMember('MAL','OISO',member).
isMember('THA','OISO',member).
isMember('VN','OISO',member).
isMember('NOK','OISO',member).
isMember('MNG','OISO',member).
isMember('CY','OISO',member).
isMember('IL','OISO',member).
isMember('ET','OISO',member).
isMember('RI','OISO',member).
isMember('SA','OISO',member).
isMember('SYR','OISO',member).
isMember('J','OISO',member).
isMember('ROK','OISO',member).
isMember('RP','OISO',member).
isMember('SGP','OISO',member).
isMember('CL','OISO',member).
isMember('MEX','OISO',member).
isMember('CDN','OISO',member).
isMember('USA','OISO',member).
isMember('C','OISO',member).
isMember('JA','OISO',member).
isMember('CO','OISO',member).
isMember('TT','OISO',member).
isMember('AUS','OISO',member).
isMember('NZ','OISO',member).
isMember('RA','OISO',member).
isMember('BR','OISO',member).
isMember('RCH','OISO',member).
isMember('ROU','OISO',member).
isMember('YV','OISO',member).
isMember('DZ','OISO',member).
isMember('LAR','OISO',member).
isMember('MA','OISO',member).
isMember('TN','OISO',member).
isMember('RSA','OISO',member).
isMember('ZW','OISO',member).
isMember('EAT','OISO',member).
isMember('ETH','OISO',member).
isMember('EAK','OISO',member).
isMember('LT','OISO','correspondent member').
isMember('EW','OISO','correspondent member').
isMember('M','OISO','correspondent member').
isMember('TM','OISO','correspondent member').
isMember('BRN','OISO','correspondent member').
isMember('BRU','OISO','correspondent member').
isMember('HONX','OISO','correspondent member').
isMember('MACX','OISO','correspondent member').
isMember('NEP','OISO','correspondent member').
isMember('PNG','OISO','correspondent member').
isMember('JOR','OISO','correspondent member').
isMember('KWT','OISO','correspondent member').
isMember('OM','OISO','correspondent member').
isMember('UAE','OISO','correspondent member').
isMember('Q','OISO','correspondent member').
isMember('BDS','OISO','correspondent member').
isMember('PE','OISO','correspondent member').
isMember('EAU','OISO','correspondent member').
isMember('MW','OISO','correspondent member').
isMember('MS','OISO','correspondent member').
isMember('AG','OISO','subscriber member').
isMember('WG','OISO','subscriber member').
isMember('WL','OISO','subscriber member').
isMember('BI','OISO','subscriber member').
isMember('AL','OICRM','National Society').
isMember('GR','OICRM','National Society').
isMember('SRB','OICRM','National Society').
isMember('MNE','OICRM','National Society').
isMember('F','OICRM','National Society').
isMember('E','OICRM','National Society').
isMember('A','OICRM','National Society').
isMember('CZ','OICRM','National Society').
isMember('D','OICRM','National Society').
isMember('H','OICRM','National Society').
isMember('I','OICRM','National Society').
isMember('FL','OICRM','National Society').
isMember('SK','OICRM','National Society').
isMember('SLO','OICRM','National Society').
isMember('CH','OICRM','National Society').
isMember('LV','OICRM','National Society').
isMember('LT','OICRM','National Society').
isMember('PL','OICRM','National Society').
isMember('UA','OICRM','National Society').
isMember('R','OICRM','National Society').
isMember('B','OICRM','National Society').
isMember('L','OICRM','National Society').
isMember('NL','OICRM','National Society').
isMember('HR','OICRM','National Society').
isMember('BG','OICRM','National Society').
isMember('RO','OICRM','National Society').
isMember('TR','OICRM','National Society').
isMember('DK','OICRM','National Society').
isMember('EW','OICRM','National Society').
isMember('SF','OICRM','National Society').
isMember('N','OICRM','National Society').
isMember('S','OICRM','National Society').
isMember('MC','OICRM','National Society').
isMember('IS','OICRM','National Society').
isMember('IRL','OICRM','National Society').
isMember('RSM','OICRM','National Society').
isMember('M','OICRM','National Society').
isMember('P','OICRM','National Society').
isMember('GB','OICRM','National Society').
isMember('AFG','OICRM','National Society').
isMember('CN','OICRM','National Society').
isMember('IR','OICRM','National Society').
isMember('PK','OICRM','National Society').
isMember('BRN','OICRM','National Society').
isMember('BD','OICRM','National Society').
isMember('MYA','OICRM','National Society').
isMember('IND','OICRM','National Society').
isMember('MAL','OICRM','National Society').
isMember('LAO','OICRM','National Society').
isMember('THA','OICRM','National Society').
isMember('K','OICRM','National Society').
isMember('VN','OICRM','National Society').
isMember('NOK','OICRM','National Society').
isMember('MNG','OICRM','National Society').
isMember('NEP','OICRM','National Society').
isMember('ET','OICRM','National Society').
isMember('RI','OICRM','National Society').
isMember('PNG','OICRM','National Society').
isMember('IRQ','OICRM','National Society').
isMember('JOR','OICRM','National Society').
isMember('KWT','OICRM','National Society').
isMember('SA','OICRM','National Society').
isMember('SYR','OICRM','National Society').
isMember('RL','OICRM','National Society').
isMember('J','OICRM','National Society').
isMember('ROK','OICRM','National Society').
isMember('UAE','OICRM','National Society').
isMember('YE','OICRM','National Society').
isMember('RP','OICRM','National Society').
isMember('Q','OICRM','National Society').
isMember('SGP','OICRM','National Society').
isMember('CL','OICRM','National Society').
isMember('AG','OICRM','National Society').
isMember('BS','OICRM','National Society').
isMember('BDS','OICRM','National Society').
isMember('BZ','OICRM','National Society').
isMember('GCA','OICRM','National Society').
isMember('MEX','OICRM','National Society').
isMember('CDN','OICRM','National Society').
isMember('USA','OICRM','National Society').
isMember('CR','OICRM','National Society').
isMember('NIC','OICRM','National Society').
isMember('PA','OICRM','National Society').
isMember('C','OICRM','National Society').
isMember('WD','OICRM','National Society').
isMember('DOM','OICRM','National Society').
isMember('RH','OICRM','National Society').
isMember('ES','OICRM','National Society').
isMember('HCA','OICRM','National Society').
isMember('WG','OICRM','National Society').
isMember('JA','OICRM','National Society').
isMember('CO','OICRM','National Society').
isMember('KN','OICRM','National Society').
isMember('WL','OICRM','National Society').
isMember('WV','OICRM','National Society').
isMember('TT','OICRM','National Society').
isMember('AUS','OICRM','National Society').
isMember('COOK','OICRM','National Society').
isMember('FJI','OICRM','National Society').
isMember('NZ','OICRM','National Society').
isMember('SLB','OICRM','National Society').
isMember('TO','OICRM','National Society').
isMember('VU','OICRM','National Society').
isMember('WS','OICRM','National Society').
isMember('RA','OICRM','National Society').
isMember('BOL','OICRM','National Society').
isMember('BR','OICRM','National Society').
isMember('RCH','OICRM','National Society').
isMember('PY','OICRM','National Society').
isMember('ROU','OICRM','National Society').
isMember('PE','OICRM','National Society').
isMember('GUY','OICRM','National Society').
isMember('SME','OICRM','National Society').
isMember('YV','OICRM','National Society').
isMember('EC','OICRM','National Society').
isMember('DZ','OICRM','National Society').
isMember('LAR','OICRM','National Society').
isMember('RMM','OICRM','National Society').
isMember('RIM','OICRM','National Society').
isMember('MA','OICRM','National Society').
isMember('RN','OICRM','National Society').
isMember('TN','OICRM','National Society').
isMember('ANG','OICRM','National Society').
isMember('RCB','OICRM','National Society').
isMember('NAM','OICRM','National Society').
isMember('ZRE','OICRM','National Society').
isMember('Z','OICRM','National Society').
isMember('BEN','OICRM','National Society').
isMember('BF','OICRM','National Society').
isMember('WAN','OICRM','National Society').
isMember('RT','OICRM','National Society').
isMember('RB','OICRM','National Society').
isMember('RSA','OICRM','National Society').
isMember('ZW','OICRM','National Society').
isMember('CI','OICRM','National Society').
isMember('GH','OICRM','National Society').
isMember('BI','OICRM','National Society').
isMember('RWA','OICRM','National Society').
isMember('EAT','OICRM','National Society').
isMember('CAM','OICRM','National Society').
isMember('RCA','OICRM','National Society').
isMember('TCH','OICRM','National Society').
isMember('CV','OICRM','National Society').
isMember('SUD','OICRM','National Society').
isMember('RG','OICRM','National Society').
isMember('LB','OICRM','National Society').
isMember('DJI','OICRM','National Society').
isMember('ETH','OICRM','National Society').
isMember('SP','OICRM','National Society').
isMember('EAK','OICRM','National Society').
isMember('WAG','OICRM','National Society').
isMember('SN','OICRM','National Society').
isMember('GNB','OICRM','National Society').
isMember('WAL','OICRM','National Society').
isMember('EAU','OICRM','National Society').
isMember('LS','OICRM','National Society').
isMember('RM','OICRM','National Society').
isMember('MW','OICRM','National Society').
isMember('MOC','OICRM','National Society').
isMember('MS','OICRM','National Society').
isMember('SD','OICRM','National Society').
isMember('STP','OICRM','National Society').
isMember('SY','OICRM','National Society').
isMember('AL','OITU',member).
isMember('GR','OITU',member).
isMember('MK','OITU',member).
isMember('SRB','OITU',member).
isMember('MNE','OITU',member).
isMember('AND','OITU',member).
isMember('F','OITU',member).
isMember('E','OITU',member).
isMember('A','OITU',member).
isMember('CZ','OITU',member).
isMember('D','OITU',member).
isMember('H','OITU',member).
isMember('I','OITU',member).
isMember('FL','OITU',member).
isMember('SK','OITU',member).
isMember('SLO','OITU',member).
isMember('CH','OITU',member).
isMember('BY','OITU',member).
isMember('LV','OITU',member).
isMember('LT','OITU',member).
isMember('PL','OITU',member).
isMember('UA','OITU',member).
isMember('R','OITU',member).
isMember('B','OITU',member).
isMember('L','OITU',member).
isMember('NL','OITU',member).
isMember('BIH','OITU',member).
isMember('HR','OITU',member).
isMember('BG','OITU',member).
isMember('RO','OITU',member).
isMember('TR','OITU',member).
isMember('DK','OITU',member).
isMember('EW','OITU',member).
isMember('SF','OITU',member).
isMember('N','OITU',member).
isMember('S','OITU',member).
isMember('MC','OITU',member).
isMember('V','OITU',member).
isMember('IS','OITU',member).
isMember('IRL','OITU',member).
isMember('RSM','OITU',member).
isMember('M','OITU',member).
isMember('MD','OITU',member).
isMember('P','OITU',member).
isMember('GB','OITU',member).
isMember('AFG','OITU',member).
isMember('CN','OITU',member).
isMember('IR','OITU',member).
isMember('PK','OITU',member).
isMember('TAD','OITU',member).
isMember('TM','OITU',member).
isMember('UZB','OITU',member).
isMember('ARM','OITU',member).
isMember('GE','OITU',member).
isMember('AZ','OITU',member).
isMember('BRN','OITU',member).
isMember('BD','OITU',member).
isMember('MYA','OITU',member).
isMember('IND','OITU',member).
isMember('BHT','OITU',member).
isMember('BRU','OITU',member).
isMember('MAL','OITU',member).
isMember('LAO','OITU',member).
isMember('THA','OITU',member).
isMember('K','OITU',member).
isMember('VN','OITU',member).
isMember('KAZ','OITU',member).
isMember('NOK','OITU',member).
isMember('KGZ','OITU',member).
isMember('MNG','OITU',member).
isMember('NEP','OITU',member).
isMember('CY','OITU',member).
isMember('IL','OITU',member).
isMember('ET','OITU',member).
isMember('RI','OITU',member).
isMember('PNG','OITU',member).
isMember('IRQ','OITU',member).
isMember('JOR','OITU',member).
isMember('KWT','OITU',member).
isMember('SA','OITU',member).
isMember('SYR','OITU',member).
isMember('RL','OITU',member).
isMember('J','OITU',member).
isMember('ROK','OITU',member).
isMember('MV','OITU',member).
isMember('OM','OITU',member).
isMember('UAE','OITU',member).
isMember('YE','OITU',member).
isMember('RP','OITU',member).
isMember('Q','OITU',member).
isMember('SGP','OITU',member).
isMember('CL','OITU',member).
isMember('AG','OITU',member).
isMember('BS','OITU',member).
isMember('BDS','OITU',member).
isMember('BZ','OITU',member).
isMember('GCA','OITU',member).
isMember('MEX','OITU',member).
isMember('CDN','OITU',member).
isMember('USA','OITU',member).
isMember('CR','OITU',member).
isMember('NIC','OITU',member).
isMember('PA','OITU',member).
isMember('C','OITU',member).
isMember('DOM','OITU',member).
isMember('RH','OITU',member).
isMember('ES','OITU',member).
isMember('HCA','OITU',member).
isMember('WG','OITU',member).
isMember('JA','OITU',member).
isMember('CO','OITU',member).
isMember('WV','OITU',member).
isMember('TT','OITU',member).
isMember('AUS','OITU',member).
isMember('FJI','OITU',member).
isMember('KIR','OITU',member).
isMember('NAU','OITU',member).
isMember('NZ','OITU',member).
isMember('SLB','OITU',member).
isMember('TO','OITU',member).
isMember('VU','OITU',member).
isMember('WS','OITU',member).
isMember('RA','OITU',member).
isMember('BOL','OITU',member).
isMember('BR','OITU',member).
isMember('RCH','OITU',member).
isMember('PY','OITU',member).
isMember('ROU','OITU',member).
isMember('PE','OITU',member).
isMember('GUY','OITU',member).
isMember('SME','OITU',member).
isMember('YV','OITU',member).
isMember('EC','OITU',member).
isMember('DZ','OITU',member).
isMember('LAR','OITU',member).
isMember('RMM','OITU',member).
isMember('RIM','OITU',member).
isMember('MA','OITU',member).
isMember('RN','OITU',member).
isMember('TN','OITU',member).
isMember('ANG','OITU',member).
isMember('RCB','OITU',member).
isMember('NAM','OITU',member).
isMember('ZRE','OITU',member).
isMember('Z','OITU',member).
isMember('BEN','OITU',member).
isMember('BF','OITU',member).
isMember('WAN','OITU',member).
isMember('RT','OITU',member).
isMember('RB','OITU',member).
isMember('RSA','OITU',member).
isMember('ZW','OITU',member).
isMember('CI','OITU',member).
isMember('GH','OITU',member).
isMember('BI','OITU',member).
isMember('RWA','OITU',member).
isMember('EAT','OITU',member).
isMember('CAM','OITU',member).
isMember('RCA','OITU',member).
isMember('TCH','OITU',member).
isMember('GQ','OITU',member).
isMember('G','OITU',member).
isMember('CV','OITU',member).
isMember('SUD','OITU',member).
isMember('SSD','OITU',member).
isMember('COM','OITU',member).
isMember('RG','OITU',member).
isMember('LB','OITU',member).
isMember('DJI','OITU',member).
isMember('ER','OITU',member).
isMember('ETH','OITU',member).
isMember('SP','OITU',member).
isMember('EAK','OITU',member).
isMember('WAG','OITU',member).
isMember('SN','OITU',member).
isMember('GNB','OITU',member).
isMember('WAL','OITU',member).
isMember('EAU','OITU',member).
isMember('LS','OITU',member).
isMember('RM','OITU',member).
isMember('MW','OITU',member).
isMember('MOC','OITU',member).
isMember('MS','OITU',member).
isMember('SD','OITU',member).
isMember('STP','OITU',member).
isMember('GR','OIntelsat',member).
isMember('F','OIntelsat',member).
isMember('E','OIntelsat',member).
isMember('A','OIntelsat',member).
isMember('CZ','OIntelsat',member).
isMember('D','OIntelsat',member).
isMember('H','OIntelsat',member).
isMember('I','OIntelsat',member).
isMember('FL','OIntelsat',member).
isMember('CH','OIntelsat',member).
isMember('PL','OIntelsat',member).
isMember('R','OIntelsat',member).
isMember('B','OIntelsat',member).
isMember('L','OIntelsat',member).
isMember('NL','OIntelsat',member).
isMember('HR','OIntelsat',member).
isMember('RO','OIntelsat',member).
isMember('TR','OIntelsat',member).
isMember('DK','OIntelsat',member).
isMember('SF','OIntelsat',member).
isMember('N','OIntelsat',member).
isMember('S','OIntelsat',member).
isMember('MC','OIntelsat',member).
isMember('V','OIntelsat',member).
isMember('IS','OIntelsat',member).
isMember('IRL','OIntelsat',member).
isMember('M','OIntelsat',member).
isMember('P','OIntelsat',member).
isMember('GB','OIntelsat',member).
isMember('AFG','OIntelsat',member).
isMember('CN','OIntelsat',member).
isMember('IR','OIntelsat',member).
isMember('PK','OIntelsat',member).
isMember('ARM','OIntelsat',member).
isMember('AZ','OIntelsat',member).
isMember('BRN','OIntelsat',member).
isMember('BD','OIntelsat',member).
isMember('IND','OIntelsat',member).
isMember('BHT','OIntelsat',member).
isMember('BRU','OIntelsat',member).
isMember('MAL','OIntelsat',member).
isMember('THA','OIntelsat',member).
isMember('VN','OIntelsat',member).
isMember('KAZ','OIntelsat',member).
isMember('KGZ','OIntelsat',member).
isMember('NEP','OIntelsat',member).
isMember('CY','OIntelsat',member).
isMember('IL','OIntelsat',member).
isMember('ET','OIntelsat',member).
isMember('RI','OIntelsat',member).
isMember('PNG','OIntelsat',member).
isMember('IRQ','OIntelsat',member).
isMember('JOR','OIntelsat',member).
isMember('KWT','OIntelsat',member).
isMember('SA','OIntelsat',member).
isMember('SYR','OIntelsat',member).
isMember('RL','OIntelsat',member).
isMember('J','OIntelsat',member).
isMember('ROK','OIntelsat',member).
isMember('OM','OIntelsat',member).
isMember('UAE','OIntelsat',member).
isMember('YE','OIntelsat',member).
isMember('RP','OIntelsat',member).
isMember('Q','OIntelsat',member).
isMember('SGP','OIntelsat',member).
isMember('CL','OIntelsat',member).
isMember('BS','OIntelsat',member).
isMember('BDS','OIntelsat',member).
isMember('GCA','OIntelsat',member).
isMember('MEX','OIntelsat',member).
isMember('CDN','OIntelsat',member).
isMember('USA','OIntelsat',member).
isMember('CR','OIntelsat',member).
isMember('NIC','OIntelsat',member).
isMember('PA','OIntelsat',member).
isMember('DOM','OIntelsat',member).
isMember('RH','OIntelsat',member).
isMember('ES','OIntelsat',member).
isMember('HCA','OIntelsat',member).
isMember('JA','OIntelsat',member).
isMember('CO','OIntelsat',member).
isMember('TT','OIntelsat',member).
isMember('AUS','OIntelsat',member).
isMember('FJI','OIntelsat',member).
isMember('NZ','OIntelsat',member).
isMember('RA','OIntelsat',member).
isMember('BOL','OIntelsat',member).
isMember('BR','OIntelsat',member).
isMember('RCH','OIntelsat',member).
isMember('PY','OIntelsat',member).
isMember('ROU','OIntelsat',member).
isMember('PE','OIntelsat',member).
isMember('YV','OIntelsat',member).
isMember('EC','OIntelsat',member).
isMember('DZ','OIntelsat',member).
isMember('LAR','OIntelsat',member).
isMember('RMM','OIntelsat',member).
isMember('RIM','OIntelsat',member).
isMember('MA','OIntelsat',member).
isMember('RN','OIntelsat',member).
isMember('TN','OIntelsat',member).
isMember('ANG','OIntelsat',member).
isMember('RCB','OIntelsat',member).
isMember('NAM','OIntelsat',member).
isMember('ZRE','OIntelsat',member).
isMember('Z','OIntelsat',member).
isMember('BEN','OIntelsat',member).
isMember('BF','OIntelsat',member).
isMember('WAN','OIntelsat',member).
isMember('RT','OIntelsat',member).
isMember('RB','OIntelsat',member).
isMember('RSA','OIntelsat',member).
isMember('ZW','OIntelsat',member).
isMember('CI','OIntelsat',member).
isMember('GH','OIntelsat',member).
isMember('RWA','OIntelsat',member).
isMember('EAT','OIntelsat',member).
isMember('CAM','OIntelsat',member).
isMember('RCA','OIntelsat',member).
isMember('TCH','OIntelsat',member).
isMember('G','OIntelsat',member).
isMember('CV','OIntelsat',member).
isMember('SUD','OIntelsat',member).
isMember('RG','OIntelsat',member).
isMember('ETH','OIntelsat',member).
isMember('SP','OIntelsat',member).
isMember('EAK','OIntelsat',member).
isMember('SN','OIntelsat',member).
isMember('EAU','OIntelsat',member).
isMember('RM','OIntelsat',member).
isMember('MW','OIntelsat',member).
isMember('MOC','OIntelsat',member).
isMember('MS','OIntelsat',member).
isMember('SD','OIntelsat',member).
isMember('AL','OIntelsat','nonsignatory user').
isMember('MK','OIntelsat','nonsignatory user').
isMember('SK','OIntelsat','nonsignatory user').
isMember('SLO','OIntelsat','nonsignatory user').
isMember('BY','OIntelsat','nonsignatory user').
isMember('LV','OIntelsat','nonsignatory user').
isMember('LT','OIntelsat','nonsignatory user').
isMember('UA','OIntelsat','nonsignatory user').
isMember('BIH','OIntelsat','nonsignatory user').
isMember('BG','OIntelsat','nonsignatory user').
isMember('MD','OIntelsat','nonsignatory user').
isMember('TAD','OIntelsat','nonsignatory user').
isMember('TM','OIntelsat','nonsignatory user').
isMember('MYA','OIntelsat','nonsignatory user').
isMember('LAO','OIntelsat','nonsignatory user').
isMember('K','OIntelsat','nonsignatory user').
isMember('NOK','OIntelsat','nonsignatory user').
isMember('MNG','OIntelsat','nonsignatory user').
isMember('MV','OIntelsat','nonsignatory user').
isMember('AG','OIntelsat','nonsignatory user').
isMember('BZ','OIntelsat','nonsignatory user').
isMember('C','OIntelsat','nonsignatory user').
isMember('WL','OIntelsat','nonsignatory user').
isMember('WV','OIntelsat','nonsignatory user').
isMember('KIR','OIntelsat','nonsignatory user').
isMember('MH','OIntelsat','nonsignatory user').
isMember('NAU','OIntelsat','nonsignatory user').
isMember('SLB','OIntelsat','nonsignatory user').
isMember('TO','OIntelsat','nonsignatory user').
isMember('TUV','OIntelsat','nonsignatory user').
isMember('VU','OIntelsat','nonsignatory user').
isMember('WS','OIntelsat','nonsignatory user').
isMember('GUY','OIntelsat','nonsignatory user').
isMember('SME','OIntelsat','nonsignatory user').
isMember('BI','OIntelsat','nonsignatory user').
isMember('GQ','OIntelsat','nonsignatory user').
isMember('COM','OIntelsat','nonsignatory user').
isMember('LB','OIntelsat','nonsignatory user').
isMember('DJI','OIntelsat','nonsignatory user').
isMember('ER','OIntelsat','nonsignatory user').
isMember('WAG','OIntelsat','nonsignatory user').
isMember('GNB','OIntelsat','nonsignatory user').
isMember('WAL','OIntelsat','nonsignatory user').
isMember('LS','OIntelsat','nonsignatory user').
isMember('STP','OIntelsat','nonsignatory user').
isMember('SY','OIntelsat','nonsignatory user').
isMember('AL','OIDB',member).
isMember('TR','OIDB',member).
isMember('AFG','OIDB',member).
isMember('IR','OIDB',member).
isMember('PK','OIDB',member).
isMember('TAD','OIDB',member).
isMember('TM','OIDB',member).
isMember('AZ','OIDB',member).
isMember('BRN','OIDB',member).
isMember('BD','OIDB',member).
isMember('BRU','OIDB',member).
isMember('MAL','OIDB',member).
isMember('KAZ','OIDB',member).
isMember('KGZ','OIDB',member).
isMember('ET','OIDB',member).
isMember('RI','OIDB',member).
isMember('IRQ','OIDB',member).
isMember('JOR','OIDB',member).
isMember('KWT','OIDB',member).
isMember('SA','OIDB',member).
isMember('SYR','OIDB',member).
isMember('RL','OIDB',member).
isMember('MV','OIDB',member).
isMember('OM','OIDB',member).
isMember('UAE','OIDB',member).
isMember('YE','OIDB',member).
isMember('Q','OIDB',member).
isMember('DZ','OIDB',member).
isMember('LAR','OIDB',member).
isMember('RMM','OIDB',member).
isMember('RIM','OIDB',member).
isMember('MA','OIDB',member).
isMember('RN','OIDB',member).
isMember('TN','OIDB',member).
isMember('BEN','OIDB',member).
isMember('BF','OIDB',member).
isMember('CAM','OIDB',member).
isMember('TCH','OIDB',member).
isMember('G','OIDB',member).
isMember('SUD','OIDB',member).
isMember('COM','OIDB',member).
isMember('RG','OIDB',member).
isMember('DJI','OIDB',member).
isMember('SP','OIDB',member).
isMember('WAG','OIDB',member).
isMember('SN','OIDB',member).
isMember('GNB','OIDB',member).
isMember('WAL','OIDB',member).
isMember('EAU','OIDB',member).
isMember('MOC','OIDB',member).
isMember('BDS','OLAES',member).
isMember('BZ','OLAES',member).
isMember('GCA','OLAES',member).
isMember('MEX','OLAES',member).
isMember('CR','OLAES',member).
isMember('NIC','OLAES',member).
isMember('PA','OLAES',member).
isMember('C','OLAES',member).
isMember('DOM','OLAES',member).
isMember('RH','OLAES',member).
isMember('ES','OLAES',member).
isMember('HCA','OLAES',member).
isMember('WG','OLAES',member).
isMember('JA','OLAES',member).
isMember('CO','OLAES',member).
isMember('TT','OLAES',member).
isMember('RA','OLAES',member).
isMember('BOL','OLAES',member).
isMember('BR','OLAES',member).
isMember('RCH','OLAES',member).
isMember('PY','OLAES',member).
isMember('ROU','OLAES',member).
isMember('PE','OLAES',member).
isMember('GUY','OLAES',member).
isMember('SME','OLAES',member).
isMember('YV','OLAES',member).
isMember('EC','OLAES',member).
isMember('MEX','OLAIA',member).
isMember('CO','OLAIA',member).
isMember('RA','OLAIA',member).
isMember('BOL','OLAIA',member).
isMember('BR','OLAIA',member).
isMember('RCH','OLAIA',member).
isMember('PY','OLAIA',member).
isMember('ROU','OLAIA',member).
isMember('PE','OLAIA',member).
isMember('YV','OLAIA',member).
isMember('EC','OLAIA',member).
isMember('E','OLAIA',observer).
isMember('I','OLAIA',observer).
isMember('P','OLAIA',observer).
isMember('CN','OLAIA',observer).
isMember('GCA','OLAIA',observer).
isMember('CR','OLAIA',observer).
isMember('NIC','OLAIA',observer).
isMember('PA','OLAIA',observer).
isMember('C','OLAIA',observer).
isMember('DOM','OLAIA',observer).
isMember('ES','OLAIA',observer).
isMember('HCA','OLAIA',observer).
isMember('GR','OMTCR',member).
isMember('F','OMTCR',member).
isMember('E','OMTCR',member).
isMember('A','OMTCR',member).
isMember('D','OMTCR',member).
isMember('H','OMTCR',member).
isMember('I','OMTCR',member).
isMember('CH','OMTCR',member).
isMember('R','OMTCR',member).
isMember('B','OMTCR',member).
isMember('L','OMTCR',member).
isMember('NL','OMTCR',member).
isMember('DK','OMTCR',member).
isMember('SF','OMTCR',member).
isMember('N','OMTCR',member).
isMember('S','OMTCR',member).
isMember('IS','OMTCR',member).
isMember('IRL','OMTCR',member).
isMember('P','OMTCR',member).
isMember('GB','OMTCR',member).
isMember('J','OMTCR',member).
isMember('CDN','OMTCR',member).
isMember('USA','OMTCR',member).
isMember('AUS','OMTCR',member).
isMember('NZ','OMTCR',member).
isMember('RA','OMTCR',member).
isMember('BR','OMTCR',member).
isMember('RSA','OMTCR',member).
isMember('M','ONAM',member).
isMember('AFG','ONAM',member).
isMember('IR','ONAM',member).
isMember('PK','ONAM',member).
isMember('UZB','ONAM',member).
isMember('BRN','ONAM',member).
isMember('BD','ONAM',member).
isMember('MYA','ONAM',member).
isMember('IND','ONAM',member).
isMember('BHT','ONAM',member).
isMember('BRU','ONAM',member).
isMember('MAL','ONAM',member).
isMember('LAO','ONAM',member).
isMember('THA','ONAM',member).
isMember('K','ONAM',member).
isMember('VN','ONAM',member).
isMember('NOK','ONAM',member).
isMember('MNG','ONAM',member).
isMember('NEP','ONAM',member).
isMember('CY','ONAM',member).
isMember('ET','ONAM',member).
isMember('RI','ONAM',member).
isMember('TL','ONAM',member).
isMember('PNG','ONAM',member).
isMember('IRQ','ONAM',member).
isMember('JOR','ONAM',member).
isMember('KWT','ONAM',member).
isMember('SA','ONAM',member).
isMember('SYR','ONAM',member).
isMember('RL','ONAM',member).
isMember('MV','ONAM',member).
isMember('OM','ONAM',member).
isMember('UAE','ONAM',member).
isMember('YE','ONAM',member).
isMember('RP','ONAM',member).
isMember('Q','ONAM',member).
isMember('SGP','ONAM',member).
isMember('CL','ONAM',member).
isMember('BS','ONAM',member).
isMember('BDS','ONAM',member).
isMember('BZ','ONAM',member).
isMember('GCA','ONAM',member).
isMember('NIC','ONAM',member).
isMember('PA','ONAM',member).
isMember('C','ONAM',member).
isMember('HCA','ONAM',member).
isMember('WG','ONAM',member).
isMember('JA','ONAM',member).
isMember('CO','ONAM',member).
isMember('WL','ONAM',member).
isMember('TT','ONAM',member).
isMember('VU','ONAM',member).
isMember('BOL','ONAM',member).
isMember('RCH','ONAM',member).
isMember('PE','ONAM',member).
isMember('GUY','ONAM',member).
isMember('SME','ONAM',member).
isMember('YV','ONAM',member).
isMember('EC','ONAM',member).
isMember('DZ','ONAM',member).
isMember('LAR','ONAM',member).
isMember('RMM','ONAM',member).
isMember('RIM','ONAM',member).
isMember('MA','ONAM',member).
isMember('RN','ONAM',member).
isMember('TN','ONAM',member).
isMember('ANG','ONAM',member).
isMember('RCB','ONAM',member).
isMember('NAM','ONAM',member).
isMember('ZRE','ONAM',member).
isMember('Z','ONAM',member).
isMember('BEN','ONAM',member).
isMember('BF','ONAM',member).
isMember('WAN','ONAM',member).
isMember('RT','ONAM',member).
isMember('RB','ONAM',member).
isMember('RSA','ONAM',member).
isMember('ZW','ONAM',member).
isMember('CI','ONAM',member).
isMember('GH','ONAM',member).
isMember('BI','ONAM',member).
isMember('RWA','ONAM',member).
isMember('EAT','ONAM',member).
isMember('CAM','ONAM',member).
isMember('RCA','ONAM',member).
isMember('TCH','ONAM',member).
isMember('GQ','ONAM',member).
isMember('G','ONAM',member).
isMember('CV','ONAM',member).
isMember('SUD','ONAM',member).
isMember('COM','ONAM',member).
isMember('RG','ONAM',member).
isMember('LB','ONAM',member).
isMember('DJI','ONAM',member).
isMember('ER','ONAM',member).
isMember('ETH','ONAM',member).
isMember('SP','ONAM',member).
isMember('EAK','ONAM',member).
isMember('WAG','ONAM',member).
isMember('SN','ONAM',member).
isMember('GNB','ONAM',member).
isMember('WAL','ONAM',member).
isMember('EAU','ONAM',member).
isMember('LS','ONAM',member).
isMember('RM','ONAM',member).
isMember('MW','ONAM',member).
isMember('MOC','ONAM',member).
isMember('MS','ONAM',member).
isMember('SD','ONAM',member).
isMember('STP','ONAM',member).
isMember('SY','ONAM',member).
isMember('HR','ONAM',observer).
isMember('CN','ONAM',observer).
isMember('ARM','ONAM',observer).
isMember('AZ','ONAM',observer).
isMember('AG','ONAM',observer).
isMember('MEX','ONAM',observer).
isMember('CR','ONAM',observer).
isMember('WD','ONAM',observer).
isMember('ES','ONAM',observer).
isMember('BR','ONAM',observer).
isMember('ROU','ONAM',observer).
isMember('GR','ONAM',guest).
isMember('E','ONAM',guest).
isMember('A','ONAM',guest).
isMember('D','ONAM',guest).
isMember('H','ONAM',guest).
isMember('I','ONAM',guest).
isMember('SLO','ONAM',guest).
isMember('CH','ONAM',guest).
isMember('PL','ONAM',guest).
isMember('NL','ONAM',guest).
isMember('BIH','ONAM',guest).
isMember('BG','ONAM',guest).
isMember('RO','ONAM',guest).
isMember('SF','ONAM',guest).
isMember('N','ONAM',guest).
isMember('S','ONAM',guest).
isMember('RSM','ONAM',guest).
isMember('P','ONAM',guest).
isMember('CDN','ONAM',guest).
isMember('DOM','ONAM',guest).
isMember('AUS','ONAM',guest).
isMember('NZ','ONAM',guest).
isMember('DK','ONC',member).
isMember('SF','ONC',member).
isMember('N','ONC',member).
isMember('S','ONC',member).
isMember('IS','ONC',member).
isMember('DK','ONIB',member).
isMember('SF','ONIB',member).
isMember('N','ONIB',member).
isMember('S','ONIB',member).
isMember('IS','ONIB',member).
isMember('AL','OANC',member).
isMember('GR','OANC',member).
isMember('F','OANC',member).
isMember('E','OANC',member).
isMember('A','OANC',member).
isMember('CZ','OANC',member).
isMember('D','OANC',member).
isMember('H','OANC',member).
isMember('I','OANC',member).
isMember('SK','OANC',member).
isMember('SLO','OANC',member).
isMember('BY','OANC',member).
isMember('LV','OANC',member).
isMember('LT','OANC',member).
isMember('PL','OANC',member).
isMember('UA','OANC',member).
isMember('R','OANC',member).
isMember('B','OANC',member).
isMember('L','OANC',member).
isMember('NL','OANC',member).
isMember('BG','OANC',member).
isMember('RO','OANC',member).
isMember('TR','OANC',member).
isMember('DK','OANC',member).
isMember('EW','OANC',member).
isMember('SF','OANC',member).
isMember('N','OANC',member).
isMember('S','OANC',member).
isMember('IS','OANC',member).
isMember('M','OANC',member).
isMember('MD','OANC',member).
isMember('P','OANC',member).
isMember('GB','OANC',member).
isMember('TAD','OANC',member).
isMember('TM','OANC',member).
isMember('UZB','OANC',member).
isMember('ARM','OANC',member).
isMember('GE','OANC',member).
isMember('AZ','OANC',member).
isMember('KAZ','OANC',member).
isMember('KGZ','OANC',member).
isMember('CDN','OANC',member).
isMember('USA','OANC',member).
isMember('GR','ONATO',member).
isMember('F','ONATO',member).
isMember('E','ONATO',member).
isMember('D','ONATO',member).
isMember('I','ONATO',member).
isMember('B','ONATO',member).
isMember('L','ONATO',member).
isMember('NL','ONATO',member).
isMember('TR','ONATO',member).
isMember('DK','ONATO',member).
isMember('N','ONATO',member).
isMember('IS','ONATO',member).
isMember('P','ONATO',member).
isMember('GB','ONATO',member).
isMember('CDN','ONATO',member).
isMember('USA','ONATO',member).
isMember('GR','OEN',member).
isMember('F','OEN',member).
isMember('E','OEN',member).
isMember('A','OEN',member).
isMember('D','OEN',member).
isMember('I','OEN',member).
isMember('CH','OEN',member).
isMember('B','OEN',member).
isMember('L','OEN',member).
isMember('NL','OEN',member).
isMember('TR','OEN',member).
isMember('DK','OEN',member).
isMember('SF','OEN',member).
isMember('N','OEN',member).
isMember('S','OEN',member).
isMember('IS','OEN',member).
isMember('IRL','OEN',member).
isMember('P','OEN',member).
isMember('GB','OEN',member).
isMember('J','OEN',member).
isMember('CDN','OEN',member).
isMember('USA','OEN',member).
isMember('AUS','OEN',member).
isMember('GR','ONSG',member).
isMember('F','ONSG',member).
isMember('E','ONSG',member).
isMember('A','ONSG',member).
isMember('CZ','ONSG',member).
isMember('D','ONSG',member).
isMember('H','ONSG',member).
isMember('I','ONSG',member).
isMember('SK','ONSG',member).
isMember('CH','ONSG',member).
isMember('PL','ONSG',member).
isMember('UA','ONSG',member).
isMember('R','ONSG',member).
isMember('B','ONSG',member).
isMember('L','ONSG',member).
isMember('NL','ONSG',member).
isMember('BG','ONSG',member).
isMember('RO','ONSG',member).
isMember('DK','ONSG',member).
isMember('SF','ONSG',member).
isMember('N','ONSG',member).
isMember('S','ONSG',member).
isMember('IRL','ONSG',member).
isMember('P','ONSG',member).
isMember('GB','ONSG',member).
isMember('J','ONSG',member).
isMember('ROK','ONSG',member).
isMember('CDN','ONSG',member).
isMember('USA','ONSG',member).
isMember('AUS','ONSG',member).
isMember('NZ','ONSG',member).
isMember('RA','ONSG',member).
isMember('BR','ONSG',member).
isMember('RSA','ONSG',member).
isMember('GR','OOECD',member).
isMember('F','OOECD',member).
isMember('E','OOECD',member).
isMember('A','OOECD',member).
isMember('CZ','OOECD',member).
isMember('D','OOECD',member).
isMember('H','OOECD',member).
isMember('I','OOECD',member).
isMember('CH','OOECD',member).
isMember('B','OOECD',member).
isMember('L','OOECD',member).
isMember('NL','OOECD',member).
isMember('TR','OOECD',member).
isMember('DK','OOECD',member).
isMember('SF','OOECD',member).
isMember('N','OOECD',member).
isMember('S','OOECD',member).
isMember('IS','OOECD',member).
isMember('IRL','OOECD',member).
isMember('P','OOECD',member).
isMember('GB','OOECD',member).
isMember('J','OOECD',member).
isMember('MEX','OOECD',member).
isMember('CDN','OOECD',member).
isMember('USA','OOECD',member).
isMember('AUS','OOECD',member).
isMember('NZ','OOECD',member).
isMember('AL','OOSCE',member).
isMember('GR','OOSCE',member).
isMember('MK','OOSCE',member).
isMember('SRB','OOSCE',member).
isMember('MNE','OOSCE',member).
isMember('F','OOSCE',member).
isMember('E','OOSCE',member).
isMember('A','OOSCE',member).
isMember('CZ','OOSCE',member).
isMember('D','OOSCE',member).
isMember('H','OOSCE',member).
isMember('I','OOSCE',member).
isMember('FL','OOSCE',member).
isMember('SK','OOSCE',member).
isMember('SLO','OOSCE',member).
isMember('CH','OOSCE',member).
isMember('BY','OOSCE',member).
isMember('LV','OOSCE',member).
isMember('LT','OOSCE',member).
isMember('PL','OOSCE',member).
isMember('UA','OOSCE',member).
isMember('R','OOSCE',member).
isMember('B','OOSCE',member).
isMember('L','OOSCE',member).
isMember('NL','OOSCE',member).
isMember('BIH','OOSCE',member).
isMember('HR','OOSCE',member).
isMember('BG','OOSCE',member).
isMember('RO','OOSCE',member).
isMember('TR','OOSCE',member).
isMember('DK','OOSCE',member).
isMember('EW','OOSCE',member).
isMember('SF','OOSCE',member).
isMember('N','OOSCE',member).
isMember('S','OOSCE',member).
isMember('MC','OOSCE',member).
isMember('V','OOSCE',member).
isMember('IS','OOSCE',member).
isMember('IRL','OOSCE',member).
isMember('RSM','OOSCE',member).
isMember('M','OOSCE',member).
isMember('MD','OOSCE',member).
isMember('P','OOSCE',member).
isMember('GB','OOSCE',member).
isMember('TAD','OOSCE',member).
isMember('TM','OOSCE',member).
isMember('UZB','OOSCE',member).
isMember('ARM','OOSCE',member).
isMember('GE','OOSCE',member).
isMember('AZ','OOSCE',member).
isMember('KAZ','OOSCE',member).
isMember('KGZ','OOSCE',member).
isMember('CY','OOSCE',member).
isMember('CDN','OOSCE',member).
isMember('USA','OOSCE',member).
isMember('ET','OOAU',member).
isMember('DZ','OOAU',member).
isMember('LAR','OOAU',member).
isMember('RMM','OOAU',member).
isMember('RIM','OOAU',member).
isMember('RN','OOAU',member).
isMember('TN','OOAU',member).
isMember('WSA','OOAU',member).
isMember('ANG','OOAU',member).
isMember('RCB','OOAU',member).
isMember('NAM','OOAU',member).
isMember('ZRE','OOAU',member).
isMember('Z','OOAU',member).
isMember('BEN','OOAU',member).
isMember('BF','OOAU',member).
isMember('WAN','OOAU',member).
isMember('RT','OOAU',member).
isMember('RB','OOAU',member).
isMember('RSA','OOAU',member).
isMember('ZW','OOAU',member).
isMember('CI','OOAU',member).
isMember('GH','OOAU',member).
isMember('BI','OOAU',member).
isMember('RWA','OOAU',member).
isMember('EAT','OOAU',member).
isMember('CAM','OOAU',member).
isMember('RCA','OOAU',member).
isMember('TCH','OOAU',member).
isMember('GQ','OOAU',member).
isMember('G','OOAU',member).
isMember('CV','OOAU',member).
isMember('SUD','OOAU',member).
isMember('SSD','OOAU',member).
isMember('COM','OOAU',member).
isMember('RG','OOAU',member).
isMember('LB','OOAU',member).
isMember('DJI','OOAU',member).
isMember('ER','OOAU',member).
isMember('ETH','OOAU',member).
isMember('SP','OOAU',member).
isMember('EAK','OOAU',member).
isMember('WAG','OOAU',member).
isMember('SN','OOAU',member).
isMember('GNB','OOAU',member).
isMember('WAL','OOAU',member).
isMember('EAU','OOAU',member).
isMember('LS','OOAU',member).
isMember('RM','OOAU',member).
isMember('MW','OOAU',member).
isMember('MOC','OOAU',member).
isMember('MS','OOAU',member).
isMember('SD','OOAU',member).
isMember('STP','OOAU',member).
isMember('SY','OOAU',member).
isMember('AG','OOAS',member).
isMember('BS','OOAS',member).
isMember('BDS','OOAS',member).
isMember('BZ','OOAS',member).
isMember('GCA','OOAS',member).
isMember('MEX','OOAS',member).
isMember('CDN','OOAS',member).
isMember('USA','OOAS',member).
isMember('CR','OOAS',member).
isMember('NIC','OOAS',member).
isMember('PA','OOAS',member).
isMember('C','OOAS',member).
isMember('WD','OOAS',member).
isMember('DOM','OOAS',member).
isMember('RH','OOAS',member).
isMember('ES','OOAS',member).
isMember('HCA','OOAS',member).
isMember('WG','OOAS',member).
isMember('JA','OOAS',member).
isMember('CO','OOAS',member).
isMember('KN','OOAS',member).
isMember('WL','OOAS',member).
isMember('WV','OOAS',member).
isMember('TT','OOAS',member).
isMember('RA','OOAS',member).
isMember('BOL','OOAS',member).
isMember('BR','OOAS',member).
isMember('RCH','OOAS',member).
isMember('PY','OOAS',member).
isMember('ROU','OOAS',member).
isMember('PE','OOAS',member).
isMember('GUY','OOAS',member).
isMember('SME','OOAS',member).
isMember('YV','OOAS',member).
isMember('EC','OOAS',member).
isMember('GR','OOAS',observer).
isMember('F','OOAS',observer).
isMember('E','OOAS',observer).
isMember('A','OOAS',observer).
isMember('D','OOAS',observer).
isMember('H','OOAS',observer).
isMember('I','OOAS',observer).
isMember('CH','OOAS',observer).
isMember('PL','OOAS',observer).
isMember('R','OOAS',observer).
isMember('B','OOAS',observer).
isMember('NL','OOAS',observer).
isMember('RO','OOAS',observer).
isMember('SF','OOAS',observer).
isMember('V','OOAS',observer).
isMember('P','OOAS',observer).
isMember('PK','OOAS',observer).
isMember('IND','OOAS',observer).
isMember('CY','OOAS',observer).
isMember('IL','OOAS',observer).
isMember('ET','OOAS',observer).
isMember('SA','OOAS',observer).
isMember('J','OOAS',observer).
isMember('ROK','OOAS',observer).
isMember('DZ','OOAS',observer).
isMember('MA','OOAS',observer).
isMember('TN','OOAS',observer).
isMember('ANG','OOAS',observer).
isMember('GQ','OOAS',observer).
isMember('BRN','OOAPEC',member).
isMember('ET','OOAPEC',member).
isMember('IRQ','OOAPEC',member).
isMember('KWT','OOAPEC',member).
isMember('SA','OOAPEC',member).
isMember('SYR','OOAPEC',member).
isMember('UAE','OOAPEC',member).
isMember('Q','OOAPEC',member).
isMember('DZ','OOAPEC',member).
isMember('LAR','OOAPEC',member).
isMember('AG','OOECS',member).
isMember('WD','OOECS',member).
isMember('WG','OOECS',member).
isMember('MNTS','OOECS',member).
isMember('KN','OOECS',member).
isMember('WL','OOECS',member).
isMember('WV','OOECS',member).
isMember('AXA','OOECS','associate member').
isMember('BVIR','OOECS','associate member').
isMember('IR','OOPEC',member).
isMember('RI','OOPEC',member).
isMember('IRQ','OOPEC',member).
isMember('KWT','OOPEC',member).
isMember('SA','OOPEC',member).
isMember('UAE','OOPEC',member).
isMember('Q','OOPEC',member).
isMember('YV','OOPEC',member).
isMember('DZ','OOPEC',member).
isMember('LAR','OOPEC',member).
isMember('WAN','OOPEC',member).
isMember('G','OOPEC',member).
isMember('AL','OOIC',member).
isMember('TR','OOIC',member).
isMember('AFG','OOIC',member).
isMember('IR','OOIC',member).
isMember('PK','OOIC',member).
isMember('TAD','OOIC',member).
isMember('TM','OOIC',member).
isMember('AZ','OOIC',member).
isMember('BRN','OOIC',member).
isMember('BD','OOIC',member).
isMember('BRU','OOIC',member).
isMember('MAL','OOIC',member).
isMember('KAZ','OOIC',member).
isMember('KGZ','OOIC',member).
isMember('ET','OOIC',member).
isMember('RI','OOIC',member).
isMember('IRQ','OOIC',member).
isMember('JOR','OOIC',member).
isMember('KWT','OOIC',member).
isMember('SA','OOIC',member).
isMember('SYR','OOIC',member).
isMember('RL','OOIC',member).
isMember('MV','OOIC',member).
isMember('OM','OOIC',member).
isMember('UAE','OOIC',member).
isMember('YE','OOIC',member).
isMember('Q','OOIC',member).
isMember('DZ','OOIC',member).
isMember('LAR','OOIC',member).
isMember('RMM','OOIC',member).
isMember('RIM','OOIC',member).
isMember('MA','OOIC',member).
isMember('RN','OOIC',member).
isMember('TN','OOIC',member).
isMember('BEN','OOIC',member).
isMember('BF','OOIC',member).
isMember('CAM','OOIC',member).
isMember('TCH','OOIC',member).
isMember('G','OOIC',member).
isMember('SUD','OOIC',member).
isMember('COM','OOIC',member).
isMember('RG','OOIC',member).
isMember('DJI','OOIC',member).
isMember('SP','OOIC',member).
isMember('WAG','OOIC',member).
isMember('SN','OOIC',member).
isMember('GNB','OOIC',member).
isMember('WAL','OOIC',member).
isMember('EAU','OOIC',member).
isMember('MOC','OOIC',member).
isMember('UZB','OOIC',observer).
isMember('AL','OPFP',member).
isMember('SRB','OPFP',member).
isMember('MNE','OPFP',member).
isMember('A','OPFP',member).
isMember('CZ','OPFP',member).
isMember('H','OPFP',member).
isMember('SK','OPFP',member).
isMember('SLO','OPFP',member).
isMember('BY','OPFP',member).
isMember('LV','OPFP',member).
isMember('LT','OPFP',member).
isMember('PL','OPFP',member).
isMember('UA','OPFP',member).
isMember('R','OPFP',member).
isMember('BG','OPFP',member).
isMember('RO','OPFP',member).
isMember('EW','OPFP',member).
isMember('SF','OPFP',member).
isMember('S','OPFP',member).
isMember('M','OPFP',member).
isMember('MD','OPFP',member).
isMember('TM','OPFP',member).
isMember('UZB','OPFP',member).
isMember('ARM','OPFP',member).
isMember('GE','OPFP',member).
isMember('AZ','OPFP',member).
isMember('KAZ','OPFP',member).
isMember('KGZ','OPFP',member).
isMember('GR','OPCA',member).
isMember('SRB','OPCA',member).
isMember('MNE','OPCA',member).
isMember('F','OPCA',member).
isMember('E','OPCA',member).
isMember('A','OPCA',member).
isMember('CZ','OPCA',member).
isMember('D','OPCA',member).
isMember('H','OPCA',member).
isMember('I','OPCA',member).
isMember('FL','OPCA',member).
isMember('SK','OPCA',member).
isMember('CH','OPCA',member).
isMember('BY','OPCA',member).
isMember('PL','OPCA',member).
isMember('UA','OPCA',member).
isMember('R','OPCA',member).
isMember('B','OPCA',member).
isMember('L','OPCA',member).
isMember('NL','OPCA',member).
isMember('BG','OPCA',member).
isMember('RO','OPCA',member).
isMember('TR','OPCA',member).
isMember('DK','OPCA',member).
isMember('SF','OPCA',member).
isMember('N','OPCA',member).
isMember('S','OPCA',member).
isMember('IS','OPCA',member).
isMember('M','OPCA',member).
isMember('P','OPCA',member).
isMember('GB','OPCA',member).
isMember('CN','OPCA',member).
isMember('IR','OPCA',member).
isMember('PK','OPCA',member).
isMember('IND','OPCA',member).
isMember('LAO','OPCA',member).
isMember('THA','OPCA',member).
isMember('K','OPCA',member).
isMember('KGZ','OPCA',member).
isMember('CY','OPCA',member).
isMember('IL','OPCA',member).
isMember('ET','OPCA',member).
isMember('IRQ','OPCA',member).
isMember('JOR','OPCA',member).
isMember('RL','OPCA',member).
isMember('J','OPCA',member).
isMember('SGP','OPCA',member).
isMember('CL','OPCA',member).
isMember('GCA','OPCA',member).
isMember('MEX','OPCA',member).
isMember('CDN','OPCA',member).
isMember('USA','OPCA',member).
isMember('NIC','OPCA',member).
isMember('PA','OPCA',member).
isMember('C','OPCA',member).
isMember('DOM','OPCA',member).
isMember('RH','OPCA',member).
isMember('ES','OPCA',member).
isMember('HCA','OPCA',member).
isMember('CO','OPCA',member).
isMember('AUS','OPCA',member).
isMember('FJI','OPCA',member).
isMember('NZ','OPCA',member).
isMember('RA','OPCA',member).
isMember('BOL','OPCA',member).
isMember('BR','OPCA',member).
isMember('RCH','OPCA',member).
isMember('PY','OPCA',member).
isMember('ROU','OPCA',member).
isMember('PE','OPCA',member).
isMember('SME','OPCA',member).
isMember('YV','OPCA',member).
isMember('EC','OPCA',member).
isMember('ZRE','OPCA',member).
isMember('BF','OPCA',member).
isMember('WAN','OPCA',member).
isMember('ZW','OPCA',member).
isMember('CAM','OPCA',member).
isMember('SUD','OPCA',member).
isMember('SN','OPCA',member).
isMember('EAU','OPCA',member).
isMember('MS','OPCA',member).
isMember('SD','OPCA',member).
isMember('MEX','ORG',member).
isMember('CO','ORG',member).
isMember('RA','ORG',member).
isMember('BOL','ORG',member).
isMember('BR','ORG',member).
isMember('RCH','ORG',member).
isMember('PY','ORG',member).
isMember('ROU','ORG',member).
isMember('PE','ORG',member).
isMember('YV','ORG',member).
isMember('EC','ORG',member).
isMember('PK','OSAARC',member).
isMember('BD','OSAARC',member).
isMember('IND','OSAARC',member).
isMember('BHT','OSAARC',member).
isMember('NEP','OSAARC',member).
isMember('MV','OSAARC',member).
isMember('CL','OSAARC',member).
isMember('F','OSPC',member).
isMember('GB','OSPC',member).
isMember('PNG','OSPC',member).
isMember('USA','OSPC',member).
isMember('AMSA','OSPC',member).
isMember('AUS','OSPC',member).
isMember('COOK','OSPC',member).
isMember('FJI','OSPC',member).
isMember('FPOL','OSPC',member).
isMember('GUAM','OSPC',member).
isMember('KIR','OSPC',member).
isMember('MH','OSPC',member).
isMember('NAU','OSPC',member).
isMember('NCA','OSPC',member).
isMember('NZ','OSPC',member).
isMember('NIUE','OSPC',member).
isMember('NMIS','OSPC',member).
isMember('PAL','OSPC',member).
isMember('PITC','OSPC',member).
isMember('SLB','OSPC',member).
isMember('TO','OSPC',member).
isMember('TUV','OSPC',member).
isMember('VU','OSPC',member).
isMember('WAFU','OSPC',member).
isMember('WS','OSPC',member).
isMember('PNG','OSPF',member).
isMember('AUS','OSPF',member).
isMember('FJI','OSPF',member).
isMember('KIR','OSPF',member).
isMember('MH','OSPF',member).
isMember('NAU','OSPF',member).
isMember('NZ','OSPF',member).
isMember('PAL','OSPF',member).
isMember('SLB','OSPF',member).
isMember('TO','OSPF',member).
isMember('TUV','OSPF',member).
isMember('VU','OSPF',member).
isMember('WS','OSPF',member).
isMember('PNG','OSparteca',member).
isMember('AUS','OSparteca',member).
isMember('COOK','OSparteca',member).
isMember('FJI','OSparteca',member).
isMember('KIR','OSparteca',member).
isMember('MH','OSparteca',member).
isMember('NAU','OSparteca',member).
isMember('NZ','OSparteca',member).
isMember('NIUE','OSparteca',member).
isMember('SLB','OSparteca',member).
isMember('TO','OSparteca',member).
isMember('TUV','OSparteca',member).
isMember('VU','OSparteca',member).
isMember('WS','OSparteca',member).
isMember('NAM','OSACU',member).
isMember('RB','OSACU',member).
isMember('RSA','OSACU',member).
isMember('LS','OSACU',member).
isMember('SD','OSACU',member).
isMember('ANG','OSADC',member).
isMember('NAM','OSADC',member).
isMember('Z','OSADC',member).
isMember('RB','OSADC',member).
isMember('RSA','OSADC',member).
isMember('ZW','OSADC',member).
isMember('EAT','OSADC',member).
isMember('LS','OSADC',member).
isMember('MW','OSADC',member).
isMember('MOC','OSADC',member).
isMember('MS','OSADC',member).
isMember('SD','OSADC',member).
isMember('RA','OMercosur',member).
isMember('BR','OMercosur',member).
isMember('PY','OMercosur',member).
isMember('ROU','OMercosur',member).
isMember('RCH','OMercosur','associate member').
isMember('AL','OUN',member).
isMember('GR','OUN',member).
isMember('MK','OUN',member).
isMember('SRB','OUN',member).
isMember('MNE','OUN',member).
isMember('AND','OUN',member).
isMember('F','OUN',member).
isMember('E','OUN',member).
isMember('A','OUN',member).
isMember('CZ','OUN',member).
isMember('D','OUN',member).
isMember('H','OUN',member).
isMember('I','OUN',member).
isMember('FL','OUN',member).
isMember('SK','OUN',member).
isMember('SLO','OUN',member).
isMember('BY','OUN',member).
isMember('LV','OUN',member).
isMember('LT','OUN',member).
isMember('PL','OUN',member).
isMember('UA','OUN',member).
isMember('R','OUN',member).
isMember('B','OUN',member).
isMember('L','OUN',member).
isMember('NL','OUN',member).
isMember('BIH','OUN',member).
isMember('HR','OUN',member).
isMember('BG','OUN',member).
isMember('RO','OUN',member).
isMember('TR','OUN',member).
isMember('DK','OUN',member).
isMember('EW','OUN',member).
isMember('SF','OUN',member).
isMember('N','OUN',member).
isMember('S','OUN',member).
isMember('MC','OUN',member).
isMember('IS','OUN',member).
isMember('IRL','OUN',member).
isMember('RSM','OUN',member).
isMember('M','OUN',member).
isMember('MD','OUN',member).
isMember('P','OUN',member).
isMember('GB','OUN',member).
isMember('AFG','OUN',member).
isMember('CN','OUN',member).
isMember('IR','OUN',member).
isMember('PK','OUN',member).
isMember('TAD','OUN',member).
isMember('TM','OUN',member).
isMember('UZB','OUN',member).
isMember('ARM','OUN',member).
isMember('GE','OUN',member).
isMember('AZ','OUN',member).
isMember('BRN','OUN',member).
isMember('BD','OUN',member).
isMember('MYA','OUN',member).
isMember('IND','OUN',member).
isMember('BHT','OUN',member).
isMember('BRU','OUN',member).
isMember('MAL','OUN',member).
isMember('LAO','OUN',member).
isMember('THA','OUN',member).
isMember('K','OUN',member).
isMember('VN','OUN',member).
isMember('KAZ','OUN',member).
isMember('NOK','OUN',member).
isMember('KGZ','OUN',member).
isMember('MNG','OUN',member).
isMember('NEP','OUN',member).
isMember('CY','OUN',member).
isMember('IL','OUN',member).
isMember('ET','OUN',member).
isMember('RI','OUN',member).
isMember('TL','OUN',member).
isMember('PNG','OUN',member).
isMember('IRQ','OUN',member).
isMember('JOR','OUN',member).
isMember('KWT','OUN',member).
isMember('SA','OUN',member).
isMember('SYR','OUN',member).
isMember('RL','OUN',member).
isMember('J','OUN',member).
isMember('ROK','OUN',member).
isMember('MV','OUN',member).
isMember('OM','OUN',member).
isMember('UAE','OUN',member).
isMember('YE','OUN',member).
isMember('RP','OUN',member).
isMember('Q','OUN',member).
isMember('SGP','OUN',member).
isMember('CL','OUN',member).
isMember('AG','OUN',member).
isMember('BS','OUN',member).
isMember('BDS','OUN',member).
isMember('BZ','OUN',member).
isMember('GCA','OUN',member).
isMember('MEX','OUN',member).
isMember('CDN','OUN',member).
isMember('USA','OUN',member).
isMember('CR','OUN',member).
isMember('NIC','OUN',member).
isMember('PA','OUN',member).
isMember('C','OUN',member).
isMember('WD','OUN',member).
isMember('DOM','OUN',member).
isMember('RH','OUN',member).
isMember('ES','OUN',member).
isMember('HCA','OUN',member).
isMember('WG','OUN',member).
isMember('JA','OUN',member).
isMember('CO','OUN',member).
isMember('KN','OUN',member).
isMember('WL','OUN',member).
isMember('WV','OUN',member).
isMember('TT','OUN',member).
isMember('AUS','OUN',member).
isMember('FJI','OUN',member).
isMember('MH','OUN',member).
isMember('NZ','OUN',member).
isMember('PAL','OUN',member).
isMember('SLB','OUN',member).
isMember('VU','OUN',member).
isMember('WS','OUN',member).
isMember('RA','OUN',member).
isMember('BOL','OUN',member).
isMember('BR','OUN',member).
isMember('RCH','OUN',member).
isMember('PY','OUN',member).
isMember('ROU','OUN',member).
isMember('PE','OUN',member).
isMember('GUY','OUN',member).
isMember('SME','OUN',member).
isMember('YV','OUN',member).
isMember('EC','OUN',member).
isMember('DZ','OUN',member).
isMember('LAR','OUN',member).
isMember('RMM','OUN',member).
isMember('RIM','OUN',member).
isMember('MA','OUN',member).
isMember('RN','OUN',member).
isMember('TN','OUN',member).
isMember('ANG','OUN',member).
isMember('RCB','OUN',member).
isMember('NAM','OUN',member).
isMember('ZRE','OUN',member).
isMember('Z','OUN',member).
isMember('BEN','OUN',member).
isMember('BF','OUN',member).
isMember('WAN','OUN',member).
isMember('RT','OUN',member).
isMember('RB','OUN',member).
isMember('RSA','OUN',member).
isMember('ZW','OUN',member).
isMember('CI','OUN',member).
isMember('GH','OUN',member).
isMember('BI','OUN',member).
isMember('RWA','OUN',member).
isMember('EAT','OUN',member).
isMember('CAM','OUN',member).
isMember('RCA','OUN',member).
isMember('TCH','OUN',member).
isMember('GQ','OUN',member).
isMember('G','OUN',member).
isMember('CV','OUN',member).
isMember('SUD','OUN',member).
isMember('SSD','OUN',member).
isMember('COM','OUN',member).
isMember('RG','OUN',member).
isMember('LB','OUN',member).
isMember('DJI','OUN',member).
isMember('ER','OUN',member).
isMember('ETH','OUN',member).
isMember('SP','OUN',member).
isMember('EAK','OUN',member).
isMember('WAG','OUN',member).
isMember('SN','OUN',member).
isMember('GNB','OUN',member).
isMember('WAL','OUN',member).
isMember('EAU','OUN',member).
isMember('LS','OUN',member).
isMember('RM','OUN',member).
isMember('MW','OUN',member).
isMember('MOC','OUN',member).
isMember('MS','OUN',member).
isMember('SD','OUN',member).
isMember('STP','OUN',member).
isMember('SY','OUN',member).
isMember('CH','OUN',observer).
isMember('V','OUN',observer).
isMember('F','OUNAVEM III',member).
isMember('H','OUNAVEM III',member).
isMember('SK','OUNAVEM III',member).
isMember('PL','OUNAVEM III',member).
isMember('R','OUNAVEM III',member).
isMember('NL','OUNAVEM III',member).
isMember('BG','OUNAVEM III',member).
isMember('RO','OUNAVEM III',member).
isMember('N','OUNAVEM III',member).
isMember('S','OUNAVEM III',member).
isMember('P','OUNAVEM III',member).
isMember('GB','OUNAVEM III',member).
isMember('PK','OUNAVEM III',member).
isMember('BD','OUNAVEM III',member).
isMember('IND','OUNAVEM III',member).
isMember('MAL','OUNAVEM III',member).
isMember('ET','OUNAVEM III',member).
isMember('JOR','OUNAVEM III',member).
isMember('FJI','OUNAVEM III',member).
isMember('NZ','OUNAVEM III',member).
isMember('RA','OUNAVEM III',member).
isMember('BR','OUNAVEM III',member).
isMember('ROU','OUNAVEM III',member).
isMember('DZ','OUNAVEM III',member).
isMember('RMM','OUNAVEM III',member).
isMember('MA','OUNAVEM III',member).
isMember('RCB','OUNAVEM III',member).
isMember('Z','OUNAVEM III',member).
isMember('WAN','OUNAVEM III',member).
isMember('ZW','OUNAVEM III',member).
isMember('EAT','OUNAVEM III',member).
isMember('EAK','OUNAVEM III',member).
isMember('SN','OUNAVEM III',member).
isMember('GNB','OUNAVEM III',member).
isMember('A','OUNAMIR',member).
isMember('D','OUNAMIR',member).
isMember('CH','OUNAMIR',member).
isMember('R','OUNAMIR',member).
isMember('PK','OUNAMIR',member).
isMember('BD','OUNAMIR',member).
isMember('IND','OUNAMIR',member).
isMember('JOR','OUNAMIR',member).
isMember('CDN','OUNAMIR',member).
isMember('AUS','OUNAMIR',member).
isMember('FJI','OUNAMIR',member).
isMember('RA','OUNAMIR',member).
isMember('ROU','OUNAMIR',member).
isMember('RMM','OUNAMIR',member).
isMember('RN','OUNAMIR',member).
isMember('TN','OUNAMIR',member).
isMember('RCB','OUNAMIR',member).
isMember('Z','OUNAMIR',member).
isMember('WAN','OUNAMIR',member).
isMember('ZW','OUNAMIR',member).
isMember('GH','OUNAMIR',member).
isMember('TCH','OUNAMIR',member).
isMember('RG','OUNAMIR',member).
isMember('DJI','OUNAMIR',member).
isMember('SN','OUNAMIR',member).
isMember('GNB','OUNAMIR',member).
isMember('MW','OUNAMIR',member).
isMember('F','OUNCRO',member).
isMember('E','OUNCRO',member).
isMember('CZ','OUNCRO',member).
isMember('D','OUNCRO',member).
isMember('SK','OUNCRO',member).
isMember('LT','OUNCRO',member).
isMember('PL','OUNCRO',member).
isMember('UA','OUNCRO',member).
isMember('R','OUNCRO',member).
isMember('B','OUNCRO',member).
isMember('NL','OUNCRO',member).
isMember('TR','OUNCRO',member).
isMember('DK','OUNCRO',member).
isMember('EW','OUNCRO',member).
isMember('SF','OUNCRO',member).
isMember('N','OUNCRO',member).
isMember('S','OUNCRO',member).
isMember('IRL','OUNCRO',member).
isMember('P','OUNCRO',member).
isMember('GB','OUNCRO',member).
isMember('PK','OUNCRO',member).
isMember('BD','OUNCRO',member).
isMember('MAL','OUNCRO',member).
isMember('NEP','OUNCRO',member).
isMember('ET','OUNCRO',member).
isMember('RI','OUNCRO',member).
isMember('JOR','OUNCRO',member).
isMember('CDN','OUNCRO',member).
isMember('USA','OUNCRO',member).
isMember('RA','OUNCRO',member).
isMember('BR','OUNCRO',member).
isMember('TN','OUNCRO',member).
isMember('WAN','OUNCRO',member).
isMember('GH','OUNCRO',member).
isMember('EAK','OUNCRO',member).
isMember('SN','OUNCRO',member).
isMember('A','OUNDOF',member).
isMember('PL','OUNDOF',member).
isMember('CDN','OUNDOF',member).
isMember('AL','OUNESCO',member).
isMember('GR','OUNESCO',member).
isMember('MK','OUNESCO',member).
isMember('SRB','OUNESCO',member).
isMember('MNE','OUNESCO',member).
isMember('AND','OUNESCO',member).
isMember('F','OUNESCO',member).
isMember('E','OUNESCO',member).
isMember('A','OUNESCO',member).
isMember('CZ','OUNESCO',member).
isMember('D','OUNESCO',member).
isMember('H','OUNESCO',member).
isMember('I','OUNESCO',member).
isMember('SK','OUNESCO',member).
isMember('SLO','OUNESCO',member).
isMember('CH','OUNESCO',member).
isMember('BY','OUNESCO',member).
isMember('LV','OUNESCO',member).
isMember('LT','OUNESCO',member).
isMember('PL','OUNESCO',member).
isMember('UA','OUNESCO',member).
isMember('R','OUNESCO',member).
isMember('B','OUNESCO',member).
isMember('L','OUNESCO',member).
isMember('NL','OUNESCO',member).
isMember('BIH','OUNESCO',member).
isMember('HR','OUNESCO',member).
isMember('BG','OUNESCO',member).
isMember('RO','OUNESCO',member).
isMember('TR','OUNESCO',member).
isMember('DK','OUNESCO',member).
isMember('EW','OUNESCO',member).
isMember('SF','OUNESCO',member).
isMember('N','OUNESCO',member).
isMember('S','OUNESCO',member).
isMember('MC','OUNESCO',member).
isMember('IS','OUNESCO',member).
isMember('IRL','OUNESCO',member).
isMember('RSM','OUNESCO',member).
isMember('M','OUNESCO',member).
isMember('MD','OUNESCO',member).
isMember('P','OUNESCO',member).
isMember('AFG','OUNESCO',member).
isMember('CN','OUNESCO',member).
isMember('IR','OUNESCO',member).
isMember('PK','OUNESCO',member).
isMember('TAD','OUNESCO',member).
isMember('TM','OUNESCO',member).
isMember('UZB','OUNESCO',member).
isMember('ARM','OUNESCO',member).
isMember('GE','OUNESCO',member).
isMember('AZ','OUNESCO',member).
isMember('BRN','OUNESCO',member).
isMember('BD','OUNESCO',member).
isMember('MYA','OUNESCO',member).
isMember('IND','OUNESCO',member).
isMember('BHT','OUNESCO',member).
isMember('MAL','OUNESCO',member).
isMember('LAO','OUNESCO',member).
isMember('THA','OUNESCO',member).
isMember('K','OUNESCO',member).
isMember('VN','OUNESCO',member).
isMember('KAZ','OUNESCO',member).
isMember('NOK','OUNESCO',member).
isMember('KGZ','OUNESCO',member).
isMember('MNG','OUNESCO',member).
isMember('NEP','OUNESCO',member).
isMember('CY','OUNESCO',member).
isMember('IL','OUNESCO',member).
isMember('ET','OUNESCO',member).
isMember('RI','OUNESCO',member).
isMember('TL','OUNESCO',member).
isMember('PNG','OUNESCO',member).
isMember('IRQ','OUNESCO',member).
isMember('JOR','OUNESCO',member).
isMember('KWT','OUNESCO',member).
isMember('SA','OUNESCO',member).
isMember('SYR','OUNESCO',member).
isMember('RL','OUNESCO',member).
isMember('J','OUNESCO',member).
isMember('ROK','OUNESCO',member).
isMember('MV','OUNESCO',member).
isMember('OM','OUNESCO',member).
isMember('UAE','OUNESCO',member).
isMember('YE','OUNESCO',member).
isMember('RP','OUNESCO',member).
isMember('Q','OUNESCO',member).
isMember('CL','OUNESCO',member).
isMember('AG','OUNESCO',member).
isMember('BS','OUNESCO',member).
isMember('BDS','OUNESCO',member).
isMember('BZ','OUNESCO',member).
isMember('GCA','OUNESCO',member).
isMember('MEX','OUNESCO',member).
isMember('CDN','OUNESCO',member).
isMember('CAYM','OUNESCO',member).
isMember('CR','OUNESCO',member).
isMember('NIC','OUNESCO',member).
isMember('PA','OUNESCO',member).
isMember('C','OUNESCO',member).
isMember('WD','OUNESCO',member).
isMember('DOM','OUNESCO',member).
isMember('RH','OUNESCO',member).
isMember('ES','OUNESCO',member).
isMember('HCA','OUNESCO',member).
isMember('WG','OUNESCO',member).
isMember('JA','OUNESCO',member).
isMember('CO','OUNESCO',member).
isMember('KN','OUNESCO',member).
isMember('WL','OUNESCO',member).
isMember('WV','OUNESCO',member).
isMember('TT','OUNESCO',member).
isMember('AUS','OUNESCO',member).
isMember('COOK','OUNESCO',member).
isMember('FJI','OUNESCO',member).
isMember('KIR','OUNESCO',member).
isMember('MH','OUNESCO',member).
isMember('NZ','OUNESCO',member).
isMember('NIUE','OUNESCO',member).
isMember('SLB','OUNESCO',member).
isMember('TO','OUNESCO',member).
isMember('TUV','OUNESCO',member).
isMember('VU','OUNESCO',member).
isMember('WS','OUNESCO',member).
isMember('RA','OUNESCO',member).
isMember('BOL','OUNESCO',member).
isMember('BR','OUNESCO',member).
isMember('RCH','OUNESCO',member).
isMember('PY','OUNESCO',member).
isMember('ROU','OUNESCO',member).
isMember('PE','OUNESCO',member).
isMember('GUY','OUNESCO',member).
isMember('SME','OUNESCO',member).
isMember('YV','OUNESCO',member).
isMember('EC','OUNESCO',member).
isMember('DZ','OUNESCO',member).
isMember('LAR','OUNESCO',member).
isMember('RMM','OUNESCO',member).
isMember('RIM','OUNESCO',member).
isMember('MA','OUNESCO',member).
isMember('RN','OUNESCO',member).
isMember('TN','OUNESCO',member).
isMember('ANG','OUNESCO',member).
isMember('RCB','OUNESCO',member).
isMember('NAM','OUNESCO',member).
isMember('ZRE','OUNESCO',member).
isMember('Z','OUNESCO',member).
isMember('BEN','OUNESCO',member).
isMember('BF','OUNESCO',member).
isMember('WAN','OUNESCO',member).
isMember('RT','OUNESCO',member).
isMember('RB','OUNESCO',member).
isMember('RSA','OUNESCO',member).
isMember('ZW','OUNESCO',member).
isMember('CI','OUNESCO',member).
isMember('GH','OUNESCO',member).
isMember('BI','OUNESCO',member).
isMember('RWA','OUNESCO',member).
isMember('EAT','OUNESCO',member).
isMember('CAM','OUNESCO',member).
isMember('RCA','OUNESCO',member).
isMember('TCH','OUNESCO',member).
isMember('GQ','OUNESCO',member).
isMember('G','OUNESCO',member).
isMember('CV','OUNESCO',member).
isMember('SUD','OUNESCO',member).
isMember('SSD','OUNESCO',member).
isMember('COM','OUNESCO',member).
isMember('RG','OUNESCO',member).
isMember('LB','OUNESCO',member).
isMember('DJI','OUNESCO',member).
isMember('ER','OUNESCO',member).
isMember('ETH','OUNESCO',member).
isMember('SP','OUNESCO',member).
isMember('EAK','OUNESCO',member).
isMember('WAG','OUNESCO',member).
isMember('SN','OUNESCO',member).
isMember('GNB','OUNESCO',member).
isMember('WAL','OUNESCO',member).
isMember('EAU','OUNESCO',member).
isMember('LS','OUNESCO',member).
isMember('RM','OUNESCO',member).
isMember('MW','OUNESCO',member).
isMember('MOC','OUNESCO',member).
isMember('MS','OUNESCO',member).
isMember('SD','OUNESCO',member).
isMember('STP','OUNESCO',member).
isMember('SY','OUNESCO',member).
isMember('MACX','OUNESCO','associate member').
isMember('ARU','OUNESCO','associate member').
isMember('BVIR','OUNESCO','associate member').
isMember('NA','OUNESCO','associate member').
isMember('A','OUNFICYP',member).
isMember('SF','OUNFICYP',member).
isMember('IRL','OUNFICYP',member).
isMember('GB','OUNFICYP',member).
isMember('CDN','OUNFICYP',member).
isMember('AUS','OUNFICYP',member).
isMember('RA','OUNFICYP',member).
isMember('AL','OUNIDO',member).
isMember('GR','OUNIDO',member).
isMember('MK','OUNIDO',member).
isMember('SRB','OUNIDO',member).
isMember('MNE','OUNIDO',member).
isMember('F','OUNIDO',member).
isMember('E','OUNIDO',member).
isMember('A','OUNIDO',member).
isMember('CZ','OUNIDO',member).
isMember('D','OUNIDO',member).
isMember('H','OUNIDO',member).
isMember('I','OUNIDO',member).
isMember('SK','OUNIDO',member).
isMember('SLO','OUNIDO',member).
isMember('CH','OUNIDO',member).
isMember('BY','OUNIDO',member).
isMember('LV','OUNIDO',member).
isMember('PL','OUNIDO',member).
isMember('UA','OUNIDO',member).
isMember('R','OUNIDO',member).
isMember('B','OUNIDO',member).
isMember('L','OUNIDO',member).
isMember('NL','OUNIDO',member).
isMember('BIH','OUNIDO',member).
isMember('HR','OUNIDO',member).
isMember('BG','OUNIDO',member).
isMember('RO','OUNIDO',member).
isMember('TR','OUNIDO',member).
isMember('DK','OUNIDO',member).
isMember('SF','OUNIDO',member).
isMember('N','OUNIDO',member).
isMember('S','OUNIDO',member).
isMember('IRL','OUNIDO',member).
isMember('M','OUNIDO',member).
isMember('MD','OUNIDO',member).
isMember('P','OUNIDO',member).
isMember('GB','OUNIDO',member).
isMember('AFG','OUNIDO',member).
isMember('CN','OUNIDO',member).
isMember('IR','OUNIDO',member).
isMember('PK','OUNIDO',member).
isMember('TAD','OUNIDO',member).
isMember('UZB','OUNIDO',member).
isMember('ARM','OUNIDO',member).
isMember('GE','OUNIDO',member).
isMember('AZ','OUNIDO',member).
isMember('BRN','OUNIDO',member).
isMember('BD','OUNIDO',member).
isMember('MYA','OUNIDO',member).
isMember('IND','OUNIDO',member).
isMember('BHT','OUNIDO',member).
isMember('MAL','OUNIDO',member).
isMember('LAO','OUNIDO',member).
isMember('THA','OUNIDO',member).
isMember('K','OUNIDO',member).
isMember('VN','OUNIDO',member).
isMember('NOK','OUNIDO',member).
isMember('KGZ','OUNIDO',member).
isMember('MNG','OUNIDO',member).
isMember('NEP','OUNIDO',member).
isMember('CY','OUNIDO',member).
isMember('IL','OUNIDO',member).
isMember('ET','OUNIDO',member).
isMember('RI','OUNIDO',member).
isMember('TL','OUNIDO',member).
isMember('PNG','OUNIDO',member).
isMember('IRQ','OUNIDO',member).
isMember('JOR','OUNIDO',member).
isMember('KWT','OUNIDO',member).
isMember('SA','OUNIDO',member).
isMember('SYR','OUNIDO',member).
isMember('RL','OUNIDO',member).
isMember('J','OUNIDO',member).
isMember('ROK','OUNIDO',member).
isMember('MV','OUNIDO',member).
isMember('OM','OUNIDO',member).
isMember('UAE','OUNIDO',member).
isMember('YE','OUNIDO',member).
isMember('RP','OUNIDO',member).
isMember('Q','OUNIDO',member).
isMember('CL','OUNIDO',member).
isMember('BS','OUNIDO',member).
isMember('BDS','OUNIDO',member).
isMember('BZ','OUNIDO',member).
isMember('GCA','OUNIDO',member).
isMember('MEX','OUNIDO',member).
isMember('CDN','OUNIDO',member).
isMember('USA','OUNIDO',member).
isMember('CR','OUNIDO',member).
isMember('NIC','OUNIDO',member).
isMember('PA','OUNIDO',member).
isMember('C','OUNIDO',member).
isMember('WD','OUNIDO',member).
isMember('DOM','OUNIDO',member).
isMember('RH','OUNIDO',member).
isMember('ES','OUNIDO',member).
isMember('HCA','OUNIDO',member).
isMember('WG','OUNIDO',member).
isMember('JA','OUNIDO',member).
isMember('CO','OUNIDO',member).
isMember('KN','OUNIDO',member).
isMember('WL','OUNIDO',member).
isMember('WV','OUNIDO',member).
isMember('TT','OUNIDO',member).
isMember('AUS','OUNIDO',member).
isMember('FJI','OUNIDO',member).
isMember('NZ','OUNIDO',member).
isMember('TO','OUNIDO',member).
isMember('VU','OUNIDO',member).
isMember('RA','OUNIDO',member).
isMember('BOL','OUNIDO',member).
isMember('BR','OUNIDO',member).
isMember('RCH','OUNIDO',member).
isMember('PY','OUNIDO',member).
isMember('ROU','OUNIDO',member).
isMember('PE','OUNIDO',member).
isMember('GUY','OUNIDO',member).
isMember('SME','OUNIDO',member).
isMember('YV','OUNIDO',member).
isMember('EC','OUNIDO',member).
isMember('DZ','OUNIDO',member).
isMember('LAR','OUNIDO',member).
isMember('RMM','OUNIDO',member).
isMember('RIM','OUNIDO',member).
isMember('MA','OUNIDO',member).
isMember('RN','OUNIDO',member).
isMember('TN','OUNIDO',member).
isMember('ANG','OUNIDO',member).
isMember('RCB','OUNIDO',member).
isMember('NAM','OUNIDO',member).
isMember('ZRE','OUNIDO',member).
isMember('Z','OUNIDO',member).
isMember('BEN','OUNIDO',member).
isMember('BF','OUNIDO',member).
isMember('WAN','OUNIDO',member).
isMember('RT','OUNIDO',member).
isMember('RB','OUNIDO',member).
isMember('ZW','OUNIDO',member).
isMember('CI','OUNIDO',member).
isMember('GH','OUNIDO',member).
isMember('BI','OUNIDO',member).
isMember('RWA','OUNIDO',member).
isMember('EAT','OUNIDO',member).
isMember('CAM','OUNIDO',member).
isMember('RCA','OUNIDO',member).
isMember('TCH','OUNIDO',member).
isMember('GQ','OUNIDO',member).
isMember('G','OUNIDO',member).
isMember('CV','OUNIDO',member).
isMember('SUD','OUNIDO',member).
isMember('COM','OUNIDO',member).
isMember('RG','OUNIDO',member).
isMember('LB','OUNIDO',member).
isMember('DJI','OUNIDO',member).
isMember('ER','OUNIDO',member).
isMember('ETH','OUNIDO',member).
isMember('SP','OUNIDO',member).
isMember('EAK','OUNIDO',member).
isMember('WAG','OUNIDO',member).
isMember('SN','OUNIDO',member).
isMember('GNB','OUNIDO',member).
isMember('WAL','OUNIDO',member).
isMember('EAU','OUNIDO',member).
isMember('LS','OUNIDO',member).
isMember('RM','OUNIDO',member).
isMember('MW','OUNIDO',member).
isMember('MOC','OUNIDO',member).
isMember('MS','OUNIDO',member).
isMember('SD','OUNIDO',member).
isMember('STP','OUNIDO',member).
isMember('SY','OUNIDO',member).
isMember('F','OUNITAR',member).
isMember('D','OUNITAR',member).
isMember('I','OUNITAR',member).
isMember('CH','OUNITAR',member).
isMember('R','OUNITAR',member).
isMember('B','OUNITAR',member).
isMember('NL','OUNITAR',member).
isMember('N','OUNITAR',member).
isMember('S','OUNITAR',member).
isMember('GB','OUNITAR',member).
isMember('CN','OUNITAR',member).
isMember('PK','OUNITAR',member).
isMember('IND','OUNITAR',member).
isMember('J','OUNITAR',member).
isMember('MEX','OUNITAR',member).
isMember('CDN','OUNITAR',member).
isMember('USA','OUNITAR',member).
isMember('JA','OUNITAR',member).
isMember('RA','OUNITAR',member).
isMember('LAR','OUNITAR',member).
isMember('TN','OUNITAR',member).
isMember('CI','OUNITAR',member).
isMember('EAU','OUNITAR',member).
isMember('F','OUNIFIL',member).
isMember('I','OUNIFIL',member).
isMember('PL','OUNIFIL',member).
isMember('SF','OUNIFIL',member).
isMember('N','OUNIFIL',member).
isMember('IRL','OUNIFIL',member).
isMember('NEP','OUNIFIL',member).
isMember('FJI','OUNIFIL',member).
isMember('GH','OUNIFIL',member).
isMember('GR','OUNIKOM',member).
isMember('F','OUNIKOM',member).
isMember('A','OUNIKOM',member).
isMember('H','OUNIKOM',member).
isMember('I','OUNIKOM',member).
isMember('PL','OUNIKOM',member).
isMember('R','OUNIKOM',member).
isMember('RO','OUNIKOM',member).
isMember('TR','OUNIKOM',member).
isMember('DK','OUNIKOM',member).
isMember('SF','OUNIKOM',member).
isMember('S','OUNIKOM',member).
isMember('IRL','OUNIKOM',member).
isMember('GB','OUNIKOM',member).
isMember('CN','OUNIKOM',member).
isMember('PK','OUNIKOM',member).
isMember('BD','OUNIKOM',member).
isMember('IND','OUNIKOM',member).
isMember('MAL','OUNIKOM',member).
isMember('THA','OUNIKOM',member).
isMember('RI','OUNIKOM',member).
isMember('SGP','OUNIKOM',member).
isMember('CDN','OUNIKOM',member).
isMember('USA','OUNIKOM',member).
isMember('FJI','OUNIKOM',member).
isMember('RA','OUNIKOM',member).
isMember('ROU','OUNIKOM',member).
isMember('YV','OUNIKOM',member).
isMember('WAN','OUNIKOM',member).
isMember('GH','OUNIKOM',member).
isMember('EAK','OUNIKOM',member).
isMember('SN','OUNIKOM',member).
isMember('I','OUNMOGIP',member).
isMember('B','OUNMOGIP',member).
isMember('DK','OUNMOGIP',member).
isMember('SF','OUNMOGIP',member).
isMember('S','OUNMOGIP',member).
isMember('ROK','OUNMOGIP',member).
isMember('RCH','OUNMOGIP',member).
isMember('ROU','OUNMOGIP',member).
isMember('GR','OMINURSO',member).
isMember('F','OMINURSO',member).
isMember('A','OMINURSO',member).
isMember('D','OMINURSO',member).
isMember('H','OMINURSO',member).
isMember('I','OMINURSO',member).
isMember('PL','OMINURSO',member).
isMember('R','OMINURSO',member).
isMember('B','OMINURSO',member).
isMember('N','OMINURSO',member).
isMember('IRL','OMINURSO',member).
isMember('CN','OMINURSO',member).
isMember('PK','OMINURSO',member).
isMember('BD','OMINURSO',member).
isMember('MAL','OMINURSO',member).
isMember('ET','OMINURSO',member).
isMember('ROK','OMINURSO',member).
isMember('USA','OMINURSO',member).
isMember('ES','OMINURSO',member).
isMember('HCA','OMINURSO',member).
isMember('RA','OMINURSO',member).
isMember('ROU','OMINURSO',member).
isMember('YV','OMINURSO',member).
isMember('TN','OMINURSO',member).
isMember('WAN','OMINURSO',member).
isMember('RT','OMINURSO',member).
isMember('GH','OMINURSO',member).
isMember('RG','OMINURSO',member).
isMember('EAK','OMINURSO',member).
isMember('F','OUNMIH',member).
isMember('A','OUNMIH',member).
isMember('R','OUNMIH',member).
isMember('NL','OUNMIH',member).
isMember('IRL','OUNMIH',member).
isMember('PK','OUNMIH',member).
isMember('BD','OUNMIH',member).
isMember('IND','OUNMIH',member).
isMember('NEP','OUNMIH',member).
isMember('JOR','OUNMIH',member).
isMember('RP','OUNMIH',member).
isMember('BS','OUNMIH',member).
isMember('BDS','OUNMIH',member).
isMember('BZ','OUNMIH',member).
isMember('GCA','OUNMIH',member).
isMember('CDN','OUNMIH',member).
isMember('USA','OUNMIH',member).
isMember('HCA','OUNMIH',member).
isMember('JA','OUNMIH',member).
isMember('KN','OUNMIH',member).
isMember('WL','OUNMIH',member).
isMember('TT','OUNMIH',member).
isMember('RA','OUNMIH',member).
isMember('GUY','OUNMIH',member).
isMember('SME','OUNMIH',member).
isMember('DZ','OUNMIH',member).
isMember('RMM','OUNMIH',member).
isMember('BEN','OUNMIH',member).
isMember('RT','OUNMIH',member).
isMember('DJI','OUNMIH',member).
isMember('A','OUNMOT',member).
isMember('H','OUNMOT',member).
isMember('CH','OUNMOT',member).
isMember('PL','OUNMOT',member).
isMember('UA','OUNMOT',member).
isMember('BG','OUNMOT',member).
isMember('DK','OUNMOT',member).
isMember('BD','OUNMOT',member).
isMember('JOR','OUNMOT',member).
isMember('ROU','OUNMOT',member).
isMember('AL','OUNOMIG',member).
isMember('GR','OUNOMIG',member).
isMember('F','OUNOMIG',member).
isMember('A','OUNOMIG',member).
isMember('CZ','OUNOMIG',member).
isMember('D','OUNOMIG',member).
isMember('H','OUNOMIG',member).
isMember('CH','OUNOMIG',member).
isMember('PL','OUNOMIG',member).
isMember('R','OUNOMIG',member).
isMember('TR','OUNOMIG',member).
isMember('DK','OUNOMIG',member).
isMember('S','OUNOMIG',member).
isMember('GB','OUNOMIG',member).
isMember('PK','OUNOMIG',member).
isMember('BD','OUNOMIG',member).
isMember('ET','OUNOMIG',member).
isMember('RI','OUNOMIG',member).
isMember('JOR','OUNOMIG',member).
isMember('ROK','OUNOMIG',member).
isMember('USA','OUNOMIG',member).
isMember('C','OUNOMIG',member).
isMember('ROU','OUNOMIG',member).
isMember('CZ','OUNOMIL',member).
isMember('CN','OUNOMIL',member).
isMember('PK','OUNOMIL',member).
isMember('IND','OUNOMIL',member).
isMember('MAL','OUNOMIL',member).
isMember('ET','OUNOMIL',member).
isMember('JOR','OUNOMIL',member).
isMember('ROU','OUNOMIL',member).
isMember('EAK','OUNOMIL',member).
isMember('GNB','OUNOMIL',member).
isMember('GR','OUNHCR',member).
isMember('SRB','OUNHCR',member).
isMember('MNE','OUNHCR',member).
isMember('F','OUNHCR',member).
isMember('E','OUNHCR',member).
isMember('A','OUNHCR',member).
isMember('D','OUNHCR',member).
isMember('H','OUNHCR',member).
isMember('I','OUNHCR',member).
isMember('CH','OUNHCR',member).
isMember('R','OUNHCR',member).
isMember('B','OUNHCR',member).
isMember('NL','OUNHCR',member).
isMember('TR','OUNHCR',member).
isMember('DK','OUNHCR',member).
isMember('SF','OUNHCR',member).
isMember('N','OUNHCR',member).
isMember('S','OUNHCR',member).
isMember('V','OUNHCR',member).
isMember('GB','OUNHCR',member).
isMember('CN','OUNHCR',member).
isMember('IR','OUNHCR',member).
isMember('PK','OUNHCR',member).
isMember('BD','OUNHCR',member).
isMember('IND','OUNHCR',member).
isMember('THA','OUNHCR',member).
isMember('IL','OUNHCR',member).
isMember('RL','OUNHCR',member).
isMember('J','OUNHCR',member).
isMember('RP','OUNHCR',member).
isMember('CDN','OUNHCR',member).
isMember('USA','OUNHCR',member).
isMember('NIC','OUNHCR',member).
isMember('CO','OUNHCR',member).
isMember('AUS','OUNHCR',member).
isMember('RA','OUNHCR',member).
isMember('BR','OUNHCR',member).
isMember('YV','OUNHCR',member).
isMember('DZ','OUNHCR',member).
isMember('MA','OUNHCR',member).
isMember('TN','OUNHCR',member).
isMember('NAM','OUNHCR',member).
isMember('ZRE','OUNHCR',member).
isMember('WAN','OUNHCR',member).
isMember('EAT','OUNHCR',member).
isMember('SUD','OUNHCR',member).
isMember('ETH','OUNHCR',member).
isMember('SP','OUNHCR',member).
isMember('EAU','OUNHCR',member).
isMember('LS','OUNHCR',member).
isMember('RM','OUNHCR',member).
isMember('F','OUNPREDEP',member).
isMember('E','OUNPREDEP',member).
isMember('CH','OUNPREDEP',member).
isMember('PL','OUNPREDEP',member).
isMember('UA','OUNPREDEP',member).
isMember('R','OUNPREDEP',member).
isMember('NL','OUNPREDEP',member).
isMember('DK','OUNPREDEP',member).
isMember('SF','OUNPREDEP',member).
isMember('N','OUNPREDEP',member).
isMember('S','OUNPREDEP',member).
isMember('IRL','OUNPREDEP',member).
isMember('P','OUNPREDEP',member).
isMember('GB','OUNPREDEP',member).
isMember('PK','OUNPREDEP',member).
isMember('BD','OUNPREDEP',member).
isMember('MAL','OUNPREDEP',member).
isMember('ET','OUNPREDEP',member).
isMember('RI','OUNPREDEP',member).
isMember('JOR','OUNPREDEP',member).
isMember('CDN','OUNPREDEP',member).
isMember('USA','OUNPREDEP',member).
isMember('NZ','OUNPREDEP',member).
isMember('BR','OUNPREDEP',member).
isMember('WAN','OUNPREDEP',member).
isMember('GH','OUNPREDEP',member).
isMember('EAK','OUNPREDEP',member).
isMember('F','OUNPROFOR',member).
isMember('E','OUNPROFOR',member).
isMember('CZ','OUNPROFOR',member).
isMember('D','OUNPROFOR',member).
isMember('CH','OUNPROFOR',member).
isMember('PL','OUNPROFOR',member).
isMember('UA','OUNPROFOR',member).
isMember('R','OUNPROFOR',member).
isMember('B','OUNPROFOR',member).
isMember('NL','OUNPROFOR',member).
isMember('DK','OUNPROFOR',member).
isMember('SF','OUNPROFOR',member).
isMember('N','OUNPROFOR',member).
isMember('S','OUNPROFOR',member).
isMember('IRL','OUNPROFOR',member).
isMember('P','OUNPROFOR',member).
isMember('GB','OUNPROFOR',member).
isMember('PK','OUNPROFOR',member).
isMember('BD','OUNPROFOR',member).
isMember('MAL','OUNPROFOR',member).
isMember('NEP','OUNPROFOR',member).
isMember('ET','OUNPROFOR',member).
isMember('RI','OUNPROFOR',member).
isMember('JOR','OUNPROFOR',member).
isMember('CDN','OUNPROFOR',member).
isMember('USA','OUNPROFOR',member).
isMember('NZ','OUNPROFOR',member).
isMember('BR','OUNPROFOR',member).
isMember('WAN','OUNPROFOR',member).
isMember('GH','OUNPROFOR',member).
isMember('EAK','OUNPROFOR',member).
isMember('F','OUNRWA',member).
isMember('B','OUNRWA',member).
isMember('TR','OUNRWA',member).
isMember('GB','OUNRWA',member).
isMember('ET','OUNRWA',member).
isMember('JOR','OUNRWA',member).
isMember('SYR','OUNRWA',member).
isMember('RL','OUNRWA',member).
isMember('J','OUNRWA',member).
isMember('USA','OUNRWA',member).
isMember('F','OUNTSO',member).
isMember('A','OUNTSO',member).
isMember('I','OUNTSO',member).
isMember('CH','OUNTSO',member).
isMember('R','OUNTSO',member).
isMember('B','OUNTSO',member).
isMember('NL','OUNTSO',member).
isMember('DK','OUNTSO',member).
isMember('SF','OUNTSO',member).
isMember('N','OUNTSO',member).
isMember('S','OUNTSO',member).
isMember('IRL','OUNTSO',member).
isMember('CN','OUNTSO',member).
isMember('CDN','OUNTSO',member).
isMember('USA','OUNTSO',member).
isMember('AUS','OUNTSO',member).
isMember('NZ','OUNTSO',member).
isMember('RA','OUNTSO',member).
isMember('RCH','OUNTSO',member).
isMember('F','OUNU',member).
isMember('E','OUNU',member).
isMember('H','OUNU',member).
isMember('CH','OUNU',member).
isMember('NL','OUNU',member).
isMember('IS','OUNU',member).
isMember('GB','OUNU',member).
isMember('CN','OUNU',member).
isMember('BD','OUNU',member).
isMember('IND','OUNU',member).
isMember('THA','OUNU',member).
isMember('J','OUNU',member).
isMember('ROK','OUNU',member).
isMember('RP','OUNU',member).
isMember('CL','OUNU',member).
isMember('GCA','OUNU',member).
isMember('MEX','OUNU',member).
isMember('CDN','OUNU',member).
isMember('USA','OUNU',member).
isMember('CR','OUNU',member).
isMember('CO','OUNU',member).
isMember('TT','OUNU',member).
isMember('AUS','OUNU',member).
isMember('RA','OUNU',member).
isMember('BR','OUNU',member).
isMember('RCH','OUNU',member).
isMember('YV','OUNU',member).
isMember('WAN','OUNU',member).
isMember('GH','OUNU',member).
isMember('SUD','OUNU',member).
isMember('ETH','OUNU',member).
isMember('EAK','OUNU',member).
isMember('AL','OUPU',member).
isMember('GR','OUPU',member).
isMember('MK','OUPU',member).
isMember('SRB','OUPU',member).
isMember('MNE','OUPU',member).
isMember('F','OUPU',member).
isMember('E','OUPU',member).
isMember('A','OUPU',member).
isMember('CZ','OUPU',member).
isMember('D','OUPU',member).
isMember('H','OUPU',member).
isMember('I','OUPU',member).
isMember('FL','OUPU',member).
isMember('SK','OUPU',member).
isMember('SLO','OUPU',member).
isMember('CH','OUPU',member).
isMember('BY','OUPU',member).
isMember('LV','OUPU',member).
isMember('LT','OUPU',member).
isMember('PL','OUPU',member).
isMember('UA','OUPU',member).
isMember('R','OUPU',member).
isMember('B','OUPU',member).
isMember('L','OUPU',member).
isMember('NL','OUPU',member).
isMember('BIH','OUPU',member).
isMember('HR','OUPU',member).
isMember('BG','OUPU',member).
isMember('RO','OUPU',member).
isMember('TR','OUPU',member).
isMember('DK','OUPU',member).
isMember('EW','OUPU',member).
isMember('SF','OUPU',member).
isMember('N','OUPU',member).
isMember('S','OUPU',member).
isMember('MC','OUPU',member).
isMember('V','OUPU',member).
isMember('IS','OUPU',member).
isMember('IRL','OUPU',member).
isMember('RSM','OUPU',member).
isMember('M','OUPU',member).
isMember('MD','OUPU',member).
isMember('P','OUPU',member).
isMember('GB','OUPU',member).
isMember('AFG','OUPU',member).
isMember('CN','OUPU',member).
isMember('IR','OUPU',member).
isMember('PK','OUPU',member).
isMember('TAD','OUPU',member).
isMember('TM','OUPU',member).
isMember('UZB','OUPU',member).
isMember('ARM','OUPU',member).
isMember('GE','OUPU',member).
isMember('AZ','OUPU',member).
isMember('BRN','OUPU',member).
isMember('BD','OUPU',member).
isMember('MYA','OUPU',member).
isMember('IND','OUPU',member).
isMember('BHT','OUPU',member).
isMember('BRU','OUPU',member).
isMember('MAL','OUPU',member).
isMember('LAO','OUPU',member).
isMember('THA','OUPU',member).
isMember('K','OUPU',member).
isMember('VN','OUPU',member).
isMember('KAZ','OUPU',member).
isMember('NOK','OUPU',member).
isMember('KGZ','OUPU',member).
isMember('HONX','OUPU',member).
isMember('MACX','OUPU',member).
isMember('MNG','OUPU',member).
isMember('NEP','OUPU',member).
isMember('CY','OUPU',member).
isMember('IL','OUPU',member).
isMember('ET','OUPU',member).
isMember('RI','OUPU',member).
isMember('TL','OUPU',member).
isMember('PNG','OUPU',member).
isMember('IRQ','OUPU',member).
isMember('JOR','OUPU',member).
isMember('KWT','OUPU',member).
isMember('SA','OUPU',member).
isMember('SYR','OUPU',member).
isMember('RL','OUPU',member).
isMember('J','OUPU',member).
isMember('ROK','OUPU',member).
isMember('MV','OUPU',member).
isMember('OM','OUPU',member).
isMember('UAE','OUPU',member).
isMember('YE','OUPU',member).
isMember('RP','OUPU',member).
isMember('Q','OUPU',member).
isMember('SGP','OUPU',member).
isMember('CL','OUPU',member).
isMember('AXA','OUPU',member).
isMember('AG','OUPU',member).
isMember('ARU','OUPU',member).
isMember('BS','OUPU',member).
isMember('BDS','OUPU',member).
isMember('BZ','OUPU',member).
isMember('GCA','OUPU',member).
isMember('MEX','OUPU',member).
isMember('BERM','OUPU',member).
isMember('BVIR','OUPU',member).
isMember('CDN','OUPU',member).
isMember('USA','OUPU',member).
isMember('CAYM','OUPU',member).
isMember('CR','OUPU',member).
isMember('NIC','OUPU',member).
isMember('PA','OUPU',member).
isMember('C','OUPU',member).
isMember('WD','OUPU',member).
isMember('DOM','OUPU',member).
isMember('RH','OUPU',member).
isMember('ES','OUPU',member).
isMember('HCA','OUPU',member).
isMember('WG','OUPU',member).
isMember('JA','OUPU',member).
isMember('MNTS','OUPU',member).
isMember('NA','OUPU',member).
isMember('SMAR','OUPU',member).
isMember('CO','OUPU',member).
isMember('PR','OUPU',member).
isMember('KN','OUPU',member).
isMember('WL','OUPU',member).
isMember('WV','OUPU',member).
isMember('TT','OUPU',member).
isMember('TUCA','OUPU',member).
isMember('AMSA','OUPU',member).
isMember('AUS','OUPU',member).
isMember('COOK','OUPU',member).
isMember('FJI','OUPU',member).
isMember('GUAM','OUPU',member).
isMember('KIR','OUPU',member).
isMember('NAU','OUPU',member).
isMember('NCA','OUPU',member).
isMember('NZ','OUPU',member).
isMember('NIUE','OUPU',member).
isMember('PITC','OUPU',member).
isMember('SLB','OUPU',member).
isMember('TO','OUPU',member).
isMember('TUV','OUPU',member).
isMember('VU','OUPU',member).
isMember('WAFU','OUPU',member).
isMember('WS','OUPU',member).
isMember('RA','OUPU',member).
isMember('BOL','OUPU',member).
isMember('BR','OUPU',member).
isMember('RCH','OUPU',member).
isMember('PY','OUPU',member).
isMember('ROU','OUPU',member).
isMember('PE','OUPU',member).
isMember('GUY','OUPU',member).
isMember('SME','OUPU',member).
isMember('YV','OUPU',member).
isMember('EC','OUPU',member).
isMember('FALK','OUPU',member).
isMember('DZ','OUPU',member).
isMember('LAR','OUPU',member).
isMember('RMM','OUPU',member).
isMember('RIM','OUPU',member).
isMember('MA','OUPU',member).
isMember('RN','OUPU',member).
isMember('TN','OUPU',member).
isMember('ANG','OUPU',member).
isMember('RCB','OUPU',member).
isMember('NAM','OUPU',member).
isMember('ZRE','OUPU',member).
isMember('Z','OUPU',member).
isMember('BEN','OUPU',member).
isMember('BF','OUPU',member).
isMember('WAN','OUPU',member).
isMember('RT','OUPU',member).
isMember('RB','OUPU',member).
isMember('RSA','OUPU',member).
isMember('ZW','OUPU',member).
isMember('CI','OUPU',member).
isMember('GH','OUPU',member).
isMember('BI','OUPU',member).
isMember('RWA','OUPU',member).
isMember('EAT','OUPU',member).
isMember('CAM','OUPU',member).
isMember('RCA','OUPU',member).
isMember('TCH','OUPU',member).
isMember('GQ','OUPU',member).
isMember('G','OUPU',member).
isMember('CV','OUPU',member).
isMember('SUD','OUPU',member).
isMember('SSD','OUPU',member).
isMember('COM','OUPU',member).
isMember('RG','OUPU',member).
isMember('LB','OUPU',member).
isMember('DJI','OUPU',member).
isMember('ER','OUPU',member).
isMember('ETH','OUPU',member).
isMember('SP','OUPU',member).
isMember('EAK','OUPU',member).
isMember('WAG','OUPU',member).
isMember('SN','OUPU',member).
isMember('GNB','OUPU',member).
isMember('WAL','OUPU',member).
isMember('EAU','OUPU',member).
isMember('LS','OUPU',member).
isMember('RM','OUPU',member).
isMember('MW','OUPU',member).
isMember('MOC','OUPU',member).
isMember('MS','OUPU',member).
isMember('MAYO','OUPU',member).
isMember('SD','OUPU',member).
isMember('HELX','OUPU',member).
isMember('STP','OUPU',member).
isMember('SY','OUPU',member).
isMember('RMM','OWADB',member).
isMember('RN','OWADB',member).
isMember('BEN','OWADB',member).
isMember('BF','OWADB',member).
isMember('RT','OWADB',member).
isMember('CI','OWADB',member).
isMember('SN','OWADB',member).
isMember('GR','OWEU',member).
isMember('F','OWEU',member).
isMember('E','OWEU',member).
isMember('D','OWEU',member).
isMember('I','OWEU',member).
isMember('B','OWEU',member).
isMember('L','OWEU',member).
isMember('NL','OWEU',member).
isMember('P','OWEU',member).
isMember('GB','OWEU',member).
isMember('TR','OWEU','associate member').
isMember('N','OWEU','associate member').
isMember('IS','OWEU','associate member').
isMember('A','OWEU',observer).
isMember('DK','OWEU',observer).
isMember('SF','OWEU',observer).
isMember('S','OWEU',observer).
isMember('IRL','OWEU',observer).
isMember('CZ','OWEU','associate partner').
isMember('H','OWEU','associate partner').
isMember('SK','OWEU','associate partner').
isMember('LV','OWEU','associate partner').
isMember('LT','OWEU','associate partner').
isMember('PL','OWEU','associate partner').
isMember('BG','OWEU','associate partner').
isMember('RO','OWEU','associate partner').
isMember('EW','OWEU','associate partner').
isMember('SRB','OWCL',member).
isMember('F','OWCL',member).
isMember('E','OWCL',member).
isMember('A','OWCL',member).
isMember('I','OWCL',member).
isMember('FL','OWCL',member).
isMember('CH','OWCL',member).
isMember('PL','OWCL',member).
isMember('B','OWCL',member).
isMember('L','OWCL',member).
isMember('NL','OWCL',member).
isMember('RO','OWCL',member).
isMember('M','OWCL',member).
isMember('P','OWCL',member).
isMember('GB','OWCL',member).
isMember('IR','OWCL',member).
isMember('PK','OWCL',member).
isMember('BD','OWCL',member).
isMember('MAL','OWCL',member).
isMember('THA','OWCL',member).
isMember('VN','OWCL',member).
isMember('HONX','OWCL',member).
isMember('CY','OWCL',member).
isMember('RI','OWCL',member).
isMember('RP','OWCL',member).
isMember('CL','OWCL',member).
isMember('RC','OWCL',member).
isMember('AG','OWCL',member).
isMember('ARU','OWCL',member).
isMember('BZ','OWCL',member).
isMember('GCA','OWCL',member).
isMember('MEX','OWCL',member).
isMember('CDN','OWCL',member).
isMember('USA','OWCL',member).
isMember('CR','OWCL',member).
isMember('NIC','OWCL',member).
isMember('PA','OWCL',member).
isMember('C','OWCL',member).
isMember('WD','OWCL',member).
isMember('DOM','OWCL',member).
isMember('RH','OWCL',member).
isMember('ES','OWCL',member).
isMember('HCA','OWCL',member).
isMember('WG','OWCL',member).
isMember('GUAD','OWCL',member).
isMember('JA','OWCL',member).
isMember('MART','OWCL',member).
isMember('NA','OWCL',member).
isMember('CO','OWCL',member).
isMember('PR','OWCL',member).
isMember('KN','OWCL',member).
isMember('WL','OWCL',member).
isMember('WV','OWCL',member).
isMember('RA','OWCL',member).
isMember('BOL','OWCL',member).
isMember('BR','OWCL',member).
isMember('RCH','OWCL',member).
isMember('PY','OWCL',member).
isMember('ROU','OWCL',member).
isMember('PE','OWCL',member).
isMember('FGU','OWCL',member).
isMember('GUY','OWCL',member).
isMember('SME','OWCL',member).
isMember('YV','OWCL',member).
isMember('EC','OWCL',member).
isMember('DZ','OWCL',member).
isMember('RMM','OWCL',member).
isMember('RN','OWCL',member).
isMember('ANG','OWCL',member).
isMember('NAM','OWCL',member).
isMember('ZRE','OWCL',member).
isMember('Z','OWCL',member).
isMember('BEN','OWCL',member).
isMember('BF','OWCL',member).
isMember('WAN','OWCL',member).
isMember('RT','OWCL',member).
isMember('RB','OWCL',member).
isMember('ZW','OWCL',member).
isMember('CI','OWCL',member).
isMember('GH','OWCL',member).
isMember('RWA','OWCL',member).
isMember('EAT','OWCL',member).
isMember('CAM','OWCL',member).
isMember('RCA','OWCL',member).
isMember('TCH','OWCL',member).
isMember('G','OWCL',member).
isMember('CV','OWCL',member).
isMember('RG','OWCL',member).
isMember('LB','OWCL',member).
isMember('EAK','OWCL',member).
isMember('WAG','OWCL',member).
isMember('SN','OWCL',member).
isMember('WAL','OWCL',member).
isMember('LS','OWCL',member).
isMember('RM','OWCL',member).
isMember('MS','OWCL',member).
isMember('SY','OWCL',member).
isMember('AL','OWFTU',member).
isMember('GR','OWFTU',member).
isMember('SRB','OWFTU',member).
isMember('MNE','OWFTU',member).
isMember('F','OWFTU',member).
isMember('E','OWFTU',member).
isMember('A','OWFTU',member).
isMember('CZ','OWFTU',member).
isMember('H','OWFTU',member).
isMember('PL','OWFTU',member).
isMember('BG','OWFTU',member).
isMember('RO','OWFTU',member).
isMember('TR','OWFTU',member).
isMember('DK','OWFTU',member).
isMember('SF','OWFTU',member).
isMember('S','OWFTU',member).
isMember('P','OWFTU',member).
isMember('AFG','OWFTU',member).
isMember('IR','OWFTU',member).
isMember('PK','OWFTU',member).
isMember('BRN','OWFTU',member).
isMember('BD','OWFTU',member).
isMember('IND','OWFTU',member).
isMember('MAL','OWFTU',member).
isMember('LAO','OWFTU',member).
isMember('THA','OWFTU',member).
isMember('K','OWFTU',member).
isMember('VN','OWFTU',member).
isMember('NOK','OWFTU',member).
isMember('HONX','OWFTU',member).
isMember('MACX','OWFTU',member).
isMember('MNG','OWFTU',member).
isMember('NEP','OWFTU',member).
isMember('CY','OWFTU',member).
isMember('ET','OWFTU',member).
isMember('RI','OWFTU',member).
isMember('TL','OWFTU',member).
isMember('PNG','OWFTU',member).
isMember('IRQ','OWFTU',member).
isMember('JOR','OWFTU',member).
isMember('KWT','OWFTU',member).
isMember('SA','OWFTU',member).
isMember('SYR','OWFTU',member).
isMember('RL','OWFTU',member).
isMember('J','OWFTU',member).
isMember('OM','OWFTU',member).
isMember('YE','OWFTU',member).
isMember('RP','OWFTU',member).
isMember('CL','OWFTU',member).
isMember('AXA','OWFTU',member).
isMember('AG','OWFTU',member).
isMember('ARU','OWFTU',member).
isMember('GCA','OWFTU',member).
isMember('MEX','OWFTU',member).
isMember('BERM','OWFTU',member).
isMember('BVIR','OWFTU',member).
isMember('CDN','OWFTU',member).
isMember('CAYM','OWFTU',member).
isMember('CR','OWFTU',member).
isMember('NIC','OWFTU',member).
isMember('PA','OWFTU',member).
isMember('C','OWFTU',member).
isMember('DOM','OWFTU',member).
isMember('RH','OWFTU',member).
isMember('ES','OWFTU',member).
isMember('HCA','OWFTU',member).
isMember('GUAD','OWFTU',member).
isMember('JA','OWFTU',member).
isMember('MART','OWFTU',member).
isMember('MNTS','OWFTU',member).
isMember('SMAR','OWFTU',member).
isMember('CO','OWFTU',member).
isMember('PR','OWFTU',member).
isMember('WL','OWFTU',member).
isMember('SPMI','OWFTU',member).
isMember('WV','OWFTU',member).
isMember('TT','OWFTU',member).
isMember('AUS','OWFTU',member).
isMember('FJI','OWFTU',member).
isMember('NCA','OWFTU',member).
isMember('NZ','OWFTU',member).
isMember('SLB','OWFTU',member).
isMember('VU','OWFTU',member).
isMember('WAFU','OWFTU',member).
isMember('RA','OWFTU',member).
isMember('BOL','OWFTU',member).
isMember('BR','OWFTU',member).
isMember('RCH','OWFTU',member).
isMember('ROU','OWFTU',member).
isMember('PE','OWFTU',member).
isMember('FGU','OWFTU',member).
isMember('GUY','OWFTU',member).
isMember('YV','OWFTU',member).
isMember('EC','OWFTU',member).
isMember('LAR','OWFTU',member).
isMember('RMM','OWFTU',member).
isMember('RN','OWFTU',member).
isMember('TN','OWFTU',member).
isMember('WSA','OWFTU',member).
isMember('ANG','OWFTU',member).
isMember('RCB','OWFTU',member).
isMember('ZRE','OWFTU',member).
isMember('BEN','OWFTU',member).
isMember('BF','OWFTU',member).
isMember('WAN','OWFTU',member).
isMember('RT','OWFTU',member).
isMember('RB','OWFTU',member).
isMember('RSA','OWFTU',member).
isMember('ZW','OWFTU',member).
isMember('CI','OWFTU',member).
isMember('GH','OWFTU',member).
isMember('EAT','OWFTU',member).
isMember('CAM','OWFTU',member).
isMember('SUD','OWFTU',member).
isMember('RG','OWFTU',member).
isMember('LB','OWFTU',member).
isMember('DJI','OWFTU',member).
isMember('ER','OWFTU',member).
isMember('ETH','OWFTU',member).
isMember('SP','OWFTU',member).
isMember('WAG','OWFTU',member).
isMember('SN','OWFTU',member).
isMember('GNB','OWFTU',member).
isMember('WAL','OWFTU',member).
isMember('EAU','OWFTU',member).
isMember('LS','OWFTU',member).
isMember('RM','OWFTU',member).
isMember('MW','OWFTU',member).
isMember('MOC','OWFTU',member).
isMember('MS','OWFTU',member).
isMember('MAYO','OWFTU',member).
isMember('REUN','OWFTU',member).
isMember('HELX','OWFTU',member).
isMember('AL','OWHO',member).
isMember('GR','OWHO',member).
isMember('MK','OWHO',member).
isMember('SRB','OWHO',member).
isMember('MNE','OWHO',member).
isMember('F','OWHO',member).
isMember('E','OWHO',member).
isMember('A','OWHO',member).
isMember('CZ','OWHO',member).
isMember('D','OWHO',member).
isMember('H','OWHO',member).
isMember('I','OWHO',member).
isMember('SK','OWHO',member).
isMember('SLO','OWHO',member).
isMember('CH','OWHO',member).
isMember('BY','OWHO',member).
isMember('LV','OWHO',member).
isMember('LT','OWHO',member).
isMember('PL','OWHO',member).
isMember('UA','OWHO',member).
isMember('R','OWHO',member).
isMember('B','OWHO',member).
isMember('L','OWHO',member).
isMember('NL','OWHO',member).
isMember('BIH','OWHO',member).
isMember('HR','OWHO',member).
isMember('BG','OWHO',member).
isMember('RO','OWHO',member).
isMember('TR','OWHO',member).
isMember('DK','OWHO',member).
isMember('EW','OWHO',member).
isMember('SF','OWHO',member).
isMember('N','OWHO',member).
isMember('S','OWHO',member).
isMember('MC','OWHO',member).
isMember('IS','OWHO',member).
isMember('IRL','OWHO',member).
isMember('RSM','OWHO',member).
isMember('M','OWHO',member).
isMember('MD','OWHO',member).
isMember('P','OWHO',member).
isMember('GB','OWHO',member).
isMember('AFG','OWHO',member).
isMember('CN','OWHO',member).
isMember('IR','OWHO',member).
isMember('PK','OWHO',member).
isMember('TAD','OWHO',member).
isMember('TM','OWHO',member).
isMember('UZB','OWHO',member).
isMember('ARM','OWHO',member).
isMember('GE','OWHO',member).
isMember('AZ','OWHO',member).
isMember('BRN','OWHO',member).
isMember('BD','OWHO',member).
isMember('MYA','OWHO',member).
isMember('IND','OWHO',member).
isMember('BHT','OWHO',member).
isMember('BRU','OWHO',member).
isMember('MAL','OWHO',member).
isMember('LAO','OWHO',member).
isMember('THA','OWHO',member).
isMember('K','OWHO',member).
isMember('VN','OWHO',member).
isMember('KAZ','OWHO',member).
isMember('NOK','OWHO',member).
isMember('KGZ','OWHO',member).
isMember('MNG','OWHO',member).
isMember('NEP','OWHO',member).
isMember('CY','OWHO',member).
isMember('IL','OWHO',member).
isMember('ET','OWHO',member).
isMember('RI','OWHO',member).
isMember('TL','OWHO',member).
isMember('PNG','OWHO',member).
isMember('IRQ','OWHO',member).
isMember('JOR','OWHO',member).
isMember('KWT','OWHO',member).
isMember('SA','OWHO',member).
isMember('SYR','OWHO',member).
isMember('RL','OWHO',member).
isMember('J','OWHO',member).
isMember('ROK','OWHO',member).
isMember('MV','OWHO',member).
isMember('OM','OWHO',member).
isMember('UAE','OWHO',member).
isMember('YE','OWHO',member).
isMember('RP','OWHO',member).
isMember('Q','OWHO',member).
isMember('SGP','OWHO',member).
isMember('CL','OWHO',member).
isMember('AG','OWHO',member).
isMember('BS','OWHO',member).
isMember('BDS','OWHO',member).
isMember('BZ','OWHO',member).
isMember('GCA','OWHO',member).
isMember('MEX','OWHO',member).
isMember('CDN','OWHO',member).
isMember('USA','OWHO',member).
isMember('CR','OWHO',member).
isMember('NIC','OWHO',member).
isMember('PA','OWHO',member).
isMember('C','OWHO',member).
isMember('WD','OWHO',member).
isMember('DOM','OWHO',member).
isMember('RH','OWHO',member).
isMember('ES','OWHO',member).
isMember('HCA','OWHO',member).
isMember('WG','OWHO',member).
isMember('JA','OWHO',member).
isMember('CO','OWHO',member).
isMember('KN','OWHO',member).
isMember('WL','OWHO',member).
isMember('WV','OWHO',member).
isMember('TT','OWHO',member).
isMember('AUS','OWHO',member).
isMember('COOK','OWHO',member).
isMember('FJI','OWHO',member).
isMember('KIR','OWHO',member).
isMember('MH','OWHO',member).
isMember('NAU','OWHO',member).
isMember('NZ','OWHO',member).
isMember('NIUE','OWHO',member).
isMember('PAL','OWHO',member).
isMember('SLB','OWHO',member).
isMember('TO','OWHO',member).
isMember('TUV','OWHO',member).
isMember('VU','OWHO',member).
isMember('WS','OWHO',member).
isMember('RA','OWHO',member).
isMember('BOL','OWHO',member).
isMember('BR','OWHO',member).
isMember('RCH','OWHO',member).
isMember('PY','OWHO',member).
isMember('ROU','OWHO',member).
isMember('PE','OWHO',member).
isMember('GUY','OWHO',member).
isMember('SME','OWHO',member).
isMember('YV','OWHO',member).
isMember('EC','OWHO',member).
isMember('DZ','OWHO',member).
isMember('LAR','OWHO',member).
isMember('RMM','OWHO',member).
isMember('RIM','OWHO',member).
isMember('MA','OWHO',member).
isMember('RN','OWHO',member).
isMember('TN','OWHO',member).
isMember('ANG','OWHO',member).
isMember('RCB','OWHO',member).
isMember('NAM','OWHO',member).
isMember('ZRE','OWHO',member).
isMember('Z','OWHO',member).
isMember('BEN','OWHO',member).
isMember('BF','OWHO',member).
isMember('WAN','OWHO',member).
isMember('RT','OWHO',member).
isMember('RB','OWHO',member).
isMember('RSA','OWHO',member).
isMember('ZW','OWHO',member).
isMember('CI','OWHO',member).
isMember('GH','OWHO',member).
isMember('BI','OWHO',member).
isMember('RWA','OWHO',member).
isMember('EAT','OWHO',member).
isMember('CAM','OWHO',member).
isMember('RCA','OWHO',member).
isMember('TCH','OWHO',member).
isMember('GQ','OWHO',member).
isMember('G','OWHO',member).
isMember('CV','OWHO',member).
isMember('SUD','OWHO',member).
isMember('COM','OWHO',member).
isMember('RG','OWHO',member).
isMember('LB','OWHO',member).
isMember('DJI','OWHO',member).
isMember('ER','OWHO',member).
isMember('ETH','OWHO',member).
isMember('SP','OWHO',member).
isMember('EAK','OWHO',member).
isMember('WAG','OWHO',member).
isMember('SN','OWHO',member).
isMember('GNB','OWHO',member).
isMember('WAL','OWHO',member).
isMember('EAU','OWHO',member).
isMember('LS','OWHO',member).
isMember('RM','OWHO',member).
isMember('MW','OWHO',member).
isMember('MOC','OWHO',member).
isMember('MS','OWHO',member).
isMember('SD','OWHO',member).
isMember('STP','OWHO',member).
isMember('SY','OWHO',member).
isMember('PR','OWHO','associate member').
isMember('AL','OWIPO',member).
isMember('GR','OWIPO',member).
isMember('MK','OWIPO',member).
isMember('SRB','OWIPO',member).
isMember('MNE','OWIPO',member).
isMember('AND','OWIPO',member).
isMember('F','OWIPO',member).
isMember('E','OWIPO',member).
isMember('A','OWIPO',member).
isMember('CZ','OWIPO',member).
isMember('D','OWIPO',member).
isMember('H','OWIPO',member).
isMember('I','OWIPO',member).
isMember('FL','OWIPO',member).
isMember('SK','OWIPO',member).
isMember('SLO','OWIPO',member).
isMember('CH','OWIPO',member).
isMember('BY','OWIPO',member).
isMember('LV','OWIPO',member).
isMember('LT','OWIPO',member).
isMember('PL','OWIPO',member).
isMember('UA','OWIPO',member).
isMember('R','OWIPO',member).
isMember('B','OWIPO',member).
isMember('L','OWIPO',member).
isMember('NL','OWIPO',member).
isMember('BIH','OWIPO',member).
isMember('HR','OWIPO',member).
isMember('BG','OWIPO',member).
isMember('RO','OWIPO',member).
isMember('TR','OWIPO',member).
isMember('DK','OWIPO',member).
isMember('EW','OWIPO',member).
isMember('SF','OWIPO',member).
isMember('N','OWIPO',member).
isMember('S','OWIPO',member).
isMember('MC','OWIPO',member).
isMember('V','OWIPO',member).
isMember('IS','OWIPO',member).
isMember('IRL','OWIPO',member).
isMember('RSM','OWIPO',member).
isMember('M','OWIPO',member).
isMember('MD','OWIPO',member).
isMember('P','OWIPO',member).
isMember('GB','OWIPO',member).
isMember('CN','OWIPO',member).
isMember('IR','OWIPO',member).
isMember('PK','OWIPO',member).
isMember('TAD','OWIPO',member).
isMember('UZB','OWIPO',member).
isMember('ARM','OWIPO',member).
isMember('GE','OWIPO',member).
isMember('AZ','OWIPO',member).
isMember('BRN','OWIPO',member).
isMember('BD','OWIPO',member).
isMember('IND','OWIPO',member).
isMember('BHT','OWIPO',member).
isMember('BRU','OWIPO',member).
isMember('MAL','OWIPO',member).
isMember('LAO','OWIPO',member).
isMember('THA','OWIPO',member).
isMember('K','OWIPO',member).
isMember('VN','OWIPO',member).
isMember('KAZ','OWIPO',member).
isMember('NOK','OWIPO',member).
isMember('KGZ','OWIPO',member).
isMember('MNG','OWIPO',member).
isMember('CY','OWIPO',member).
isMember('IL','OWIPO',member).
isMember('ET','OWIPO',member).
isMember('RI','OWIPO',member).
isMember('IRQ','OWIPO',member).
isMember('JOR','OWIPO',member).
isMember('SA','OWIPO',member).
isMember('RL','OWIPO',member).
isMember('J','OWIPO',member).
isMember('ROK','OWIPO',member).
isMember('UAE','OWIPO',member).
isMember('YE','OWIPO',member).
isMember('RP','OWIPO',member).
isMember('Q','OWIPO',member).
isMember('SGP','OWIPO',member).
isMember('CL','OWIPO',member).
isMember('BS','OWIPO',member).
isMember('BDS','OWIPO',member).
isMember('GCA','OWIPO',member).
isMember('MEX','OWIPO',member).
isMember('CDN','OWIPO',member).
isMember('USA','OWIPO',member).
isMember('CR','OWIPO',member).
isMember('NIC','OWIPO',member).
isMember('PA','OWIPO',member).
isMember('C','OWIPO',member).
isMember('DOM','OWIPO',member).
isMember('RH','OWIPO',member).
isMember('ES','OWIPO',member).
isMember('HCA','OWIPO',member).
isMember('JA','OWIPO',member).
isMember('CO','OWIPO',member).
isMember('KN','OWIPO',member).
isMember('WL','OWIPO',member).
isMember('TT','OWIPO',member).
isMember('AUS','OWIPO',member).
isMember('FJI','OWIPO',member).
isMember('NZ','OWIPO',member).
isMember('RA','OWIPO',member).
isMember('BOL','OWIPO',member).
isMember('BR','OWIPO',member).
isMember('RCH','OWIPO',member).
isMember('PY','OWIPO',member).
isMember('ROU','OWIPO',member).
isMember('PE','OWIPO',member).
isMember('GUY','OWIPO',member).
isMember('SME','OWIPO',member).
isMember('YV','OWIPO',member).
isMember('EC','OWIPO',member).
isMember('DZ','OWIPO',member).
isMember('LAR','OWIPO',member).
isMember('RMM','OWIPO',member).
isMember('RIM','OWIPO',member).
isMember('MA','OWIPO',member).
isMember('RN','OWIPO',member).
isMember('TN','OWIPO',member).
isMember('ANG','OWIPO',member).
isMember('RCB','OWIPO',member).
isMember('NAM','OWIPO',member).
isMember('ZRE','OWIPO',member).
isMember('Z','OWIPO',member).
isMember('BEN','OWIPO',member).
isMember('BF','OWIPO',member).
isMember('WAN','OWIPO',member).
isMember('RT','OWIPO',member).
isMember('RSA','OWIPO',member).
isMember('ZW','OWIPO',member).
isMember('CI','OWIPO',member).
isMember('GH','OWIPO',member).
isMember('BI','OWIPO',member).
isMember('RWA','OWIPO',member).
isMember('EAT','OWIPO',member).
isMember('CAM','OWIPO',member).
isMember('RCA','OWIPO',member).
isMember('TCH','OWIPO',member).
isMember('G','OWIPO',member).
isMember('SUD','OWIPO',member).
isMember('RG','OWIPO',member).
isMember('LB','OWIPO',member).
isMember('SP','OWIPO',member).
isMember('EAK','OWIPO',member).
isMember('WAG','OWIPO',member).
isMember('SN','OWIPO',member).
isMember('GNB','OWIPO',member).
isMember('WAL','OWIPO',member).
isMember('EAU','OWIPO',member).
isMember('LS','OWIPO',member).
isMember('RM','OWIPO',member).
isMember('MW','OWIPO',member).
isMember('MS','OWIPO',member).
isMember('SD','OWIPO',member).
isMember('AL','OWMO',member).
isMember('GR','OWMO',member).
isMember('MK','OWMO',member).
isMember('SRB','OWMO',member).
isMember('MNE','OWMO',member).
isMember('F','OWMO',member).
isMember('E','OWMO',member).
isMember('A','OWMO',member).
isMember('CZ','OWMO',member).
isMember('D','OWMO',member).
isMember('H','OWMO',member).
isMember('I','OWMO',member).
isMember('SK','OWMO',member).
isMember('SLO','OWMO',member).
isMember('CH','OWMO',member).
isMember('BY','OWMO',member).
isMember('LV','OWMO',member).
isMember('LT','OWMO',member).
isMember('PL','OWMO',member).
isMember('UA','OWMO',member).
isMember('R','OWMO',member).
isMember('B','OWMO',member).
isMember('L','OWMO',member).
isMember('NL','OWMO',member).
isMember('BIH','OWMO',member).
isMember('HR','OWMO',member).
isMember('BG','OWMO',member).
isMember('RO','OWMO',member).
isMember('TR','OWMO',member).
isMember('DK','OWMO',member).
isMember('EW','OWMO',member).
isMember('SF','OWMO',member).
isMember('N','OWMO',member).
isMember('S','OWMO',member).
isMember('IS','OWMO',member).
isMember('IRL','OWMO',member).
isMember('M','OWMO',member).
isMember('MD','OWMO',member).
isMember('P','OWMO',member).
isMember('GB','OWMO',member).
isMember('AFG','OWMO',member).
isMember('CN','OWMO',member).
isMember('IR','OWMO',member).
isMember('PK','OWMO',member).
isMember('TAD','OWMO',member).
isMember('TM','OWMO',member).
isMember('UZB','OWMO',member).
isMember('ARM','OWMO',member).
isMember('GE','OWMO',member).
isMember('AZ','OWMO',member).
isMember('BRN','OWMO',member).
isMember('BD','OWMO',member).
isMember('MYA','OWMO',member).
isMember('IND','OWMO',member).
isMember('BRU','OWMO',member).
isMember('MAL','OWMO',member).
isMember('LAO','OWMO',member).
isMember('THA','OWMO',member).
isMember('K','OWMO',member).
isMember('VN','OWMO',member).
isMember('KAZ','OWMO',member).
isMember('NOK','OWMO',member).
isMember('KGZ','OWMO',member).
isMember('HONX','OWMO',member).
isMember('MACX','OWMO',member).
isMember('MNG','OWMO',member).
isMember('NEP','OWMO',member).
isMember('COCO','OWMO',member).
isMember('CY','OWMO',member).
isMember('IL','OWMO',member).
isMember('ET','OWMO',member).
isMember('RI','OWMO',member).
isMember('PNG','OWMO',member).
isMember('IRQ','OWMO',member).
isMember('JOR','OWMO',member).
isMember('KWT','OWMO',member).
isMember('SA','OWMO',member).
isMember('SYR','OWMO',member).
isMember('RL','OWMO',member).
isMember('J','OWMO',member).
isMember('ROK','OWMO',member).
isMember('MV','OWMO',member).
isMember('OM','OWMO',member).
isMember('UAE','OWMO',member).
isMember('YE','OWMO',member).
isMember('RP','OWMO',member).
isMember('Q','OWMO',member).
isMember('SGP','OWMO',member).
isMember('CL','OWMO',member).
isMember('AG','OWMO',member).
isMember('ARU','OWMO',member).
isMember('BS','OWMO',member).
isMember('BDS','OWMO',member).
isMember('BZ','OWMO',member).
isMember('GCA','OWMO',member).
isMember('MEX','OWMO',member).
isMember('CDN','OWMO',member).
isMember('USA','OWMO',member).
isMember('CR','OWMO',member).
isMember('NIC','OWMO',member).
isMember('PA','OWMO',member).
isMember('C','OWMO',member).
isMember('WD','OWMO',member).
isMember('DOM','OWMO',member).
isMember('RH','OWMO',member).
isMember('ES','OWMO',member).
isMember('HCA','OWMO',member).
isMember('JA','OWMO',member).
isMember('NA','OWMO',member).
isMember('CO','OWMO',member).
isMember('WL','OWMO',member).
isMember('TT','OWMO',member).
isMember('AUS','OWMO',member).
isMember('COOK','OWMO',member).
isMember('FJI','OWMO',member).
isMember('FPOL','OWMO',member).
isMember('NCA','OWMO',member).
isMember('NZ','OWMO',member).
isMember('NIUE','OWMO',member).
isMember('SLB','OWMO',member).
isMember('VU','OWMO',member).
isMember('WS','OWMO',member).
isMember('RA','OWMO',member).
isMember('BOL','OWMO',member).
isMember('BR','OWMO',member).
isMember('RCH','OWMO',member).
isMember('PY','OWMO',member).
isMember('ROU','OWMO',member).
isMember('PE','OWMO',member).
isMember('GUY','OWMO',member).
isMember('SME','OWMO',member).
isMember('YV','OWMO',member).
isMember('EC','OWMO',member).
isMember('DZ','OWMO',member).
isMember('LAR','OWMO',member).
isMember('RMM','OWMO',member).
isMember('RIM','OWMO',member).
isMember('MA','OWMO',member).
isMember('RN','OWMO',member).
isMember('TN','OWMO',member).
isMember('ANG','OWMO',member).
isMember('RCB','OWMO',member).
isMember('NAM','OWMO',member).
isMember('ZRE','OWMO',member).
isMember('Z','OWMO',member).
isMember('BEN','OWMO',member).
isMember('BF','OWMO',member).
isMember('WAN','OWMO',member).
isMember('RT','OWMO',member).
isMember('RB','OWMO',member).
isMember('RSA','OWMO',member).
isMember('ZW','OWMO',member).
isMember('CI','OWMO',member).
isMember('GH','OWMO',member).
isMember('BI','OWMO',member).
isMember('RWA','OWMO',member).
isMember('EAT','OWMO',member).
isMember('CAM','OWMO',member).
isMember('RCA','OWMO',member).
isMember('TCH','OWMO',member).
isMember('G','OWMO',member).
isMember('CV','OWMO',member).
isMember('SUD','OWMO',member).
isMember('COM','OWMO',member).
isMember('RG','OWMO',member).
isMember('LB','OWMO',member).
isMember('DJI','OWMO',member).
isMember('ER','OWMO',member).
isMember('ETH','OWMO',member).
isMember('SP','OWMO',member).
isMember('EAK','OWMO',member).
isMember('WAG','OWMO',member).
isMember('SN','OWMO',member).
isMember('GNB','OWMO',member).
isMember('WAL','OWMO',member).
isMember('EAU','OWMO',member).
isMember('LS','OWMO',member).
isMember('RM','OWMO',member).
isMember('MW','OWMO',member).
isMember('MOC','OWMO',member).
isMember('MS','OWMO',member).
isMember('SD','OWMO',member).
isMember('STP','OWMO',member).
isMember('SY','OWMO',member).
isMember('AL','OWToO',member).
isMember('GR','OWToO',member).
isMember('F','OWToO',member).
isMember('E','OWToO',member).
isMember('A','OWToO',member).
isMember('CZ','OWToO',member).
isMember('D','OWToO',member).
isMember('H','OWToO',member).
isMember('I','OWToO',member).
isMember('SK','OWToO',member).
isMember('SLO','OWToO',member).
isMember('CH','OWToO',member).
isMember('PL','OWToO',member).
isMember('R','OWToO',member).
isMember('B','OWToO',member).
isMember('NL','OWToO',member).
isMember('BIH','OWToO',member).
isMember('HR','OWToO',member).
isMember('BG','OWToO',member).
isMember('RO','OWToO',member).
isMember('TR','OWToO',member).
isMember('SF','OWToO',member).
isMember('RSM','OWToO',member).
isMember('M','OWToO',member).
isMember('MD','OWToO',member).
isMember('P','OWToO',member).
isMember('AFG','OWToO',member).
isMember('CN','OWToO',member).
isMember('IR','OWToO',member).
isMember('PK','OWToO',member).
isMember('TM','OWToO',member).
isMember('UZB','OWToO',member).
isMember('GE','OWToO',member).
isMember('BD','OWToO',member).
isMember('IND','OWToO',member).
isMember('MAL','OWToO',member).
isMember('LAO','OWToO',member).
isMember('K','OWToO',member).
isMember('VN','OWToO',member).
isMember('KAZ','OWToO',member).
isMember('NOK','OWToO',member).
isMember('KGZ','OWToO',member).
isMember('MNG','OWToO',member).
isMember('NEP','OWToO',member).
isMember('CY','OWToO',member).
isMember('IL','OWToO',member).
isMember('ET','OWToO',member).
isMember('RI','OWToO',member).
isMember('IRQ','OWToO',member).
isMember('JOR','OWToO',member).
isMember('KWT','OWToO',member).
isMember('SYR','OWToO',member).
isMember('RL','OWToO',member).
isMember('J','OWToO',member).
isMember('ROK','OWToO',member).
isMember('MV','OWToO',member).
isMember('UAE','OWToO',member).
isMember('YE','OWToO',member).
isMember('CL','OWToO',member).
isMember('GCA','OWToO',member).
isMember('MEX','OWToO',member).
isMember('CDN','OWToO',member).
isMember('USA','OWToO',member).
isMember('NIC','OWToO',member).
isMember('PA','OWToO',member).
isMember('C','OWToO',member).
isMember('DOM','OWToO',member).
isMember('RH','OWToO',member).
isMember('ES','OWToO',member).
isMember('WG','OWToO',member).
isMember('JA','OWToO',member).
isMember('CO','OWToO',member).
isMember('RA','OWToO',member).
isMember('BOL','OWToO',member).
isMember('BR','OWToO',member).
isMember('RCH','OWToO',member).
isMember('PY','OWToO',member).
isMember('ROU','OWToO',member).
isMember('PE','OWToO',member).
isMember('YV','OWToO',member).
isMember('EC','OWToO',member).
isMember('DZ','OWToO',member).
isMember('LAR','OWToO',member).
isMember('RMM','OWToO',member).
isMember('RIM','OWToO',member).
isMember('MA','OWToO',member).
isMember('RN','OWToO',member).
isMember('TN','OWToO',member).
isMember('ANG','OWToO',member).
isMember('RCB','OWToO',member).
isMember('ZRE','OWToO',member).
isMember('Z','OWToO',member).
isMember('BEN','OWToO',member).
isMember('BF','OWToO',member).
isMember('WAN','OWToO',member).
isMember('RT','OWToO',member).
isMember('ZW','OWToO',member).
isMember('CI','OWToO',member).
isMember('GH','OWToO',member).
isMember('BI','OWToO',member).
isMember('RWA','OWToO',member).
isMember('EAT','OWToO',member).
isMember('CAM','OWToO',member).
isMember('TCH','OWToO',member).
isMember('G','OWToO',member).
isMember('SUD','OWToO',member).
isMember('RG','OWToO',member).
isMember('ETH','OWToO',member).
isMember('EAK','OWToO',member).
isMember('WAG','OWToO',member).
isMember('SN','OWToO',member).
isMember('GNB','OWToO',member).
isMember('WAL','OWToO',member).
isMember('EAU','OWToO',member).
isMember('LS','OWToO',member).
isMember('RM','OWToO',member).
isMember('MW','OWToO',member).
isMember('MS','OWToO',member).
isMember('STP','OWToO',member).
isMember('SY','OWToO',member).
isMember('NA','OWToO','associate member').
isMember('PR','OWToO','associate member').
isMember('V','OWToO',observer).
isMember('GR','OWTrO',member).
isMember('F','OWTrO',member).
isMember('E','OWTrO',member).
isMember('A','OWTrO',member).
isMember('CZ','OWTrO',member).
isMember('D','OWTrO',member).
isMember('H','OWTrO',member).
isMember('I','OWTrO',member).
isMember('FL','OWTrO',member).
isMember('SK','OWTrO',member).
isMember('SLO','OWTrO',member).
isMember('CH','OWTrO',member).
isMember('PL','OWTrO',member).
isMember('B','OWTrO',member).
isMember('L','OWTrO',member).
isMember('NL','OWTrO',member).
isMember('RO','OWTrO',member).
isMember('TR','OWTrO',member).
isMember('DK','OWTrO',member).
isMember('SF','OWTrO',member).
isMember('N','OWTrO',member).
isMember('S','OWTrO',member).
isMember('IS','OWTrO',member).
isMember('IRL','OWTrO',member).
isMember('M','OWTrO',member).
isMember('P','OWTrO',member).
isMember('GB','OWTrO',member).
isMember('PK','OWTrO',member).
isMember('BRN','OWTrO',member).
isMember('BD','OWTrO',member).
isMember('MYA','OWTrO',member).
isMember('IND','OWTrO',member).
isMember('BRU','OWTrO',member).
isMember('MAL','OWTrO',member).
isMember('THA','OWTrO',member).
isMember('CY','OWTrO',member).
isMember('IL','OWTrO',member).
isMember('ET','OWTrO',member).
isMember('RI','OWTrO',member).
isMember('KWT','OWTrO',member).
isMember('J','OWTrO',member).
isMember('ROK','OWTrO',member).
isMember('MV','OWTrO',member).
isMember('RP','OWTrO',member).
isMember('Q','OWTrO',member).
isMember('SGP','OWTrO',member).
isMember('CL','OWTrO',member).
isMember('AG','OWTrO',member).
isMember('BDS','OWTrO',member).
isMember('BZ','OWTrO',member).
isMember('GCA','OWTrO',member).
isMember('MEX','OWTrO',member).
isMember('CDN','OWTrO',member).
isMember('USA','OWTrO',member).
isMember('CR','OWTrO',member).
isMember('NIC','OWTrO',member).
isMember('C','OWTrO',member).
isMember('WD','OWTrO',member).
isMember('DOM','OWTrO',member).
isMember('ES','OWTrO',member).
isMember('HCA','OWTrO',member).
isMember('JA','OWTrO',member).
isMember('CO','OWTrO',member).
isMember('WL','OWTrO',member).
isMember('WV','OWTrO',member).
isMember('TT','OWTrO',member).
isMember('AUS','OWTrO',member).
isMember('FJI','OWTrO',member).
isMember('NZ','OWTrO',member).
isMember('RA','OWTrO',member).
isMember('BOL','OWTrO',member).
isMember('BR','OWTrO',member).
isMember('RCH','OWTrO',member).
isMember('PY','OWTrO',member).
isMember('ROU','OWTrO',member).
isMember('PE','OWTrO',member).
isMember('GUY','OWTrO',member).
isMember('SME','OWTrO',member).
isMember('YV','OWTrO',member).
isMember('RMM','OWTrO',member).
isMember('RIM','OWTrO',member).
isMember('MA','OWTrO',member).
isMember('TN','OWTrO',member).
isMember('NAM','OWTrO',member).
isMember('Z','OWTrO',member).
isMember('BF','OWTrO',member).
isMember('WAN','OWTrO',member).
isMember('RT','OWTrO',member).
isMember('RB','OWTrO',member).
isMember('RSA','OWTrO',member).
isMember('ZW','OWTrO',member).
isMember('CI','OWTrO',member).
isMember('GH','OWTrO',member).
isMember('BI','OWTrO',member).
isMember('EAT','OWTrO',member).
isMember('CAM','OWTrO',member).
isMember('RCA','OWTrO',member).
isMember('G','OWTrO',member).
isMember('RG','OWTrO',member).
isMember('DJI','OWTrO',member).
isMember('EAK','OWTrO',member).
isMember('SN','OWTrO',member).
isMember('GNB','OWTrO',member).
isMember('WAL','OWTrO',member).
isMember('EAU','OWTrO',member).
isMember('LS','OWTrO',member).
isMember('RM','OWTrO',member).
isMember('MW','OWTrO',member).
isMember('MOC','OWTrO',member).
isMember('MS','OWTrO',member).
isMember('SD','OWTrO',member).
isMember('MK','OWTrO',observer).
isMember('TAD','OWTrO',observer).
isMember('TM','OWTrO',observer).
isMember('AZ','OWTrO',observer).
isMember('LAO','OWTrO',observer).
isMember('KAZ','OWTrO',observer).
isMember('KGZ','OWTrO',observer).
isMember('LAR','OWTrO',observer).
isMember('SUD','OWTrO',observer).
isMember('ETH','OWTrO',observer).
isMember('SP','OWTrO',observer).
isMember('AL','OWTrO',applicant).
isMember('BY','OWTrO',applicant).
isMember('LV','OWTrO',applicant).
isMember('LT','OWTrO',applicant).
isMember('UA','OWTrO',applicant).
isMember('R','OWTrO',applicant).
isMember('HR','OWTrO',applicant).
isMember('BG','OWTrO',applicant).
isMember('EW','OWTrO',applicant).
isMember('MD','OWTrO',applicant).
isMember('CN','OWTrO',applicant).
isMember('UZB','OWTrO',applicant).
isMember('ARM','OWTrO',applicant).
isMember('K','OWTrO',applicant).
isMember('VN','OWTrO',applicant).
isMember('MNG','OWTrO',applicant).
isMember('NEP','OWTrO',applicant).
isMember('JOR','OWTrO',applicant).
isMember('SA','OWTrO',applicant).
isMember('YE','OWTrO',applicant).
isMember('RC','OWTrO',applicant).
isMember('PA','OWTrO',applicant).
isMember('EC','OWTrO',applicant).
isMember('DZ','OWTrO',applicant).
isMember('GQ','OWTrO',applicant).
isMember('CV','OWTrO',applicant).
isMember('STP','OWTrO',applicant).
isMember('SY','OWTrO',applicant).
isMember('GR','OZC',member).
isMember('F','OZC',member).
isMember('E','OZC',member).
isMember('A','OZC',member).
isMember('CZ','OZC',member).
isMember('D','OZC',member).
isMember('H','OZC',member).
isMember('I','OZC',member).
isMember('SK','OZC',member).
isMember('CH','OZC',member).
isMember('PL','OZC',member).
isMember('R','OZC',member).
isMember('B','OZC',member).
isMember('L','OZC',member).
isMember('NL','OZC',member).
isMember('BG','OZC',member).
isMember('RO','OZC',member).
isMember('DK','OZC',member).
isMember('SF','OZC',member).
isMember('N','OZC',member).
isMember('S','OZC',member).
isMember('IRL','OZC',member).
isMember('P','OZC',member).
isMember('GB','OZC',member).
isMember('J','OZC',member).
isMember('CDN','OZC',member).
isMember('USA','OZC',member).
isMember('AUS','OZC',member).
isMember('RSA','OZC',member).
