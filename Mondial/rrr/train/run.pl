%execution: yap -l run.pl
%change N with {1..5}

:-[aleph].
:-read_all(mNf).   %refers to mNf.b,.f,.n

:- set(search, rls).
:- set(rls_type, rrr).
:- set(tries, 10).

:- set(evalfn, accuracy).       %Sets the evaluation function for a search.
:- set(minacc, 0.7).            %V is an floating point number between 0 and 1 (default 0.0). Set a lower bound on the minimum accuracy of an acceptable clause.
:- set(clauselength,5).         %V is a positive integer (default 4). Sets upper bound on number of literals in an acceptable clause.
:- set(minpos, 2).              %V is a positive integer (default 1). Set a lower bound on the number of positive examples to be covered by an acceptable clause.

:- induce_cover.
:- write_rules('mN.rules').

