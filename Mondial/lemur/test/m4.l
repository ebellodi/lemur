:-set(neg_ex,given).

output(christian_religion/1).
input_cw(christian_religion/1).
input(population/3).

input(politics/5).

input(economy/6).

input(language/3).

input(ethnicGroup/3).

input(borders/3).

input(continent/2).

input(encompasses/3).

input(organization/6).

input(isMember/3).

determination(christian_religion/1, = /2).
determination(christian_religion/1, >= /2).
determination(christian_religion/1, =< /2).
determination(christian_religion/1,politics/5).
determination(christian_religion/1,economy/6).
determination(christian_religion/1,language/3).
determination(christian_religion/1,ethnicGroup/3).
determination(christian_religion/1,borders/3).
determination(christian_religion/1,continent/2).
determination(christian_religion/1,encompasses/3).
determination(christian_religion/1,organization/3).
determination(christian_religion/1,isMember/3).
determination(christian_religion/1,christian_religion/1).




modeh(*,christian_religion(+state)).

modeb(2,population(+state,-popgr,-pop)).

modeb(1,politics(+state,-date,-name,-motherstate,-type)).
modeb(1,economy(+state,-gdp,-float,-float,-float,-float)).
modeb(1,language(+state,-language,-perl)).
modeb(1,language(-state,+language,-perl)).
modeb(1,ethnicGroup(+state,-group,-perg)).
modeb(2,(+group = #group)).
modeb(1,encompasses(+state,-cont,-pere)).
modeb(1,encompasses(-state,+cont,-pere)).
modeb(1,continent(+cont,-area)).
modeb(1,(+cont = #cont)).
modeb(1,borders(+state,-state,-length)).
modeb(1,borders(-state,+state,-length)).

modeb(2,organization(+org,-name,-city,-state,-region,-date)).
modeb(2,organization(-org,-name,-city,+state,-region,-date)).
modeb(1,isMember(+state,-org,-mtype)).
modeb(1,(+mtype= #mtype)).
modeb(1,christian_religion(+state)).



banned([christian_religion(A)],[christian_religion(A1),=(A1,A)]).
banned([christian_religion(A)],[christian_religion(A)]).
banned([politics(A,B,C,D,E)],[politics(A,B,C,D,E)]).
