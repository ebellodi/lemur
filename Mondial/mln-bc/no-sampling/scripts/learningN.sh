java -cp /home/collab/Boostr/WILL.jar edu.wisc.cs.will.Boosting.Common.RunBoostedModels \
-target christian_religion \
-trees 20 \
-negPosRatio -1 \
-save -l -train ../data/trainN/ \
-mln -mlnClause \
-numMLNClause 3 \
-mlnClauseLen 3 \
1> risN.txt 2>stderrN.txt
