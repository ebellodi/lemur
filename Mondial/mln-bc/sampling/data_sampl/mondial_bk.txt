// Add this to indicate that first letter caps is constant and first letter
// lower-case is a variable.
// Use quotes(") to make sure words beginning with lower case are considered
// as constants.
// To totally reverse the syntax,(e.g. prolog format), set
// usePrologVariables:true.


useStdLogicVariables: true.
setParam: minPosCoverage=3.

mode: christian_religion(+State).
mode: country(-Country,+State,-Capital,-Province,-AreaS,-PopN).
mode: population(+State,-Popgr,-Pop).
mode: politics(+State,-Date,-Name,-Motherstate,-Type).
mode: equalTypeP(+Type,+Type).
mode: economy(+State,-Gdp,-Agric,-Serv,-Indus,-Infl).
mode: greaterGdp(+Gdp,+Gdp).
mode: lowerGdp(+Gdp,+Gdp).
mode: language(+State,-Language,-Perl).
mode: language(-State,+Language,-Perl).
mode: ethnicGroup(+State,-Group,-Perg).
mode: equalGroup(+Group,+Group).
mode: encompasses(+State,-Cont,-Pere).
mode: encompasses(-State,+Cont,-Pere).
mode: continent(+Cont,-Area).
mode: equalCont(+Cont,+Cont).
mode: borders(+State,-State,-Length).
//mode: borders(-State,+State,-Length).
mode: organization(+Org,-Name,-City,-State,-Region,-Date).
mode: organization(-Org,-Name,-City,+State,-Region,-Date).
//mode: isMember(+State,-Org,-Mtype).
//mode: equalTypeM(+Mtype,+Mtype).

greaterGdp(c1, c2) :- number(c1),number(c2),c1 >= c2.
lowerGdp(c1, c2) :-  number(c1),number(c2),c1 =< c2.
equalGroup(c1, c2) :-  c1 = c2.
equalCont(c1, c2) :-  c1 = c2.
//equalTypeM(c1, c2) :-  c1 = c2.
equalTypeP(c1, c2) :-  c1 = c2.


