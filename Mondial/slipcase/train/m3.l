output(christian_religion/1).

input_cw(christian_religion/1).

input(population/3).

input(politics/5).

input(economy/6).

input(language/3).

input(ethnicGroup/3).

input(borders/3).

input(continent/2).

input(encompasses/3).

input(organization/6).

input(isMember/3).


%LNC = ['Bahai','Buddhist','Cao Dai','Druze','Hindu','Hoa Hao','Jewish','Judaism','Mayan','Muslim','Taoist','Yezidi'], %religioni NON CRISTIANE

modeh(*,christian_religion(+state)).

modeb(2,population(+state,-popgr,-pop)).
/*modeb(*,(+popgr >= #popgr)).
modeb(*,(+popgr =< #popgr)).
modeb(*,(+pop >= #pop)).
modeb(*,(+pop =< #pop)).*/
modeb(1,politics(+state,-date,-name,-motherstate,-type)).
%modeb(2,politics(-state,-date,-name,+state,-type)).
%modeb(1,politics(-state,-date,-name,-motherstate,+type)).
%modeb(2,(+type= #type)).   slipcase dà errore
modeb(1,economy(+state,-gdp,-float,-float,-float,-float)).
%modeb(*,(+gdp >= #gdp)).
%modeb(*,(+gdp =< #gdp)).
modeb(1,language(+state,-language,-perl)).
modeb(1,language(-state,+language,-perl)).
/*modeb(*,(+perl >= #perl)). %commentati
modeb(*,(+perl =< #perl)).*/
%modeb(2,(+language = # language)).
modeb(1,ethnicGroup(+state,-group,-perg)).
%modeb(2,ethnicGroup(-state,+group,-perg)).
/*modeb(*,(+perg >= #perg)).%commentati
modeb(*,(+perg =< #perg)).*/
%modeb(2,(+group = #group)).
modeb(1,encompasses(+state,-cont,-pere)).
/*modeb(*,(+pere >= #pere)).
modeb(*,(+pere =< #pere)).*/
modeb(1,encompasses(-state,+cont,-pere)).
modeb(1,continent(+cont,-area)).
%modeb(1,(+cont = #cont)).
modeb(1,borders(+state,-state,-length)).
modeb(1,borders(-state,+state,-length)).
/*modeb(*,(+length >= #length)).
modeb(*,(+length =< #length)).*/

/*modeb(*,(+area >= #area)).
modeb(*,(+area =< #area)).*/
/*modeb(*,(+pere >= #pere)).
modeb(*,(+pere =< #pere)).*/

modeb(2,organization(+org,-name,-city,-state,-region,-date)).
modeb(2,organization(-org,-name,-city,+state,-region,-date)).
modeb(1,isMember(+state,-org,-mtype)).
%modeb(2,isMember(-state,+org,-mtype)).
%modeb(1,(+mtype= #mtype)).
modeb(1,christian_religion(+state)).


lookahead(borders(A,B,_),[christian_religion(B)]).
lookahead(borders(B,A,_),[christian_religion(B)]).
%lookahead(borders(A,B,_),[encompasses(A,_C,_P)]).
%lookahead(borders(B,A,_),[encompasses(B,_C,_P)]).
lookahead(encompasses(_,_,_),[borders(_,_,_)]).
lookahead(ethnicGroup(_,_,_),[borders(_,_,_)]).
lookahead(language(_,_,_),[borders(_,_,_)]).

%aggiunti
%lookahead(ethnicGroup(_S,G,_P),[G = _Gr]).
%lookahead(language(_S,L,_P),[L = _La]).
%lookahead(isMember(_S,O,T),[O = _Or, T = _Ty]).

banned([christian_religion(A)],[christian_religion(A1),=(A1,A)]).
banned([christian_religion(A)],[christian_religion(A)]).
banned([politics(A,B,C,D,E)],[politics(A,B,C,D,E)]).
