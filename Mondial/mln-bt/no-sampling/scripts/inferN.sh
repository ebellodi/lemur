#change N with {1..5}

java -cp ../Boostr/WILL.jar edu.wisc.cs.will.Boosting.Common.RunBoostedModels \
-aucJarPath "../Boostr" \
-target christian_religion \
-trees 20 \
-model ../data/trainN/models \
-testNegPosRatio -1 \
-save -i -test ../data/testN/ \
1>inferN.txt
