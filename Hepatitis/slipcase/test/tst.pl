/* test according to the method of Davis and Goadrich*/

:-source.
:-[slipcase].

t:-
	open('cll.pl',write,S), %AI fold 
	open('res_atoms.csv',write,S1),  
/*	open('atoms_pos.pl',write,S0),
	close(S0),
	open('atoms_neg.pl',write,S2),
	close(S2),*/
	%set(single_var,true),			
	load_models('all.kb',_),
	reconsult('hep1.l'),
	ta(f1,'hep1.rules',S,S1),
	ta(f2,'hep2.rules',S,S1),
	ta(f3,'hep3.rules',S,S1),
	ta(f4,'hep4.rules',S,S1),
	ta(f5,'hep5.rules',S,S1),
	close(S),
	close(S1).

ta(Model,FileOut,S,S1):-
	find_ga(Model,LG,Pos,Neg),
	set(compiling,on),
	load(FileOut,Th1,R1),	%FILE RULES
	set(compiling,off),
	assert_all(Th1),  
        assert_all(R1),
	compute_CLL_atoms(LG,0,0,CLL1,LG1),
	format("compute_CLL_atoms post~n",[]),
        retract_all(Th1),
	retract_all(R1),
	keysort(LG1,LGOrd1),
	format(S,"cll(~a,post,~d,~d,[",[Model,Pos,Neg]),
	writes(LGOrd1,S),
	format(S1,"~a;~f~n",[Model,CLL1]).



writes([H-H1],S):-
	format(S,"~f - (~p)]).~n~n",[H,H1]).

writes([H-H1|T],S):-
	format(S,"~f - (~p),~n",[H,H1]),
	writes(T,S).

find_ga(M,LG,Pos,Neg):-                     
   findall(type(M,A,R),type(M,A,R),LP),
   findall(\+ type(M,A,R),neg(type(M,A,R)),LN),
   length(LP,Pos),
   length(LN,Neg),
   append([LP,LN],LG),
   length(LG,_Le).
%   format("lungh. LG=~d~n",[Le]).

write_list([],_).

write_list([H|T],S):-
	format(S,"~p~n",[H]),
	write_list(T,S).

generate_file_names(File,FileKB,FileOut,FileL,FileLPAD):-
    generate_file_name(File,".kb",FileKB),
    generate_file_name(File,".rules",FileOut),
    generate_file_name(File,".cpl",FileLPAD),
    generate_file_name(File,".l",FileL).

compute_CLL_atoms([],_N,CLL,CLL,[]):-!.


compute_CLL_atoms([\+ H|T],N,CLL0,CLL1,[PG- (\+ H)|T1]):-!,
	rule_n(NR),
        init_test(NR),
%	write((\+ H)),nl,
	get_node(H,BDD),!,
	ret_prob(BDD,PG),
	end_test,!,
		%
		PG1 is 1-PG,
			
		(PG1=:=0.0->
			CLL2 is CLL0-10
%			format("-inf~n",[])
		;
			CLL2 is CLL0+ log(PG1)
%			format("~f~n",[PG])
		),		
		N1 is N+1,
	compute_CLL_atoms(T,N1,CLL2,CLL1,T1).	

compute_CLL_atoms([H|T],N,CLL0,CLL1,[PG-H|T1]):-
       	rule_n(NR),
 	init_test(NR),
%	write(H),nl,
	get_node(H,BDD),!,
	ret_prob(BDD,PG),
	end_test,!,
		(PG=:=0.0->
			CLL2 is CLL0-10
%			format("-inf~n",[])
		;	
			CLL2 is CLL0+ log(PG)
%			format("~f~n",[PG])
		),
		N1 is N+1,
	compute_CLL_atoms(T,N1,CLL2,CLL1,T1).		

