#script for training on train_neg.txt and train_pos.txt with RDN-B
#change N with {1..5}

java -cp ../Boostr/WILL.jar edu.wisc.cs.will.Boosting.Common.RunBoostedModels \
-target type \
-trees 20 \
-save -l -train ../data_sampl/trainN/ \
-mln -mlnClause \
-numMLNClause 3 \
-mlnClauseLen 3 \
1>risN.txt 2>stderrN.txt
