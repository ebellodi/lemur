/*

EMBLEM and SLIPCASE

Copyright (c) 2011, Fabrizio Riguzzi and Elena Bellodi

*/
:-use_module(library(lists)).
:-use_module(library(random)).
:-use_module(library(system)).

:-dynamic setting/2,last_id/1, rule/5.
:- op(500,fx,#).
:- op(500,fx,'-#').

:-[revise].

setting(epsilon_em,0.0001).
setting(epsilon_em_fraction,-1.00001).
setting(eps,0.0001).
setting(eps_f,0.00001).

/* if the difference in log likelihood in two successive em iteration is smaller
than epsilon_em, then em stops */
setting(epsilon_sem,2).

/* number of random restarts of em */
setting(random_restarts_REFnumber,1).
setting(random_restarts_number,1).
setting(iterREF,-1).
setting(iter,-1).
setting(examples,atoms).
setting(group,1).
setting(d,1).  
setting(verbosity,1).
setting(logzero,log(0.000001)).
setting(initial_clauses_modeh,1).
%setting(max_iter,10).  
setting(max_var,10).     % specialize_rule
setting(max_rules,10).  % generalize_theory
%setting(beamsize,100).  
%setting(specialization,bottom).
setting(specialization,mode).
/* allowed values: mode,bottom */

/*Algorithm 2-paper Paes. et al parameters*/
setting(p1,0.5).
setting(p2,0.5).
/*Algorithm 4-paper Paes. et al parameter*/
setting(p,0.5).

sl(File):-
  generate_file_names(File,FileKB,FileIn,FileBG,FileOut,FileL),
  reconsult(FileL),
  load_models(FileKB,DB),  
  assert(database(DB)),
  statistics(walltime,[_,_]),
  (file_exists(FileBG)->
    set(compiling,on),
    load(FileBG,_ThBG,RBG),
    set(compiling,off),
    generate_clauses(RBG,_RBG1,0,[],ThBG), 
    assert_all(ThBG)
  ;
    true
  ),
  (file_exists(FileIn)->
    set(compiling,on),
    load(FileIn,_Th1,R1),
    set(compiling,off)
  ;
    setting(megaex_bottom,MB),
    deduct(MB,DB,[],InitialTheory),   
    length(InitialTheory,_LI),  %-LI=number of rules of the theory
    remove_duplicates(InitialTheory,R1)
  ),
  write('Initial theory'),nl,nl,
  write_rules(R1,user_output),nl,
  %cycle_struct_theory(1,DB,R1,0.001,0,R2,CLL2),% +R1(teoria iniziale) con acc=0.000001; -R2,CLL2
    learn_struct(DB,R1,R2,CLL2), 
  %  At the end the parameters of the best theory found so far are computed with
  %  EMBLEM and the resulting theory is returned.
  %  learn_params(DB,R2,R,CLL),  
  statistics(walltime,[_,WT]),
  WTS is WT/1000,
  %format("CLL after EMBLEM ~f~n",[CLL]),
  %format("Best theory after EMBLEM:~n",[]),
  format("Best theory:~n",[]),
  write_rules(R2,user_output),
  format("Best score ~f~n",[CLL2]),
  format("Total execution time ~f~n~n",[WTS]),
  listing(setting/2),
  open(FileOut,write,Stream),
  format(Stream,'/*Final Acc ~f~n',[CLL2]),
  format(Stream,'Execution time ~f~n',[WTS]),
  tell(Stream),
  listing(setting/2),
  format(Stream,'*/~n~n',[]),
  told, 
  open(FileOut,append,Stream1),
  write_rules(R2,Stream1),
  close(Stream1).


learn_struct(DB,R1,R,CLL):-   
  generate_clauses(R1,R2,0,[],Th1), %R2=R1,Th1=regole con bdd e operatori per bdd
  assert_all(Th1),  
  assert_all(R2),!,
  /*findall(R-HN,(rule(R,HL,_BL,_Lit),length(HL,HN)),L),  
  keysort(L,LS),
  get_heads(LS,LSH),  
  length(LSH,NR),   
  init(NR,LSH),*/
  init(1,[1]),
  retractall(v(_,_,_)),
  length(DB,NEx),  
  (setting(examples,atoms)->
    setting(group,G),  
    derive_bdd_nodes_groupatoms(DB,NEx,G,[],Nodes,0,CLLN,LE,[]),!%-Nodes,CLL0
  ;
    derive_bdd_nodes(DB,NEx,[],Nodes,0,CLLN),!
  ),
  length(LE,LN),
  CLL0 is CLLN/LN,  %esempi coperti/tot esempi
  format("Initial Acc ~f~n~n",[CLL0]),
/*  setting(random_restarts_number,N),
  random_restarts(N,Nodes,CLL0,CLL,initial,Par,LE),   %output:CLL,Par
  format("CLL after EMBLEM = ~f~n",[CLL]),*/
  retract_all(Th1),
  retract_all(R2),!,
  end,  
/* update_theory(R2,Par,R3), 
  write('updated Theory'),nl,
  write_rules(R3,user_output),
  setting(max_iter,M),
  cycle_struct([(R3,CLL)],DB,R3,R,M,CLL,-inf,CLL1).*/

  /*
  recupera la miglior revisione da Alg.2 (Paes et al.)
  writeln('Addition/deletion of antecedents'),
  cycle_struct_body(1,DB,R1,R3,CLL0,-inf,CLL3), % +:R1,CLL0  -R3,CLL3
  */
  %recupera la miglior revisione da Alg.4 
  writeln('Start cycle over theories'),
  cycle_struct_theory(1,DB,R1,CLL0,0,R,CLL).% +R1(teoria iniziale) con CLL0; -R,CLL

%cycle_struct_theory(10000,_DB,R,S,_SP,R,S):-!.

%%%%%%%%%%%%%%%%%%%%%%Algorithm 4 Paes et al.%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cycle_struct_theory(_M,_DB,R,S,SP,R,S):-
  setting(eps,Eps),
  setting(eps_f,EpsF),
  A is S-SP,
  X is -S*EpsF,
  format("******USCITA S-SP = ~f-~f = ~f<~f  ~f<~f ?~n",[S,SP,A,Eps,A,X]),
  ((S-SP)<Eps
  ;
   (X is -S*EpsF,
    Y is S-SP, 
    Y<X)
  ),
  !.

%chiamata:   (1,DB,R1,CLL0,0,R,CLL) - R1 teoria iniziale nel .cpl(1 clausola)
cycle_struct_theory(M,DB,R0,Score0,SP0,R,Score):-
  format("*** Iteration ~d ***",[M]),nl,nl,  %M usato solo per debug
  format("* Body Rev. *",[]),nl,nl,
  findall((R0,C),member(C,R0),LRC),
  cycle_struct_body(DB,LRC,Score0,LC),   %cycle_struct_body([m1],R0=[def_rule(active(_336505),[],true)],LC=[[def_rule(active(_336505),[ashby_alert(_362813,_336505,_362815)],true)]]
  format("* Theory Rev. *",[]),nl,nl,
  theory_revisions_rules(R0,LT),  %LT: lista teorie ottenute per revisione di R0 con aggiunta o eliminazione di clausole
  append(LC,LT,LR),
  length(LR,NR),	
  write('Total number of revisions '),write(NR),nl,nl,
  random(N), 
  setting(p,P),
  writeln(N),
  %'with prob p' branch
  (N<P->
	score_refinements_random(LR,1,NR,DB,R0,Score0,SP0,R3,S3,SP)
	/*input: lista tutte Revs;  output: prima Rev con score > della teoria corrente*/
  ;
  %'otherwise' branch
	score_refinements(LR,1,NR,DB,R0,Score0,SP0,R3,S3,SP)
	/*input: lista tutte Revs; output: teoria con score piu' alto, se maggiore della teoria corrente */
  ),
  write('Best THEORY refinement:'),nl,
  write_rules(R3,user_output),
  M1 is M+1,
  format("~nBest THEORY score (Acc) ~f~n~n",[S3]),
  cycle_struct_theory(M1,DB,R3,S3,SP,R,Score).


%%%%%%%%%%%%%%%%%%%%%%%%%%%Algorithm 2 Paes et al.%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cycle_struct_body(_DB,[],_CLL0,[]):-!.

cycle_struct_body(DB,[(R,C)|T],CLL0,[R3|T3]):-  
  R1 = [C],
  writeln('Clause:'),
  write_rules(R1,user_output),
  %  generate_clauses(R1,R2,0,[],Th1),
/*  generate_clauses(R0,R2,0,[],Th1),
  assert_all(Th1),
  assert_all(R2),!,
  findall(def_rule(_,_,_),(def_rule(H,_BL,_Lit)),L),
  %findall(RN-HN,(rule(RN,HL,_BL,_Lit),length(HL,HN)),L),  
  %  keysort(L,LS),
  %  get_heads(LS,LSH),
  %  length(LSH,NR),
  length(L,NR),
  get_heads1(L,LSH),  %LSH lista di 1
  init(NR,LSH),
  retractall(v(_,_,_)),
  length(DB,NEx),
  (setting(examples,atoms)->
	  setting(group,G),
	  derive_bdd_nodes_groupatoms(DB,NEx,G,[],Nodes,0,CLLN,LE,[]),!
  ;
	  derive_bdd_nodes(DB,NEx,[],Nodes,0,CLLN),!
  ),
  length(LE,LN),
  CLL0 is CLLN/LN,  %esempi coperti/tot esempi
  format("Accuracy ~f~n~n",[CLL0]),
  end,
  retract_all(Th1),
  retract_all(R2),!,
  */
  random(N1), 
  %  N1 is 0.6,
  writeln(N1),
  setting(p1,P1),
  %addition test 
  (N1<P1->
	theory_revisions_addbody(R1,LRC),  %LRC: raffinamenti teoria mono-clausola
  	length(LRC,NRev),	
  	write('Number of revisions (add body): '),write(NRev),nl,nl,
	score_refinements_random_b(LRC,R,1,NRev,DB,R1,CLL0,/*SP0,*/R3,S3/*,SP*/)
	/*input: lista Revs generate da add_body; output R3: prima Rev con score S3 > di quello della teoria corrente (CLL0)*/
  ;
  %N1>P1
  random(N2),
    %N2 is 0.6,
  writeln(N2),
  setting(p2,P2),
  %deletion test 
  (N2<P2->
	theory_revisions_delbody(R1,LRC),  
  	length(LRC,NRev),
  	write('Number of revisions (del body) '),write(NRev),nl,nl,
	score_refinements_random_b(LRC,R,1,NRev,DB,R1,CLL0,R3,S3)
	/*input: lista Revs generate da remove_body; output R3: prima Rev con score > della teoria corrente*/
  ;
  %'otherwise' branch
	theory_revisions_addbody(R1,LRA),
	theory_revisions_delbody(R1,LRD),
	append(LRA,LRD,LRC),
  	length(LRC,NRev),
  	write('Number of revisions (add+del body) '),write(NRev),nl,nl,
	score_refinements_b(LRC,R,R1,1,NRev,DB,R1,CLL0,R3,S3)
	/*input: unione delle 2 liste di Revs generate da add/remove_body; output: teoria con score piu' alto S3, se maggiore della teoria corrente */
  )),
  write('Best BODY refinement:'),nl,
  write_rules(R3,user_output),
  format("~nBest BODY score (Acc) ~f~n~n",[S3]),
  cycle_struct_body(DB,T,CLL0,T3).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% score_refinements_random_b/10
%se tutte le revisioni sono peggiori restituisce R0,S0
score_refinements_random_b([],_R,_N,_NR,_DB,R,S,/*SP,*/R,S/*,SP*/):-!.

score_refinements_random_b(LR,TR,Nrev,TotRev,DB,R0,S0,/*SP0,*/R,S/*,SP*/):-  
  random(0,TotRev,Pos),  		
  nth0(Pos,LR,R1,Rest),   %choose a Rev. R1 at random
  format('*** Random rev. ~d of ~d rimaste~n',[Nrev,TotRev]),
  write_rules(R1,user_output),
  replace(TR,R1,R0,T1),   %replaces in theory R clause R0 with clause R1 and outputs T1
  generate_clauses(T1,R2,0,[],Th1),
  %  generate_clauses(R1,R2,0,[],Th1),
  assert_all(Th1),
  assert_all(R2),!,
  findall(def_rule(_,_,_),(def_rule(H,_BL,_Lit)),L),
  %findall(RN-HN,(rule(RN,HL,_BL,_Lit),length(HL,HN)),L),  
  %  keysort(L,LS),
  %  get_heads(LS,LSH),
  %  length(LSH,NR),
  length(L,NR),
  get_heads1(L,LSH),
  init(NR,LSH),
  retractall(v(_,_,_)),
  length(DB,NEx),
  (setting(examples,atoms)->
    setting(group,G),  
    derive_bdd_nodes_groupatoms(DB,NEx,G,[],Nodes,0,CLLN,LE,[]),!
  ; 
    derive_bdd_nodes(DB,NEx,[],Nodes,0,CLLN),!
  ),
  length(LE,LNe),
  %format("Nodes ~d LE ~d",[LN,LNe]),nl,
  CLL0 is CLLN/LNe,  %esempi coperti/tot esempi
  format("Accuracy ~f~n~n",[CLL0]),   %calcolo lo score senza aggiornare i parametri (restano 0.5)
/*
  setting(random_restarts_REFnumber,N),
  random_restarts_ref(N,Nodes,CLL0,CLL,initial,Par,LE),  */
  end,
/*  update_theory(R2,Par,R3),
  write('Updated refinement'),nl,
  write_rules(R3,user_output), 
  Score = CLL,  
  write('Score (CLL) '),write(Score),nl,nl,nl,*/
  retract_all(Th1),
  retract_all(R2),!,
  Score = CLL0,
  (Score>S0->
	  %  R=R1,
    R=T1,
    S=Score
    %SP=S0
  ;
    Nrev1 is Nrev+1,  
    TotRev1 is TotRev-1,
    score_refinements_random_b(Rest,TR,Nrev1,TotRev1,DB,TR,S0,/*SP0,*/R,S/*,SP*/)
  ).

replace([],_,_,[]).

replace([H|T],[C],[H],[C|T]):-!.

replace([H|T],R1,R0,[H|T1]):-
	replace(T,R1,R0,T1).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% score_refinements_random/10  (per teorie)
score_refinements_random([],_N,_NR,_DB,R,S,SP,R,S,SP):-!.

score_refinements_random(LR,Nrev,TotRev,DB,R0,S0,SP0,R,S,SP):-  
  random(0,TotRev,Pos),  		
  nth0(Pos,LR,R1,Rest),   %choose a Rev. R1 at random
  format('***Random Rev. ~d of ~d rimaste~n',[Nrev,TotRev]),
  write_rules(R1,user_output),   
  generate_clauses(R1,R2,0,[],Th1),
  assert_all(Th1),
  assert_all(R2),!,
  findall(def_rule(_,_,_),(def_rule(H,_BL,_Lit)),L),
  %findall(RN-HN,(rule(RN,HL,_BL,_Lit),length(HL,HN)),L),  
  %  keysort(L,LS),
  %  get_heads(LS,LSH),
  %  length(LSH,NR),
  length(L,NR),
  get_heads1(L,LSH),
  init(NR,LSH),
  retractall(v(_,_,_)),
  length(DB,NEx),
  (setting(examples,atoms)->
    setting(group,G),  
    derive_bdd_nodes_groupatoms(DB,NEx,G,[],Nodes,0,CLLN,LE,[]),!
  ; 
    derive_bdd_nodes(DB,NEx,[],Nodes,0,CLLN),!
  ),
  length(LE,LN),
  CLL0 is CLLN/LN,  %esempi coperti/tot esempi
  format("Acc ~f~n~n",[CLL0]),   %calcolo lo score senza aggiornare i parametri (restano 0.5)
/*
  setting(random_restarts_REFnumber,N),
  random_restarts_ref(N,Nodes,CLL0,CLL,initial,Par,LE),  */
  end,
/*  update_theory(R2,Par,R3),
  write('Updated refinement'),nl,
  write_rules(R3,user_output), 
  Score = CLL,  
  write('Score (CLL) '),write(Score),nl,nl,nl,*/
  retract_all(Th1),
  retract_all(R2),!,
  Score = CLL0,
  (Score>=S0->
    R=R1,
    S=Score,
    SP=S0
  ;
    Nrev1 is Nrev+1, 
    TotRev1 is TotRev-1,
    score_refinements_random(Rest,Nrev1,TotRev1,DB,R0,S0,SP0,R,S,SP)
  ).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% score_refinements_b/10   (per body)
score_refinements_b([],_TR,_R00,_N,_NR,_DB,R,S,R,S):-!.

score_refinements_b([R1|T],TR,R00,Nrev,TotRev,DB,R0,S0,R,S):-  
  format('*** Rev. ~d of ~d~n',[Nrev,TotRev]),
  write_rules(R1,user_output),   
  replace(TR,R1,R00,T1),
  generate_clauses(T1,R2,0,[],Th1),
  assert_all(Th1),
  assert_all(R2),!,
  findall(def_rule(_,_,_),(def_rule(H,_BL,_Lit)),L),
  %findall(RN-HN,(rule(RN,HL,_BL,_Lit),length(HL,HN)),L),  
  %  keysort(L,LS),
  %  get_heads(LS,LSH),
  %  length(LSH,NR),
  length(L,NR),
  get_heads1(L,LSH),
  init(NR,LSH),
  retractall(v(_,_,_)),
  length(DB,NEx),
  (setting(examples,atoms)->
    setting(group,G),  
    derive_bdd_nodes_groupatoms(DB,NEx,G,[],Nodes,0,CLLN,LE,[]),!
  ; 
    derive_bdd_nodes(DB,NEx,[],Nodes,0,CLLN),!
  ),
  length(LE,LN),
  CLL0 is CLLN/LN,  %esempi coperti/tot esempi
  format("Acc ~f~n~n",[CLL0]),
  /*setting(random_restarts_REFnumber,N),
  random_restarts_ref(N,Nodes,CLL0,CLL,initial,Par,LE), */
  end,
  /*update_theory(R2,Par,R3),
  write('Updated refinement'),nl,
  write_rules(R3,user_output), 
  Score = CLL,  
  write('Score (CLL) '),write(Score),nl,nl,nl,*/
  retract_all(Th1),
  retract_all(R2),!,
/*compares the score and theory found so far with the latest refinement R3 and associated Score*/
  Score = CLL0,
  (Score>S0->
    R4=R1,	%R3,
    S4=Score
    %    SP1=S0
  ;
    R4=R0,
    S4=S0
    % SP1=SP0
  ),
  Nrev1 is Nrev+1,  
  score_refinements_b(T,TR,R00,Nrev1,TotRev,DB,R4,S4,/*,SP1*/R,S/*,SP*/).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% score_refinements/10
score_refinements([],_N,_NR,_DB,R,S,SP,R,S,SP):-!.

score_refinements([R1|T],Nrev,TotRev,DB,R0,S0,SP0,R,S,SP):-  %scans the list of revised theories; returns S,R, the best (highest) score and revised theory R
  format('*** Rev. ~d of ~d~n',[Nrev,TotRev]),
  write_rules(R1,user_output),   
  generate_clauses(R1,R2,0,[],Th1),
  assert_all(Th1),
  assert_all(R2),!,
  findall(def_rule(_,_,_),(def_rule(H,_BL,_Lit)),L),
  %findall(RN-HN,(rule(RN,HL,_BL,_Lit),length(HL,HN)),L),  
  %  keysort(L,LS),
  %  get_heads(LS,LSH),
  %  length(LSH,NR),
  length(L,NR),
  get_heads1(L,LSH),
  init(NR,LSH),
  retractall(v(_,_,_)),
  length(DB,NEx),
  (setting(examples,atoms)->
    setting(group,G),  
    derive_bdd_nodes_groupatoms(DB,NEx,G,[],Nodes,0,CLLN,LE,[]),!
  ; 
    derive_bdd_nodes(DB,NEx,[],Nodes,0,CLLN),!
  ),
  length(LE,LN),
  CLL0 is CLLN/LN,  %esempi coperti/tot esempi
  format("CLL ~f~n~n",[CLL0]),
  /*setting(random_restarts_REFnumber,N),
  random_restarts_ref(N,Nodes,CLL0,CLL,initial,Par,LE), */
  end,
  /*update_theory(R2,Par,R3),
  write('Updated refinement'),nl,
  write_rules(R3,user_output), 
  Score = CLL,  
  write('Score (CLL) '),write(Score),nl,nl,nl,*/
  retract_all(Th1),
  retract_all(R2),!,
/*compares the score and theory found so far with the latest refinement R3 and associated Score*/
  Score = CLL0,
  (Score>=S0->
    R4=R1,	%R3,
    S4=Score,
    SP1=S0
  ;
    R4=R0,
    S4=S0,
    SP1=SP0
  ),
  Nrev1 is Nrev+1,  
  score_refinements(T,Nrev1,TotRev,DB,R4,S4,SP1,R,S,SP).


em(File):-
  generate_file_names(File,FileKB,FileIn,FileBG,FileOut,FileL),
  reconsult(FileL),
  load_models(FileKB,DB),
  (file_exists(FileBG)->
    set(compiling,on),
    load(FileBG,_ThBG,RBG),
    set(compiling,off),
    generate_clauses(RBG,_RBG1,0,[],ThBG), 
    assert_all(ThBG)
  ;
    true
  ),
  set(compiling,on),
  load(FileIn,_TH,R0),
  set(compiling,off),
  set(verbosity,3),
  statistics(walltime,[_,_]),      
  learn_params(DB,R0,R,CLL),
  statistics(walltime,[_,CT]),
  CTS is CT/1000,
  format("EM: Final CLL ~f~n",[CLL]),
  format("Execution time ~f~n~n",[CTS]),
  write_rules(R,user_output),
  listing(setting/2),
  open(FileOut,write,Stream),
  format(Stream,'/* EMBLEM Final CLL ~f~n',[CLL]),
  format(Stream,'Execution time ~f~n',[CTS]),
  tell(Stream),
  listing(setting/2),
  format(Stream,'*/~n~n',[]),
  told,
  open(FileOut,append,Stream1),
  write_rules(R,Stream1),
  close(Stream1).

learn_params(DB,R0,R,CLL):-  
  generate_clauses(R0,R1,0,[],Th0), 
  assert_all(Th0),
  assert_all(R1),!,
  findall(R-HN,(rule(R,HL,_BL,_Lit),length(HL,HN)),L),
  keysort(L,LS),
  get_heads(LS,LSH),
  length(LSH,NR),
  init(NR,LSH),
  retractall(v(_,_,_)),
  length(DB,NEx),
  (setting(examples,atoms)->
    setting(group,G),  
    derive_bdd_nodes_groupatoms(DB,NEx,G,[],Nodes,0,CLL0,LE,[]),!   
  ; 
   derive_bdd_nodes(DB,NEx,[],Nodes,0,CLL0),!      
  ),
  setting(random_restarts_number,N),
  random_restarts(N,Nodes,CLL0,CLL,initial,Par,LE),  %computes new parameters Par
  end,  
  retract_all(Th0),
  retract_all(R1),!,
  update_theory(R1,Par,R).  %replaces in R1 the probabilities Par and outputs R


update_theory(R,initial,R):-!.

update_theory([],_Par,[]).

update_theory([def_rule(H,B,L)|T0],Par,[def_rule(H,B,L)|T]):-!,
  update_theory(T0,Par,T).

update_theory([(H:-B)|T0],Par,[(H:-B)|T]):-!,
  update_theory(T0,Par,T).

update_theory([rule(N,_H,_B,_L)|T0],Par,T):-
  member([N,[1.0|_T]],Par),!,  
  update_theory(T0,Par,T).

update_theory([rule(N,H,B,L)|T0],Par,[rule(N,H1,B,L)|T]):-
  member([N,P],Par),!, 
  reverse(P,P1),
  update_head_par(H,P1,H1),  
  update_theory(T0,Par,T).

update_head_par([],[],[]).

update_head_par([H:_P|T0],[HP|TP],[H:HP|T]):-
  update_head_par(T0,TP,T).
  
%until no antecedent can improve the score
%S=BestLL; SP=PreviousBestLL
/*cycle_struct_body(_M,_DB,R,R,S,SP,S):-
  setting(eps,Eps),
  setting(eps_f,EpsF),
  ((S-SP)<Eps
  ;
   (X is -S*EpsF,
    Y is S-SP, 
    Y<X)
  ),
  !.
*/



/*
insert_in_order([],C,BeamSize,[C]):-
  BeamSize>0,!.

insert_in_order(Beam,_New,0,Beam):-!.

insert_in_order([(Th1,Heuristic1)|RestBeamIn],(Th,Heuristic),BeamSize,BeamOut):-
  Heuristic>Heuristic1,!,
  % larger heuristic, insert here
  NewBeam=[(Th,Heuristic),(Th1,Heuristic1)|RestBeamIn],
  length(NewBeam,L),
  (L>BeamSize->
    nth(L,NewBeam,_Last,BeamOut)
  ;
    BeamOut=NewBeam
  ).
  
insert_in_order([(Th1,Heuristic1)|RestBeamIn],(Th,Heuristic),BeamSize,
[(Th1,Heuristic1)|RestBeamOut]):-
  BeamSize1 is BeamSize -1,
  insert_in_order(RestBeamIn,(Th,Heuristic),BeamSize1,
  RestBeamOut).
*/


remove_int_atom_list([],[]).

remove_int_atom_list([A|T],[A1|T1]):-
  A=..[F,_|Arg],
  A1=..[F|Arg],
  remove_int_atom_list(T,T1).



remove_int_atom(A,A1):-
  A=..[F,_|T],
  A1=..[F|T].

get_heads1([],[]).

get_heads1([_H|T],[1|TN]):-
  get_heads1(T,TN).


get_heads([],[]).

get_heads([_-H|T],[H|TN]):-
  get_heads(T,TN).

derive_bdd_nodes([],_E,Nodes,Nodes,CLL,CLL).

derive_bdd_nodes([H|T],E,Nodes0,Nodes,CLL0,CLL):-
  get_output_atoms(O),
  generate_goal(O,H,[],GL),
  (prob(H,P)->
    CardEx is P*E
  
  ;
    CardEx is 1.0
  ),
  init_bdd,
  one(One),
  get_node_list(GL,One,BDD,CardEx),
  ret_prob(BDD,HP),
  (HP=:=0.0->
    setting(logzero,LZ),
    CLL1 is CLL0+LZ*CardEx
  ;
    CLL1 is CLL0+log(HP)*CardEx
  ),
  end_bdd,
  append(Nodes0,[[BDD,CardEx]],Nodes1),
  derive_bdd_nodes(T,E,Nodes1,Nodes,CLL1,CLL).


get_node_list([],BDD,BDD,_CE).

get_node_list([H|T],BDD0,BDD,CE):-
  get_node(H,BDD1),
  and(BDD0,BDD1,BDD2),
  get_node_list(T,BDD2,BDD,CE).

  
derive_bdd_nodes_groupatoms([],_E,_G,Nodes,Nodes,CLL,CLL,LE,LE).

derive_bdd_nodes_groupatoms([H|T],E,G,Nodes0,Nodes,CLL0,CLL,LE0,LE):-  %[H|T] models
  get_output_atoms(O),
  generate_goal(O,H,[],GL),
  length(GL,NA),
  (prob(H,P)->
    CardEx is P*E/NA
  ;
    CardEx is 1.0
  ),
  get_node_list_groupatoms(GL,BDDs,CardEx,G,CLL0,CLL1,LE0,LE1),
  append(Nodes0,BDDs,Nodes1),
  derive_bdd_nodes_groupatoms(T,E,G,Nodes1,Nodes,CLL1,CLL,LE1,LE).

get_node_list_groupatoms([],[],_CE,_Gmax,CLL,CLL,LE,LE).

%versione con CLL = Accuratezza
get_node_list_groupatoms([H|T],[[BDD,CE1]|BDDT],CE,Gmax,CLL0,CLL,LE0,LE):-
  init_bdd,  
  one(One),
  get_bdd_group([H|T],T1,Gmax,G,One,BDD,CE,LE0,LE1),  %output=BDD,CLL
  CE1 is CE*(Gmax-G),
  ret_prob(BDD,HP),
  end_bdd,
  (HP =:=0.0->
    CLL2 is CLL0	%esempio non coperto
  ;
    CLL2 is CLL0+1	%esempio coperto
  ),
  get_node_list_groupatoms(T1,BDDT,CE,Gmax,CLL2,CLL,LE1,LE).


get_bdd_group([],[],G,G,BDD,BDD,_CE,LE,LE):-!.

get_bdd_group(T,T,0,0,BDD,BDD,_CE,LE,LE):- !.

get_bdd_group([H|T],T1,Gmax,G1,BDD0,BDD,CE,[H|LE0],LE):-
%  write('prima di getnode'),
%  write(H),flush_output,
  get_node(H,BDD1),  %creates bdd for atomo H
  and(BDD0,BDD1,BDD2),
  G is Gmax-1,
  get_bdd_group(T,T1,G,G1,BDD2,BDD,CE,LE0,LE).
  
/* EM start */
random_restarts(0,_Nodes,CLL,CLL,Par,Par,_LE):-!.

random_restarts(N,Nodes,CLL0,CLL,Par0,Par,LE):-
  setting(verbosity,Ver),
  (Ver>2->
    setting(random_restarts_number,NMax),
    Num is NMax-N+1,
    format("Restart number ~d~n~n",[Num]),
    flush_output
  ;
    true
  ),
  randomize,      
  setting(epsilon_em,EA),
  setting(epsilon_em_fraction,ER),
  length(Nodes,L),
  setting(iter,Iter),
  em(Nodes,EA,ER,L,Iter,CLLR,Par1),  
  setting(verbosity,Ver),
  (Ver>2->
    format("Random_restart: CLL ~f~n",[CLLR])
  ;
    true
  ),
  N1 is N-1,
  (CLLR>CLL0->     
    random_restarts(N1,Nodes,CLLR,CLL,Par1,Par,LE)
  ;
    random_restarts(N1,Nodes,CLL0,CLL,Par0,Par,LE)
  ).


random_restarts_ref(0,_Nodes,CLL,CLL,Par,Par,_LE):-!.

random_restarts_ref(N,Nodes,CLL0,CLL,Par0,Par,LE):-
  setting(verbosity,Ver),
  (Ver>2->
    setting(random_restarts_REFnumber,NMax),
    Num is NMax-N+1,
    format("Restart number ~d~n~n",[Num]),
    flush_output
  ;
    true
  ),
  setting(epsilon_em,EA),
  setting(epsilon_em_fraction,ER),
  length(Nodes,L),
  setting(iterREF,Iter),
  em(Nodes,EA,ER,L,Iter,CLLR,Par1),  
  setting(verbosity,Ver),
  (Ver>2->
    format("Random_restart: CLL ~f~n",[CLLR])
  ;
    true
  ),
  N1 is N-1,
  (CLLR>CLL0->  
    random_restarts_ref(N1,Nodes,CLLR,CLL,Par1,Par,LE)
  ;
    random_restarts_ref(N1,Nodes,CLL0,CLL,Par0,Par,LE)
  ).


randomize([],[]):-!.

randomize([rule(N,V,NH,HL,BL,LogF)|T],[rule(N,V,NH,HL1,BL,LogF)|T1]):-
  length(HL,L),
  Int is 1.0/L,
  randomize_head(Int,HL,0,HL1),
  randomize(T,T1).

randomize_head(_Int,['':_],P,['':PNull1]):-!,
  PNull is 1.0-P,
  (PNull>=0.0->
    PNull1 =PNull
  ;
    PNull1=0.0
  ).
  
randomize_head(Int,[H:_|T],P,[H:PH1|NT]):-
  PMax is 1.0-P,
  random(0,PMax,PH1),
  P1 is P+PH1,  
  randomize_head(Int,T,P1,NT).



update_head([],[],_N,[]):-!.  

update_head([H:_P|T],[PU|TP],N,[H:P|T1]):-
  P is PU/N,
  update_head(T,TP,N,T1).


/* EM end */    
  
  
/* utilities */

generate_file_names(File,FileKB,FileIn,FileBG,FileOut,FileL):-
  generate_file_name(File,".kb",FileKB),
  generate_file_name(File,".cpl",FileIn),
  generate_file_name(File,".rules",FileOut),
  generate_file_name(File,".bg",FileBG),
  generate_file_name(File,".l",FileL).
        
generate_file_name(File,Ext,FileExt):-
  name(File,FileString),
  append(FileString,Ext,FileStringExt),
  name(FileExt,FileStringExt).
   
load_models(File,ModulesList):-  %carica le interpretazioni, 1 alla volta
  open(File,read,Stream),
  read_models(Stream,ModulesList),
  close(Stream).


read_models(Stream,[Name1|Names]):-
  read(Stream,begin(model(Name))),!,
  (number(Name)->
     name(Name,NameStr),
     append("i",NameStr,Name1Str),
     name(Name1,Name1Str)
  ;
     Name1=Name
  ),
  read_all_atoms(Stream,Name1),
  read_models(Stream,Names).

read_models(_S,[]).


read_all_atoms(Stream,Name):-
  read(Stream,At),
  At \=end(model(_Name)),!,
  (At=neg(Atom)->    
    Atom=..[Pred|Args],
    Atom1=..[Pred,Name|Args],
    assertz(neg(Atom1))
  ;
    (At=prob(Pr)->
      assertz(prob(Name,Pr))
    ;
      At=..[Pred|Args],
      Atom1=..[Pred,Name|Args],
      assertz(Atom1)
    )
  ),
  read_all_atoms(Stream,Name).    

read_all_atoms(_S,_N).


write_param(initial,S):-!,
  format("~nInitial parameters~n",[]),
  findall(rule(R,H,B,Lit),rule(R,H,B,Lit),LDis),
  findall(rule(d,[H:1.0],B,Lit),def_rule(H,B,Lit),LDef),
  append(LDis,LDef,L),
  write_model(L,S).

write_param(L,S):-
  reverse(L,L1),
  write_par(L1,S).


write_par([],S):-
  findall(rule(d,[H:1.0],B,Lit),def_rule(H,B,Lit),L),
  write_model(L,S).

write_par([[N,P]|T],S):-
  rule(N,HL0,BL),
  reverse(P,PR),
  new_par(PR,HL0,HL),
  copy_term((HL,BL),(HL1,BL1)),
  numbervars((HL1,BL1),0,_M),
  write_disj_clause(S,(HL1:-BL1)),
  write_par(T,S).


write_rules([],_S).

write_rules([rule(_N,HL,BL,Lit)|T],S):-
  copy_term((HL,BL,Lit),(HL1,BL1,Lit1)),
  numbervars((HL1,BL1,Lit1),0,_M),
  write_disj_clause(S,(HL1:-BL1)),
%  write(Lit1),nl,
  write_rules(T,S).

write_rules([def_rule(HL,BL,Lit)|T],S):-
  copy_term((HL,BL,Lit),(HL1,BL1,Lit1)),
  numbervars((HL1,BL1,Lit1),0,_M),
  write_def_clause(S,(HL1:-BL1)),
  %format(S,"~p.~n",[HL1]),
%  write(Lit1),nl,
  write_rules(T,S).

new_par([],[],[]).

new_par([HP|TP],[Head:_|TO],[Head:HP|TN]):-
  new_par(TP,TO,TN).


write_model([],_Stream):-!.

write_model([rule(_N,HL,BL)|Rest],Stream):-
  copy_term((HL,BL),(HL1,BL1)),
  numbervars((HL1,BL1),0,_M),
  write_disj_clause(Stream,(HL1:-BL1)),
  write_model(Rest,Stream).

write_def_clause(S,(H:-[])):-!,
  format(S,"~p.~n",[H]).

write_def_clause(S,(H:-B)):-
  format(S,"~p :-~n",[H]),
  write_body(S,B).

write_disj_clause(S,(H:-[])):-!,
  write_head(S,H),
  format(S,".~n~n",[]).
    
write_disj_clause(S,(H:-B)):-
  write_head(S,H),
  write(S,' :-'),
  nl(S),
  write_body(S,B).


write_head(S,[A:1.0|_Rest]):-!,
  format(S,"~p",[A]).
  
write_head(S,[A:P,'':_P]):-!, 
  format(S,"~p:~g",[A,P]).

write_head(S,[A:P]):-!,
  format(S,"~p:~g",[A,P]).

write_head(S,[A:P|Rest]):-
  format(S,"~p:~g ; ",[A,P]),
  write_head(S,Rest).


write_body(S,[A]):-!,
  format(S,"\t~p.~n~n",[A]).
    
write_body(S,[A|T]):-
  format(S,"\t~p,~n",[A]),
  write_body(S,T).


list2or([],true):-!.

list2or([X],X):-
    X\=;(_,_),!.

list2or([H|T],(H ; Ta)):-!,
    list2or(T,Ta).


list2and([],true):-!.

list2and([X],X):-
    X\=(_,_),!.

list2and([H|T],(H,Ta)):-!,
    list2and(T,Ta).
 

deduct(0,_DB,Th,Th).

deduct(NM,DB,InTheory0,InTheory):-
  get_head_atoms(O),
  sample(1,DB,[M],DB1),
  generate_head(O,M,[],HL),
  generate_body(HL,InTheory1),
  append(InTheory0,InTheory1,InTheory2),
  NM1 is NM-1,
  deduct(NM1,DB1,InTheory2,InTheory).

        
get_head_atoms(O):-
  findall(A,modeh(_,A),O).


generate_head([],_M,HL,HL):-!.

generate_head([A|T],M,H0,H1):-
  functor(A,F,N),    
  functor(F1,F,N),   
  F1=..[F|Arg],
  Pred1=..[F,M|Arg],
  findall((A,Pred1),call(Pred1),L),
  setting(initial_clauses_per_megaex,IC),   %IC: represents how many samples are extracted from the list L of example
  sample(IC,L,L1),   %+IC,L, -L1
  append(H0,L1,H2),
  generate_head(T,M,H2,H1).


sample(0,List,[],List):-!.

sample(N,List,List,[]):-
  length(List,L),
  L=<N,!.

sample(N,List,[El|List1],L):-
  length(List,L),
  random(0,L,Pos),
  nth0(Pos,List,El,Rest),
  N1 is N-1,
  sample(N1,Rest,List1,L).

sample(0,_List,[]):-!.

sample(N,List,List):-
  length(List,L),
  L=<N,!.

sample(N,List,[El|List1]):-
  length(List,L),
  random(0,L,Pos),
  nth0(Pos,List,El,Rest),
  N1 is N-1,
  sample(N1,Rest,List1).

generate_body([],[]):-!.

generate_body([(A,H)|T],[rule(R,[Head:0.5,'':0.5],[],BodyList)|CL0]):-
  findall((R,B),modeb(R,B),BL),
  A=..[F|ArgsTypes],
  H=..[F,M|Args],
  setting(d,D),
  cycle_modeb(ArgsTypes,Args,[],[],BL,a,[],BLout0,D,M),
  remove_duplicates(BLout0,BLout),
  variabilize(((H,A):-BLout),CLV),  %+(Head):-Bodylist;  -CLV:(Head):-Bodylist with variables _num in place of constants
  copy_term((H:-BLout),CLa),
  numbervars(CLa,0,_N1),
  copy_term(CLV,CLav),
  numbervars(CLav,0,_N1v),
  CLV=(Head1:-BodyList1),
  remove_int_atom(Head1,Head),
  remove_int_atom_list(BodyList1,BodyList),
  get_next_rule_number(R),
  generate_body(T,CL0).


variabilize((H:-B),(H1:-B1)):-
  variabilize_list([H|B],[H1|B1],[],_AS,_M).


variabilize_list([],[],A,A,_M).

variabilize_list([(H,Mode)|T],[H1|T1],A0,A,M):-
  builtin(H),!,
  H=..[F|Args],
  Mode=..[F|ArgTypes],
  variabilize_args(Args,ArgTypes, Args1,A0,A1),
  H1=..[F,M|Args1],
  variabilize_list(T,T1,A1,A,M).

variabilize_list([(H,Mode)|T],[H1|T1],A0,A,M):-
  H=..[F,_M|Args],
  Mode=..[F|ArgTypes],
  variabilize_args(Args,ArgTypes, Args1,A0,A1),
  H1=..[F,M|Args1],
  variabilize_list(T,T1,A1,A,M).


variabilize_args([],[],[],A,A).

variabilize_args([C|T],[C|TT],[C|TV],A0,A):-!,
  variabilize_args(T,TT,TV,A0,A).

variabilize_args([C|T],[# _Ty|TT],[C|TV],A0,A):-!,
  variabilize_args(T,TT,TV,A0,A).

variabilize_args([C|T],[-# _Ty|TT],[C|TV],A0,A):-!,
  variabilize_args(T,TT,TV,A0,A).

variabilize_args([C|T],[_Ty|TT],[V|TV],A0,A):-
  member(C/V,A0),!,
  variabilize_args(T,TT,TV,A0,A).

variabilize_args([C|T],[_Ty|TT],[V|TV],A0,A):-
  variabilize_args(T,TT,TV,[C/V|A0],A).


cycle_modeb(ArgsTypes,Args,ArgsTypes,Args,_BL,L,L,L,_,_M):-!.

cycle_modeb(_ArgsTypes,_Args,_ArgsTypes1,_Args1,_BL,_L,L,L,0,_M):-!.

cycle_modeb(ArgsTypes,Args,_ArgsTypes0,_Args0,BL,_L0,L1,L,D,M):-
  find_atoms(BL,ArgsTypes,Args,ArgsTypes1,Args1,L1,L2,M),
  D1 is D-1,
  cycle_modeb(ArgsTypes1,Args1,ArgsTypes,Args,BL,L1,L2,L,D1,M).


find_atoms([],ArgsTypes,Args,ArgsTypes,Args,L,L,_M).

find_atoms([(R,H)|T],ArgsTypes0,Args0,ArgsTypes,Args,L0,L1,M):-
  H=..[F|ArgsT],
  findall((A,H),instantiate_query(ArgsT,ArgsTypes0,Args0,F,M,A),L),
  call_atoms(L,[],At),
  remove_duplicates(At,At1),
  (R = '*' ->
    R1= +inf
  ;
    R1=R
  ),
  sample(R1,At1,At2),
  extract_output_args(At2,ArgsT,ArgsTypes0,Args0,ArgsTypes1,Args1),
  append(L0,At2,L2),
  find_atoms(T,ArgsTypes1,Args1,ArgsTypes,Args,L2,L1,M).


call_atoms([],A,A).

call_atoms([(H,M)|T],A0,A):-
  findall((H,M),H,L),
  append(A0,L,A1),
  call_atoms(T,A1,A).


extract_output_args([],_ArgsT,ArgsTypes,Args,ArgsTypes,Args).

extract_output_args([(H,_At)|T],ArgsT,ArgsTypes0,Args0,ArgsTypes,Args):-
  builtin(H),!,
  H=..[_F|ArgsH],
  add_const(ArgsH,ArgsT,ArgsTypes0,Args0,ArgsTypes1,Args1),
  extract_output_args(T,ArgsT,ArgsTypes1,Args1,ArgsTypes,Args).

extract_output_args([(H,_At)|T],ArgsT,ArgsTypes0,Args0,ArgsTypes,Args):-
  H=..[_F,_M|ArgsH],
  add_const(ArgsH,ArgsT,ArgsTypes0,Args0,ArgsTypes1,Args1),
  extract_output_args(T,ArgsT,ArgsTypes1,Args1,ArgsTypes,Args).


add_const([],[],ArgsTypes,Args,ArgsTypes,Args).

add_const([_A|T],[+_T|TT],ArgsTypes0,Args0,ArgsTypes,Args):-!,
  add_const(T,TT,ArgsTypes0,Args0,ArgsTypes,Args).

add_const([A|T],[-Type|TT],ArgsTypes0,Args0,ArgsTypes,Args):-!,
  (already_present(ArgsTypes0,Args0,A,Type)->
    ArgsTypes1=ArgsTypes0,
    Args1=Args0
  ;
    ArgsTypes1=[+Type|ArgsTypes0],
    Args1=[A|Args0]
  ),
  add_const(T,TT,ArgsTypes1,Args1,ArgsTypes,Args).

add_const([A|T],[-# Type|TT],ArgsTypes0,Args0,ArgsTypes,Args):-!,
  (already_present(ArgsTypes0,Args0,A,Type)->
    ArgsTypes1=ArgsTypes0,
    Args1=Args0
  ;
    ArgsTypes1=[+Type|ArgsTypes0],
    Args1=[A|Args0]
  ),
  add_const(T,TT,ArgsTypes1,Args1,ArgsTypes,Args).

add_const([_A|T],[# _|TT],ArgsTypes0,Args0,ArgsTypes,Args):-!,
  add_const(T,TT,ArgsTypes0,Args0,ArgsTypes,Args).

add_const([A|T],[A|TT],ArgsTypes0,Args0,ArgsTypes,Args):-
  add_const(T,TT,ArgsTypes0,Args0,ArgsTypes,Args).


already_present([+T|_TT],[C|_TC],C,T):-!.

already_present([_|TT],[_|TC],C,T):-
  already_present(TT,TC,C,T).


instantiate_query(ArgsT,ArgsTypes,Args,F,M,A):-
  instantiate_input(ArgsT,ArgsTypes,Args,ArgsB),
  A1=..[F|ArgsB],
  (builtin(A1)->
    A=A1
  ;
    A=..[F,M|ArgsB]
  ).


instantiate_input([],_AT,_A,[]).

instantiate_input([-_Type|T],AT,A,[_V|TA]):-!,
  instantiate_input(T,AT,A,TA).

instantiate_input([+Type|T],AT,A,[H|TA]):-!,
  find_val(AT,A,+Type,H),
  instantiate_input(T,AT,A,TA).

instantiate_input([# Type|T],AT,A,[H|TA]):-!,
  find_val(AT,A,+Type,H),
  instantiate_input(T,AT,A,TA).

instantiate_input([-# _Type|T],AT,A,[_V|TA]):-!,
  instantiate_input(T,AT,A,TA).


instantiate_input([C|T],AT,A,[C|TA]):-
  instantiate_input(T,AT,A,TA).


find_val([T|_TT],[A|_TA],T,A).

find_val([_T|TT],[_A|TA],T,A):-
  find_val(TT,TA,T,A).


get_output_atoms(O):-
  findall((A/Ar),output((A/Ar)),O).


generate_goal([],_H,G,G):-!.

generate_goal([P/A|T],H,G0,G1):-
  functor(Pred,P,A),
  Pred=..[P|Rest],
  Pred1=..[P,H|Rest],
  findall(Pred1,call(Pred1),L),
  findall(\+ Pred1,call(neg(Pred1)),LN),
  append(G0,L,G2),
  append(G2,LN,G3),
  generate_goal(T,H,G3,G1).
  


:-[inference_sl].
