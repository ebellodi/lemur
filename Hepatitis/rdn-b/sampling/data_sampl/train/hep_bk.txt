
// Add this to indicate that first letter caps is constant and first letter lower-case is a variable.
// Use quotes(") to make sure words beginning with lower case are considered as constants.
// To totally reverse the syntax,(e.g. prolog format), set
// usePrologVariables:true.
useStdLogicVariables: true.

// Set the mode for ILP/Tree search.
// + indicates that this variable must already be grounded/appear before in the clause/tree.
// - indicates that a new variable could be used. 

mode: type(+M, @TYPE_B).
mode: type(+M, @TYPE_C).

mode: b_rel11(-B, +M).
mode: b_rel12(-I, +M).
mode: b_rel13(-A, +M).
mode: b_rel11(+B, -M).
mode: b_rel12(+I, -M).
mode: b_rel13(+A, -M).
mode: fibros(+B, #F).
mode: activity(+B, #A).
mode: sex(+M, #Sex).
mode: age(+M, #Age).
mode: got(+I, #Got).
mode: gpt(+I, #Gpt).
mode: tbil(+I, #Tbil).
mode: dbil(+I, #Dbil).
mode: che(+I, #Che).
mode: ttt(+I, #Ttt).
mode: ztt(+I, #Ztt).
mode: tcho(+I, #Tcho).
mode: tp(+I, #Tp).
mode: dur(+A, #Dur).
mode: fibros(+B, -F).
mode: activity(+B, -A).
mode: sex(+M, -Sex).
mode: age(+M, -Age).
mode: got(+I, -Got).
mode: gpt(+I, -Gpt).
mode: tbil(+I, -Tbil).
mode: dbil(+I, -Dbil).
mode: che(+I, -Che).
mode: ttt(+I, -Ttt).
mode: ztt(+I, -Ztt).
mode: tcho(+I, -Tcho).
mode: tp(+I, -Tp).
mode: dur(+A, -Dur).

// Specific to RDNBoost tree learning.
// Look at the advanced documentation on webpage for more details


//from the article pag.14
setParam: treeDepth=4.   
setParam: nodeSize=2.
setParam: numOfClauses=8.
