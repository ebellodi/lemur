#change N with {1..5}

java -cp ../Boostr/WILL.jar edu.wisc.cs.will.Boosting.Common.RunBoostedModels \
-target type \
-trees 20 \
-save -l -train ../data_sampl/trainN/ \
1> risN.txt 2>stderrN.txt
