#change N with {1..5}

java -cp ../Boostr/WILL.jar edu.wisc.cs.will.Boosting.Common.RunBoostedModels \
-aucJarPath "../Boostr" \
-target type \
-trees 20 \
-model ../data_sampl/trainN/models \
-save -i -test ../data_sampl/testN/ \
1>inferN.txt
