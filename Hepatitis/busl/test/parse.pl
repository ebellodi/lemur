:-source.

c:-
c('hep1.result'), %MLN con regole dai pesi < 0.01 eliminate
c('hep2.result'),
c('hep3.result'),
c('hep4.result'),
c('hep5.result').

	
c(File):-
	open(File,read,S),
	atom_concat(File,'.out',FileOut),
	open(FileOut,write,SO),
	read_sum(S,SO),
	close(S),
	close(SO).
	
read_sum(S,SO):-
	get_code(S,A),
	A\= -1,!,
	([A]=")"->
		put_char(SO,')'),
		put_char(SO,'.')
	;
		(A=10->
			put_code(SO,46),
			put_code(SO,10)
		;
			((A>=65,A=<90)->
				A1 is A+32,
				put_code(SO,A1)
			;
				put_code(SO,A)
			)
		)
	),	
	read_sum(S,SO).
	
read_sum(_S,_SO).	
	
