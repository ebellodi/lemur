%execution: yap -l run.pl
% change N with {1..5} (5-fold cv)

:-[aleph].
%:-use_module(library(aleph)).
:-read_all(hepN).  
:- set(minpos, 2).%V is a positive integer (default 1). Set a lower bound on the number of positive examples to be covered by an acceptable clause. If the best clause covers positive examples below this number, then it is not added to the current theory. This can be used to prevent Aleph from adding ground unit clauses to the theory (by setting the value to 2).

:- induce.
:- write_rules('hepN.rules').

