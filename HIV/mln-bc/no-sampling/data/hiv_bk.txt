// Add this to indicate that first letter caps is constant and first letter
// lower-case is a variable.
// Use quotes(") to make sure words beginning with lower case are considered
// as constants.
// To totally reverse the syntax,(e.g. prolog format), set
// usePrologVariables:true.

useStdLogicVariables: true.
setParam: minPosCoverage=3.


mode: g41L(+Model). 
mode: g67N(+Model). 
mode: g70R(+Model). 
mode: g210W(+Model). 
mode: g215FY(+Model). 
mode: g219EQ(+Model). 

