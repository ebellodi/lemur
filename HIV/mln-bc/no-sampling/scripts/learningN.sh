#change N with {1..5} (5-fold cv)

java -cp ../Boostr/WILL.jar edu.wisc.cs.will.Boosting.Common.RunBoostedModels \
-target g41L,g67N,g70R,g210W,g215FY,g219EQ \
-trees 20 \
-negPosRatio -1 \
-save -l -train ../data/trainN/ \
-mln -mlnClause \
-numMLNClause 3 \
-mlnClauseLen 3 \
1> risN.txt 2>stderrN.txt
