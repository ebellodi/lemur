#Change N with {1..5}

java -cp ../Boostr/WILL.jar edu.wisc.cs.will.Boosting.Common.RunBoostedModels \
-target g41L,g67N,g70R,g210W,g215FY,g219EQ \
-trees 20 \
-save -l -train ../data_sampl/trainN/ \
-mln -mlnClause \
-numMLNClause 3 \
-mlnClauseLen 3 \
1> risN.txt 2>stderrN.txt
