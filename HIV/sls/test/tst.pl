/* test according to the method of Davis and Goadrich*/
:-source.
:-consult(sls).

t:-
	open('cll.pl',write,S),
	open('res_atoms.csv',write,S1),
	set(single_var,false),		%******SETTAGGIO DI SINGLEVAR
	ta(hiv1,'hiv1_test.kb',S,1,S1),
	ta(hiv2,'hiv2_test.kb',S,2,S1),
	ta(hiv3,'hiv3_test.kb',S,3,S1),
	ta(hiv4,'hiv4_test.kb',S,4,S1),
	ta(hiv5,'hiv5_test.kb',S,5,S1),
	close(S),
	close(S1).

ta(File,FileKB,S,N,S1):-
	abolish_tset,
	generate_file_names(File,_FileKB,FileOut,FileL,FileLPAD),
	reconsult(FileL),
	format("file l~n"),
	flush_output,
	load_models(FileKB,_),
	format("filekb~n"),
	flush_output,
	find_ga(LG,Pos,Neg),
	format("find_ga~n"),
	flush_output,
        set(compiling,on),
	load(FileLPAD,Th,R),
	assert_all(Th),  %aggiunge le clausole nella forma espansa (es: ('41L'(_161309,_161308):-get_var_n(0,[],[0.2,0.8],_161333),equality(_161333,0,_161308)))
	assert_all(R),%aggiunge le clausole nella forma rule(0,['41L':0.2,'' :0.8],[])
	set(compiling,off),
	format("loaded LPAD~n"),
	flush_output,
%	listing(sameperson),
%	compute_CLL_atoms([\+samebib(_,class_1278,class_10)],0,0,CLL0,LG0),
	compute_CLL_atoms(LG,0,0,CLL0,LG0),
	retract_all(Th),
	retract_all(R),
	 retract(rule_n(_)),
         assert(rule_n(0)),
	format("compute_cll pre~n"),
	flush_output,
	keysort(LG0,LGOrd0),
	format(S,"cll(~a,pre,~d,~d,[",[File,Pos,Neg]),
	flush_output,
	writes(LGOrd0,S),
/* da usare sia quando si usano tutte le chiamate a ta per tutti i fold (resetta la kb), sia quando si testa un fold alla volta perchè il successivo load(FileOut) carica le regole ma non cancella le precedenti del load(lpad), cioè del file .cpl.*/

%DOPO L'APPRENDIMENTO
	set(compiling,on),
	load(FileOut,Th1,R1),	%FILE RULES:le regole con prob. 0 non vengono caricate, quelle con 1 sì.
	set(compiling,off),
	format("loaded Fileout~n"),
	assert_all(Th1),  
	assert_all(R1),
	flush_output,
%	compute_CLL_atoms([\+sameperson(_146392,avyarakovacheva,aviktordanchenko)],0,0,CLL1,LG1),
	compute_CLL_atoms(LG,0,0,CLL1,LG1),
	retract_all(Th1),
	retract_all(R1),
	format("compute_cll post~n~n"),
	flush_output,
	keysort(LG1,LGOrd1),
	format(S,"cll(~a,post,~d,~d,[",[File,Pos,Neg]),
	writes(LGOrd1,S),
%	format("CLL0 ~f CLL1 ~f~n",[CLL0,CLL1]),
	format(S1,"~a;~f;~f~n",[File,CLL0,CLL1]).

abolish_tset:-		%cancella le regole precedenti
	abolish('41L'/1), %DB=true+bdd+modello
	abolish('67N'/1),
	abolish('70R'/1),
	abolish('210W'/1),
	abolish('215FY'/1),
	abolish('219EQ'/1),
	abolish(neg/1).
abolish_rules:-
	retract(rule_n(_)),
	assert(rule_n(0)),
	abolish('41L'/3), %DB=true+bdd+modello
	abolish('67N'/3),
	abolish('70R'/3),
	abolish('210W'/3),
	abolish('215FY'/3),
	abolish('219EQ'/3).

writes([H-H1],S):-
	format(S,"~f - (~p)]).~n~n",[H,H1]).

writes([H-H1|T],S):-
	format(S,"~f - (~p),~n",[H,H1]),
	writes(T,S).



find_ga(LG,Pos,Neg):-
/*	setof(A,(Y,B)^sameperson(Y,A,B),LA),
	setof(B,(Y,A)^sameperson(Y,A,B),LB),
	append(LA,LB,L),
	remove_duplicates(L,L1),
	findall(sameperson(Y,A,B),(member(A,L1),member(B,L1),sameperson(Y,A,B)),LGP),
	findall(\+ sameperson(Y,A,B),(member(A,L1),member(B,L1),\+ sameperson(Y,A,B)),LGN),%genera tutti i possibili atomi negativi*/
	findall('41L'(Y),'41L'(Y),LGP1),
	findall('67N'(Y),'67N'(Y),LGP2),
	findall('70R'(Y),'70R'(Y),LGP3),
	findall('210W'(Y),'210W'(Y),LGP4),
	findall('215FY'(Y),'215FY'(Y),LGP5),
	findall('219EQ'(Y),'219EQ'(Y),LGP6),
	findall(\+'41L'(Y),neg('41L'(Y)),LGN1),%trova gli atomi negativi nel file kb*/
	findall(\+'67N'(Y),neg('67N'(Y)),LGN2),
	findall(\+'70R'(Y),neg('70R'(Y)),LGN3),
	findall(\+'210W'(Y),neg('210W'(Y)),LGN4),
	findall(\+'215FY'(Y),neg('215FY'(Y)),LGN5),
	findall(\+'219EQ'(Y),neg('219EQ'(Y)),LGN6),
	append([LGP1,LGP2,LGP3,LGP4,LGP5,LGP6],LGP),
	append([LGN1,LGN2,LGN3,LGN4,LGN5,LGN6],LGN),
	length(LGP,Pos),
	length(LGN,Neg),
/*	sample(50,LGN0,LGN),
	length(LGP,Pos),
	length(LGN,Neg),*/
	append(LGP,LGN,LG).


/*
find_ga(LG,Pos,Neg):-                     %preso da /home/rzf/CondSEM/trunk/uw-cse/prolognegext/ai/tst.pl
	setof(A,(Y,B,C)^(
	advisedby(Y,A,B);advisedby(Y,B,A);
	hasposition(Y,A,B);professor(Y,A);
	tempadvisedby(Y,A,B);tempadvisedby(Y,B,A);
	publication(Y,B,A);inphase(Y,A,B);yearsinprogram(Y,A,B);
	sameperson(Y,A,B);sameperson(Y,B,A);ta(Y,B,A,C);
	taught_by(Y,B,A,C);student(Y,A);projectmember(Y,B,A)),L1),
	findall(advisedby(Y,A,B),(member(A,L1),member(B,L1),advisedby(Y,A,B)),LGP),
	findall(\+ advisedby(Y,A,B),(member(A,L1),member(B,L1),\+ advisedby(Y,A,B)),LGN),%genera tutti i possibili atomi negativi
	length(LGP,Pos),
	length(LGN,Neg),
	format("lungh. LGP=~d~n",[Pos]),
	format("lungh. LGN=~d~n",[Neg]),
%	sample(50,LGN0,LGN),
%	length(LGP,Pos),
%	length(LGN,Neg),
	append(LGP,LGN,LG).
*/
write_list([],_).

write_list([H|T],S):-
	format(S,"~p~n",[H]),
	write_list(T,S).

generate_file_names(File,FileKB,FileOut,FileL,FileLPAD):-
    generate_file_name(File,".kb",FileKB),
    generate_file_name(File,".rules",FileOut),
    generate_file_name(File,".cpl",FileLPAD),
    generate_file_name(File,".l",FileL).


compute_CLL_atoms([],_N,CLL,CLL,[]):-!.

compute_CLL_atoms([\+ H|T],N,CLL0,CLL1,[PG- (\+ H)|T1]):-!,
%	format("~d~n",[N]),
/*arg(2,H,person193) ->
%write((\+ H)),nl,
% (arg(3,H,person13) ->
      N1 is N+1,
      compute_CLL_atoms(T,N1,CLL0,CLL1,T1)
 ;*/
	rule_n(NR),
        init_test(NR),
%	write((\+ H)),nl,
%	flush_output,
	get_node(H,BDD),!,
	ret_prob(BDD,PG),
%	format("- Prob ~f CLL0 ~f~n",[PG,CLL0]),
	end_test,!,
	PG1 is 1-PG,
/*		(PG1>=0->
			
		(PG=0.0->*/
	(PG1=:=0.0->
			CLL2 is CLL0-10
%			format("Neg PG=0 CLL=~f~n",[CLL2])
		;
			CLL2 is CLL0+ log(PG1)
%			format("Neg ~f CLL=~f~n",[PG,CLL2])
	),
/*	;
		CLL2=CLL0),*/
	N1 is N+1,
	compute_CLL_atoms(T,N1,CLL2,CLL1,T1).	

compute_CLL_atoms([H|T],N,CLL0,CLL1,[PG-H|T1]):-
%	format("~d~n",[N]),
	rule_n(NR),
%	(NR =\= 559 ->	format("NR=~d~n",[NR])),
 	init_test(NR),
%	write(H),nl,
%	flush_output,
	get_node(H,BDD),!,
%	format("getnode~n"),
%	flush_output,
	ret_prob(BDD,PG),
	end_test,!,
%	format("+ Prob ~f CLL0 ~f~n",[PG,CLL0]),
	(PG=:=0.0->
			CLL2 is CLL0-10
%			format("Pos PG=0 CLL=~f~n",[CLL2])
		;	
			CLL2 is CLL0+ log(PG)
%			format("Pos ~f CLL=~f~n",[PG,CLL2])
	),
	N1 is N+1,
	compute_CLL_atoms(T,N1,CLL2,CLL1,T1).		
