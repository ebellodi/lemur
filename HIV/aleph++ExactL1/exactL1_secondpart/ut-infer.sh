#change M with {1..5}

ut-infer -i hivM1_weight.mln -e hiv1_test.db -r hivM1.result -q g41L -exact > inferM1.log
ut-infer -i hivM2_weight.mln -e hiv2_test.db -r hivM2.result -q g67N -exact > inferM2.log
ut-infer -i hivM3_weight.mln -e hiv3_test.db -r hivM3.result -q g70R -exact > inferM3.log
ut-infer -i hivM4_weight.mln -e hiv4_test.db -r hivM4.result -q g210W -exact > inferM4.log
ut-infer -i hivM5_weight.mln -e hiv5_test.db -r hivM5.result -q g215FY -exact > inferM5.log
ut-infer -i hivM6_weight.mln -e hiv6_test.db -r hivM6.result -q g219EQ -exact > inferM6.log





