
:-source.

c:-
	open('cll.pl',write,S),
	open('res_atoms.csv',write,S1),
	reconsult(hiv1_test),  %file hiv1_test.pl
	c('hiv11.result',S,S1),
	c('hiv12.result',S,S1),
	c('hiv13.result',S,S1),
	c('hiv14.result',S,S1),
	c('hiv15.result',S,S1),
	c('hiv16.result',S,S1),
	reconsult(hiv2_test),
	c('hiv21.result',S,S1),
	c('hiv22.result',S,S1),
	c('hiv23.result',S,S1),
	c('hiv24.result',S,S1),
	c('hiv25.result',S,S1),
	c('hiv26.result',S,S1),
	reconsult(hiv3_test),
	c('hiv31.result',S,S1),
	c('hiv32.result',S,S1),
	c('hiv33.result',S,S1),
	c('hiv34.result',S,S1),
	c('hiv35.result',S,S1),
	c('hiv36.result',S,S1),
	reconsult(hiv4_test),
	c('hiv41.result',S,S1),
	c('hiv42.result',S,S1),
	c('hiv43.result',S,S1),
	c('hiv44.result',S,S1),
	c('hiv45.result',S,S1),
	c('hiv46.result',S,S1),
	reconsult(hiv5_test),
	c('hiv51.result',S,S1),
	c('hiv52.result',S,S1),
	c('hiv53.result',S,S1),
	c('hiv54.result',S,S1),
	c('hiv55.result',S,S1),
	c('hiv56.result',S,S1).




/*	reconsult(ai),
	c('ai.result',S,S1),
	reconsult(graphics),
	c('graphics.result',S,S1),
	reconsult(language),
	c('language.result',S,S1),
	reconsult(systems),
	c('systems.result',S,S1),
	reconsult(theory),
	c('theory.result',S,S1).*/
	
writes([H-H1],S):-
	format(S,"~f - (~p)]).~n~n",[H,H1]).

writes([H-H1|T],S):-
	format(S,"~f - (~p),~n",[H,H1]),
	writes(T,S).

c(File,S,S1):-
	atom_concat(File,'.out',File1),
	open(File1,read,SI),
	read_sum(SI,0,N,0,Pos,0,Neg,0,Sum,[],L),
	keysort(L,L1),
	format(S,"cll('~a',post,~d,~d,[",[File,Pos,Neg]),
	writes(L1,S),
	format(S1,"~f~n",[Sum]),
	close(SI).

read_sum(S,N,N2,Pos0,Pos1,Neg0,Neg1,S0,S1,L0,[P-A1|L1]):-
	read(S,A),
	A\=end_of_file,!,
	read(S,P),
	(call(A)->
		A1=A,
		Pos2 is Pos0+1,
		Neg2 is Neg0
	;
		A1=(\+ A),
		Pos2 is Pos0,
		Neg2 is Neg0+1
	),
	LP is log(P),
	S2 is S0 + LP,
	N1 is N+1,	
	read_sum(S,N1,N2,Pos2,Pos1,Neg2,Neg1,S2,S1,L0,L1).
	
read_sum(_S,N,N,Pos,Pos,Neg,Neg,S,S,L,L).	
	
read_db(S,SO):-
	read_line(S,L,End),
	(L=[97,100|_]->
		true
	;
		format(SO,"~s",[L])
	),
	(End=f->
		read_db(S,SO)
	;
		true
	).
	
read_db(_S,_SO).		

read_line(S,[A1|T],End):-
	get_code(S,A),
	(A= -1->
		A1=10,
		T=[],
		End=t
	;
		(A=10->
			A1=10,
			T=[],
			End=f
		;
			A1=A,
			read_line(S,T,End)
		)
	).
	
read_line(_S,_SO).		
		
	
