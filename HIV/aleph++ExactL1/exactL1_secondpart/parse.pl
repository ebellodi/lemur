:-source.


c:-
	c('hiv11.result'),
	c('hiv12.result'),
	c('hiv13.result'),
	c('hiv14.result'),
	c('hiv15.result'),
	c('hiv16.result'),
	c('hiv21.result'),
	c('hiv22.result'),
	c('hiv23.result'),
	c('hiv24.result'),
	c('hiv25.result'),
	c('hiv26.result'),
	c('hiv31.result'),
	c('hiv32.result'),
	c('hiv33.result'),
	c('hiv34.result'),
	c('hiv35.result'),
	c('hiv36.result'),
	c('hiv41.result'),
	c('hiv42.result'),
	c('hiv43.result'),
	c('hiv44.result'),
	c('hiv45.result'),
	c('hiv46.result'),
	c('hiv51.result'),
	c('hiv52.result'),
	c('hiv53.result'),
	c('hiv54.result'),
	c('hiv55.result'),
	c('hiv56.result').

c(File):-
	open(File,read,S),
	atom_concat(File,'.out',FileOut),
	open(FileOut,write,SO),
	read_sum(S,SO),
	close(S),
	close(SO).
	
read_sum(S,SO):-
	get_code(S,A),
	A\= -1,!,
	([A]=")"->
		put_char(SO,')'),
		put_char(SO,'.')
	;
		(A=10->
			put_code(SO,46),
			put_code(SO,10)
		;
			((A>=65,A=<90)->
				A1 is A+32,
				put_code(SO,A1)
			;
				put_code(SO,A)
			)
		)
	),	
	read_sum(S,SO).
	
read_sum(_S,_SO).	
	
