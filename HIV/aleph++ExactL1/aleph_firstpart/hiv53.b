:- modeh(1,'70R'(+m)).   %target

:- modeb(1,'67N'(+m)). 
:- modeb(1,'41L'(+m)). 
:- modeb(1,'210W'(+m)). 
:- modeb(1,'215FY'(+m)). 
:- modeb(1,'219EQ'(+m)). 

:- determination('70R'/1,'67N'/1).
:- determination('70R'/1,'41L'/1).
:- determination('70R'/1,'211W'/1).
:- determination('70R'/1,'215FY'/1).
:- determination('70R'/1,'219EQ'/1).

41L(m5).
41L(m14).
41L(m16).
41L(m23).
41L(m28).
41L(m36).
41L(m42).
41L(m43).
41L(m49).
41L(m51).
41L(m52).
41L(m57).
41L(m60).
41L(m63).
41L(m64).
41L(m68).
41L(m70).
41L(m71).
41L(m73).
41L(m78).
41L(m80).
41L(m81).
41L(m85).
41L(m89).
41L(m91).
41L(m102).
41L(m104).
41L(m105).
41L(m106).
41L(m108).
41L(m112).
41L(m113).
41L(m120).
41L(m122).
41L(m125).
41L(m129).
41L(m130).
41L(m131).
41L(m140).
41L(m141).
41L(m144).
41L(m148).
41L(m157).
41L(m167).
41L(m175).
41L(m177).
41L(m179).
41L(m185).
41L(m186).
41L(m189).
41L(m193).
41L(m204).
41L(m206).
41L(m207).
41L(m221).
41L(m222).
41L(m224).
41L(m225).
41L(m228).
41L(m235).
41L(m236).
41L(m238).
41L(m239).
41L(m245).
41L(m249).
41L(m251).
41L(m253).
41L(m256).
41L(m257).
41L(m258).
41L(m267).
41L(m270).
41L(m271).
41L(m284).
41L(m285).
41L(m289).
67N(m5).
67N(m6).
67N(m7).
67N(m9).
67N(m16).
67N(m17).
67N(m21).
67N(m24).
67N(m35).
67N(m36).
67N(m38).
67N(m49).
67N(m52).
67N(m54).
67N(m59).
67N(m60).
67N(m62).
67N(m67).
67N(m70).
67N(m71).
67N(m74).
67N(m78).
67N(m79).
67N(m80).
67N(m83).
67N(m85).
67N(m90).
67N(m91).
67N(m104).
67N(m106).
67N(m107).
67N(m112).
67N(m113).
67N(m117).
67N(m124).
67N(m125).
67N(m130).
67N(m131).
67N(m145).
67N(m147).
67N(m166).
67N(m172).
67N(m174).
67N(m186).
67N(m191).
67N(m193).
67N(m199).
67N(m211).
67N(m212).
67N(m217).
67N(m219).
67N(m221).
67N(m222).
67N(m224).
67N(m231).
67N(m237).
67N(m246).
67N(m251).
67N(m253).
67N(m258).
67N(m263).
67N(m264).
67N(m267).
67N(m270).
67N(m272).
67N(m273).
67N(m291).
210W(m14).
210W(m28).
210W(m42).
210W(m60).
210W(m63).
210W(m64).
210W(m71).
210W(m81).
210W(m89).
210W(m91).
210W(m102).
210W(m104).
210W(m106).
210W(m108).
210W(m120).
210W(m122).
210W(m125).
210W(m129).
210W(m130).
210W(m142).
210W(m144).
210W(m162).
210W(m166).
210W(m177).
210W(m189).
210W(m203).
210W(m204).
210W(m207).
210W(m213).
210W(m222).
210W(m224).
210W(m237).
210W(m238).
210W(m243).
210W(m258).
210W(m267).
210W(m271).
210W(m284).
210W(m285).
215FY(m2).
215FY(m5).
215FY(m9).
215FY(m10).
215FY(m14).
215FY(m16).
215FY(m21).
215FY(m23).
215FY(m24).
215FY(m28).
215FY(m30).
215FY(m35).
215FY(m42).
215FY(m43).
215FY(m49).
215FY(m51).
215FY(m57).
215FY(m59).
215FY(m60).
215FY(m63).
215FY(m64).
215FY(m65).
215FY(m67).
215FY(m68).
215FY(m70).
215FY(m71).
215FY(m72).
215FY(m73).
215FY(m78).
215FY(m80).
215FY(m81).
215FY(m85).
215FY(m89).
215FY(m91).
215FY(m102).
215FY(m104).
215FY(m105).
215FY(m106).
215FY(m107).
215FY(m108).
215FY(m112).
215FY(m113).
215FY(m117).
215FY(m120).
215FY(m121).
215FY(m122).
215FY(m123).
215FY(m125).
215FY(m126).
215FY(m127).
215FY(m129).
215FY(m130).
215FY(m131).
215FY(m133).
215FY(m139).
215FY(m140).
215FY(m141).
215FY(m142).
215FY(m144).
215FY(m147).
215FY(m148).
215FY(m156).
215FY(m157).
215FY(m162).
215FY(m164).
215FY(m165).
215FY(m166).
215FY(m167).
215FY(m168).
215FY(m169).
215FY(m170).
215FY(m173).
215FY(m174).
215FY(m177).
215FY(m179).
215FY(m185).
215FY(m189).
215FY(m192).
215FY(m193).
215FY(m197).
215FY(m203).
215FY(m204).
215FY(m205).
215FY(m206).
215FY(m207).
215FY(m208).
215FY(m213).
215FY(m216).
215FY(m218).
215FY(m221).
215FY(m222).
215FY(m224).
215FY(m225).
215FY(m228).
215FY(m231).
215FY(m235).
215FY(m236).
215FY(m237).
215FY(m238).
215FY(m243).
215FY(m245).
215FY(m249).
215FY(m251).
215FY(m253).
215FY(m256).
215FY(m257).
215FY(m258).
215FY(m267).
215FY(m270).
215FY(m271).
215FY(m272).
215FY(m284).
215FY(m285).
215FY(m289).
215FY(m290).
219EQ(m5).
219EQ(m7).
219EQ(m9).
219EQ(m10).
219EQ(m16).
219EQ(m17).
219EQ(m21).
219EQ(m24).
219EQ(m30).
219EQ(m35).
219EQ(m38).
219EQ(m41).
219EQ(m49).
219EQ(m52).
219EQ(m54).
219EQ(m57).
219EQ(m59).
219EQ(m62).
219EQ(m67).
219EQ(m74).
219EQ(m79).
219EQ(m83).
219EQ(m85).
219EQ(m106).
219EQ(m107).
219EQ(m112).
219EQ(m117).
219EQ(m123).
219EQ(m124).
219EQ(m126).
219EQ(m137).
219EQ(m145).
219EQ(m165).
219EQ(m169).
219EQ(m172).
219EQ(m174).
219EQ(m183).
219EQ(m191).
219EQ(m192).
219EQ(m193).
219EQ(m194).
219EQ(m196).
219EQ(m199).
219EQ(m203).
219EQ(m211).
219EQ(m212).
219EQ(m217).
219EQ(m219).
219EQ(m221).
219EQ(m222).
219EQ(m231).
219EQ(m243).
219EQ(m246).
219EQ(m251).
219EQ(m254).
219EQ(m260).
219EQ(m263).
219EQ(m264).
219EQ(m269).
219EQ(m291).
