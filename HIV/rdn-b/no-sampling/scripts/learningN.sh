#script for training on train_neg.txt and train_pos.txt with RDN-B
#change N with {1..5} (5-fold cv)

java -cp ../Boostr/WILL.jar edu.wisc.cs.will.Boosting.Common.RunBoostedModels \
-target g41L,g67N,g70R,g210W,g215FY,g219EQ \
-trees 20 \
-negPosRatio -1 \
-save -l -train ../data/trainN/ \
1> risN.txt 2>stderrN.txt
