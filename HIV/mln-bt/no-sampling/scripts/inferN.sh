#change N with {1..5} (5-fold cv)

java -cp ../Boostr/WILL.jar edu.wisc.cs.will.Boosting.Common.RunBoostedModels \
-aucJarPath "../Boostr" \
-target g41L,g67N,g70R,g210W,g215FY,g219EQ \
-trees 20 \
-model ../data/trainN/models \
-testNegPosRatio -1 \
-save -i -test ../data/testN/ \
1>inferN.txt
