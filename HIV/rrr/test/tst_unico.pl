/* test according to the method of Davis and Goadrich*/
%il file cll.pl contiene un unico fatto con tutti gli esempi dai vari fold ordinati per probabilità. poi si applica tstpr_unico.pl per avere una curva roc complessiva per il dataset anzichè per il singolo fold.

:-source.
:-consult(slipcover).

go:- t([1,2,3,4,5]). %5 folds

%builds cll.pl for fold H
%'hivH_test.kb' for fold H; 
%hiv1.l: bias for run 1 of all folds (target pred=41L) - hiv6.l: bias for run 6 of all folds (target pred=219EQ)
%6 runs (6 target pred.) for every fold
t([]):-!.
t([H|T]):-
	format("~n*******FOLD~d******~n",[H]),
	%generate_cll_file(H,FileCll),  
	generate_file_name1(H,"cll_unico_f",".pl",FileCll),
	open(FileCll,write,S),
	set(single_var,false),	%hiv11-16
	ta(hiv1,H,P1,N1,L1),	%P,N: pos e neg target pred.
	ta(hiv2,H,P2,N2,L2),
	ta(hiv3,H,P3,N3,L3),
	ta(hiv4,H,P4,N4,L4),
	ta(hiv5,H,P5,N5,L5),
	ta(hiv6,H,P6,N6,L6),
	P is P1+P2+P3+P4+P5+P6,
	N is N1+N2+N3+N4+N5+N6,
	append([L1,L2,L3,L4,L5,L6],Ltot),
	ta1(H,Ltot,P,N,S),
	close(S),
	t(T).



ta(File,H/*FileKB*/,Pos,Neg,LG1):-	%H=fold
	abolish_tset,
	generate_file_names1(H,File,FileOut,FileL,FileKbtest),
	reconsult(FileL), %hivH.l
	format("file ~a~n",[FileL]),
	flush_output,
	load_models(FileKbtest,_),
	format("file ~a~n",[FileKbtest]),
	flush_output,
	find_ga(LG,Pos,Neg),
	%format("find_ga~n"),
	flush_output,
/* da usare sia quando si usano tutte le chiamate a ta per tutti i fold (resetta la kb), sia quando si testa un fold alla volta perchè il successivo load(FileOut) carica le regole ma non cancella le precedenti del load(lpad), cioè del file .cpl.*/

%DOPO L'APPRENDIMENTO
	set(compiling,on),
	load(FileOut,Th1,R1),  %hiv11.rules-->hiv16.rules 1=H
	%FILE RULES:le regole con prob. 0 non vengono caricate
	set(compiling,off),
	format("theory ~a~n",[FileOut]),
	assert_all(Th1),  
	assert_all(R1),
	flush_output,
	compute_CLL_atoms(LG,0,0,_CLL1,LG1),
	retract_all(Th1),
	retract_all(R1),
	flush_output.
	
ta1(H,Ltot,Pos,Neg,S):-
	name(H,HString),
	append("hiv",HString,FileString),
	name(Fold,FileString),
	keysort(Ltot,LGOrd1),
	format(S,"cll(~a,post,~d,~d,[",[Fold,Pos,Neg]),
	writes(LGOrd1,S).


abolish_tset:-		%cancella le regole precedenti
	abolish('41L'/1), %DB=true+bdd+modello
	abolish('67N'/1),
	abolish('70R'/1),
	abolish('210W'/1),
	abolish('215FY'/1),
	abolish('219EQ'/1),
	abolish(neg/1).
abolish_rules:-
	retract(rule_n(_)),
	assert(rule_n(0)),
	abolish('41L'/3), %DB=true+bdd+modello
	abolish('67N'/3),
	abolish('70R'/3),
	abolish('210W'/3),
	abolish('215FY'/3),
	abolish('219EQ'/3).

writes([H-H1],S):-
	format(S,"~f - (~p)]).~n~n",[H,H1]).

writes([H-H1|T],S):-
	format(S,"~f - (~p),~n",[H,H1]),
	writes(T,S).



find_ga(LG,Pos,Neg):-
	findall('41L'(Y),'41L'(Y),LGP1),
	findall('67N'(Y),'67N'(Y),LGP2),
	findall('70R'(Y),'70R'(Y),LGP3),
	findall('210W'(Y),'210W'(Y),LGP4),
	findall('215FY'(Y),'215FY'(Y),LGP5),
	findall('219EQ'(Y),'219EQ'(Y),LGP6),
	findall(\+'41L'(Y),neg('41L'(Y)),LGN1),%trova gli atomi negativi nel file kb
	findall(\+'67N'(Y),neg('67N'(Y)),LGN2),
	findall(\+'70R'(Y),neg('70R'(Y)),LGN3),
	findall(\+'210W'(Y),neg('210W'(Y)),LGN4),
	findall(\+'215FY'(Y),neg('215FY'(Y)),LGN5),
	findall(\+'219EQ'(Y),neg('219EQ'(Y)),LGN6),
	append([LGP1,LGP2,LGP3,LGP4,LGP5,LGP6],LGP),
	append([LGN1,LGN2,LGN3,LGN4,LGN5,LGN6],LGN),
	length(LGP,Pos),
	length(LGN,Neg),
	append(LGP,LGN,LG).


write_list([],_).

write_list([H|T],S):-
	format(S,"~p~n",[H]),
	write_list(T,S).



generate_file_names1(H,File,FileOut,FileL,FileKbtest):-
	generate_file_name(File,".l",FileL),  %File=hiv1..6
	generate_file_name(H,File,"_test.rules",FileOut),
	generate_file_name1(H,"hiv","_test.kb",FileKbtest).


generate_file_name(H,File,Ext,FileExt):-
  name(File,FileString), %File=hiv1
  name(H,FoldString),
  nth0(3, List, FoldString, FileString), %output=List
  flatten(List, FlattenedList),
  %name(FileFoldString,List),
  %  append(FileString,FoldString,FileFoldString),
  append(FlattenedList,Ext,FileStringExt),
  name(FileExt,FileStringExt).

generate_file_name1(H,File,Ext,FileExt):-
  name(H,HString),
  length(File,N),
  nth0(N, List, HString, File),
  flatten(List, FlattenedList), %FlattenedList=file senza estensione
  append(FlattenedList,Ext,FileStringExt),
  name(FileExt,FileStringExt).


compute_CLL_atoms([],_N,CLL,CLL,[]):-!.

compute_CLL_atoms([\+ H|T],N,CLL0,CLL1,[PG- (\+ H)|T1]):-!,
	rule_n(NR),
        init_test(NR),
%	write((\+ H)),nl,
%	flush_output,
	get_node(H,BDD),!,
	ret_prob(BDD,PG),
	end_test,!,
	PG1 is 1-PG,
	(PG1=:=0.0->
			CLL2 is CLL0-10
		;
			CLL2 is CLL0+ log(PG1)
	),
	N1 is N+1,
	compute_CLL_atoms(T,N1,CLL2,CLL1,T1).	

compute_CLL_atoms([H|T],N,CLL0,CLL1,[PG-H|T1]):-
%	format("~d~n",[N]),
	rule_n(NR),
 	init_test(NR),
%	write(H),nl,
%	flush_output,
	get_node(H,BDD),!,
	ret_prob(BDD,PG),
	end_test,!,
	(PG=:=0.0->
			CLL2 is CLL0-10
		;	
			CLL2 is CLL0+ log(PG)
	),
	N1 is N+1,
	compute_CLL_atoms(T,N1,CLL2,CLL1,T1).		
