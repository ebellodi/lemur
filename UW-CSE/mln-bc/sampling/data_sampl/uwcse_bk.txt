
// Add this to indicate that first letter caps is constant and first letter lower-case is a variable.
// Use quotes(") to make sure words beginning with lower case are considered as constants.
// To totally reverse the syntax,(e.g. prolog format), set
// usePrologVariables:true.
useStdLogicVariables: true.
setParam: minPosCoverage=3.


// Set the mode for ILP/Tree search.
// + indicates that this variable must already be grounded/appear before in the clause/tree.
// - indicates that a new variable could be used. 

mode: advisedby(+Person, +Person). 
mode: advisedby(+Person, -Person).
mode: advisedby(-Person, +Person).

mode: publication(+Pub, -Person).
mode: publication(-Pub, +Person).
mode: professor(+Person).
mode: student(+Person).
mode: courseLevel(+Course, -Level).
mode: courseLevel(-Course, +Level).
mode: courseLevel(+Course, #Level).
mode: hasPosition(+Person, -Position).
mode: hasPosition(+Person, #Position).
mode: inPhase(+Person, -Phase).
mode: inPhase(+Person, #Phase).
mode: tempAdvisedBy(+Person, -Person).
mode: tempAdvisedBy(-Person, +Person).
mode: yearsInProgram(+Person, -Year).
mode: yearsInProgram(+Person, #Year).

mode: taughtBy(+Course, -Person, -Quarter).
mode: taughtBy(-Course, +Person, -Quarter).
mode: taughtBy(+Course, +Person, -Quarter).

mode: ta(+Course, -Person, -Quarter).
mode: ta(-Course, +Person, -Quarter).
mode: ta(+Course, +Person, -Quarter).


