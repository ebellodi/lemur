/* test according to the method of Davis and Goadrich*/

%USARE YAP 5.1.4 PER LANCIARE IL TEST, LA VERSIONE 6 Dà ERRORE DI ACCESS TO ILLEGAL ADDRESS
:-source.
:-consult(slipcover_lemur).

t1(FileOut,N1,T1,F1):-
	open(FileOut,write,S), %AI fold 
	set(single_var,false),			
	taa(N1,T1,F1,S,1,S1),
	close(S).

t(FileOut,N1,T1,F1,N2,T2,F2,N3,T3,F3,N4,T4,F4):-
	open(FileOut,write,S), %AI fold 
	set(single_var,true),			
	taa(N1,T1,F1,S,1,S1),
	taa(N2,T2,F2,S,2,S1),
	taa(N3,T3,F3,S,3,S1),
	taa(N4,T4,F4,S,4,S1),
	close(S).

taa(Model,File,FileKB,S,N,S1):-
	abolish_tset,
	generate_file_names(File,_FileKB,FileOut,FileL,FileLPAD),
	format("~n~a~n",[File]),
	reconsult(FileL),
	format("file l~n"),
	load_models(FileKB,DB),
	format("filekb~n"),
	find_ex(DB,LG,Pos,Neg),

/*	set(compiling,on),
	load(FileLPAD,Th,R),		%file CPL
	set(compiling,off),
	format("loaded LPAD~n"),
	assert_all(Th),  
	assert_all(R),
	flush_output,

	compute_CLL_atoms(LG,0,0,CLL0,LG0),
	format("compute_cll pre~n"),
	retract_all(Th),
	retract_all(R),
	retract(rule_n(_)),
	assert(rule_n(0)),
	keysort(LG0,LGOrd0),
	format(S,"cll(~a,pre,~d,~d,[",[File,Pos,Neg]),
	writes(LGOrd0,S),
%	abolish_rules,
*/
	set(compiling,on),
	load(FileOut,Th1,R1),	%FILE RULES
	format("loaded rules~n"),
	set(compiling,off),
	assert_all(Th1),  
	assert_all(R1),
	flush_output,
	compute_CLL_atoms(LG,0,0,CLL1,LG1),
	format("compute_cll post~n~n"),
	retract_all(Th1),
	retract_all(R1),
	keysort(LG1,LGOrd1),
%	format(S,"cll(~a,post,~d,~d,[",[File,Pos,Neg]),
	writes(LGOrd1,S),
	format("CLL ~a;~f~n",[File,CLL1]).

abolish_tset:-		%cancella il file kb precedente(fatti con 1 argomento in +, il modello)
	abolish(advisedby/3), 
	abolish(taughtby/4),
	abolish(courselevel/3),
	abolish(hasposition/3),
	abolish(inphase/3),
	abolish(tempadvisedby/3),
	abolish(yearsinprogram/3),
	abolish(ta/4),
	abolish(professor/2),
	abolish(student/2),
	abolish(projectmember/3),
	abolish(sameperson/3),
	abolish(sametitle/3),
	abolish(samecourse/3),
	abolish(samephase/3),
	abolish(sameposition/3),
	abolish(samelevel/3),
	abolish(samequarter/3),
	abolish(sameproject/3),
	abolish(publication/3),
	abolish(neg/1).

		
writes([H-H1],S):-
	H1 = advisedby(_,_,_),
	!,
	format(S,"~f 1~n",[H]).
writes([H-H1],S):-
	format(S,"~f 0~n",[H]).

writes([H-H1|T],S):-
	H1 = advisedby(_,_,_),
	!,
	format(S,"~f 1~n",[H]),
	writes(T,S).
writes([H-H1|T],S):-
	format(S,"~f 0~n",[H]),	
	writes(T,S).


find_ga(M,LG,Pos,Neg):-                   
	setof(A,(B,C)^(
	advisedby(M,A,B);advisedby(M,B,A);
	hasposition(M,A,B);professor(M,A);
	tempadvisedby(M,A,B);tempadvisedby(M,B,A);
	publication(M,B,A);inphase(M,A,B);yearsinprogram(M,A,B);
	sameperson(M,A,B);sameperson(M,B,A);ta(M,B,A,C);
	taughtby(M,B,A,C);student(M,A);projectmember(M,B,A)),L1),
	findall(advisedby(M,A,B),(member(A,L1),member(B,L1),advisedby(M,A,B)),LGP),
	findall(\+ advisedby(M,A,B),(member(A,L1),member(B,L1),\+ advisedby(M,A,B)),LGN),%generates all possible  negative atoms
	length(LGP,Pos),
	length(LGN,Neg),
%	sample(50,LGN0,LGN),
%	length(LGP,Pos),
%	length(LGN,Neg),
	append(LGP,LGN,LG).

write_list([],_).

write_list([H|T],S):-
	format(S,"~p~n",[H]),
	write_list(T,S).

generate_file_names(File,FileKB,FileOut,FileL,FileLPAD):-
    generate_file_name(File,".kb",FileKB),
    generate_file_name(File,".rules",FileOut),
    generate_file_name(File,".cpl",FileLPAD),
    generate_file_name(File,".l",FileL),!.

compute_CLL_atoms([],_N,CLL,CLL,[]):-!.


compute_CLL_atoms([\+ H|T],N,CLL0,CLL1,[PG- (\+ H)|T1]):-!,
	rule_n(NR),
        init_test(NR),
	write((\+ H)),
	get_node(H,BDD),!,
	ret_prob(BDD,PG),
  write(PG),nl,
	end_test,!,
		%
		PG1 is 1-PG,
			
		(PG1=:=0.0->
			CLL2 is CLL0-10
		;
			CLL2 is CLL0+ log(PG1)
		),		
		N1 is N+1,
	compute_CLL_atoms(T,N1,CLL2,CLL1,T1).	

compute_CLL_atoms([H|T],N,CLL0,CLL1,[PG-H|T1]):-
       	rule_n(NR),
 	init_test(NR),
	write(H),
	get_node(H,BDD),!,
	ret_prob(BDD,PG),
	end_test,!,
		(PG=:=0.0->
			CLL2 is CLL0-10,
			format("-inf~n",[])
		;	
			CLL2 is CLL0+ log(PG),
			format("~f~n",[PG])
		),
		N1 is N+1,
	compute_CLL_atoms(T,N1,CLL2,CLL1,T1).		


find_ex(DB,LG,Pos,Neg):-
  findall(P/A,output(P/A),LP),
%  setting(neg_ex,cw),
  find_ex_pred(LP,DB,[],LG,0,Pos,0,Neg).


find_ex_pred([],_DB,LG,LG,Pos,Pos,Neg,Neg).

find_ex_pred([P/A|T],DB,LG0,LG,Pos0,Pos,Neg0,Neg):-
  functor(At,P,A),
  find_ex_db(DB,At,LG0,LG1,Pos0,Pos1,Neg0,Neg1),
  find_ex_pred(T,DB,LG1,LG,Pos1,Pos,Neg1,Neg).

find_ex_db([],_At,LG,LG,Pos,Pos,Neg,Neg).

find_ex_db([H|T],At,LG0,LG,Pos0,Pos,Neg0,Neg):-
  At=..[P|L],
  At1=..[P,H|L],
  findall(At1,At1,LP),
  findall(\+ At1,neg(At1),LN),
  length(LP,NP),
  length(LN,NN),
  append([LG0,LP,LN],LG1),
  Pos1 is Pos0+NP,
  Neg1 is Neg0+NN,
  find_ex_db(T,At,LG1,LG,Pos1,Pos,Neg1,Neg).


find_ex_pred_cw([],_DB,LG,LG,Pos,Pos,Neg,Neg).

find_ex_pred_cw([P/A|T],DB,LG0,LG,Pos0,Pos,Neg0,Neg):-
  functor(At,P,A),
  get_types(At,Types),
  remove_duplicates(Types,Types1),
  find_ex_db_cw(DB,At,Types1,LG0,LG1,Pos0,Pos1,Neg0,Neg1),
  find_ex_pred_cw(T,DB,LG1,LG,Pos1,Pos,Neg1,Neg).

get_types(At,Types):-
  modeh(_,At),
  At=..[_|Args],
  get_args(Args,Types).

get_args([],[]).

get_args([+H|T],[H|T1]):-!,
  get_args(T,T1).

get_args([-H|T],[H|T1]):-!,
  get_args(T,T1).

get_args([#H|T],[H|T1]):-!,
  get_args(T,T1).

get_args([-#H|T],[H|T1]):-!,
  get_args(T,T1).

get_args([H|T],[H|T1]):-
  get_args(T,T1).




get_constants([],_M,[]).

get_constants([Type|T],M,[(Type,Co)|C]):-
  find_pred_using_type(Type,LP),
  find_constants(LP,M,[],Co),
  get_constants(T,M,C).

find_pred_using_type(T,L):-
  setof((P,Ar,A),pred_type(T,P,Ar,A),L).

pred_type(T,P,Ar,A):-
  modeh(_,S),
  S=..[P|Args],
  length(Args,Ar),
  scan_args(Args,T,1,A).

pred_type(T,P,Ar,A):-
  modeb(_,S),
  S=..[P|Args],
  length(Args,Ar),
  scan_args(Args,T,1,A).

scan_args([+T|_],T,A,A):-!.

scan_args([-T|_],T,A,A):-!.

scan_args([#T|_],T,A,A):-!.

scan_args([-#T|_],T,A,A):-!.

scan_args([_|Tail],T,A0,A):-
  A1 is A0+1,
  scan_args(Tail,T,A1,A).

find_constants([],_M,C,C).

find_constants([(P,Ar,A)|T],M,C0,C):-
  gen_goal(1,Ar,A,Args,ArgsNoV,V),
  G=..[P,M|Args],
  setof(V,ArgsNoV^G,LC),
  append(C0,LC,C1),
  remove_duplicates(C1,C2),
  find_constants(T,M,C2,C).


gen_goal(Arg,Ar,_A,[],[],_):-
  Arg =:= Ar+1,!.

gen_goal(A,Ar,A,[V|Args],ArgsNoV,V):-!,
  Arg1 is A+1,
  gen_goal(Arg1,Ar,A,Args,ArgsNoV,V).
  
gen_goal(Arg,Ar,A,[ArgV|Args],[ArgV|ArgsNoV],V):-
  Arg1 is Arg+1,
  gen_goal(Arg1,Ar,A,Args,ArgsNoV,V).
  


find_ex_db_cw([],_At,_Ty,LG,LG,Pos,Pos,Neg,Neg).

find_ex_db_cw([H|T],At,Types,LG0,LG,Pos0,Pos,Neg0,Neg):-
  get_constants(Types,H,C),
  At=..[P|L],
  get_types(At,TypesA),
  length(L,N),
  length(LN,N),
  At1=..[P,H|LN],
  findall(At1,At1,LP),
  setof(\+ At1,neg_ex(LN,TypesA,At1,C),LNeg),
  length(LP,NP),
  length(LNeg,NN),
  append([LG0,LP,LNeg],LG1),
  Pos1 is Pos0+NP,
  Neg1 is Neg0+NN,
  find_ex_db_cw(T,At,Types,LG1,LG,Pos1,Pos,Neg1,Neg).

neg_ex([],[],At1,_C):-
  \+ At1.

neg_ex([H|T],[HT|TT],At1,C):-
  member((HT,Co),C),
  member(H,Co),
  neg_ex(T,TT,At1,C).
