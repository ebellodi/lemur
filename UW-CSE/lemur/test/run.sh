yap -l test.pl -g "t1(test_ai,ai,ai_train,'ai.kb'),halt."
java -jar auc.jar test_ai list > ai.auclist.out 
echo "ai" >> Areas
grep Area ai.auclist.out >> Areas

yap -l test.pl -g "t1(test_graphics,graphics,graphics_train,'graphics.kb'),halt."
java -jar auc.jar test_graphics list > graphics.auclist.out 
echo "graphics" >> Areas
grep Area graphics.auclist.out >> Areas

yap -l test.pl -g "t1(test_language,language,language_train,'language.kb'),halt."
java -jar auc.jar test_language list > language.auclist.out 
echo "language" >> Areas
grep Area language.auclist.out >> Areas

yap -l test.pl -g "t1(test_systems,systems,systems_train,'systems.kb'),halt."
java -jar auc.jar test_systems list > systems.auclist.out 
echo "systems" >> Areas
grep Area systems.auclist.out >> Areas

yap -l test.pl -g "t1(test_theory,theory,theory_train,'theory.kb'),halt."
java -jar auc.jar test_theory list > theory.auclist.out 
echo "theory" >> Areas
grep Area theory.auclist.out >> Areas
