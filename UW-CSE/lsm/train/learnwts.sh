learnwts -i ai-rules.mln  -o structAI_noqE.mln  -t graphics.db,language.db,systems.db,theory.db -d -dCG true -ne advisedBy  1>ai-struct_noqE.log
learnwts -i graphics-rules.mln  -o structGR_noqE.mln  -t ai.db,language.db,systems.db,theory.db -d -dCG true -ne advisedBy  1>gr-struct_noqE.log
learnwts -i language-rules.mln  -o structLAN_noqE.mln  -t ai.db,graphics.db,systems.db,theory.db -d -dCG true -ne advisedBy  1>lan-struct_noqE.log
learnwts -i systems-rules.mln  -o structSYS_noqE.mln  -t ai.db,graphics.db,language.db,theory.db -d -dCG true -ne advisedBy  1>sys-struct_noqE.log
learnwts -i theory-rules.mln  -o structTHE_noqE.mln  -t ai.db,language.db,systems.db,graphics.db -d -dCG true -ne advisedBy  1>the-struct_noqE.log