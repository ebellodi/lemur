/* test according to the method of Davis and Goadrich*/


:-source.
:-consult(sls).

t:-
	open('cll.pl',write,S), %AI fold 
	open('res_atoms.csv',write,S1),  %replace with the other correspodent folds
/*	open('atoms_pos.pl',write,S0),
	close(S0),
	open('atoms_neg.pl',write,S2),
	close(S2),*/
	set(single_var,false),			
	ta(ai,ai_train,'ai.kb',S,1,S1),
	ta(graphics,graphics_train,'graphics.kb',S,2,S1),
	ta(language,language_train,'language.kb',S,3,S1),
	ta(systems,systems_train,'systems.kb',S,4,S1),
	ta(theory,theory_train,'theory.kb',S,5,S1),
	close(S),
	close(S1).

ta(Model,File,FileKB,S,N,S1):-
	abolish_tset,
	generate_file_names(File,_FileKB,FileOut,FileL,FileLPAD),
	format("~n~a~n",[File]),
	reconsult(FileL),
	 format("file l~n"),
	load_models(FileKB,_),
	format("filekb~n"),
	find_ga(Model,LG,Pos,Neg),
        set(compiling,on),
	load(FileLPAD,Th,R),		%file CPL
	set(compiling,off),
	 format("loaded LPAD~n"),
        assert_all(Th),  
	assert_all(R),
	flush_output,
	compute_CLL_atoms(LG,0,0,CLL0,LG0),
	format("compute_cll pre~n"),
        retract_all(Th),
	retract_all(R),
	retract(rule_n(_)),
	assert(rule_n(0)),
	keysort(LG0,LGOrd0),
	format(S,"cll(~a,pre,~d,~d,[",[File,Pos,Neg]),
	writes(LGOrd0,S),
%	abolish_rules,
	set(compiling,on),
	load(FileOut,Th1,R1),	%FILE RULES
	format("loaded rules~n"),
	set(compiling,off),
	assert_all(Th1),  
        assert_all(R1),
	flush_output,
	compute_CLL_atoms(LG,0,0,CLL1,LG1),
	  format("compute_cll post~n~n"),
        retract_all(Th1),
	retract_all(R1),
	keysort(LG1,LGOrd1),
	format(S,"cll(~a,post,~d,~d,[",[File,Pos,Neg]),
	writes(LGOrd1,S),
	format(S1,"~a;~f~f~n",[File,CLL0,CLL1]).

abolish_tset:-		%cancella il file kb precedente(fatti con 1 argomento in +, il modello)
	abolish(advisedby/3), 
	abolish(taughtby/4),
	abolish(courselevel/3),
	abolish(hasposition/3),
	abolish(inphase/3),
	abolish(tempadvisedby/3),
	abolish(yearsinprogram/3),
	abolish(ta/4),
	abolish(professor/2),
	abolish(student/2),
	abolish(projectmember/3),
	abolish(sameperson/3),
	abolish(sametitle/3),
	abolish(samecourse/3),
	abolish(samephase/3),
	abolish(sameposition/3),
	abolish(samelevel/3),
	abolish(samequarter/3),
	abolish(sameproject/3),
	abolish(publication/3),
	abolish(neg/1).

		
writes([H-H1],S):-
	format(S,"~f - (~p)]).~n~n",[H,H1]).

writes([H-H1|T],S):-
	format(S,"~f - (~p),~n",[H,H1]),
	writes(T,S).


find_ga(M,LG,Pos,Neg):-                   
	setof(A,(B,C)^(
	advisedby(M,A,B);advisedby(M,B,A);
	hasposition(M,A,B);professor(M,A);
	tempadvisedby(M,A,B);tempadvisedby(M,B,A);
	publication(M,B,A);inphase(M,A,B);yearsinprogram(M,A,B);
	sameperson(M,A,B);sameperson(M,B,A);ta(M,B,A,C);
	taughtby(M,B,A,C);student(M,A);projectmember(M,B,A)),L1),
	findall(advisedby(M,A,B),(member(A,L1),member(B,L1),advisedby(M,A,B)),LGP),
	findall(\+ advisedby(M,A,B),(member(A,L1),member(B,L1),\+ advisedby(M,A,B)),LGN),%generates all possible  negative atoms
	length(LGP,Pos),
	length(LGN,Neg),
%	sample(50,LGN0,LGN),
%	length(LGP,Pos),
%	length(LGN,Neg),
	append(LGP,LGN,LG).

write_list([],_).

write_list([H|T],S):-
	format(S,"~p~n",[H]),
	write_list(T,S).

generate_file_names(File,FileKB,FileOut,FileL,FileLPAD):-
    generate_file_name(File,".kb",FileKB),
    generate_file_name(File,".rules",FileOut),
    generate_file_name(File,".cpl",FileLPAD),
    generate_file_name(File,".l",FileL).

compute_CLL_atoms([],_N,CLL,CLL,[]):-!.


compute_CLL_atoms([\+ H|T],N,CLL0,CLL1,[PG- (\+ H)|T1]):-!,
	rule_n(NR),
        init_test(NR),
%	write((\+ H)),nl,
	get_node(H,BDD),!,
	ret_prob(BDD,PG),
	end_test,!,
		%
		PG1 is 1-PG,
			
		(PG1=:=0.0->
			CLL2 is CLL0-10
%			format("-inf~n",[])
		;
			CLL2 is CLL0+ log(PG1)
%			format("~f~n",[PG])
		),		
		N1 is N+1,
	compute_CLL_atoms(T,N1,CLL2,CLL1,T1).	

compute_CLL_atoms([H|T],N,CLL0,CLL1,[PG-H|T1]):-
       	rule_n(NR),
 	init_test(NR),
%	write(H),nl,
	get_node(H,BDD),!,
	ret_prob(BDD,PG),
	end_test,!,
		(PG=:=0.0->
			CLL2 is CLL0-10
%			format("-inf~n",[])
		;	
			CLL2 is CLL0+ log(PG)
%			format("~f~n",[PG])
		),
		N1 is N+1,
	compute_CLL_atoms(T,N1,CLL2,CLL1,T1).		

