#change N with {1..5} (5-fold cv)

java -cp ../Boostr/WILL.jar edu.wisc.cs.will.Boosting.Common.RunBoostedModels \
-aucJarPath "../Boostr" \
-target advisedby \
-trees 20 \
-model ../data/trainN/models \
-testNegPosRatio -1 \
-save -i -test ../data/testN/ \
1>inferN.txt
