

ut-infer -i ai_weight.mln -e aitest.db -r ai.result -q advisedBy -exact > infer-ai.log
ut-infer -i graphics_weight.mln -e graphicstest.db -r graphics.result -q advisedBy -exact > infer-graphics.log
ut-infer -i language_weight.mln -e languagetest.db -r language.result -q advisedBy -exact > infer-language.log
ut-infer -i systems_weight.mln -e systemstest.db -r systems.result -q advisedBy -exact > infer-systems.log
ut-infer -i theory_weight.mln -e theorytest.db -r theory.result -q advisedBy -exact > infer-theory.log
