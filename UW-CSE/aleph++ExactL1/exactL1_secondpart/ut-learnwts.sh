ut-learnwts -i ai_train.mln -o ai_weight.mln -t graphicsneg.db,languageneg.db,systemsneg.db,theoryneg.db -ne advisedBy -d -l1Reg -l1Penalty 0.1 1>learnwts-ai.log
ut-learnwts -i graphics_train.mln -o graphics_weight.mln -t aineg.db,languageneg.db,systemsneg.db,theoryneg.db -ne advisedBy -d -l1Reg -l1Penalty 0.1 1>learnwts-graphics.log
ut-learnwts -i language_train.mln -o language_weight.mln -t graphicsneg.db,aineg.db,systemsneg.db,theoryneg.db -ne advisedBy -d -l1Reg -l1Penalty 0.1 1>learnwts-language.log
ut-learnwts -i systems_train.mln -o systems_weight.mln -t graphicsneg.db,languageneg.db,aineg.db,theoryneg.db -ne advisedBy -d -l1Reg -l1Penalty 0.1 1>learnwts-systems.log
ut-learnwts -i theory_train.mln -o theory_weight.mln -t graphicsneg.db,languageneg.db,systemsneg.db,aineg.db -ne advisedBy -d -l1Reg -l1Penalty 0.1 1>learnwts-theory.log

